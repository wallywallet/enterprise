import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

import java.io.File
import java.io.FileInputStream
import java.util.*

val deploymentGroup = "org.wallywallet"
val deploymentVersion = "1.3.3"

val kotlinVersion: String by project
val logbackVersion: String by project
// exposed is removed in favor of multiplatform sqldelight: val exposedVersion: String by project
val kotlinxHtmlJvmVersion: String by project
val zxingVersion: String by project

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose") version("1.8.0-alpha03")
    id("org.jetbrains.kotlin.plugin.compose")
    id("java")
    kotlin("plugin.serialization")
    id("maven-publish")
    //id("io.ktor.plugin") version("3.0.3")
}

val serializationVersion = "1.8.0"   // https://github.com/Kotlin/kotlinx.serialization
val bigNumVersion = "0.3.10"         // https://github.com/ionspin/kotlin-multiplatform-bignum
val composeVersion = "1.8.0-alpha03"         // https://github.com/JetBrains/compose-multiplatform/releases
val ktorVersion = "3.1.0"            // https://github.com/ktorio/ktor

val mpthreadsVersion = "0.4.0"
val libnexakotlinVersion = "0.4.10"
val scriptmachineVersion = "1.2.2"


dependencies {
    // Nexa
    implementation("org.nexa:mpthreads:$mpthreadsVersion")  // https://gitlab.com/nexa/mpthreads/-/packages
    implementation("org.nexa:libnexakotlin:$libnexakotlinVersion")  // https://gitlab.com/nexa/libnexakotlin/-/packages
    implementation("org.nexa:scriptmachine:$scriptmachineVersion")  // https://gitlab.com/nexa/nexascriptmachinekotlin/-/packages
    testImplementation("org.nexa","nexarpc","1.3.0")  // https://gitlab.com/nexa/nexarpckotlin/-/packages

    testImplementation(kotlin("test-junit"))
    testImplementation("org.slf4j:slf4j-simple:2.0.16")  // https://mvnrepository.com/artifact/org.slf4j/slf4j-simple

    implementation(compose.desktop.currentOs)
    implementation(compose.desktop.windows_x64)
    implementation(compose.desktop.macos_x64)
    implementation(compose.desktop.macos_arm64)
    implementation(compose.desktop.linux_x64)
    //implementation("org.jetbrains.skiko:skiko-awt-runtime-windows-x64:0.9.7") // Windows x64
    //implementation("org.jetbrains.skiko:skiko-awt-runtime-macos-x64:0.9.7") // macOS x64
    //implementation("org.jetbrains.skiko:skiko-awt-runtime-linux-x64:0.9.7") // Linux x64


    //implementation("org.jetbrains.compose.runtime:runtime:$composeVersion")
    //implementation("org.jetbrains.compose.foundation:foundation:$composeVersion")
    //implementation("org.jetbrains.compose.material3:material3:$composeVersion")
    implementation("androidx.annotation:annotation:1.7.1")
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-swing:1.8.0")
    implementation("org.slf4j:slf4j-simple:1.7.36")

    // JSON Serialization
    //implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", serializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", serializationVersion)

    // for bigintegers
    implementation("com.ionspin.kotlin:bignum:$bigNumVersion")
    implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:$bigNumVersion")

    // Database
    //implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    //implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    //implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    // gradle database dependencies described here: https://github.com/JetBrains/Exposed/wiki/DataBase-and-DataSource
    //implementation("org.xerial:sqlite-jdbc:3.41.2.1")
    //implementation("com.h2database:h2:1.4.197")

    // This lets the embedded kotlin load native classes
    implementation("net.java.dev.jna:jna-platform:5.14.0")  // https://mvnrepository.com/artifact/net.java.dev.jna/jna-platform

    // Scripting
    implementation(kotlin("reflect"))
    // implementation(kotlin("script-util"))
    implementation(kotlin("script-runtime"))
    implementation(kotlin("compiler-embeddable"))
    implementation(kotlin("scripting-compiler-embeddable"))
    implementation(kotlin("scripting-jsr223"))

    //runtimeOnly("org.jetbrains.kotlin:kotlin-main-kts:$kotlinVersion")
    //implementation
    runtimeOnly("org.jetbrains.kotlin:kotlin-scripting-jsr223:$kotlinVersion")

    implementation("com.google.zxing:core:$zxingVersion") // QR code creation

    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion") // CIO engine, you can use another engine if needed
    implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-client-json:$ktorVersion")
    implementation("io.ktor:ktor-client-serialization:$ktorVersion")

    // NFT
    implementation("com.squareup.okio:okio:3.9.1")  // https://mvnrepository.com/artifact/com.squareup.okio/okio
}

tasks.test {
    useJUnit()
    testLogging {
        events.addAll(listOf(TestLogEvent.PASSED, TestLogEvent.SKIPPED, TestLogEvent.FAILED, TestLogEvent.STANDARD_OUT, TestLogEvent.STANDARD_ERROR))
    }
    outputs.upToDateWhen { false }
}

tasks.withType<KotlinCompile> {
    compilerOptions {
        //jvmTarget.set(JvmTarget.JVM_17)
        jvmTarget.set(JvmTarget.JVM_17)
    }
}

compose.desktop {
    application {
        mainClass = "org.wallywallet.wew.WallyEnterpriseWallet"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "wew"
        }
    }
}

val prop = Properties().apply {
    try
    {
        load(FileInputStream(File(rootProject.rootDir, "local.properties")))
    }
    catch(_:Exception)
    {

    }
}

group = deploymentGroup
version = deploymentVersion
publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            artifactId = "wew"
            from(components["java"])
            //artifact(tasks.findByName("sourcesJar"))
        }
    }
    repositories {
        maven {
            // Project ID number is shown just below the project name in the project's home screen
            url = uri("https://gitlab.com/api/v4/projects/15615113/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = prop.getProperty("NexaDeployTokenValue")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

// Grab my libraries from the jars directory (for local development)
fun DependencyHandlerScope.localOrReleased(localFile: String, dependencyNotation: Any, forceLocal:Boolean? = null)
{
    val fl = forceLocal ?: project.property("localJars").toString().trim().toBooleanStrict()

    val hasLocal = if (fl)
    {
        val nexaJvm = File(rootProject.rootDir,localFile)
        if (nexaJvm.exists())
        {
            println("Using local library: $localFile")
            implementation(files(nexaJvm))
            true
        }
        else
        {
            println("WARNING: Even though LOCAL_JARS is true $localFile pulled from its released version because it" +
                " does not exist at ${nexaJvm.absolutePath}.")
            false
        }
    } else false

    if (!hasLocal)
    {
        println("Using release library for: $localFile")
        implementation(dependencyNotation)
    }
}

sourceSets {
    create("niftyWew") {
        kotlin.srcDir("../plugins/niftyWew/kotlin")
    }
    create("examplePlugin") {
        kotlin.srcDir("../plugins/example/kotlin")
    }
}

dependencies {
    "niftyWewImplementation"(kotlin("stdlib"))
    "niftyWewImplementation"(project)
    "niftyWewImplementation"(implementation(compose.desktop.currentOs)!!)
    "niftyWewImplementation"(implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", serializationVersion))
    "niftyWewImplementation"(implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", serializationVersion))
    "niftyWewImplementation"(implementation("io.ktor","ktor-client-core",ktorVersion))
    "niftyWewImplementation"(implementation("io.ktor","ktor-client-cio",ktorVersion)) // CIO engine, you can use another engine if needed
    "niftyWewImplementation"(implementation("io.ktor","ktor-client-content-negotiation",ktorVersion))
    "niftyWewImplementation"(implementation("io.ktor","ktor-client-json",ktorVersion))
    "niftyWewImplementation"(implementation("io.ktor","ktor-client-serialization",ktorVersion))

    "niftyWewImplementation"(implementation("org.nexa","mpthreads",mpthreadsVersion))
    "niftyWewImplementation"(implementation("org.nexa","libnexakotlin",libnexakotlinVersion))
    "niftyWewImplementation"(implementation("org.nexa","scriptmachine",scriptmachineVersion))
    "niftyWewImplementation"(implementation("com.squareup.okio", "okio","3.9.0"))

    "examplePluginImplementation"(kotlin("stdlib"))
    "examplePluginImplementation"(project)
    "examplePluginImplementation"(implementation(compose.desktop.currentOs)!!)
    "examplePluginImplementation"(implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", serializationVersion))
    "examplePluginImplementation"(implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", serializationVersion))
    "examplePluginImplementation"(implementation("io.ktor","ktor-client-core",ktorVersion))
    "examplePluginImplementation"(implementation("io.ktor","ktor-client-cio",ktorVersion)) // CIO engine, you can use another engine if needed
    "examplePluginImplementation"(implementation("io.ktor","ktor-client-content-negotiation",ktorVersion))
    "examplePluginImplementation"(implementation("io.ktor","ktor-client-json",ktorVersion))
    "examplePluginImplementation"(implementation("io.ktor","ktor-client-serialization",ktorVersion))

    "examplePluginImplementation"(implementation("org.nexa","mpthreads",mpthreadsVersion))
    "examplePluginImplementation"(implementation("org.nexa","libnexakotlin",libnexakotlinVersion))
    "examplePluginImplementation"(implementation("org.nexa","scriptmachine",scriptmachineVersion))

}

tasks.register<Jar>("niftyWew") {
    group = "build"
    archiveBaseName.set("niftyWew")
    archiveVersion.set("1.0")
    from(sourceSets["niftyWew"].output)
    println("Plugin destination directory: ${layout.buildDirectory.dir("plugins").get()}")
    destinationDirectory.set(layout.buildDirectory.dir("plugins"))
}

tasks.named("niftyWew") {
    //dependsOn("compileNiftyWewKotlin")
    dependsOn("niftyWewClasses")
    // outputs.upToDateWhen { false }  // Always rerun this because it runs the program
}

tasks.register<Jar>("examplePlugin") {
    group = "build"
    archiveBaseName.set("examplePlugin")
    archiveVersion.set("1.0")
    from(sourceSets["examplePlugin"].output)
    destinationDirectory.set(file("${layout.buildDirectory}/plugins"))
}

tasks.named("examplePlugin") {
    dependsOn("examplePluginClasses")
}


tasks.named("build") {
    dependsOn("compileNiftyWewKotlin")
    dependsOn("niftyWew")
    dependsOn("examplePlugin")
}

tasks.withType<Test> {
    println("there is a test task called $this")
    maxHeapSize = "4096m"
    minHeapSize = "2048m"
    //jvmArgs = listOf("-Xmx4096m")
    jvmArgs = listOf("-XX:MaxMetaspaceSize=16g")
    println("Max heap size: ${this.maxHeapSize}")
    doFirst {
        println("JVM Arguments: $javaLauncher ${javaLauncher.orNull?.executablePath}")
    }
}
