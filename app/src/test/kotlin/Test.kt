// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.wew

import androidx.compose.ui.graphics.ImageBitmap
import com.ionspin.kotlin.bignum.integer.BigInteger
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import org.nexa.libnexakotlin.*
import kotlinx.coroutines.*
import kotlin.test.*
import java.lang.IllegalStateException
import java.security.MessageDigest
import java.text.DecimalFormat
import java.util.logging.ConsoleHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter
import kotlin.random.Random
import kotlinx.serialization.Serializable
import org.nexa.libnexakotlin.fromHex
import org.nexa.libnexakotlin.libnexa
import org.nexa.libnexakotlin.newWallet
import org.nexa.libnexakotlin.openOrNewWallet
import org.nexa.libnexakotlin.toHex
import org.nexa.nexarpc.*
import org.nexa.threads.millisleep
import org.wallywallet.wew.cli.*
import java.lang.Thread.sleep
import java.net.URLEncoder
import java.util.*
import java.util.StringJoiner
import kotlin.io.path.Path
import kotlin.io.path.deleteIfExists

import javax.script.Bindings
import javax.script.ScriptContext
import javax.script.ScriptEngineManager
import javax.script.SimpleScriptContext
import kotlin.io.print
import kotlin.io.println
import kotlin.random.nextInt
import kotlin.time.ExperimentalTime
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.delay
import org.junit.Assert.assertTrue
import java.time.Instant
import kotlin.test.Test


// Configure these values to connect to your NEXA regtest network instance
val NexaRegtestPortOverride = NexaRegtestPort
val RPC_USER = "regtest"
val RPC_PASSWORD = "regtest"
val REGTEST_IP="localhost"

val TEST_BCH = false
val BchRegtestRpcPort = 18332  // Regtest RPC is the same as testnet port


val MILLION = 1000000L
val WAITAMT = 10000L
val btcFormat = DecimalFormat("##,##0.########")

private val LogIt = Logger.getLogger("BU.wallet.TEST")

fun Long.toBigDecimal(): BigDecimal = BigDecimal.fromLong(this)
fun Int.toBigDecimal(): BigDecimal = BigDecimal.fromInt(this)

fun mBCHtoSAT(mBCH: BigDecimal): Long
{
    return (mBCH*100.toBigDecimal()*1000.toBigDecimal()).toLong()
}
fun mBCHtoSAT(mBCH: Long): Long = mBCHtoSAT(mBCH.toBigDecimal())

fun BCHtoSAT(mBCH: BigDecimal): Long
{
    return (mBCH*100.toBigDecimal()*1000000.toBigDecimal()).toLong()
}
fun BCHtoSAT(mBCH: Long): Long = mBCHtoSAT(mBCH.toBigDecimal())


fun mycheck(b:Boolean, onFail: (()->String?)? = null)
{
    if  (!b)
    {
        val v = onFail?.invoke()
        if (v != null) LogIt.severe(v)
        throw IllegalStateException(v)
    }
}

suspend fun waitUntil(timeout:Long, predicate: ()->Boolean):Boolean
{
    var countdown = timeout
    while(!predicate())
    {
        LogIt.finer("Waiting  " + timeout + " at " + countdown)
        if (countdown<=0)
        {
            return false
        }
        min(countdown, 500).let {
            delay(it)
            countdown-=it
        }
    }
    return true
}

fun ForceNewWallet(name: String, cs:ChainSelector): Bip44Wallet
{
    val nm = "test_" + name
    deleteDatabase(nm)
    return newWallet(nm, cs)
}


@kotlin.time.ExperimentalTime
fun waitFor(maxWaitMs: Long, check: () -> Boolean)
{
    val start: kotlin.time.TimeMark = kotlin.time.TimeSource.Monotonic.markNow()
    while (!check())
    {
        sleep(500)
        if (start.elapsedNow().inWholeMilliseconds > maxWaitMs) throw Exception("wait for failed")
    }
}

class UnitTest
{
    fun getNexaRpc(): NexaRpc
    {
        LogIt.info("This test requires a Nexa full node running on regtest at ${REGTEST_IP} and port $NexaRegtestRpcPort")

        // Set up RPC connection
        val rpcConnection = "http://$RPC_USER:$RPC_PASSWORD@$REGTEST_IP:" + NexaRegtestRpcPort
        //LogIt.info("Connecting to: " + rpcConnection)
        val nexaRpc = NexaRpcFactory.create(rpcConnection)
        val ret = nexaRpc.listunspent()
        check(ret.size > 0)
        return nexaRpc
    }

    @BeforeTest
    fun initJvm()
    {
        initializeLibNexa()
    }


    @Test
    fun testSimpleEval()
    {
        val engine = ScriptEngineManager().getEngineByExtension("kts") // as KotlinJsr223JvmLocalScriptEngine
        val sc = SimpleScriptContext()
        sc.setBindings(engine.createBindings(), ScriptContext.ENGINE_SCOPE)
        val engineScope: Bindings = sc.getBindings(ScriptContext.ENGINE_SCOPE)
        engineScope.put("x", 1)
        val res1 = engine.eval("""val y = 2""", engineScope)
        val res2 = engine.eval("x + y", engineScope)
        // inside the interpreter: this::class.members.toTypedArray()
        println(res2)
    }

    @Test
    fun testEngineFactory()
    {
        val a = ScriptEngineManager()
        val engine = a.getEngineByExtension("kts")
        val factory = engine.factory
        //Assert.assertNotNull(factory)
        factory?.apply {
            print(languageName)
            print(languageVersion)
            //Assert.assertEquals("kotlin", engineName)
            //Assert.assertEquals(KotlinCompilerVersion.VERSION, engineVersion)
            //Assert.assertEquals(listOf("kts"), extensions)
            //Assert.assertEquals(listOf("text/x-kotlin"), mimeTypes)
            //Assert.assertEquals(listOf("kotlin"), names)
            //Assert.assertEquals("obj.method(arg1, arg2, arg3)", getMethodCallSyntax("obj", "method", "arg1", "arg2", "arg3"))
            //Assert.assertEquals("print(\"Hello, world!\")", getOutputStatement("Hello, world!"))
            //Assert.assertEquals(KotlinCompilerVersion.VERSION, getParameter(ScriptEngine.LANGUAGE_VERSION))
            val sep = System.getProperty("line.separator")
            val prog = arrayOf("val x: Int = 3", "var y = x + 2")
            //Assert.assertEquals(prog.joinToString(sep) + sep, getProgram(*prog))
        }
        //engine.eval("val p = 2")
        //print(engine.eval("p+2"))
    }

    @Test
    fun testRpc()
    {
        getNexaRpc()
        if (TEST_BCH)
        {
            LogIt.info("This test requires a Bitcoin Cash full node running on regtest at ${REGTEST_IP} and port $BchRegtestRpcPort")
            val bchRpcConnection =  "http://$RPC_USER:$RPC_PASSWORD@$REGTEST_IP:" + BchRegtestRpcPort
            //LogIt.info("Connecting to: " + rpcConnection)
            val bchRpc = NexaRpcFactory.create(bchRpcConnection)
            val ret2 = bchRpc.getblockcount()
            check(ret2 > 100)
        }
    }

    // @Test
    fun autoTest()
    {
        LogIt.level = Level.INFO
        val consoleHandler = ConsoleHandler()
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1\$tc] %4\$s: %5\$s\n")
        val formatter = SimpleFormatter()
        consoleHandler.formatter = formatter
        LogIt.addHandler(ConsoleHandler())
        allTests(RPC_USER, RPC_PASSWORD, REGTEST_IP, NexaRegtestRpcPort)
    }

    @Test
    fun testHDderivation()
    {
        val secret = ByteArray(32, { 1 })
        val newSecret = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, true, 0)
        //val newSecret = Native.Hd44DeriveChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, index.toInt())
        //val newSecret = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, index).first
        //println("HD key: " + newSecret.toHex())
        check(newSecret.first.toHex() == "9c219cb84e3199b076aaa0f954418407b1e8d54f79103612fbaf04598bc8be55")

        val newSecret2 = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, 0)
        //println("HD key: " + newSecret2.toHex())
        check(newSecret2.first.toHex() == "bc732a2fafc593296ee8447bc3694be5e9d26827a6cd553248976dc30e566829")

        val newSecret3 = libnexa.deriveHd44ChildKey(secret, 27, AddressDerivationKey.ANY, 0, false, 1)
        //println("HD key: " + newSecret3.toHex())
        check(newSecret3.first.toHex() == "4d0f9fcd6675e49e2e884f62ca86152877ae8addbdcef985d81afd507230de9b")
    }

    @Test
    fun rediscover()
    {
        val rpc = getNexaRpc()
        rpc.generate(1)  // in case chain is too old
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")
        val w = openOrNewWallet("emptyWallet", cs)
        w.chainstate?.prehistoryDate = 0
        w.chainstate?.prehistoryHeight = 0

        w.rediscover()
        sleep(3000)
        if (!w.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("POST REDISCOVER")
        val wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
        w.save(true)
    }

    // @Test
    fun largeWalletInfo()
    {
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet statistics")
        //val w = ForceNewWallet("largeWallet", cs)

        val feeder = openOrNewWallet("feederWallet", cs)

        val w = openOrNewWallet("largeWallet", cs)
        if (!w.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }

        var wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
        var fi = feeder.statistics()
        LogIt.info("Feeder Wallet: ${feeder.balance} ${feeder.balanceConfirmed}:${feeder.balanceUnconfirmed} at ${feeder.chainstate?.syncedHeight}  Transactions: ${feeder.numTx()}  Txos: ${feeder.numTxos()} Utxos: ${feeder.numUtxos()} Addrs: ${fi.numUsedAddrs} of ${fi.numUsedAddrs + fi.numUnusedAddrs}")

        w.save(true)
        feeder.save(true)
    }

    // @Test
    fun rediscoverLargeWallet()
    {
        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")

        val feeder = openOrNewWallet("feederWallet", cs)
        val w = openOrNewWallet("largeWallet", cs)

        w.rediscover()
        feeder.rediscover()
        sleep(3000)
        if (!w.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(500000))
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("POST REDISCOVER")
        val wi = w.statistics()
        LogIt.info("Wallet: ${w.balance} ${w.balanceConfirmed}:${w.balanceUnconfirmed} at ${w.chainstate?.syncedHeight}  Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Addrs: ${wi.numUsedAddrs} of ${wi.numUsedAddrs + wi.numUnusedAddrs}")
        val fi = feeder.statistics()
        LogIt.info("Feeder Wallet: ${feeder.balance} ${feeder.balanceConfirmed}:${feeder.balanceUnconfirmed} at ${feeder.chainstate?.syncedHeight}  Transactions: ${feeder.numTx()}  Txos: ${feeder.numTxos()} Utxos: ${feeder.numUtxos()} Addrs: ${fi.numUsedAddrs} of ${fi.numUsedAddrs + fi.numUnusedAddrs}")

        w.save(true)
        feeder.save(true)
    }

    // @Test
    fun largeWallet()
    {
        val ITERS = 1
        val rpcConnection = "http://$RPC_USER:$RPC_PASSWORD@$REGTEST_IP:" + NexaRegtestRpcPort
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = NexaRpcFactory.create(rpcConnection, RPC_USER, RPC_PASSWORD)
        rpc.generate(1)

        val cs = ChainSelector.NEXAREGTEST
        LogIt.info("Test wallet syncing from genesis")
        //val w = ForceNewWallet("largeWallet", cs)

        val feeder = openOrNewWallet("feederWallet", cs)

        val w = openOrNewWallet("largeWallet", cs)
        if (!w.sync(120000))
        {
            throw Exception("Wallet sync timeout")
        }
        if (!feeder.sync(10000))  // should be syncing the entire time w is syncing
        {
            throw Exception("Wallet sync timeout")
        }

        LogIt.info("initial balances: ${feeder.balance}  ${w.balance}")

        val addr = listOf(w.getnewaddress(), w.getnewaddress(), w.getnewaddress(), w.getnewaddress(), w.getnewaddress())
        val faddr = listOf(feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress(), feeder.getnewaddress())

        // move some balance from the wallet to the feeder; if there is nothing in the wallet then ok skip
        try
        {
            w.send(100000000L, faddr[0])
        }
        catch (e:WalletNotEnoughBalanceException)
        {

        }

        if (feeder.balance < 1000000L)
        {
            LogIt.info("filling up the feeder wallet")

            rpc.generate(1)
            for (i in range(0, 10))
            {
                for (a in addr)
                {
                    var amt = 100000
                    while (true)
                        try
                        {
                            rpc.sendtoaddress(a.toString(), BigDecimal.fromInt(amt))
                            break
                        }
                        catch (e: NexaRpcException)
                        {
                            if ("Maximum inputs allowed" in e.toString() ||
                                "requires a transaction fee" in e.toString())
                            {
                                if (amt < 1000) break // just hope we sent enough, otherwise test will fail later
                                amt = amt / 2
                            }
                            else throw e
                        }
                }
                for (a in faddr)
                {
                    var amt = 100000
                    while (true)
                        try
                        {
                            rpc.sendtoaddress(a.toString(), BigDecimal.fromInt(amt))
                            break
                        }
                        catch (e: NexaRpcException)
                        {
                            if ("Maximum inputs allowed" in e.toString() ||
                                "requires a transaction fee" in e.toString()
                            )
                            {
                                if (amt < 1000) break // just hope we sent enough, otherwise test will fail later
                                amt = amt / 2
                            }
                            else throw e
                        }
                }
            }
            var blkHash = rpc.generate(1)
            LogIt.info("block $blkHash should have ${10 * (faddr.size + addr.size)} tx")
        }

        val addr2 = listOf(w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress(),w.getnewaddress())

        val veryStart = millinow()
        var totalTx = 0
        for (i in range(0, ITERS))
        {
            val st = millinow()
            var numTx = 0
            for (j in range(0, 2))
            {
                for (a in addr2)
                {
                    try
                    {
                        w.send(Random.nextLong(5000L) + 1000L, a)
                        numTx++
                    }
                    catch(e: WalletNotEnoughBalanceException)
                    {
                        LogIt.info("Not enough balance: Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()}")
                        LogIt.info("generating block")
                        for (ad in addr)
                            rpc.sendtoaddress(ad.toString(), BigDecimal.fromInt(1000000))
                        rpc.generate(1)
                    }
                    try
                    {
                        feeder.send(Random.nextLong(5000L) + 1000L, a)
                        numTx++
                    }
                    catch(e: WalletNotEnoughBalanceException)
                    {
                        LogIt.info("Feeder Not enough balance: Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()}")
                        LogIt.info("generating block")
                        rpc.generate(10)
                        try
                        {
                            for (ad in faddr)
                                rpc.sendtoaddress(ad.toString(), BigDecimal.fromInt(10000000))
                        }
                        catch(e: NexaRpcException)
                        {
                            var amt = w.balance / 10
                            while(true) try
                            {
                                w.send(amt, faddr[0])
                                break
                            }
                            catch(e: WalletNotEnoughBalanceException)
                            {
                                amt = amt / 2
                            }
                            millisleep(500U)
                            rpc.generate(1)
                        }
                    }

                }
            }
            val end = millinow()
            totalTx += numTx
            rpc.generate(1)
            val wstats = w.statistics()
            LogIt.info("Loop: $i TPS: ${numTx.toFloat()/((end-st).toFloat()/1000f)} Elapsed: ${(end-veryStart)/1000} Interval: ${(end-st)/1000} NumTx: $totalTx  Wallet: Transactions: ${w.numTx()}  Txos: ${w.numTxos()} Utxos: ${w.numUtxos()} Used Addresses: ${wstats.numUsedAddrs} Unused Addresses: ${wstats.numUnusedAddrs}")
        }
    }

    fun allTests(rpcuser: String, rpcpassword: String, ip: String, port:Int)
    {
        runBlocking {
            TestRequestMgr(rpcuser, rpcpassword, ip, port)
        }

        val genesisHash = Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371")

        val rpcConnection = "http://$rpcuser:$rpcpassword@$ip:" + port
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = NexaRpcFactory.create(rpcConnection, rpcuser, rpcpassword)
        val peerInfo = rpc.getpeerinfo()
        mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

        var blockcount = rpc.getblockcount().toLong()

        /*
    // Test repeated wallet sync
    LogIt.info("Repeated Wallet Sync Test")
    runBlocking {
        val cnxnMgr = RegTestCnxnMgr("BCtempR","127.0.0.1", NexaRegtestPort)
        val bchChain = Blockchain(ChainSelector.NEXAREGTEST, "BCtempR", cnxnMgr, genesisHash, Hash256(), genesisHash, 0, 2.toBigInteger(), PlatformContext(), "rbch")
        bchChain.deleteAllPersistentData()
        bchChain.setTestDelayIntervals()
        GlobalScope.launch { cnxnMgr.run() }
        bchChain.start()

        for (cnt in range(0,5))
        {
            LogIt.info("Sync ${cnt}")
            val w = ForceNewWallet("BCtempR" + cnt, bchChain.chainSelector)
            w.addBlockchain(bchChain, 0, null)
            mycheck(
                waitUntil(20000, { w.synced(blockcount) }),
                { w.chainstate!!.chain.name + ": Blockchain did not sync: " + w.chainstate!!.chain.curHeight + "  Expecting: " + blockcount })
            w.stop()
        }
        bchChain.stop()
        cnxnMgr.stop()
    }

     */

        // Basic wallet functional test
        LogIt.info("Basic wallet functional test")
        runBlocking {
            // val cnxnMgr = RegTestCnxnMgr("BCtemp", "127.0.0.1", NexaRegtestPortOverride)
            val cnxnMgr = MultiNodeCnxnMgr("rnex", ChainSelector.NEXAREGTEST,arrayOf(IpPort("127.0.0.1",NexaRegtestPortOverride)))
            cnxnMgr.exclusiveNodeSet = mutableSetOf("127.0.0.1:$NexaRegtestPortOverride")
            val chain = Blockchain(ChainSelector.NEXAREGTEST, "rnex", cnxnMgr, genesisHash, Hash256(), genesisHash, 0, BigInteger.fromInt(2),  "rnex")
            chain.deleteAllPersistentData()
            chain.setTestDelayIntervals()
            cnxnMgr.start()
            chain.start()
            delay(2000)

            LogIt.info("Test wallet syncing from genesis")
            val w = ForceNewWallet("wtemp1", chain.chainSelector)
            w.addBlockchain(chain, chain.checkpointHeight, null)  // Test syncing from tip
            TestWallet(w, rpcuser, rpcpassword, ip, port)

            LogIt.info("Test wallet from tip")
            val w2 = ForceNewWallet("wtemp2", chain.chainSelector)
            w2.addBlockchain(chain, 0, null)  // Test starting from 0
            TestWallet(w2, rpcuser, rpcpassword, ip, port)

            LogIt.info("TestWallet tests complete, stopping temporary blockchain")
            chain.stop()
            cnxnMgr.stop()
            w.stop()
            w2.stop()
        }

        LogIt.info("Wallet Rewind Test")
        runBlocking {
            val cnxnMgr = //RegTestCnxnMgr("BCtemp1", "127.0.0.1", NexaRegtestPortOverride)
                MultiNodeCnxnMgr("rnex", ChainSelector.NEXAREGTEST,arrayOf(IpPort("127.0.0.1",NexaRegtestPortOverride)))
            val bchChain = Blockchain(ChainSelector.NEXAREGTEST, "BCtemp1", cnxnMgr, genesisHash, genesisHash, genesisHash, 0, BigInteger.fromInt(2), "rbch2")
            bchChain.deleteAllPersistentData()
            bchChain.setTestDelayIntervals()
            cnxnMgr.start()
            bchChain.start()
            delay(2000)

            LogIt.info("Test wallet syncing from genesis")
            val w = ForceNewWallet("BCtemp2", bchChain.chainSelector)
            w.addBlockchain(bchChain, bchChain.checkpointHeight, null)
            delay(5000)  // Wait for wallet to install bloom, etc

            TestWalletRewind(w, rpc)

            LogIt.info("TestWallet tests complete, stopping temporary blockchain")
            bchChain.stop()
            cnxnMgr.stop()
            w.stop()
        }

        LogIt.info("ALL TESTS COMPLETE")
    }

    // @Test
    fun NftCashQR(): List<Pair<iTransaction, ImageBitmap>>?
    {
        val NUM_QRS = 1
        val QR_SIZE = 1024
        val chain = ChainSelector.NEXA

        val CASH_AMT = 1.mexa
        val MIN_FEE = 1000.sat

        val SftGroup:GroupId = GroupId("nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqpct50qqcu8tmt0mp8ej42mzhnst4gf3wc5auqwenxfnkc243mpa7ytmjre0r")
        val TdppReason: String = "Crypto Adria Promotion"
        val NFTY_SERVER_FQDN = "niftyart.cash"

        val w = org.wallywallet.wew.cli.openWallet("nftFaucet")
        var count = 0
        w.sync()

        val changeAddr = w.getnewaddress()

        // Find cash for each tx
        val cashUtxo = mutableListOf<Spendable>()
        w.forEachUtxo {
            val grp = it.groupInfo
            // We can use this output as an input because it has enough cash
            if ((grp == null)&&(it.amount >= CASH_AMT + MIN_FEE))
            {
                cashUtxo.add(it)
                count+=1
            }
            (count >= NUM_QRS)
        }

        if (cashUtxo.size < NUM_QRS)
        {
            println("ERROR: you only have ${cashUtxo.size} available cash prevouts.  Do some splitting!")
            return null
        }

        // Now find NFTs
        val nftUtxo = mutableListOf<Spendable>()
        val sftUtxo = mutableListOf<Spendable>()
        w.forEachUtxo {
            val grp = it.groupInfo
            if (grp != null)
            {
                if ((!grp.isAuthority()) && (grp.groupId == SftGroup))
                {
                    sftUtxo.add(it)
                }
                if ((!grp.isAuthority()) && (grp.tokenAmt == 1L))
                {
                    nftUtxo.add(it)
                }
            }
            (sftUtxo.size >= NUM_QRS) && (nftUtxo.size > NUM_QRS)
        }

        if (nftUtxo.size < NUM_QRS)
        {
            println("ERROR: you only have ${nftUtxo.size} available NFT prevouts.  Load up more NFTs!")
            return null
        }
        if (sftUtxo.size < NUM_QRS)
        {
            println("ERROR: you only have ${sftUtxo.size} available NFT prevouts.  Load up more copies of your SFT!")
            return null
        }

        val ret = mutableListOf<Pair<iTransaction, ImageBitmap>>()
        // I've collected all the needed inputs.  Now generate the partial transactions
        for (i in 0 until NUM_QRS)
        {
            val tx = txFor(chain)
            val cashIn = cashUtxo[i].amount
            tx.add(txInputFor(cashUtxo[i]))
            tx.add(txInputFor(nftUtxo[i]))
            tx.add(txInputFor(sftUtxo[i]))

            val change = txOutputFor(chain, cashIn-MIN_FEE-CASH_AMT, changeAddr.lockingScript())
            val cash = txOutputFor(chain, CASH_AMT, SatoshiScript(chain, SatoshiScript.Type.TEMPLATE, OP.C0, OP.TMPL_SCRIPT) )
            val sft = txOutputFor(NEXA, sftUtxo[i].amount, SatoshiScript.grouped(chain, sftUtxo[i].groupInfo!!.groupId,sftUtxo[i].groupInfo!!.tokenAmt).add(OP.TMPL_SCRIPT) )
            val nft = txOutputFor(NEXA, nftUtxo[i].amount, SatoshiScript.grouped(chain, nftUtxo[i].groupInfo!!.groupId,nftUtxo[i].groupInfo!!.tokenAmt).add(OP.TMPL_SCRIPT) )
            tx.add(change)
            tx.add(cash)
            tx.add(sft)
            tx.add(nft)

            // Sign my inputs and my change output, giving the scanner the rest
            val stx = w.signTransaction(tx, NexaSigHashType().firstNIn(3).firstNOut(1).build())

            // TDPP_FLAG_NOPOST
            val tdppFlags = TDPP_FLAG_NOFUND or TDPP_FLAG_NOSHUFFLE or TDPP_FLAG_PARTIAL
            val reason = URLEncoder.encode(TdppReason,"utf-8")
            val chainName = chainToURI[NEXA]
            val qrString = "tdpp://" + NFTY_SERVER_FQDN + "/tx?chain=$chainName&tx=${stx.toHex()}&flags=$tdppFlags&reason=$reason"
            val q = qr(qrString, QR_SIZE)!!
            ret.add(Pair(stx,q))
            println("TX: " + stx.debugDump())
            println("TX HEX: " + stx.toHex())
            println(q)
        }
        return ret
    }

    @Test
    fun TestHash()
    {
        LogIt.info("TestHash")

        val tmp =
            "010000000393b0f317ac53fcd6781d0f6c38a1d182290930f0dd513be38d769e3b146d5e62000000006a473044022043643689bec00bd1321df7f971945cf54f7e447377e9d9b74ebb56d84af729f4022035445b973377bbf186e4df4361de1d79d98ce2e5af051f329f44e5b377236e3f412102e630f56e18b49653a2f679d99140e921986c7ff0939daf3eaefac57b98535d0dfeffffffa9886a7905dd0f71f3056758ce725a155ae427a80335ccbde40a5dde92cb73c10100000069463043021f37a063ef506db5f808ad40a0132ac888ae0407abc3c15cf5cf7535a3a0b0990220069cc4dce75a217d757e238740dd0cdc9bf733ba414e12ed54a3f901dddf7d8041210228aeddf6c7a1c943c1d379a3a5218c5676bbc95f89f005481efa5a79f69be960fefffffff0c1567649ea846ea4b16201f62ee1c74622bb7f3f16acf8ce02a3ec7814d63d000000006b483045022100add55f73990267a6516cb32b30e65b045025c04643a6468c0758a86853562a590220676c995386121590e7c56f7d3570c2e3d029fba004f63a72fb652bcc9876b53d412103c37c0afa84d74f836a62aa9c7549f58979371aa09179e36e66ce4280f0d08b6bfeffffff02b4852000000000001976a914523c4b26ea97d29deebc8477017f478519b7bf5288acc0ce7217000000001976a9145e8ee27109bed3a46c688d123bfa9c21da69426388acec000000"
        val bin = tmp.fromHex()
        val jnihash = libnexa.hash256(bin)
        val step1 = MessageDigest.getInstance("SHA-256").digest(bin)
        val hash = MessageDigest.getInstance("SHA-256").digest(step1)
        if (!(hash contentEquals jnihash))
        {
            throw IllegalStateException("Hash algorithm issue")
        }
    }

    @Test
    fun TestKeyDerivation()
    {
        LogIt.info("TestKeyDerivation")
        val secret = libnexa.secureRandomBytes(32)

        for (i in 0..1000)
        {
            val result = libnexa.deriveHd44ChildKey(secret, 44, AddressDerivationKey.BTC, 0, false, i)
            mycheck(result.first.size == 32, { "derived key too small: " + result.first.toHex() })
            LogIt.finest(result.first.toHex())
        }
    }

    @OptIn(ExperimentalTime::class)
    suspend fun TestRequestMgr(rpcuser: String, rpcpassword: String, ip: String, port:Int)
    {
        LogIt.info("TestRequestMgr")
        val rpcConnection = "http://$rpcuser:$rpcpassword@$ip:$port"
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = NexaRpcFactory.create(rpcConnection, rpcuser, rpcpassword)

        val cnxnMgr = MultiNodeCnxnMgr("rnex", ChainSelector.NEXAREGTEST,arrayOf(IpPort("127.0.0.1",NexaRegtestPortOverride)))
        val reqMgr = RequestMgr(cnxnMgr, Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"))
        cnxnMgr.start()
        millisleep(2000U) // Let the connection come up
        org.wallywallet.chartie.waitFor(5000) { cnxnMgr.getAnotherNode(setOf()) != null }

        // Check that the headers read are the same as the blocks loaded via RPC
        val hdrs = reqMgr.getBlockHeaders(Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"), Signaler(false))
        var height = 1L
        println("Comparing ${hdrs.size} headers against RPC")
        for (hdr in hdrs)
        {
            val rpcBlock = rpc.getblock(height)
            //println(rpcBlock.hash)
            //println(hdr.hash.toHex())
            mycheck(hdr.height == height)
            mycheck(rpcBlock.hash.toHex() == hdr.hash.toHex())
            height += 1
        }

        // Test the getBlockHeaderByHash function
        height = 1L
        println("Comparing ${hdrs.size} getBlockHeaderByHash headers against RPC")
        for (hdr in hdrs)
        {
            val rpcBlock = rpc.getblock(height)

            val hdr2 = reqMgr.getBlockHeaderByHash(hdr.hash)!!
            //println(rpcBlock.hash())
            //println(hdr.hash.toHex())
            //println(hdr2.hash.toHex())
            mycheck(rpcBlock.hash.toHex() == hdr2.hash.toHex())
            height += 1
        }

        // Test the getBlock function
        println("Comparing ${hdrs.size} blocks of requestTxInBlock against RPC")
        reqMgr.allowFilteredBlock = false // we need ALL the tx because we want to compare against the RPC
        for (hdr in hdrs)
        {
            val txes = reqMgr.requestTxInBlock(hdr.hash)
            val rpcBlock = rpc.getblock(org.nexa.nexarpc.HashId(hdr.hash.hash))
            mycheck(txes.size == rpcBlock.txidem.size)
            val lkup = mutableMapOf<String, HashId>()
            for (t in rpcBlock.txidem)
            {
                lkup[t.toHex()] = t
            }
            for (i in 0..txes.size - 1)
            {
                mycheck(txes[i].idem.toHex() in lkup)
            }

        }

        // We still want all transactions, even though we are testing with the merkleblock mode
        // so install a filter that matches everything
        reqMgr.allowFilteredBlock = true
        reqMgr.net.setAddressFilter(ByteArray(1, {255.toByte()}), {})
        for (hdr in hdrs)
        {
            val txes = reqMgr.requestTxInBlock(hdr.hash)
            val rpcBlock = rpc.getblock(org.nexa.nexarpc.HashId(hdr.hash.hash))
            mycheck(txes.size == rpcBlock.txidem.size)
            val lkup = mutableMapOf<String, HashId>()
            for (t in rpcBlock.txidem)
            {
                lkup[t.toHex()] = t
            }
            for (i in 0..txes.size - 1)
            {
                mycheck(txes[i].idem.toHex() in lkup)
            }

        }


    }

    // @Test skip this for CI
    fun testelectrumclient()
    {
        LogIt.info("This test requires an electrum cash server running at ${REGTEST_IP}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}")

        val c = try
        {
            ElectrumClient(ChainSelector.NEXAREGTEST, REGTEST_IP, DEFAULT_NEXAREG_TCP_ELECTRUM_PORT, "Electrum@${REGTEST_IP}:${DEFAULT_NEXAREG_TCP_ELECTRUM_PORT}", useSSL=false)
        } catch (e: java.net.ConnectException)
        {
            LogIt.warning("Cannot connect: Skipping Electrum tests: ${e}")
            null
        } catch (e: java.net.SocketTimeoutException)
        {
            LogIt.warning("Cannot connect: Skipping Electrum tests: ${e}")
            null
        }
        org.junit.Assume.assumeTrue(c != null)
        val cnxn = c!!

        cnxn.start()

        val ret = cnxn.call("server.version", listOf("4.0.1", "1.4"), 1000)
        if (ret != null) LogIt.info(sourceLoc() + ": Server Version returned: " + ret)

        val version = cnxn.version()
        LogIt.info(sourceLoc() + ": Version API call returned: " + version.first + " " + version.second)

        /* TODO enable when electrscash is updated
        val features = cnxn.features()
        LogIt.info(sourceLoc() + ": genesis block hash:" + features.genesis_hash)
        check("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206" == features.genesis_hash)
        check("sha256" == features.hash_function)
        check(features.server_version.contains("ElectrsCash"))  // Clearly this may fail if you connect a different server to this regression test
         */

        val ret2 = cnxn.call("blockchain.block.header", listOf(100, 102), 1000)
        LogIt.info(ret2)

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00", 1000)  // doesn't exist
            assert(false)
        } catch (e: ElectrumNotFound)
        {
            assert(e.message!!.contains("tx not in"))
        }

        //cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d0b", 1000)

        try
        {
            cnxn.getTx("zz5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 1000) // bad hash
            assert(false)
        } catch (e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 1000) // bad hash (short)
            assert(false)
        } catch (e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d".repeat(10), 1000) // bad hash (large)
            assert(false)
        } catch (e: ElectrumIncorrectRequest)
        {
        }

        val (name, ver) = cnxn.version(1000)
        LogIt.info("Server name $name, server version $ver")

        cnxn.call("server.banner", null) {
            LogIt.info("Server Banner reply is: " + it)
        }

        @Serializable
        data class BannerReply(val result: String)
        cnxn.parse("server.banner", null, BannerReply.serializer()) {
            LogIt.info("Server Banner reply is: " + it!!.result)
        }


        //@UseExperimental(kotlinx.serialization.ImplicitReflectionSerializer::class)
        //val b:BannerReply? = cnxn.parse("server.banner", null, 1000)
        //LogIt.info(b?.result)

        cnxn.subscribe("blockchain.headers.subscribe") {
            LogIt.info("Received blockchain header notification: ${it}")
        }

        val header = cnxn.getHeader(10000000)  // beyond the tip
        LogIt.info(header.toString())

        try
        {
            cnxn.getHeader(-1)  // beyond the tip
            assert(false)
        }
        catch (e: ElectrumIncorrectRequest)
        {
            check("error" in e.toString())
            LogIt.info(e.toString())
        }
        LogIt.info(header.toString())


        // This code gets the first coinbase transaction and then checks its history.  Based on the normal regtest generation setup, there should be at least 100
        // blocks that generate to this same output.
        val tx = cnxn.getTxAt(1, 0)
        LogIt.info(tx.toHex())
        val txBlkHeader = blockHeaderFor(cnxn.chainSelector, BCHserialized(cnxn.getHeader(1), SerializationType.HASH))
        // parent of block 1 is genesis block
        check(txBlkHeader.hashPrevBlock == Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"))
        // ancestor of block 1 is genesis block
        check(txBlkHeader.hashAncestor == Hash256("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"))
        tx.debugDump()

        /* TODO add in when get_first_use is committed to electrscash.  TODO: check server capabilities
        val firstUse = cnxn.getFirstUse(tx.outputs[0].script)
        LogIt.info("first use in block ${firstUse.block_hash}:${firstUse.block_height}, transaction ${firstUse.tx_hash}")
        check(firstUse.tx_hash != null)
        check(firstUse.block_height!! == 1)
        check(tx.hash.toHex() == firstUse.tx_hash!![0])
        check(txBlkHeader.hash.toHex() == firstUse.block_hash)

        // doesn't exist
        val firstUse2 = cnxn.getFirstUse("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00")
        check(firstUse2.tx_hash == null)
        check(firstUse2.block_hash == null)
        */

        val uses = cnxn.getHistory(tx.outputs[0].script)
        for (use in uses)
        {
            LogIt.info("used in block ${use.first} tx ${use.second}")
        }

        assert(uses.size >= 100)  // Might be wrong if the regtest chain startup is changed.

        val headers = cnxn.getHeadersFor(cnxn.chainSelector, 0, 1000, 30000)
        for (i in headers.indices)
        {
            val hdr = cnxn.getHeader(i, 5000)
            val hdrObj = blockHeaderFor(cnxn.chainSelector,BCHserialized(hdr,SerializationType.NETWORK))
            check(hdrObj.hash == headers[i].hash)
            // fails in some weird, probably kotlin bug way: check(hdrObj.equals(headers[i]))
        }

        cnxn.close("finished")
    }


    suspend fun TestWallet(wallet: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int)
    {
        LogIt.info("TestWallet")
        mycheck(wallet.balance == 0.toLong(), { wallet.name + ": Test Malfunction, initial balance incorrect" })

        //val dbgWal = wallet as RamWallet
        val rpcConnection = "http://$rpcuser:$rpcpassword@$ip:$port"
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = NexaRpcFactory.create(rpcConnection, rpcuser, rpcpassword)
        val peerInfo = rpc.getpeerinfo()
        mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

        var blockcount = rpc.getblockcount().toLong()
        println("Rpc " + blockcount)
        println("Blockchain " + wallet.blockchain.curHeight)
        println("Wallet " + wallet.syncedHeight)
        mycheck(
            waitUntil(120000, { println("RPC: $blockcount  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight}"); wallet.blockchain.curHeight == blockcount }),
            { wallet.blockchain.name + ": Blockchain did not sync: " + wallet.blockchain.curHeight + "  Expecting: " + blockcount })

        mycheck(
            waitUntil(120000, { println("RPC: $blockcount  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight}"); wallet.syncedHeight == blockcount }),
            { wallet.blockchain.name + ": Wallet did not sync: " + wallet.blockchain.curHeight + "  Expecting: " + blockcount })

        println("Rpc " + blockcount)
        println("Blockchain " + wallet.blockchain.curHeight)
        println("Wallet " + wallet.syncedHeight)

        // Test sending to this wallet
        val myAddr = wallet.newDestination()
        val amt = 100000L
        //LogIt.info(wallet.coinTicker +": wallet addresses quantity: " + dbgWal.receiving.size)
        var txrpc = rpc.sendtoaddress(myAddr.address.toString(), BigDecimal.fromLong(amt/100))
        LogIt.info("Wallet initial height: " + wallet.syncedHeight)
        LogIt.info("Seed addr: " + myAddr.address.toString() + " Seed tx:" + txrpc.toString())

        blockcount = rpc.getblockcount().toLong()
        //println("Rpc " + blockcount)
        //println("Blockchain " + wallet.blockchain.curHeight)
        //println("Wallet " + wallet.chainstate!!.syncedHeight)
        mycheck(waitUntil(15000, { wallet.synced(blockcount) }), { wallet.name + ": Wallet did not initially sync.  At " + wallet.syncedHeight + ". Chain is at height: " + rpc.getblockcount() })

        mycheck(
            waitUntil(20000, { wallet.balanceUnconfirmed == amt }),
            { wallet.name + ": Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + amt })
        mycheck(wallet.balanceConfirmed == 0.toLong(), { wallet.name + ": Confirmed balance incorrect" })

        rpc.generate(1)
        var height = rpc.getblockcount().toLong()
        mycheck(waitUntil(30000, {
            println("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight}")
            wallet.synced(height)
        }), { wallet.name + ": Wallet did not sync to " + height + ".  Its at height: " + rpc.getblockcount() })

        mycheck(wallet.balanceUnconfirmed == 0.toLong(), { wallet.name + ": Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() })
        mycheck(wallet.balanceConfirmed == amt, { wallet.name + ": Confirmed balance incorrect, is: " + wallet.balance.toString() })


        // Test sending back to full noed
        val startingBal = wallet.balanceConfirmed
        val otherAddr = rpc.getnewaddress()
        var tx: iTransaction = wallet.send(10000, otherAddr)
        LogIt.info("Send back to full node: " + tx.idem)

        mycheck(wallet.balanceUnconfirmed <= startingBal - 10000)  // it'll be less than because of tx fees
        var unconfBal = wallet.balanceUnconfirmed

        // check that the transaction we sent was accepted in the mempool
        mycheck(waitUntil(20000, { rpc.getrawtxpool().size >= 1 }), { "Transaction was not accepted by full node" })
        rpc.generate(1)
        // Check that the transaction we sent was mined
        var mempool = rpc.getrawtxpool()
        mycheck(mempool.size == 0)

        height = rpc.getblockcount()
        mycheck(waitUntil(20000, {
            LogIt.info("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight}")
            wallet.synced(height)
        }), { "wallet did not sync to " + height + ".  Its at height: " + rpc.getblockcount() })

        // Check that the balance became confirmed once the wallet synced with the new block
        mycheck((wallet.balanceConfirmed < startingBal) && (wallet.balanceConfirmed > 0), { "Balance: " + wallet.balanceConfirmed + "  Expected: " + unconfBal })
        mycheck(wallet.balanceUnconfirmed == 0.toLong())

        var nextDestination = wallet.newDestination()
        delay(1000)

        val dests1 = mutableListOf<PayDestination>()
        for (i in 1..5)
        {
            // Test sending to this wallet

            // Note, if we create a destination and then immediately instruct a node to send to it, there is a race condition between our registration of the new bloom filters into nodes and the send.  So instead we create the new destination
            // for the NEXT iteration of the loop at the beginning of this iteration.
            val myAddr1 = nextDestination
            nextDestination = wallet.newDestination()

            val qty1 = CurrencyDecimal(Random.nextInt(1000, 10000).toLong())
            val qty1Sat = (qty1*BigDecimal.fromInt(100)).toLong()

            val bal1 = wallet.balance

            LogIt.info("RPC Send " + qty1.toString() + " to " + myAddr1.address.toString())
            try
            {
                val result = rpc.sendtoaddress(myAddr1.address.toString(), qty1)
                LogIt.info(result.toHex())
            } catch (e: Exception)
            {
                LogIt.info(e.message)
            }

            mycheck(
                waitUntil(20000, { wallet.balanceUnconfirmed == qty1Sat }),
                { "Transaction was not noticed by wallet 1. Balance is " + wallet.balanceUnconfirmed + ".  Expecting " + qty1Sat })

            mycheck(waitUntil(10000, { rpc.getrawtxpool().size == 1 }))
            // Check that balance didn't change
            mycheck(wallet.balanceConfirmed == bal1)

            rpc.generate(1)
            height = rpc.getblockcount().toLong()

            mycheck(waitUntil(20000, {
                println("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight}");
                wallet.balance == bal1 + qty1Sat }))

            dests1.add(myAddr1)

            val numTxes = Random.nextInt(20) + 5
            LogIt.info("Wallet Balance: " + wallet.balanceConfirmed + " Unconfirmed: " + wallet.balanceUnconfirmed)
            val total = wallet.balanceConfirmed + wallet.balanceUnconfirmed
            check(total == wallet.balance)
            for (j in 1..numTxes)
            {

                val a1 = dests1[Random.nextInt(0, dests1.size)]

                val qtytmp = Random.nextInt(1000, 4000).toLong()

                val sz = rpc.getrawtxpool().size
                val txtmp = wallet.send(qtytmp, a1.lockingScript())
                LogIt.info("Sent " + qtytmp + " NexaSats to " + a1.address.toString() + " in TX id: " + txtmp.id.toHex() + " idem:" + txtmp.idem.toHex() + ".  Balance: " + wallet.balanceConfirmed + "  Unconfirmed: " + wallet.balanceUnconfirmed)
                mycheck(waitUntil(10000, {
                    val txpool = rpc.getrawtxpool()
                    txpool.size == sz+1
                }), {
                    LogIt.severe("TX not accepted: ${txtmp.toHex()}")
                    null
                })

                if (wallet.balance < total - (j * 1000))  // I'm sending to myself so should only lose fees which we can estimate to be less than 1k SAT per tx
                {
                    LogIt.severe("Balance problem!")
                }
            }


            // Make sure that my tx have propagated
            mycheck(waitUntil(20000, {
                val txpool = rpc.getrawtxpool()
                println("txpool size: ${txpool.size}")
                txpool.size == numTxes
            }))

            rpc.generate(1)
            height = rpc.getblockcount().toLong()
            // Generated block should have confirmed all of the tx I created
            mycheck(waitUntil(20000, {
                LogIt.info("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight} balance: ${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed}")
                wallet.balanceUnconfirmed == 0.toLong()
            }), { "Transaction was not noticed by wallet 1.  Unconfirmed balance is " + wallet.balanceUnconfirmed })

            LogIt.info("loop " + i.toString())
        }

        LogIt.info("TestWallet completed")
    }

    fun txosToString(txos: Map<iTxOutpoint, Spendable>): String
    {
        val ret = StringJoiner(",\n")
        for (t in txos)
        {
            ret.add(t.key.toHex() + " amt: " + t.value.amount)
        }
        return "[" + ret.toString() + "]"
    }

    suspend fun TestWalletRewind(wallet: Wallet, rpc: NexaRpc)
    {
        rpc.generate(1) // If a prior test left the blockchain in a "tie" state, break the tie so that this test starts cleanly

        // Test sending to this wallet
        val myAddr = wallet.newDestination()
        // Send 1000
        var txrpc = rpc.sendtoaddress(myAddr.address.toString(), 10.toBigDecimal())
        LogIt.info("Wallet initial height: " + wallet.syncedHeight)
        LogIt.info("Initial funding tx:" + txrpc.toString())

        var height = rpc.getblockcount()
        // Initial sync can take a while if your regtest is big
        mycheck(waitUntil(300000, { wallet.synced(height) }), { "Wallet did not initially sync (at " + wallet.syncedHeight + ". Chain is at height: " + rpc.getblockcount() })

        mycheck(waitUntil(5000, { wallet.balanceUnconfirmed == 1000L }), { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(100) })
        mycheck(wallet.balanceConfirmed == 0.toLong(), { "Confirmed balance incorrect" })

        val block1Hash = rpc.generate(1)[0]
        LogIt.info("Moving forward to " + block1Hash)
        height = rpc.getblockcount()
        mycheck(waitUntil(10000, { wallet.synced(height) }), { "wallet did not sync to " + height + ".  Its at height: " + rpc.getblockcount() })

        mycheck(waitUntil(10000, { wallet.balanceUnconfirmed == 0L }), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() })
        mycheck(waitUntil(4000, { wallet.balanceConfirmed == 1000L }), { "Confirmed balance incorrect, is: " + wallet.balanceConfirmed.toString() })

        rpc.invalidateblock(block1Hash)
        LogIt.info("Invalidated " + block1Hash + " abandoning tx " + txrpc.toString())
        // race condition abandoning the transaction so loop until it works.  (It may not have been re-processed yet after the block invalidate)
        mycheck(waitUntil(5000, { rpc.getrawtxpool().contains(txrpc) }), { "unwound tx didn't appear in mempool" })
        val abandonRet = rpc.abandontransaction(txrpc)  // This RPC is not provided by the library so fall back to the query interface
        if (abandonRet != null) LogIt.info(abandonRet.toString())
        mycheck(rpc.getrawtxpool().size == 0, { "Expecting empty mempool, found: " + rpc.getrawtxpool().toString() + "\n  tx that should be abandoned: " + rpc.getrawtransaction(txrpc).toString() })
        val forkHashes = rpc.generate(2)
        LogIt.info("Forked with " + forkHashes[0] + " and " + forkHashes[1])

        // 1000 balance should be gone because of taking the other fork

        // Wait for this wallet to sync against the new header chain
        mycheck(waitUntil(50000, { wallet.balanceUnconfirmed == 0L }), { "Post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(0) })
        mycheck(waitUntil(1000000, { wallet.balanceConfirmed == 0L }), { "Post reorg confirmed balance incorrect, is " + wallet.balanceConfirmed })

        LogIt.info("TestWalletRewind simple rewind completed")
        // ----------------------------
        LogIt.info("Mempool size is " + rpc.getrawtxpool().size)

        val myAddr2 = wallet.newDestination()

        // Send 3000 in 2 transactions
        txrpc = rpc.sendtoaddress(myAddr.address.toString(), 10.toBigDecimal())
        rpc.sendtoaddress(myAddr2.address.toString(), 20.toBigDecimal())

        mycheck(rpc.getrawtxpool().size == 2)
        mycheck(wallet.synced(rpc.getblockcount().toLong()), { "Wallet did not initially sync (at " + wallet.syncedHeight + ". Chain is at height: " + rpc.getblockcount() })

        mycheck(waitUntil(5000, { wallet.balanceUnconfirmed == 3000L }), { "Unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(3000) })
        mycheck(wallet.balanceConfirmed == 0.toLong(), { "Confirmed balance incorrect" })

        val block2Hash = rpc.generate(1)[0]

        val btcdAddr = rpc.getnewaddress()

        val tx2 = wallet.send(1000, btcdAddr)
        LogIt.info("Send back to full node: " + tx2.idem.toHex())

        mycheck(waitUntil(5000, { rpc.getrawtxpool().size == 1 }), { "tx did not enter txpool" })

        val block3Hash = rpc.generate(1)[0]

        mycheck(waitUntil(20000, { wallet.balanceUnconfirmed == 0.toLong() }), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() })
        // Bracket the expected amount to account for the tx fee
        mycheck(waitUntil(20000, { wallet.balanceConfirmed < 3000 - 1000  && wallet.balanceConfirmed > 3000 - 2000 }), { "Confirmed balance incorrect, is: " + wallet.balanceConfirmed.toString() })  // > -11 because tx fee

        //LogIt.info("Wallet TXOs:\n" + txosToString(wallet.txos))
        // Invalidate this branch
        rpc.invalidateblock(block3Hash)
        LogIt.info("Invalidated " + block3Hash)
        mycheck(waitUntil(5000, { rpc.getrawtxpool().size > 0 }), { "invalidated block's tx did not enter txpool" })

        LogIt.info("evicting: " + tx2.id.toHex())
        rpc.evicttransaction(tx2.id.toHex())  // Clear my tx out of the node's mempool

        mycheck(waitUntil(5000, { rpc.getrawtxpool().size == 0 }), { "can't remove tx from txpool" })

        rpc.generate(3)
        height = rpc.getblockcount()

        mycheck(waitUntil(20000,{
            LogIt.info("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight} balance: ${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed}")
            wallet.synced(height) } ))

        //LogIt.info("Wallet TXOs:\n" + txosToString(wallet.txos))
        // Now my send of 1000 should be gone, because we rewound and switched to a different fork
        mycheck(wallet.balanceUnconfirmed == 0.toLong(), { "Unconfirmed balance should be 0, is: " + wallet.balanceUnconfirmed.toString() })
        mycheck(wallet.balanceConfirmed == 3000L,  {"Confirmed balance incorrect, is: " + wallet.balanceConfirmed.toString() })

        rpc.invalidateblock(block2Hash)
        mycheck(waitUntil(5000, { rpc.getrawtxpool().size == 2 }), { "unwound tx didn't appear in mempool" })
        rpc.abandontransaction(txrpc)  // Abandon one but let the other replay

        // Rewind all the tx in this test
        rpc.generate(5)  // I need enough blocks to exceed the other fork before this wallet will move over
        height = rpc.getblockcount()

        mycheck(waitUntil(20000,{
            LogIt.info("RPC: $height  chain: ${wallet.blockchain.curHeight}  wallet: ${wallet.syncedHeight} balance: ${wallet.balanceConfirmed}:${wallet.balanceUnconfirmed}")
            wallet.synced(height) } ))

        //LogIt.info("Wallet TXOs:\n" + txosToString(wallet.txos))

        mycheck(waitUntil(20000, { wallet.balanceUnconfirmed == 0L }), { "Post reorg, unconfirmed balance incorrect. Was: " + wallet.balanceUnconfirmed + "  Expecting: " + mBCHtoSAT(0) })
        mycheck(waitUntil(20000, { wallet.balanceConfirmed == 0L }), { "Post reorg confirmed balance incorrect, is " + wallet.balanceConfirmed })


        LogIt.info("TestWalletRewind completed")
    }

    /*
suspend fun TestMultiBlockchain(wallet1: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int,
                               wallet2: Wallet, rpcuser2: String, rpcpassword2: String, ip2: String, port2:Int
)
{
    //var rpc1 = BitcoinJSONRPCClient("http://bu:bu@127.0.0.1:18332/")
    var rpc1 = BitcoinJSONRPCClient("http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port)
    val peerInfo = rpc1.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var rpc2 = BitcoinJSONRPCClient("http://" + rpcuser2 + ":" + rpcpassword2 + "@" + ip2 + ":" + port2)
    val peerInfo2 = rpc2.peerInfo
    mycheck(peerInfo2.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    mycheck(wallet1.synced(rpc1.getblockcount().toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc1.getblockcount()})
    mycheck(wallet2.synced(rpc2.getblockcount().toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc2.getblockcount()})


    // Test sending to this wallet
    val myAddr1 = wallet1.newDestination()
    val myAddr2 = wallet2.newDestination()
    rpc1.sendToAddress(myAddr1.address.toString(), 0.1.toBigDecimal())
    rpc2.sendToAddress(myAddr2.address.toString(), 0.09.toBigDecimal())

    mycheck(waitUntil(10000, { wallet1.balanceUnconfirmed == BCHtoSAT(0.1.toBigDecimal())}), { "Transaction was not noticed by wallet 1" } )
    mycheck(waitUntil(10000, { wallet2.balanceUnconfirmed == BCHtoSAT(0.09.toBigDecimal())}), { "Transaction was not noticed by wallet 2" } )

    mycheck(wallet1.balanceConfirmed == 0.toLong())
    mycheck(wallet2.balanceConfirmed == 0.toLong())

    rpc2.generate(1)

    mycheck(waitUntil(10000, { wallet2.balanceConfirmed == BCHtoSAT(0.09.toBigDecimal())}), { "Confirmation was not noticed by wallet 2"})
    mycheck(wallet1.balanceUnconfirmed == BCHtoSAT(0.1.toBigDecimal()))  // finding a block on 2 should not affect 1
    mycheck(wallet1.balanceConfirmed == 0.toLong())  // finding a block on 2 should not affect 1

    rpc1.generate(1)

    mycheck(waitUntil(10000, {wallet1.balanceConfirmed == BCHtoSAT(0.1.toBigDecimal())}))
    mycheck(wallet2.balanceConfirmed == BCHtoSAT(0.09.toBigDecimal()))

    LogIt.info("TestMultiBlockchain completed")
}
     */


    fun analyzebug(txes: List<iTransaction>, rawMempool: List<String>):String
    {
        val mytx: MutableSet<String> = mutableSetOf()
        val mptx: MutableSet<String> = mutableSetOf()
        for (t in txes) mytx.add(t.idem.toHex())
        for (tx in rawMempool) mptx.add(tx)

        val diff = mytx - mptx
        var s: String = "Missing TX: "
        for (txhash in diff)
        {
            s = s + txhash + " "
        }
        s = s + "\nMempool size: " + rawMempool.size + " Expected: " + mytx.size
        return s
    }

    /*
suspend fun TestMultiBlockchainLongevity(wallet1: Wallet, rpcuser: String, rpcpassword: String, ip: String, port:Int,
                                wallet2: Wallet, rpcuser2: String, rpcpassword2: String, ip2: String, port2:Int
)
{
    //var rpc1 = BitcoinJSONRPCClient("http://bu:bu@127.0.0.1:18332/")
    var rpc1 = BitcoinJSONRPCClient("http://" + rpcuser + ":" + rpcpassword + "@" + ip + ":" + port)
    val peerInfo = rpc1.peerInfo
    mycheck(peerInfo.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    var rpc2 = BitcoinJSONRPCClient("http://" + rpcuser2 + ":" + rpcpassword2 + "@" + ip2 + ":" + port2)
    val peerInfo2 = rpc2.peerInfo
    mycheck(peerInfo2.size >= 1)  // really just 1 unless I'm testing this simultaneously with another wallet

    mycheck(wallet1.synced(rpc1.getblockcount().toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc1.getblockcount()})
    mycheck(wallet2.synced(rpc2.getblockcount().toLong()), { "Wallet did not initially sync. Chain is at height: " + rpc2.getblockcount()})

    val dests1 = mutableListOf<PayDestination>()
    val dests2 = mutableListOf<PayDestination>()

    for (i in 1..100000)
    {
        // Test sending to this wallet
        val myAddr1 = wallet1.newDestination()
        val myAddr2 = wallet2.newDestination()

        val qty1 = Random.nextInt(100, 10000).toBigDecimal(currencyMath).setScale(currencyScale)/1000.toBigDecimal(currencyMath)
        val qty2 = Random.nextInt(100, 10000).toBigDecimal(currencyMath).setScale(currencyScale)/1000.toBigDecimal(currencyMath)

        val bal1 = wallet1.balanceConfirmed
        val bal2 = wallet2.balanceConfirmed

        LogIt.info("Send " + qty1.toString() + " to " + myAddr1.address.toString())
        LogIt.info("Send " + qty2.toString() + " to " + myAddr2.address.toString())
        try
        {
            val tx1 = rpc1.sendToAddress(myAddr1.address.toString(), qty1)
            LogIt.info("Send " + qty1.toString() + " to " + myAddr1.address.toString() + " in tx " + tx1)
            val tx2 = rpc2.sendToAddress(myAddr2.address.toString(), qty2)
            LogIt.info("Send " + qty2.toString() + " to " + myAddr2.address.toString() + " in tx " + tx2)
        }
        catch(e: Exception)
        {
            LogIt.info(e.message)
        }

        mycheck(waitUntil(4000, { wallet1.balanceUnconfirmed == BCHtoSAT(qty1) }), { "Transaction was not noticed by wallet 1" })
        mycheck(waitUntil(4000, { wallet2.balanceUnconfirmed == BCHtoSAT(qty2) }), { "Transaction was not noticed by wallet 2" })

        // Check that balance didn't change
        mycheck(wallet1.balanceConfirmed == bal1)
        mycheck(wallet2.balanceConfirmed == bal2)

        rpc2.generate(1)

        mycheck(waitUntil(4000, { wallet2.balanceConfirmed == bal2 + BCHtoSAT(qty2) }), { "Confirmation was not noticed by wallet 2" })

        rpc1.generate(1)

        mycheck(waitUntil(4000, { wallet1.balanceConfirmed == bal1 + BCHtoSAT(qty1) }))

        dests1.add(myAddr1)
        dests2.add(myAddr2)

        val numTxes = Random.nextInt(24)

        var txes1:MutableList<iTransaction> = mutableListOf()
        var txes2:MutableList<iTransaction> = mutableListOf()
        for (j in 1..numTxes)
        {
            val a1 = dests1[Random.nextInt(0, dests1.size)]
            val a2 = dests2[Random.nextInt(0, dests2.size)]

            val qtytmp1 = Random.nextInt(10, 1000).toBigDecimal(currencyMath).setScale(currencyScale)/10000.toBigDecimal(currencyMath)
            val qtytmp2 = Random.nextInt(10, 1000).toBigDecimal(currencyMath).setScale(currencyScale)/10000.toBigDecimal(currencyMath)

            // These wallets are on different blockchains so I have to send to my own wallet here
            txes1.add(wallet1.send(BCHtoSAT(qtytmp1), a1.outputScript()))
            txes2.add(wallet2.send(BCHtoSAT(qtytmp2), a2.outputScript()))
        }

        // Make sure that my tx have propagated
        mycheck(waitUntil(10000, { rpc1.getRawMemPool().size == numTxes }),
            {
                val rawMempool = rpc1.getRawMemPool()
                analyzebug(txes1, rawMempool)
            })
        mycheck(waitUntil(20000, {
            val mpsize = rpc2.getRawMemPool().size
            LogIt.info("rpc2 mempool size " + mpsize)
            mpsize == numTxes
        }),
            {
                val rawMempool = rpc2.getRawMemPool()
                analyzebug(txes2, rawMempool)
                //"Mempool size: " + rpc2.rawMemPool.size + " Expected: " + numTxes
            })

        rpc2.generate(1)
        rpc1.generate(1)
        mycheck(waitUntil(4000, { wallet1.balanceUnconfirmed == 0.toLong() }), { "Transaction was not noticed by wallet 1" })
        mycheck(waitUntil(4000, { wallet2.balanceUnconfirmed == 0.toLong() }), { "Transaction was not noticed by wallet 2" })

        LogIt.info("loop " + i.toString())
    }

    LogIt.info("TestMultiBlockchainLongevity completed")
}

     */

}


class TestMultiBlockchain
{
    // @BeforeTest
    fun initJvm()
    {
        initializeLibNexa()
    }

    @kotlin.time.ExperimentalTime
    // @Test  // CI only runs Nexa regtest so this test can't be run
    fun testMultichain()
    {
        LogIt.info("This test requires a Nexa full node running on regtest at localhost and port ${NexaRegtestRpcPort}")

        // Set up RPC connection

        val rpcConnection = "http://$RPC_USER:$RPC_PASSWORD@127.0.0.1:$NexaRegtestRpcPort"
        //LogIt.info("Connecting to: " + rpcConnection)
        val nexaRpc = NexaRpcFactory.create(rpcConnection)
        var nexablks = nexaRpc.getblockcount()
        check(nexablks > 100)

        LogIt.info("This test requires a Bitcoin Cash full node running on regtest at localhost and port ${BchRegtestRpcPort}")
        val bchRpcConnection = "http://127.0.0.1:" + BchRegtestRpcPort
        //LogIt.info("Connecting to: " + rpcConnection)
        val bchRpc = NexaRpcFactory.create(bchRpcConnection) as JvmNexaRpc
        var bchblks = bchRpc.getblockcount()
        check(bchblks > 100)

        // clean up
        for(i in listOf("RNEX.db", "RBCH.db", "test_rbch.db", "test_rnex.db"))
        {
            Path(i).deleteIfExists()
        }

        val RBCH = connectBlockchain(ChainSelector.BCHREGTEST)
        val RNEX = connectBlockchain(ChainSelector.NEXAREGTEST)


        val rbch = newWallet("test_rbch", RBCH)
        val rnex = newWallet("test_rnex", RNEX)

        while((rbch.syncedHeight != bchblks) || (rnex.syncedHeight != nexablks))
        {
            sleep(1000)
            println("RBCH: ${rbch.syncedHeight}:${rbch.blockchain.curHeight} of ${bchblks}  RNEX: ${rnex.syncedHeight}:${rnex.blockchain.curHeight} of ${nexablks}")
        }

        // clear out any txpool items
        bchRpc.generate(1)
        nexaRpc.generate(1)
        nexablks++
        bchblks++

        val bchaddr = rbch.getnewaddress()
        val nexaddr = rnex.getnewaddress()

        bchRpc.sendtoaddress(bchaddr.toString(), UbchDecimal(1L))
        nexaRpc.sendtoaddress(nexaddr.toString(), NexaDecimal(1000000L))

        check(bchRpc.bch_getmempoolinfo().size == 1L)
        check(nexaRpc.gettxpoolinfo().size == 1L)

        check(rbch.balanceUnconfirmed == 100L*MILLION )
        check(rnex.balanceUnconfirmed == 100L*MILLION )

        waitFor(WAITAMT) { rnex.syncedHeight == nexablks }
        waitFor(WAITAMT) { rbch.syncedHeight == bchblks }

        println("RBCH: ${rbch.balanceConfirmed}:${rbch.balanceUnconfirmed} at ${rbch.syncedHeight}:${rbch.blockchain.curHeight} of ${bchblks}  RNEX: ${rnex.balanceConfirmed}:${rnex.balanceUnconfirmed} at ${rnex.syncedHeight}:${rnex.blockchain.curHeight} of ${nexablks}")

        bchRpc.generate(1)
        nexaRpc.generate(1)
        nexablks++
        bchblks++
        check(bchRpc.bch_getmempoolinfo().size == 0L)
        check(nexaRpc.gettxpoolinfo().size == 0L)

        waitFor(WAITAMT) { rnex.syncedHeight == nexablks }
        waitFor(WAITAMT) { rbch.syncedHeight == bchblks }
        println("RBCH: ${rbch.balanceConfirmed}:${rbch.balanceUnconfirmed} at ${rbch.syncedHeight}:${rbch.blockchain.curHeight} of ${bchblks}  RNEX: ${rnex.balanceConfirmed}:${rnex.balanceUnconfirmed} at ${rnex.syncedHeight}:${rnex.blockchain.curHeight} of ${nexablks}")

        check(rbch.balanceConfirmed == 100L*MILLION )
        check(rnex.balanceConfirmed == 100L*MILLION )
        check(rbch.balanceUnconfirmed == 0L )
        check(rnex.balanceUnconfirmed == 0L )

        val bchaddr2 = bchRpc.getnewaddress()
        val nexaddr2 = nexaRpc.getnewaddress()

        val result = rbch.send(10000, bchaddr2)
        //rnex.send(10000, nexaddr2)

        waitFor(WAITAMT) { bchRpc.bch_getmempoolinfo().size == 1L }
        //check(nexaRpc.gettxpoolinfo().size == 1L)

        println("RBCH: ${rbch.balanceConfirmed}:${rbch.balanceUnconfirmed} at ${rbch.syncedHeight}:${rbch.blockchain.curHeight} of ${bchblks}  RNEX: ${rnex.balanceConfirmed}:${rnex.balanceUnconfirmed} at ${rnex.syncedHeight}:${rnex.blockchain.curHeight} of ${nexablks}")
        val blkhash = bchRpc.generate(1)[0]
        nexaRpc.generate(1)
        nexablks++
        bchblks++
        waitFor(WAITAMT) { rnex.syncedHeight == nexablks }
        waitFor(WAITAMT) { rbch.syncedHeight == bchblks }

        println("RBCH: ${rbch.balanceConfirmed}:${rbch.balanceUnconfirmed} at ${rbch.syncedHeight}:${rbch.blockchain.curHeight} of ${bchblks}  RNEX: ${rnex.balanceConfirmed}:${rnex.balanceUnconfirmed} at ${rnex.syncedHeight}:${rnex.blockchain.curHeight} of ${nexablks}")
        println(blkhash.toHex())
    }

}

class OracleTest
{
    @Test
    fun TestCliParse(): Unit
    {
        runBlocking {
            initializeLibNexa()

            val result = getData("http://wallywallet.org/_api/v0/now/hourlyavg/usdt/nexa")

            /** NEED TO FIGURE OUT HOW TO CALL CORUN() FROM TEST**/
            //var result = ""
            //corun{result = getOracleData("http://wallywallet.org/_api/v0/now/hourlyavg/usdt/nexa")}

            val oracleObject = parseOracleObject(result)

            // String.fromHex() from libexakotlin.libnexa
            val byteArray = oracleObject.msg.data.fromHex()
            val parsedData = parseOracleData(byteArray)

            assertTrue(parsedData.tickerA == "NEXA")
            assertTrue(parsedData.tickerB == "USDT")
            val now = Instant.now().toEpochMilli() / 1000
            assertTrue((parsedData.epochSeconds > now-3600) && (parsedData.epochSeconds < now))
            println((parsedData.price / 1e16).toString())       // corresponding price from DB * 1e16

            // store in data type used by wallywallet.org DB
            val priceDataPoint = PriceDataPoint()
            priceDataPoint.setVals(parsedData.tickerA, parsedData.tickerB, parsedData.epochSeconds, parsedData.price)

            val checkData = priceDataPoint.toByteArray()

            // this is the message arg for signHashSchnorr()
            val hashedData = libnexa.sha256(checkData)

            /** Continue with sig verification **/
        }
    }

    private fun getData(urlString: String): String {
        var data  = ""
        runBlocking {
            data = getOracleData(urlString)
        }
        return data
    }

    private suspend fun getOracleData(urlString: String): String
    {
        val client = HttpClient(CIO)
        return try
        {
            val response: HttpResponse = client.get(urlString)
            response.bodyAsText()
        }
        catch (e: Exception)
        {
            println("Error: ${e.message}")
            ""
        }
        finally
        {
            client.close()
        }
    }
}
