//import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.*
import org.wallywallet.wew.cli.*
import org.wallywallet.wew.cli.openWallet

val w = openWallet("nftFaucet")
val SftGroup: GroupId = GroupId("nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqq3dnu5tvxdzgnhf0qlysek3flfgw7w4hx8tq9qdz6cuuc07sez69leula2dq")
val addr = PayAddress("nexa:nqtsq5g5t52ae5236ylhzevlfmndrcw7ls96tusnpy8kdhaw")


fun SplitSFT(numQrs: Int, outAddress: PayAddress)
{
    // globals to be commented out when script-ified
    val w = openWallet("nftFaucet")
    val SftGroup: GroupId = GroupId("nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqq3dnu5tvxdzgnhf0qlysek3flfgw7w4hx8tq9qdz6cuuc07sez69leula2dq")
    val addr = "nexa:nqtsq5g5t52ae5236ylhzevlfmndrcw7ls96tusnpy8kdhaw"
    // end globals

    val payAddress = PayAddress(addr)

    val chain = ChainSelector.NEXA
    val singleSftUtxo = mutableListOf<Spendable>()
    val multiSftUtxo = mutableListOf<Spendable>()

    w.forEachUtxo {
        val grp = it.groupInfo
        if (grp != null)
        {
            if ((!grp.isAuthority()) && (grp.groupId == SftGroup))
            {
                if (grp.tokenAmt == 1L)
                    singleSftUtxo.add(it)
                else
                    multiSftUtxo.add(it)
            }
        }
        false
    }

    if (singleSftUtxo.size >= numQrs)
    {
        println("Nothing to do: already have ${singleSftUtxo.size} SFT UTXOs ready to go!")
        return
    }

    val MAX_SPLITS = 250  // extra room for nexa change
    for (utxo in multiSftUtxo)
    {
        val tokInAmt = utxo.groupInfo!!.tokenAmt
        val tx = txFor(chain)
        while(true)
        {
            val sft = txOutputFor(chain, dust(chain), payAddress.groupedLockingScript(utxo.groupInfo!!.groupId, 1))
            tx.add(sft)
            if ((tx.outputs.size > MAX_SPLITS) || (tx.outputs.size >= tokInAmt)) break
        }
        if (tx.outputs.size < tokInAmt)  // spend left over tokens to myself in 1 output
        {
            val sft = txOutputFor(chain, dust(chain), payAddress.groupedLockingScript(utxo.groupInfo!!.groupId, tokInAmt-tx.outputs.size))
            tx.add(sft)
        }
        // If for some reason we didn't split anything then just skip this split
        if (tx.outputs.size < 2) break
        tx.add(txInputFor(utxo))
        w.txCompleter(tx, 0, TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.SIGN)
        print("SFT Splitter transaction:")
        print(tx)
        print(tx.toHex())
        w.send(tx)
    }
}