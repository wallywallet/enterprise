// Copyright (c) 2019 Bitcoin Unlimited
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.wew

import com.ionspin.kotlin.bignum.decimal.BigDecimal
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.NexaScript
import org.nexa.nexarpc.NexaRpc
import org.nexa.nexarpc.NexaRpcFactory
import kotlin.test.Test
import kotlin.test.BeforeTest
private val LogIt = GetLog("wew.test.multisig")


// val NexaDbg = "https://debug.nexa.org"
val NexaDbg = "http://localhost:7998"

class LowestByteArray:Comparator<ByteArray>
{
    override fun compare(a: ByteArray?, b: ByteArray?): Int
    {
        if (a == null && b == null) return 0
        if (a == null) return -1
        if (b == null) return 1
        val minSize = min(a.size, b.size)
        for (i in 0 until minSize)
        {
            val cmp = a[i].toPositiveInt().compareTo(b[i].toPositiveInt())
            if (cmp != 0) return cmp
        }
        return a.size.compareTo(b.size)
    }
}

class LowestDestAddress:Comparator<PayDestination>
{
    override fun compare(a: PayDestination?, b: PayDestination?): Int
    {
        if (a == null && b == null) return 0
        if (a == null) return -1
        if (b == null) return 1
        return LowestByteArray().compare(a.pubkey, b.pubkey)
    }
}

fun createUnlockingScript(template: SatoshiScript?, constraint: SatoshiScript?, satisfier: SatoshiScript?): SatoshiScript
{
    val ret = SatoshiScript(satisfier?.chainSelector ?: template?.chainSelector ?: constraint?.chainSelector ?: throw IllegalArgumentException("Provide at least one script"))
    if (template != null) ret.add(OP.push(template.toByteArray()))
    if (constraint != null) ret.add(OP.push(constraint.toByteArray()))
    if (satisfier != null )
      return ret + satisfier
    else return ret
}

class MultisigTest
{
    lateinit var rpc: NexaRpc
    @BeforeTest
    fun init()
    {
        initializeLibNexa()

        val rpcConnection = "http://$REGTEST_IP:" + NexaRegtestRpcPort
        LogIt.info("Connecting to: " + rpcConnection)
        rpc = NexaRpcFactory.create(rpcConnection, RPC_USER, RPC_PASSWORD)
        val tipIdx = rpc.getblockcount()
        if (tipIdx < 102)
            rpc.generate((101 - tipIdx).toInt())
        else
        {
            val tip = rpc.getblock(tipIdx)
            // The tip is so old that this node won't think its synced so we need to produce a block
            if (epochSeconds() - tip.time > 1000) rpc.generate(1)
        }

    }

    fun formMultisig(cs:ChainSelector, requiredSigs: Int, dests:List<ByteArray>): NexaScript
    {
        assert(requiredSigs <= dests.size) // or it is impossible to unlock
        assert(requiredSigs > 0) // Well if its 0 its anyone can spend which is likely not intended
        val ss = NexaScript(OP.push(requiredSigs), chainSelector = cs)
        dests.forEach { ss.add(OP.push(it))}
        ss.add(OP.push(dests.size)).add(OP.CHECKMULTISIGVERIFY)
        return ss
    }


    @Test
    fun testmanualmultisig()
    {
        val cs = ChainSelector.NEXAREGTEST
        // create 3 wallets to act as participants in the multisig operation
        val parts = arrayOf(openOrNewWallet("w0", cs),openOrNewWallet("w1", cs),openOrNewWallet("w2", cs))

        // now get a destination from each wallet to form the multisig address
        //val dests = parts.map { it.getNewDestination() }
        val gdests = parts.map { it.getDestinationAtIndex(0) }  // for debug always get the same one
        // sort the destinations so that all participants produce the same multisig script
        val dests = gdests.sortedWith(LowestDestAddress())

        // if the wallet does not offer destinations with a simple public key, then it can't be used for multisig
        val msigTemplateScript = formMultisig(cs,dests.size/2 + 1, dests.map { it.pubkey!! })
        //val msigTemplateScript = formMultisig(cs,1, listOf(dests.first().pubkey!!))
        //val msigTemplateScript = formMultisig(cs,1, dests.map { it.pubkey!! })
        println("multisig script is ${msigTemplateScript.toAsm()}")

        val locking = msigTemplateScript.constraint()  // TODO rename fn call to locking()
        check(locking.type == SatoshiScript.Type.TEMPLATE)
        println("locking script is ${locking.toAsm()}")

        val addr = locking.address
        check(addr != null)
        // this check will only work if getDestinationAtIndex
        //check(addr.toString() == "nexareg:nqtsq99jmapuhdlh6rvuhy6jtvrt6p23kn3swasq4etq95pe")
        println("multisig address is $addr")

        // ok send some money to this address, using the full node
        val fundingTxIdem = rpc.sendtoaddress(addr.toString(), BigDecimal.fromInt(10000))
        println("funded with tx $fundingTxIdem")

        // Get info about this send
        val fundingTxDetails = rpc.gettransactiondetails(fundingTxIdem)
        println("$fundingTxDetails")
        val rawfundingTxBa = rpc.getrawtransaction(fundingTxIdem)
        val fundingTx = NexaTransaction.fromHex(cs, rawfundingTxBa.toHex())

        // Now lets spend this multisig
        val backAddr = rpc.getnewaddress()

        val spendingTx = NexaTransaction(cs)
        val outputIdx = fundingTx.findOutput(addr)
        val spendingUtxo = fundingTx.outputs[outputIdx]
        val inAmt = spendingUtxo.amount
        val inp = NexaTxInput(Spendable(cs, fundingTx.outpoints[outputIdx], inAmt))

        spendingTx.add(inp)
        spendingTx.add(NexaTxOutput(cs, inAmt-500, PayAddress(backAddr).lockingScript() ))
        val unsignedTx = spendingTx.BCHserialize(SerializationType.NETWORK).toByteArray()

        // sign the transaction
        val sighash = byteArrayOf()  // NexaSigHashType().all().build()
        val sig0 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, sighash, 0, inAmt, msigTemplateScript.toByteArray(), dests[0].secret!!.getSecret())
        val sig1 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, sighash, 0, inAmt, msigTemplateScript.toByteArray(), dests[1].secret!!.getSecret())
        // val sig2 = libnexa.signTxOneInputUsingSchnorr(unsignedTx, NexaSigHashType().all().build() , 0, inAmt, fundingTx.outputs[outputIdx].script.toByteArray(), dests[2].secret!!.getSecret())

        // see https://spec.nexa.org/cryptography/multisignature/?h=op_che#public-multisignature-op_checkmultisigverify for multisig formulation
        val satisfier = SatoshiScript(cs).add(OP.C3, OP.push(sig0),OP.push(sig1))
        //val satisfier = SatoshiScript(cs).add(OP.C1, OP.push(sig0))

        spendingTx.inputs[0].script = createUnlockingScript(msigTemplateScript, null, satisfier)

        println("Spend with: $spendingTx")
        // println("SPEND HEX: ${spendingTx.toHex()}")
        // println("$NexaDbg/tx/${spendingTx.toHex()}?idx=0")
        println("$NexaDbg/tx/${spendingTx.toHex()}?idx=0&utxo=${spendingUtxo.BCHserialize(SerializationType.NETWORK).toHex()}")

        val txid = rpc.sendrawtransaction(spendingTx.toHex())
        println("spending tx id is $txid")

        rpc.generate(1)
        println("done")
    }

}