@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.oracle

import org.nexa.libnexakotlin.*
import Nexa.npl.*
import java.time.Instant

/** This is the mainnet oracle public key */
val OraclePubKey = byteArrayOf()

/** This is the testnet/regtest oracle public key.  You can ask the oracle to sign messages with this pubkey, and it will do so
 * regardless of whether the event has occurred. */
val TestOraclePubKey = byteArrayOf() // NPubKey("WallyOraclePubKey.test")



/** Convert this string to a ByteArray of size [zeroPadTo], padding it out with 0s */
fun String.toPaddedByteArray(zeroPadTo: Int): ByteArray = this.toByteArray().copyOf(zeroPadTo)


/** The oracle periodically issues messages that relate the price of [tickerA] in units of [tickerB].
 * For ease of processing these prices must be specified in integers (Long).  Therefore, it is necessary for
 * [tickerB]'s value to be sufficiently low relative to [tickerA] to minimize error due to the granularity of [tickerB].
 * To accomplish this, [tickerB] may be specified as fractional units of a more commonly used unit.  For example, [tickerB]
 * could be defined as 1/10 of a cent rather than a whole dollar.  This unit definition is beyond the scope of the general
 * message format, and should be defined based on the specific items involved.
 * */
class PriceDataPoint(name: String? = null, _nsl: NSL? = null):PackedStructure(name, _nsl)
{
    constructor(_tickerA: String, _tickerB:String, _epochSeconds: Long, _priceAinB: Long):this()
    {
        tickerA.curVal = _tickerA.toPaddedByteArray(8)
        tickerB.curVal = _tickerB.toPaddedByteArray(8)
        epochSeconds.curVal = _epochSeconds
        priceAinB.curVal = _priceAinB
    }

    val tickerA: NBytes by PBytes(8)
    val tickerB: NBytes by PBytes(8)
    val epochSeconds: NInt by PInt(8)
    // Since this is an integer, the B asset should be its finest unit (e.g. pennies rather than dollars)
    // Or for large granularity units, you could create an artificial division.
    // This finest unit is therefore implied by the name of asset B and does not need to be otherwise specified.
    val priceAinB: NInt by PInt(8)
}

/** This ticker is Nexa expressed in MEX (million NEXA).  The ticker itself uses the convention that 'N means *10^N (like scientific notation eN).
 * So since N MEX is N*1e6 (N*10^6) NEXA, the ticker is the string "NEXA'6".  Although "e" is used in scientific notation, it is not used here to
 * avoid confusion with tickers that end with that letter.*/
val NexaTicker = "NEXA'6"

/** This ticker is USDT expressed in pennies.  The ticker itself uses the convention that .N means *10^-N (like scientific notation e-N).
 * So since N USDT pennies is N*1e-2 (N*10^-2) USDT, the ticker is the string "USDT.2". */
val UsdtInPenniesTicker = "USDT.2"


/** Server APIs: construct a Nexa/USDT price data point */
fun NexaUsdtMessage(priceAinB: Long, priceTimeEpochSeconds:Long? = null):PriceDataPoint
{
    val time = priceTimeEpochSeconds ?: Instant.now().epochSecond
    return PriceDataPoint(NexaTicker, UsdtInPenniesTicker, time, priceAinB)
}

/** Server APIs: sign a price data point message */
fun SignMessage(msg: PriceDataPoint, oracleSecret: Secret): ByteArray
{
    val pdp = msg.pack()

    val msgHash = libnexa.sha256(pdp)
    val oracleSig = libnexa.signHashSchnorr(msgHash, oracleSecret.getSecret())
    return oracleSig
}


/** This blockchain (Nexa contract) function verifies the signature and ticker names for a Nexa/USDT price data point.
 * The script will fail if the signature is incorrect or if this message is NOT for the Nexa/USDT pair.
 *
 * @NOTE: You likely need to subsequently verify some constraints on the date and price contained in this message!
 *
 */
fun NSL.verifyNexaUsdtMsg(oracleSig: NSig, oracleMsg:PriceDataPoint)
{
    checkDataSigVerify(oracleSig, oracleMsg, NPubKey(OraclePubKey))
    oracleMsg.tickerA equalVerify NCBytes(NexaTicker,8)
    oracleMsg.tickerB equalVerify NCBytes(UsdtInPenniesTicker, 8)
}


/** Verify the signature on an oracle [PriceDataPoint] message, at the app level.
 * @param msg The message to verify
 * @param pubkey The pubkey to use.  By default, the mainnet oracle pubkey is used.  But you might pass [TestOraclePubKey] or your own key during testing.
 * @param sig The signature
 * @return true if the message is properly signed
 */
fun VerifyMessage(msg: PriceDataPoint, sig: ByteArray, pubkey: ByteArray = OraclePubKey): Boolean
{
    val msgbytes = msg.pack()

    TODO("wrong, needs to be DSV-type verification") // return Wallet.verifyMessage(msgbytes, address.data, sig)
}

/** Verify the signature on an oracle [PriceDataPoint] message, at the app level.
 * @param msgsig The message to verify, and the oracle's signature in combined (script-push) format.
 * @param pubkey The pubkey to use.  By default, the mainnet oracle pubkey is used.  But you might pass [TestOraclePubKey] or your own key during testing.
 * @return true if the message is properly signed
 */
fun VerifyMessage(msgsig: ByteArray, pubkey: ByteArray = OraclePubKey): Boolean
{
    // val s = NexaScript(msgsig)
    TODO("DSV verify")
}




class WallyOracle
{
}