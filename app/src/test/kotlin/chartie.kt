package org.wallywallet.chartie

import java.util.logging.Logger
import org.nexa.nexarpc.NexaRpcFactory
import org.nexa.nexarpc.toHex
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.ofGroup
import org.nexa.libnexakotlin.simpleapi.payTo
import Nexa.npl.*

import java.util.*
import kotlin.time.ExperimentalTime

val REGTEST_RPC_PORT = 7328  //
val REGTEST_RPC_USER = "regtest"
val REGTEST_RPC_PASSWORD = "regtest"
val REGTEST_RPC_IP="localhost"

val REGTEST_P2P_PORT = 7327 // NexaRegtestPort
val REGTEST_P2P_IP = "localhost"

val NUM_WEEKS = 4

private val LogIt = Logger.getLogger("BU.wallet.TEST")

@kotlin.time.ExperimentalTime
fun waitFor(maxWaitMs: Long, check: () -> Boolean)
{
    val start: kotlin.time.TimeMark = kotlin.time.TimeSource.Monotonic.markNow()
    while (!check())
    {
        Thread.sleep(500)
        if (start.elapsedNow().inWholeMilliseconds > maxWaitMs) throw Exception("wait for failed")
    }
}

class Chartie
{
    val TDD_DOMAIN = "www.wallywallet.org"
    // val TDD_DOMAIN = "192.168.1.5"
    init
    {
        calcStackX(false, false)
        initRefactor()
        org.nexa.scriptmachine.Initialize()
    }

   val chartieDef = Nexa("chartie")
   {

        group("chartie")
        {
            descriptor {
                ticker = "Chartie"
                name = "Chartie"
                doc = this::class.java.classLoader.getResource("chartie.tdd.in").readText()   // Path("chartie.tdd.in").load()
                docUri = "http://$TDD_DOMAIN/chartie.tdd"
            }
            authority(GroupAuthorityFlags.ALL_AUTHORITIES)

            subgroup("chartboy")
            {
                descriptor {
                    ticker = "Chartie"
                    name = "Chart Boy"
                    doc =  this::class.java.classLoader.getResource("chartboy.tdd.in").readText() // Path("chartboy.tdd.in").load()
                    docUri = "http://$TDD_DOMAIN/chartboy.tdd"
                }
                media("chartboy.zip")
                mint(100)
                mint(1, "nexareg:nqtsq5g5ghrpz3af0yt6tlx99f8d78rsukz9wt9nsqsr7y6n")
            }

            subgroup("chartgirl")
            {
                descriptor {
                    ticker = "Chartie"
                    name = "Chart Girl"
                    doc = this::class.java.classLoader.getResource("chartgirl.tdd.in").readText() // Path("chartgirl.tdd.in").load()
                    docUri = "http://$TDD_DOMAIN/chartgirl.tdd"
                }
                media("chartgirl.zip")
                mint(100)
            }

            subgroup("subgroupData") {
                id = "01020304".fromHex()
                mint(1)
            }

        }

        group("holderArgsHashedDataCarrier", GroupFlags.of(GroupFlag.COVENANT))
        {
            // The initial data is located in the holderArgs
            mint(1UL, 1) {
                face {
                    rule("dataXform")
                    {
                        // To avoid the complexity of encoding script ints, we define our datacarrier integer as a 4 byte little-endian number
                        val counter = NBytes("counter")

                        holderArgs(counter)
                        script {
                                val gid = thisGroup()
                                val gout = groupedOutputN(gid, 0.nx)  // there can be only 1 output because the data carrier only has 1 token in the group
                                val cs = constraintScriptForOutputN(gout)
                                // The output MUST be: push(groupId) push(group_quantity) push(this_template) push(argsHash)
                                //                     33            3                     1     20             1   20 or 32
                                val (prefix: NBytes, argsHash: NBytes) = split(cs, (33 + 3 + 1 + 20 + 1).nx)
                                if (!cc.compiling) check(argsHash.curVal!!.size == 20)
                                val nextVal = counter.toInt() + 1.nx
                                val nextBytes = nextVal.toBytes(4.nx)
                                val nextArgs = "04".fromHex().nx + nextBytes
                                if (!cc.compiling) println(nextArgs.curVal?.toHex())
                                val nextArgsHash = nextArgs.hash160()
                                nextArgsHash equalVerify argsHash
                        }
                    }
                }
            }
        }

        group("mintIfOtherGroup")
        {
            authority(GroupAuthorityFlags.ALL_AUTHORITIES)
            for(i in 0 .. NUM_WEEKS)
            {
                var epochTime = Date(2023,6, 21).time/1000
                epochTime = epochTime + 7*24*60*60
                authority(GroupAuthorityFlags.MINT)
                {
                    rule {
                        checkLockTimeVerify(epochTime.nx)
                        val ng = countInputsByGroup(groupIdOf("holderArgsHashedDataCarrier"))
                        verify(ng gt 0.nx)
                    }
                }
            }
        }

        group("earlyAdopter")
        {
            mint(10)  // Test 2 separate mints
            mint(1, "nexareg:nqtsq5g5ghrpz3af0yt6tlx99f8d78rsukz9wt9nsqsr7y6n")
            mint(10)  // Anyone who notices can grab this token
            {
                face {
                    rule("anyoneCanSpend")
                    {
                        script {}
                    }
                }
            }
        }

        group("constPuzzleSolver", GroupFlags.of(GroupFlag.COVENANT))
        {
            authority(GroupAuthorityFlags.RESCRIPT)
            mint(10UL) {
                face {
                    rule("scriptPuzzle")
                    {
                        val f = NInt("a")
                        val s = NInt("b")
                        spenderArgs(f, s)
                        script {
                            var a = f
                            var b = s
                            var c = NInt()
                            for (i in range(0, 5))
                            {
                                c = a + b
                                a = b
                                b = c
                            }
                            c numEqualVerify 7227.nx
                        }
                    }
                }
            }
        }

        group("puzzleSolver", GroupFlags.of(GroupFlag.COVENANT))
        {
            authority(GroupAuthorityFlags.RESCRIPT)
            mint(10UL, 7227.nx) {
                face {
                    rule("scriptPuzzle")
                    {
                        val final = NInt("fin")
                        val f = NInt("a")
                        val s = NInt("b")
                        holderArgs(final)
                        spenderArgs(f, s)
                        script {
                            var a = f
                            var b = s
                            var c = NInt()
                            for (i in range(0,5))
                            {
                                c = a + b
                                a = b
                                b = c
                            }
                            c numEqualVerify final
                        }
                    }
                }
            }

        }
    }


    @OptIn(ExperimentalTime::class)
    // this isn't a CI capable test
    //@Test
    fun regTestDeployment()
    {
        val WallyMobileAddr = "nexareg:nqtsq5g5ymnm27av8k9jqeqlsn2ylaucz7he299vn6d40xrg"
        val rpcConnection = "http://$REGTEST_RPC_USER:$REGTEST_RPC_PASSWORD@$REGTEST_RPC_IP:$REGTEST_RPC_PORT"
        LogIt.info("Connecting to: " + rpcConnection)
        var rpc = NexaRpcFactory.create(rpcConnection, REGTEST_RPC_USER, REGTEST_RPC_PASSWORD)

        chartieDef.compile()

        //val rule = chartieDef.findRule("scriptPuzzle")
        //rule.eval(listOf(), listOf(), spenderParams = listOf(119, 829))
        //rule.smeval(listOf(), spenderParams = listOf(119, 829))

        val chartieWallet = try { openWallet("ChartieWalletRegTest") }
        catch (e : Exception)
        {
            println(e.toString())
            newWallet("ChartieWalletRegTest", ChainSelector.NEXAREGTEST)
        }

        // Since this is a test, clean up the whole wallet and start fresh
        (chartieWallet as CommonWallet).chainstate!!.chain.req.net.exclusiveNodes(setOf("$REGTEST_P2P_IP:$REGTEST_P2P_PORT"))
        //(chartieWallet as CommonWallet).cleanUnconfirmed()
        // chartieWallet.rediscover(false, true)

        val height = rpc.getblockcount()
        while (!chartieWallet.sync(2000, height))
        {
            println("sync in progress: target: $height chain: ${(chartieWallet as CommonWallet).chainstate!!.chain.curHeight} wallet: ${(chartieWallet as CommonWallet).chainstate!!.syncedHeight}")
        }

        val fundingAmt = 10000000
        while (chartieWallet.balance < fundingAmt)  // fund the wallet so we can deploy.
        {
            val fundingAddr = chartieWallet.getNewAddress()
            var txrpc = rpc.sendtoaddress(fundingAddr.toString(), NexaDecimal(fundingAmt.toLong()))  // going to send 1000000 NexaSatoshi
            rpc.generate(1)
            val height = rpc.getblockcount()
            while (!chartieWallet.sync(10000, height))
            {
                println("sync failed: target: $height chain: ${(chartieWallet as CommonWallet).chainstate!!.chain.curHeight} sync: ${(chartieWallet as CommonWallet).chainstate!!.syncedHeight}")
            }
            chartieWallet.save()
        }

        val initialTxPoolSize = rpc.gettxpoolinfo().size

        chartieDef.check()
        val txes = chartieDef.deploy(chartieWallet)
        println("Deployment requires ${txes.size} transactions:")
        for (tx in txes)
        {
            println("${tx.idem}:")
            println("  ${tx.toHex()}")
            tx.debugDump()
            println("\n")
        }
        try
        {
            waitFor(5000) { rpc.gettxpoolinfo().size == txes.size.toLong() + initialTxPoolSize }
        }
        catch(e:Exception)
        {
            // nothing to do except the if stmt below
        }
        if (rpc.gettxpoolinfo().size < txes.size)
        {
            println("some transactions did not commit!")
            val txpool = rpc.getrawtxpool()
            println("txpool:")
            for (txhex in txpool) println(txhex.hash.toHex())
            check(rpc.gettxpoolinfo().size.toInt() == txes.size)
        }
        else
        {
            rpc.generate(1)
            check(chartieWallet.sync(10000, rpc.getblockcount()))

            NexaExtend(chartieDef)
            {
                group("chartie")
                {
                    subgroup("chartiepatch")
                    {
                        descriptor {
                            ticker = "cpatch"
                            name = "Nexan Patch"
                            doc = this::class.java.classLoader.getResource("chartiepatch.tdd.in").readText()
                            docUri = "http://$TDD_DOMAIN/chartiepatch.tdd"
                        }
                        media("chartiepatch") {
                            // requires("chartboy", "chartgirl")  // TODO Indicate that this media file requires that these groups have IDs before it is built
                            substitute("info.json", "chartgirlGroupId" to groupIdOf("chartgirl").toString(), "chartboyGroupId" to groupIdOf("chartboy").toString())
                        }
                        mint(100)
                    }
                }
            }

            val newTxes = chartieDef.deploy(chartieWallet)
            println("Extension deployment requires ${newTxes.size} transactions:")
            for (tx in newTxes)
            {
                println("${tx.idem}:")
                println("  ${tx.toHex()}")
                tx.debugDump()
                println("\n")
            }
            try
            {
                waitFor(5000) { rpc.gettxpoolinfo().size == txes.size.toLong() + initialTxPoolSize }
            }
            catch(e:Exception)
            {
                // nothing to do except the if stmt below
            }
            if (rpc.gettxpoolinfo().size < txes.size)
            {
                println("some transactions did not commit!")
                val txpool = rpc.getrawtxpool()
                println("txpool:")
                for (txhex in txpool) println(txhex.hash.toHex())
                check(rpc.gettxpoolinfo().size.toInt() == txes.size)
            }


            val tx = chartieWallet.send(1000 payTo WallyMobileAddr)
            println("pay tx: ${tx.toHex()}")
            val ea = chartieDef.group("earlyAdopter")?.gid
            check(ea != null)
            waitFor(5000, {chartieWallet.getBalanceIn(ea, minConfirms = 1) == 10L})
            val bal = chartieWallet.getBalanceIn(ea, minConfirms = 1)
            check(bal == 10L)  // not 20 because the anyonecanpay tokens are not part of this wallet
            println(chartieWallet)
            val grptx = chartieWallet.send(2 ofGroup ea payTo WallyMobileAddr)
            println("grptx Idem: ${grptx.idem.toHex()} Hex: ${grptx.toHex()}")
            waitFor(10000) { rpc.gettxpoolinfo().size.toInt() == 2 }
        }
    }

}