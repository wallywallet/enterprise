package org.wallywallet.wew
import org.nexa.scriptmachine.ScriptMachine
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.*
import org.junit.Test
import org.junit.Before

import Nexa.npl.*
import org.nexa.libnexakotlin.libnexa
import java.time.Instant
import kotlin.random.Random


class FuzzScriptGenerator
{
    init
    {
        calcStackX(false, false)
        initRefactor()
        org.nexa.scriptmachine.Initialize()
    }

    @Test fun Arith()
    {
        val n1 = Nexa("testArithSimple") {
            val spenderA = NInt("spenderA")
            val holderA = NInt("holderA")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(spenderA), listOf(holderA)) {
                        script {
                            val step1 = holderA + 5.nx
                            val step2 = step1 * step1
                            val step3 = step1 + step2 + spenderA
                            val s4 = (step3 xor NCInt(0x54783782)) + (step2 or NCInt(0xa55aa55a))
                            val s5 = s4 and NCInt(0x7fffffff)
                            result(s5)
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")

        println("Script:\n" + (NexaScript(OP.C3, OP.C2) + rule.script.script()).toHex())

        var result = rule.eval(listOf(1), listOf(2))
        println(result)
    }

    @Test fun Hash()
    {
        val n1 = Nexa("testArithSimple") {
            val sA = NBytes("spenderA")
            val hA = NBytes("holderA")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(sA), listOf(hA)) {
                        script {
                            val sha256 = sA.sha256()
                            // val sha1 = sA.sha1()
                            val dsha256 = sA.hash256()
                            val dsha160 = sA.hash160()
                            val cat = sha256 + dsha256 + dsha160
                            val fin = cat.hash256()
                            fin equalVerify hA
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")

        println("Script:\n" + (NexaScript(OP.C3, OP.NUM2BIN, OP.push(byteArrayOf(1,2,3,4,5,6))) + rule.script.script()).toHex())
    }

    @Test fun If()
    {
        val n1 = Nexa("testArithSimple") {
            val sA = NBytes("spenderA")
            val hA = NBytes("holderA")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(sA), listOf(hA)) {
                        script {
                            val a = sA xor hA
                            if_(a, {
                                verify(a)
                            }, {
                                verify(hA)
                            })
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")

        println("Script:\n" + (NexaScript(OP.push(byteArrayOf(0,2,1,3,4,5)), OP.push(byteArrayOf(1,2,3,4,5,6))) + rule.script.script()).toHex())
    }

    @Test fun catslice()
    {
        val n1 = Nexa("testArithSimple") {
            val sA = NBytes("spenderA")
            val hA = NBytes("holderA")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(sA), listOf(hA)) {
                        script {
                            val catted = sA + sA + sA + sA + hA + hA + hA + hA
                            val cat4 = catted + catted + catted
                            val sl1 = cat4.split(NCInt(20))
                            val sl2 = cat4.split(NCInt(50))
                            val recomb = sl1.first + sl2.first + sl1.second + sl2.second
                            val ha = recomb.hash256()
                            verify(ha)
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")

        println("Script:\n" + (NexaScript(OP.push(byteArrayOf(0,2,1,3,4,5)), OP.push(byteArrayOf(1,2,3,4,5,6))) + rule.script.script()).toHex())
    }
}