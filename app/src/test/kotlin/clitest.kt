@file:OptIn(ExperimentalTime::class, ExperimentalTime::class)

package org.wallywallet.wew

import com.ionspin.kotlin.bignum.decimal.BigDecimal
import org.nexa.libnexakotlin.*
import org.nexa.nexarpc.NexaRpc
import org.nexa.nexarpc.NexaRpcFactory
import java.io.BufferedReader
import java.io.File
import java.io.OutputStream
import java.util.concurrent.TimeoutException
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.time.ExperimentalTime

private val LogIt = GetLog("wew.clitest")

fun checkelse(b:Boolean, onFail: (()->String?)? = null)
{
    if  (!b)
    {
        val v = onFail?.invoke()
        if (v != null)
        {
            println("CHECK FAILED: $v")
            LogIt.severe(v)
        }
        throw IllegalStateException(v)
    }
}

class CliTest
{
    lateinit var th: org.nexa.threads.iThread
    lateinit var fifoSend: File
    lateinit var fifoRecv: File

    var req: OutputStream
    var reply: BufferedReader

    init  // Before all tests
    {
        th = org.nexa.threads.Thread { WallyEnterpriseWallet.run(WallyEnterpriseWallet.CliType.Fifo) }

        fifoSend = File("in",)
        fifoRecv = File("out")

        if (!fifoSend.exists() || fifoRecv.isFile)
        {
            throw Exception("Fifo ${fifoSend.absolutePath} was never created (or is a file)")
        }
        if (!fifoRecv.exists() || fifoRecv.isFile)
        {
            throw Exception("Fifo ${fifoRecv.absolutePath} was never created (or is a file)")
        }

        req = fifoSend.outputStream()
        reply = fifoRecv.bufferedReader()
    }

    fun getNexaRpc(): NexaRpc
    {
        LogIt.info("This test requires a Nexa full node running on regtest at ${REGTEST_IP} and port $NexaRegtestRpcPort")
        // Set up RPC connection
        val rpcConnection = "http://$RPC_USER:$RPC_PASSWORD@$REGTEST_IP:" + NexaRegtestRpcPort
        val nexaRpc = NexaRpcFactory.create(rpcConnection)
        val tipIdx = nexaRpc.getblockcount()
        if (tipIdx < 102)
            nexaRpc.generate((101 - tipIdx).toInt())
        else
        {
            val tip = nexaRpc.getblock(tipIdx)
            // The tip is so old that this node won't think its synced so we need to produce a block
            if (epochSeconds() - tip.time > 1000) nexaRpc.generate(1)
        }
        return nexaRpc
    }

    fun issue(cmd: String, timeout: Long = 10000): String
    {
        req.write((cmd + "\n").toByteArray())
        req.flush()
        val sb = StringBuilder()
        val result = reply.lines()
        val end = millinow() + timeout
        while(millinow() < end)
        {
            for (r in result)
            {
                if (r == "|>>") return sb.toString()
                else sb.append(r)
            }
        }
        throw TimeoutException()
    }

    @BeforeTest
    fun before()
    {
        // initializeLibNexa()
    }


    @Test
    fun testhelp()
    {
        val h = issue("help()")
        checkelse("Welcome" in h, {h})
    }

    @Test
    fun testgarbage()
    {
        // Test a bad line
        var result = issue("dkeia;d93ds.z39se3")
        val priorcmd = WallyEnterpriseWallet.cmdCount()-1
        check("Exception" in result)

        // See if the prior result is cached (as an exception)
        result = issue("""result""")
        check("ScriptException" in result)

        // See if the prior exception is cached (as an exception)
        result = issue("""lastException""")
        check("ScriptException" in result)

        // Make sure the CLI works after an exception
        val h = issue("help()")
        check("Welcome" in h)

        // See if the exception is in resultOf
        result = issue("resultOf[$priorcmd]")
        check("ScriptException" in result)
    }


    @Test
    fun testprint()
    {
        var result = issue("""print("this is a test")""")
        check("this is a test" in result)

        result = issue("""p("this is a test2")""")
        check("this is a test2" in result)

        result = issue("""pr("this is a test3")""")
        check("this is a test3" in result)
    }

    @Test
    fun testconst()
    {
        var result = issue("""NEXA""")
        check("NEXA" in result)

        result = issue("""RNEX""")
        print(result)
        check("NEXAREGTEST" in result)

        result = issue("""TNEX""")
        check("NEXATESTNET" in result)
    }

    @Test
    fun testbasicwallet()
    {
        val f = File("clitest.db")
        if (f.exists()) f.delete()
        var result = issue("""val w = newWallet("clitest", RNEX)""")
        println(result)

        result = issue("""w""")
        println("RESULT IS $result")
        check("clitest" in result)

        result = issue("""w.getnewaddress()""")
        check("nexareg:" in result)

        // See if the prior result is cached
        result = issue("""result""")
        check("nexareg:" in result)
    }

    @Test
    fun testglobals()
    {
        var result = issue("""val w = openOrNewWallet("clitest", RNEX)""")
        println(result)

        result = issue("""blockchains.keys""")
        check("NEXAREGTEST" in result)

        // See if blockhains[RNEX] is properly typed
        result = issue("blockchains[RNEX]!!.name")
        check("RNEX" in result)

        result = issue("""wallets""")
        check("clitest" in result)
        result = issue("""wallets["clitest"]!!.name""")
        check("clitest" in result)

        result = issue("""cnxns""")
        check("NEXAREGTEST" in result)
        result = issue("""cnxns[RNEX]!!.name""")
        check("nexareg" in result)
    }

    @Test
    fun testgui()
    {
        issue("""button(image("test.png")) { println("kaching!") }""")

        issue("""image("test.png",50)""")
        issue("""image("test.svg",100)""")

        issue("""box(row ("foo", button(image("test.png",50)) { println("kaching!") }, NL, box(text(" bar "))))""")

        issue("""box(row ("foo", button(image("test.png",50)) { println("kaching!") }, box(text(" bar "))))""")

        issue("""expander(text("boo"), box(row ("foo", button(image("test.png",50)) { println("kaching!") }, box(text(" bar ")))))""")

    }



    @Test
    fun testmultiwallet()
    {
        //deleteWallet("clitest1", ChainSelector.NEXAREGTEST)
        //deleteWallet("clitest2", ChainSelector.NEXAREGTEST)
        var f = File("clitest1.db")
        if (f.exists()) f.delete()
        f = File("clitest2.db")
        if (f.exists()) f.delete()

        val rpc = getNexaRpc()

        var result = issue("""val w1 = newWallet("clitest1", RNEX)""")
        result = issue("""w1""")
        checkelse("clitest1" in result, {"expected clitest1 in $result"})

        result = issue("""val w2 = newWallet("clitest2", RNEX)""")
        result = issue("""w2""")
        checkelse("clitest2" in result, {"expected clitest2 in $result"})

        waitFor(10000, { issue("w1.blockchain.net.p2pCnxns.size > 0") == "true" })
        waitFor(10000, { issue("w2.blockchain.net.p2pCnxns.size > 0") == "true" })

        waitFor(10000, { issue("w1.synced() && w2.synced()") == "true" })

        val w1Addr = issue("w1.getnewaddress()")
        checkelse("nexareg:" in w1Addr, {"expected nexareg: in $w1Addr"})
        val w2Addr = issue("w2.getnewaddress()")
        checkelse("nexareg:" in w2Addr, {"expected nexareg: in $w2Addr"})

        val tx1 = rpc.sendtoaddress(w1Addr, BigDecimal.fromInt(1000))
        val tx2 = rpc.sendtoaddress(w2Addr, BigDecimal.fromInt(2000))

        println(tx1)
        println(tx2)

        // remember balanceUnconfirmed is in satoshi
        waitFor(10000, { val tmp = issue("w1.balanceUnconfirmed"); tmp.split(" ")[0].toInt() == 100000 })
        waitFor(10000, { issue("w2.balanceUnconfirmed").split(" ")[0].toInt() == 200000 })

        rpc.generate(1)

        waitFor(10000, { val tmp = issue("w1.balance"); tmp.split(" ")[0].toInt() == 100000 })
        waitFor(10000, { issue("w2.balance").split(" ")[0].toInt() == 200000 })

        issue("""w1.send(50000, "$w2Addr")""")
        issue("""w2.send(5000, "$w1Addr")""")

        waitFor(10000, { val tmp = issue("w1.balanceUnconfirmed")
            println(tmp)
            tmp.split(" ")[0].toInt() > 0 })
        waitFor(10000, { issue("w2.balanceUnconfirmed").split(" ")[0].toInt() > 0 })

        rpc.generate(1)
    }
}
