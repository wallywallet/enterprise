@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.wew
import org.nexa.scriptmachine.ScriptMachine
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.*
import org.junit.Test
import org.junit.Before

import Nexa.npl.*
import okio.FileNotFoundException
import org.nexa.libnexakotlin.libnexa
import java.time.Instant
import kotlin.random.Random
import java.lang.management.ManagementFactory

fun bao(vararg elements: Byte) = byteArrayOf(*elements)


/** Convert this string to a ByteArray of size [zeroPadTo], padding it out with 0s */
fun String.toPaddedByteArray(zeroPadTo: Int): ByteArray = this.toByteArray().copyOf(zeroPadTo)


fun GroupedTxOutput(groupedQty: Long, gid: GroupId, address: PayAddress, nexaSatQty:Long? = null):NexaTxOutput
{
    val cs = address.blockchain
    val ret = NexaTxOutput(cs)
    if (nexaSatQty != null) ret.amount = nexaSatQty else ret.amount = dust(cs)
    ret.script = address.groupedLockingScript(gid, groupedQty)
    return ret
}

fun expectVerifyFail(fn:()->Unit)
{
    try
    {
        fn()
        check(false)  // Verify should have failed
    }
    catch(e:NSL.VerifyException)
    {
        check(true)
    }
}

/** Pass objects to be converted into Nexa Arguments format.
 * This produces a Nexa VM script that PUSHes each argument onto the stack.
 * Nested arrays are flattened. That is, if an array or list is passed, each element in the list is pushed separately.
 * Null items are skipped.  Use the number 0 or false to push a stack element of length 0 (remember, 0, FALSE, and a zero length stack item are the same).
 */
fun NexaArgs(vararg args: Any?, chainSelector: ChainSelector = defaultChain): SatoshiScript
{
    val ret = SatoshiScript(chainSelector)
    for (a in args)
    {
        if (a != null)
        {
            when (a)
            {
                // If an array of stuff was passed, push each item in the array as a separate stack item by calling this function recursively
                is ArrayList<*> ->
                {
                    if (a.size != 0)
                        ret.add(NexaArgs(*(a.toArray()), chainSelector = chainSelector).flatten())
                }

                is List<*>      ->
                {
                    if (a.size != 0)
                    {
                        ret.add(NexaArgs(*(a.toTypedArray()), chainSelector = chainSelector).flatten())
                    }
                }

                is Boolean      -> if (a) ret.add(OP.PUSHTRUE) else ret.add(OP.PUSHFALSE)
                is ByteArray    -> ret.add(OP.push(a))
                is Int          -> ret.add(OP.push(a))
                is UInt         -> ret.add(OP.push(a.toLong()))
                is Long         -> ret.add(OP.push(a))
                is ULong        -> ret.add(OP.push(a.toLong()))

                else -> throw IllegalArgumentException("Cannot place arguments of type ${a::class.simpleName} onto the Nexa VM stack: $a")
            }
        }
    }
    return ret
}

fun NexaScript.satisfier(holderArgs: SatoshiScript, spenderArgs: SatoshiScript): SatoshiScript
{
    val satisfier = if (holderArgs.size == 0)
    {
        SatoshiScript(
            chainSelector, SatoshiScript.Type.SATOSCRIPT,
            OP.push(flatten()),
            OP.PUSHFALSE
        )
    }
    else
    {
        SatoshiScript(
            chainSelector, SatoshiScript.Type.SATOSCRIPT,
            OP.push(flatten()),
            OP.push(holderArgs.toByteArray())
        )
    }
    satisfier.add(spenderArgs.toByteArray())

    return satisfier
}

fun setupOI(template: NexaScript, holderArgs: List<Any>, spenderArgs: List<Any>, amount: Long): Pair<NexaTxOutput, NexaTxInput>
{
    val cs:ChainSelector = template.chainSelector
    val holderScript : SatoshiScript = NexaArgs(holderArgs)
    val utxo = template.output(amount, holderScript)

    val fakesp = Spendable(cs)
    fakesp.outpoint = NexaTxOutpoint(Hash256())  // the utxo won't be stored anywhere so the prevout hash is fake
    val inp = NexaTxInput(fakesp, amount)
    val spenderScript : SatoshiScript = NexaArgs(spenderArgs)
    inp.script = template.satisfier(holderScript, spenderScript)
    return Pair(utxo, inp)
}

// Given a template script and args, this function creates a transaction with a single input that spends the template script, and includes a fake UTXO
fun setupUtxoTx(template: NexaScript, holderArgs: List<Any>, spenderArgs: List<Any>): Pair<NexaTxOutput, NexaTransaction>
{
    val cs:ChainSelector = template.chainSelector
    val holderScript : SatoshiScript = NexaArgs(holderArgs)
    val utxo = template.output(1000, holderScript)

    val tx = NexaTransaction(cs)
    val fakesp = Spendable(cs)
    fakesp.amount = 1000
    fakesp.outpoint = NexaTxOutpoint(Hash256())  // the utxo won't be stored anywhere so the prevout hash is fake
    val inp = NexaTxInput(fakesp)
    val spenderScript : SatoshiScript = NexaArgs(spenderArgs)
    inp.script = template.satisfier(holderScript, spenderScript)
    tx.add(inp)
    return Pair(utxo, tx)
}

class TxBuilder(val cs:ChainSelector, val tx: iTransaction= txFor(cs))
{
    val sigBuilders=mutableListOf<iTxInput.(tx: iTransaction, idx:Int) -> SatoshiScript>()

    fun sign()
    {
        for (s in sigBuilders.withIndex())
        {
            if (tx.inputs[s.index].script.size == 0)
                tx.inputs[s.index].script = s.value.invoke(tx.inputs[s.index], tx, s.index)
        }
    }

    fun input(sp: Spendable, sigbuilder: iTxInput.(tx: iTransaction, idx:Int) -> SatoshiScript): iTxInput
    {
        val ret = txInputFor(sp)
        tx.add(ret)
        sigBuilders.add(sigbuilder)
        return ret
    }

    fun input(builder: iTxInput.() -> Unit): iTxInput
    {
        val ret = txInputFor(cs)
        ret.builder()
        tx.add(ret)
        return ret
    }

    // Create a data output
    fun data(ba: ByteArray): iTxOutput
    {
        val ret = txOutputFor(cs)
        ret.amount = 0
        ret.script = SatoshiScript(cs, SatoshiScript.Type.SATOSCRIPT, OP.RETURN, OP.push(ba))
        tx.add(ret)
        return ret
    }

    // Pay out to an arbitrary script, using the script template style
    fun pay(nexaAmt: Long, script: SatoshiScript, args: List<ByteArray>, visArgs: List<ByteArray>?=null, group: GroupId? = null, groupAmt: Long = 0): iTxOutput
    {
        val va = if (visArgs != null) NexaArgs(visArgs, chainSelector = script.chainSelector) else null
        return pay(nexaAmt, script, NexaArgs(args, chainSelector = script.chainSelector), va, group, groupAmt)
    }
    // Pay out to an arbitrary script, using the script template style
    fun pay(nexaAmt: Long, script: SatoshiScript, args: SatoshiScript, visArgs: SatoshiScript?=null, group: GroupId? = null, groupAmt: Long = 0): iTxOutput
    {
        val tmp = P2T(script, args, visArgs)
        val lockingScript = if (group != null)
        {
            tmp.groupedLockingScript(group, groupAmt)
        }
        else
        {
            tmp.lockingScript()
        }
        val ret = txOutputFor(cs)
        ret.amount = nexaAmt
        ret.script = lockingScript
        tx.add(ret)
        return ret
    }
    // Pay out either just nexa, or to a group and nexa
    fun pay(nexaAmt: Long, dest: PayDestination, group: GroupId? = null, groupAmt: Long = 0): iTxOutput
    {
        val ret = txOutputFor(cs)
        ret.amount = nexaAmt
        ret.script = if (group != null) dest.groupedLockingScript(group, groupAmt) else dest.lockingScript()
        tx.add(ret)
        return ret
    }

    // Pay out a group, with the minimum nexa
    fun pay(dest: PayDestination, group: GroupId, groupAmt: Long): iTxOutput
    {
        val ret = txOutputFor(cs)
        ret.amount = dust(cs)
        ret.script = if (group != null) dest.groupedLockingScript(group, groupAmt) else dest.lockingScript()
        tx.add(ret)
        return ret
    }
}

class P2T(tmpl: SatoshiScript, args: SatoshiScript, visArgs: SatoshiScript?=null): Pay2TemplateDestination(tmpl.chainSelector)
{
    init {
        template = tmpl
        constraint = args
        visibleConstraint = visArgs ?: SatoshiScript(tmpl.chainSelector)
    }
}

// USE NexaArgs(args, chainselector)
fun scriptArgs(cs:ChainSelector, args: List<ByteArray>):SatoshiScript
{
    val ret = SatoshiScript(cs).add(args)
    ret.type = SatoshiScript.Type.PUSH_ONLY
    return ret
}

fun txBuilder(cs:ChainSelector, builder: TxBuilder.()->Unit):Pair<List<NexaTxOutput>, NexaTransaction>
{
    val txb = TxBuilder(cs)
    txb.builder()
    txb.sign()  // final signing
    val utxo = MutableList<NexaTxOutput>(txb.tx.inputs.size, { txb.tx.inputs[it].spendable.prevout as NexaTxOutput})
    return Pair(utxo, txb.tx as NexaTransaction)
}

// Given a template script and args, this function creates a transaction with a single input that spends the template script, and includes a fake UTXO
fun setupUtxoTx(fakesp: Spendable, template: NexaScript, holderArgs: List<Any>, spenderArgs: (iTransaction)->List<Any?>): Pair<NexaTxOutput, NexaTransaction>
{
    val cs:ChainSelector = template.chainSelector
    val holderScript : SatoshiScript = NexaArgs(holderArgs)
    val utxo = template.output(1000, holderScript)

    val tx = NexaTransaction(cs)
    val inp = NexaTxInput(fakesp)
    tx.add(inp)
    val spenderScript : SatoshiScript = NexaArgs(spenderArgs(tx))
    inp.script = template.satisfier(holderScript, spenderScript)
    return Pair(utxo, tx)
}

fun setupGroupedUtxoTx(gid: GroupId, holderDest: PayDestination, template: NexaScript, holderArgs: List<Any>, spenderArgsCreator: (iTransaction)->List<Any>, outputCreator: (iTransaction) -> Unit ): Pair<NexaTxOutput, NexaTransaction>
{
    val cs:ChainSelector = template.chainSelector
    val holderArgsScript : SatoshiScript = NexaArgs(holderArgs)
    val gi = GroupInfo(gid, 123)
    val utxo = template.output(1000, holderArgsScript, gi)

    val tx = NexaTransaction(cs)
    outputCreator(tx)
    val fakesp = Spendable(cs)
    fakesp.backingPayDestination = holderDest
    fakesp.amount = 1000
    fakesp.outpoint = NexaTxOutpoint(Hash256())  // the utxo won't be stored anywhere so the prevout hash is fake
    val inp = NexaTxInput(fakesp)
    tx.add(inp)
    val spenderArgs: List<Any> = spenderArgsCreator(tx)
    val spenderScript : SatoshiScript = NexaArgs(spenderArgs)
    inp.script = template.satisfier(holderArgsScript, spenderScript)
    return Pair(utxo, tx)
}


/** Load the script machine stacks with the passed values.  Does not remove any existing items.
 * This occurs by creating two scripts and executing them.
 * */
/*
fun ScriptMachine.loadStacks(stack: List<Any>, alt: List<Any>)
{
    swapStacks()  // In case there's stuff on the stacks already
    if (true)
    {
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
        for (i in alt)
        {
            when (i)
            {
                is Int       -> bin.add(OP.push(i))
                is Long      -> bin.add(OP.push(i))
                is ByteArray -> bin.add(OP.push(i))
            }
        }
        val result = this.eval(bin)
        org.nexa.nsl.mycheck(result == true)
    }
    swapStacks()
    if (true)
    {
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
        for (i in stack)
        {
            when (i)
            {
                is Int       -> bin.add(OP.push(i))
                is Long      -> bin.add(OP.push(i))
                is ByteArray -> bin.add(OP.push(i))
            }
        }
        val result = this.eval(bin)
        org.nexa.nsl.mycheck(result == true)
    }
}
*/

class NslTest
{
    init
    {
        //loadCalcStackX()
        val maxMemory = Runtime.getRuntime().maxMemory() / (1024 * 1024)  // Convert to MB
        println("Max heap size: ${maxMemory}MB")
        val jvmArgs = ManagementFactory.getRuntimeMXBean().inputArguments
        println("JVM arguments:")
        jvmArgs.forEach { println(it) }
        try
        {

            stackX.load()  // failed? run GenerateXlat.main() once
        }
        catch(e:Exception)
        {
            calcStackX()
        }
        initRefactor()
        org.nexa.scriptmachine.Initialize()
    }

    @Before
    fun init()
    {
    }

    @Test fun functional()
    {
        val a = SD(0,1)
        val b = SD(0,1)
        val c = SD(0,1,2)
        // Verify that the hashcode is based on content not pointers
        check(a.hashCode() == b.hashCode())
        check(a.hashCode() != c.hashCode())
        val d = SD(0,1)
        val e = SD(0,1)
        check (d == e)
        check(StateTransition(a,b).hashCode() == StateTransition(d,e).hashCode())
        check(StateTransition(a,b).hashCode() != StateTransition(d,c).hashCode())

        check(StateTransition(a,b) == StateTransition(d,e))

        val altA = SD(byteArrayOf(0,1), byteArrayOf(2,3))
        val altA1 = SD(byteArrayOf(0,1), byteArrayOf(2,3))

        val altB = SD(byteArrayOf(3,2), byteArrayOf(1,0))
        val altB1 = SD(byteArrayOf(3,2), byteArrayOf(1,0))

        check(StateTransition(altA,altB) == StateTransition(altA1,altB1))
        check(StateTransition(altA,altA1) != StateTransition(altA1,altB1))

        val test = StateTransitions()
        test.add(SD(byteArrayOf(0), byteArrayOf(1)), SD(1,0), OP.FROMALTSTACK, OP.SWAP)

        val st = StateTransition(SD(byteArrayOf(0), byteArrayOf(1)), SD(1,0))
        val result = test.table.get(st)
        check(result != null)

        val simpleSt = StateTransition(SD(0), SD())
        var sdx = SD(0,1,2,3,4)
        sdx.apply(simpleSt)
        check(sdx == SD(0,1,2,3))

        val simpleSt2 = StateTransition(SD(0,1), SD(0))
        sdx = SD(0,1,2,3,4)
        sdx.apply(simpleSt2)
        check(sdx == SD(0,1,2,3))

        val simpleSt3 = StateTransition(SD(0,1), SD(1,0))
        sdx = SD(0,1,2,3)
        sdx.apply(simpleSt3)
        check(sdx == SD(0,1,3,2))

        // Example that pops something off of the altstack
        val simpleSt4 = StateTransition(SD(byteArrayOf(0), byteArrayOf(1)), SD(0,1))
        sdx = SD(0,1,2,3)
        check(!sdx.apply(simpleSt4))
        sdx = SD(byteArrayOf(0,1), byteArrayOf(2,3))
        sdx.apply(simpleSt4)
        check(sdx == SD(byteArrayOf(0,1,3), byteArrayOf(2)))

        val simpleSt5 = StateTransition(SD(byteArrayOf(), byteArrayOf(0,1)), SD(byteArrayOf(), byteArrayOf(1,0)))
        sdx = SD(byteArrayOf(0,1), byteArrayOf(2,3))
        sdx.apply(simpleSt5)
        check(sdx == SD(byteArrayOf(0,1), byteArrayOf(3,2)))

        val simpleSt6 = StateTransition(SD(byteArrayOf(0,1,2,3), byteArrayOf(4)), SD(byteArrayOf(1,3,2,0,2,2), byteArrayOf(4)))
        val simpleSt7 = StateTransition(SD(byteArrayOf(0,1,2,3), byteArrayOf(4)), SD(byteArrayOf(1,3,2,0,2,2), byteArrayOf(4)))

        check(simpleSt6.hashCode() == simpleSt7.hashCode())
        check(simpleSt6 == simpleSt7)

    }

    @Test fun testSearchStateTransitions()
    {
        val baseSt = StateTransitions()
        initBasicOps(baseSt)
        val nst2 = searchStateTransitions(4, SD(0,1), baseSt)
        //nst2.clean(4)
        println(nst2.table.size)
        val nst0a2 = searchStateTransitions(4, SD(bao(), bao(0,1)), baseSt)
        //nst0a2.clean(4)
        println(nst0a2.table.size)

        val nst3 = searchStateTransitions(3, SD(0,1,2), baseSt)
        //nst3.clean(6)

        val nst0a3 = searchStateTransitions(3, SD(bao(), bao(0,1,2)), baseSt)
        //nst0a3.clean(6)
        println(nst0a3)

    }

    @Test fun testSize()
    {
        var exitStep: NInt? = null
        val n1 = Nexa("testSize") {
            val spenderSolnA = NBytes("spenderA")
            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(spenderSolnA), listOf()) {
                        script {
                            val step1 = spenderSolnA.size()
                            verify(step1 eq NCInt(4))
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")
        var result = rule.eval(listOf("00010203".fromHex()), listOf())
        println(result)

        println("Script hex: ${rule.script.toHex()}")
        println("Script ASM: ${rule.script}")

        rule.smeval(listOf("00010203".fromHex()), listOf())
        check(rule.script.toHex() == "6c82549d75")

        // d3 because you can't load the stack machine with
        expectVerifyFail { rule.eval(listOf("00".fromHex()), listOf()) }
        expectVerifyFail { rule.smeval(listOf("00".fromHex()), listOf()) }

        expectVerifyFail { rule.eval(listOf("0102030405".fromHex()), listOf()) }
        expectVerifyFail { rule.smeval(listOf("0102030405".fromHex()), listOf()) }
    }

    @Test fun testVectors()
    {
        var exitStep: NInt? = null
        val n1 = Nexa("testVectors") {
            val correct = NBytes("correct")
            val input = NBytes("input")
            val correctInt = NInt("correct")
            val inputInt = NInt("input")

            group("g1") {
                face {
                    rule("reverse", null, listOf(correct), listOf(input))
                    {
                        script {
                            verify(input.reverse() eq correct)
                        }
                    }
                    rule("inc", null, listOf(correctInt), listOf(inputInt))
                    {
                        script {
                            verify(inputInt.inc() eq correctInt)
                        }
                    }
                    rule("dec", null, listOf(correctInt), listOf(inputInt))
                    {
                        script {
                            verify(inputInt.dec() eq correctInt)
                        }
                    }
                }
            }
        }
        n1.compile()
        val test = "0102".fromHex()

        data class TestVec(val name: String, val correct: List<Any>, val test: List<Any>)

        val testVecs = listOf(
            TestVec("reverse", listOf(test), listOf(test.reversedArray())),
            TestVec("inc", listOf(6), listOf(5)),
            TestVec("dec", listOf(3), listOf(4))
        )

        for (t in testVecs)
        {
            val rule = n1.findRule(t.name)
            println("test ${t.name}")
            var result = rule.eval(t.correct, t.test)
            check(result.stack.size == 0)
            rule.smeval(t.correct, t.test)
        }
    }

    @Test fun testHash()
    {
        var exitStep: NInt? = null
        val n1 = Nexa("testHash") {
            val correct = NBytes("correct")
            val correctSha256 = NBytes("sha256")
            val correctHash256 = NBytes("hash256")
            val correctRipemd160 = NBytes("ripemd160")
            val toHash = NBytes("preimage")

            group("g1") {
                face {
                    /*
                    rule("r", null,
                        listOf(correctSha256, correctHash256, correctRipemd160),
                        listOf(toHash)) {
                        script {
                            verify(toHash.sha256() eq correctSha256)
                            verify(toHash.hash256() eq correctHash256)
                            verify(toHash.ripemd160() eq correctRipemd160)
                        }
                    }

                     */

                    rule(
                        "r0", null,
                        listOf(correct),
                        listOf(toHash)
                    )
                    {
                        script {
                            verify(toHash.sha256() eq correct)
                        }
                    }
                    rule(
                        "r1", null,
                        listOf(correct),
                        listOf(toHash)
                    )
                    {
                        script {
                            verify(toHash.hash256() eq correct)
                        }
                    }
                    rule(
                        "r2", null,
                        listOf(correct),
                        listOf(toHash)
                    )
                    {
                        script {
                            verify(toHash.hash160() eq correct)
                        }
                    }
                    rule(
                        "r3", null,
                        listOf(correct),
                        listOf(toHash)
                    )
                    {
                        script {
                            verify(toHash.ripemd160() eq correct)
                        }
                    }
                    rule(
                        "r4", null,
                        listOf(correct),
                        listOf(toHash)
                    )
                    {
                        script {
                            verify(toHash.sha1() eq correct)
                        }
                    }
                }
            }
        }
        n1.compile()
        val test = "0102".fromHex()

        //val rule = n1.findRule("r")
        //var result = rule.eval(listOf(Hash.sha256(test), Hash.hash256(test), Hash.hash160(test)), listOf(test))
        //println(result)
        //var result1 = rule.smeval(listOf(Hash.sha256(test), Hash.hash256(test), Hash.hash160(test)), listOf(test))
        //println(result)

        if (true)
        {
            val rule = n1.findRule("r0")
            var result = rule.eval(listOf(libnexa.sha256(test)), listOf(test))
            check(result.stack.size == 0)
            rule.smeval(listOf(libnexa.sha256(test)), listOf(test))
        }
        if (true)
        {
            val rule = n1.findRule("r1")
            var result = rule.eval(listOf(libnexa.hash256(test)), listOf(test))
            check(result.stack.size == 0)
            rule.smeval(listOf(libnexa.hash256(test)), listOf(test))

        }
        if (true)
        {
            val rule = n1.findRule("r2")
            var result = rule.eval(listOf(libnexa.hash160(test)), listOf(test))
            check(result.stack.size == 0)
            rule.smeval(listOf(libnexa.hash160(test)), listOf(test))
        }
        // TODO ripemd160 and sha1 when the Hash primitives are added to libnexa.so
    }


    @Test fun testArithSimple()
    {
        var exitStep: NInt? = null
        val n1 = Nexa("testArithSimple") {
            val spenderSolnA = NInt("spenderA")
            val spenderSolnB = NInt("spenderB")
            val holderA = NInt("holderA")
            val holderB = NInt("holderB")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("r", null, listOf(spenderSolnA), listOf(holderA)) {
                        script {
                            val step1 = holderA + 5.nx
                            val step2 = step1 - spenderSolnA
                            val step3 = step1 + holderA
                            result(step3)
                        }
                    }
                }
            }
        }
        n1.compile()
        val rule = n1.findRule("r")
        var result = rule.eval(listOf(1), listOf(2))
        println(result)
    }

    /** run tests focused on math (ScriptNum, not bigint) operations */
    @Test fun testArith()
    {
        var exitStep:NInt? = null
        val n1 = Nexa("testArith") {
            val spenderSolnA = NInt("spenderA")
            val spenderSolnB = NInt("spenderB")
            val holderA = NInt("holderA")
            val holderB = NInt("holderB")

            group("g1") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("addSub", null, listOf(spenderSolnA), listOf(holderA, holderB)) {
                        script {
                            val step1 = holderA + holderB
                            val step2 = step1 - spenderSolnA
                            result(step2)
                        }
                    }
                    rule("addSubMulDiv", null, listOf(spenderSolnA), listOf(holderA, holderB)) {
                        script {
                            val step1 = (holderA + holderB) - spenderSolnA
                            val step2 = step1 * holderA * 10
                            val step3 = step2 / holderB
                            result(step3)
                        }
                    }

                    rule("addSubMulDiv", null, listOf(spenderSolnA), listOf(holderA, holderB)) {
                        script {
                            val step1 = (holderA + holderB) - spenderSolnA
                            val step2 = step1 * holderA * 10
                            val step3 = step2 / holderB
                            result(step3)
                        }
                    }

                    /* shift opcodes are disabled
                    rule("bitShift", listOf(spenderSolnA), listOf(holderA)) {
                        script {
                            val step1 = (spenderSolnA shl 4) or holderA
                            result(step1 shr 2)
                        }
                    }
                     */

                    rule("minMax", null, listOf(spenderSolnA, spenderSolnB), listOf(holderA, holderB)) {
                        script {
                            result(min(spenderSolnA,spenderSolnB) + max(holderA, holderB))
                        }
                    }

                    rule("spendMath", null, listOf(spenderSolnA), listOf(holderA, holderB)) {
                        script {
                            val step1 = spenderSolnA xor holderA
                            val step2 = (holderB or spenderSolnA) and step1
                            val step3 = step1 + step2
                            val step4 = step1/10
                            exitStep = (step4 + step3.inc()) * step2.dec() / holderB
                            result(exitStep!!)

                        }
                    }
                }
            }
        }

        n1.compile()

        // evaluate the passed rule as a dataflow, and then evaluate the compiled rule in the real script machine, and check that the values match
        // (and that they match the passed expected value).
        fun testEval(ruleName: String, env: NPL, holderArgs: List<Int>, solveArgs: List<Int>, correct: Int)
        {
            for (g in env.groups)
            {
                for (i in g.contract.interfaces)
                {
                    for (r in i.rules)
                    {
                        if (r.key == ruleName)
                        {
                            val rule = r.value
                            println("$ruleName is:\n${rule}")
                            var result = rule.eval(holderArgs, solveArgs)
                            println("($holderArgs <| $solveArgs) -> ${(result.stack[0] as NInt).curVal}")
                            check((result.stack[0] as NInt).curVal == correct.toLong())

                            val sm = ScriptMachine()
                            sm.loadStacks(solveArgs, holderArgs)
                            val ok = sm.eval(rule.script.script())
                            mycheck(ok)
                            val top = sm.mainStackAt(0)
                            println("VM eval result: " + top)
                            mycheck(correct == top.split(" ").last().toInt())
                            sm.delete()
                        }
                    }
                }
            }
        }

        testEval("addSub", n1, listOf(1), listOf(2,3), 4)
        testEval("addSubMulDiv", n1, listOf(1), listOf(2,3), 26)
        testEval("minMax", n1, listOf(1,2), listOf(3,4), 5)
        // bitshift is disabled
        // testEval("bitShift", n1, listOf(0xA), listOf(0x5), 0xA shl 4 or 0x5 shr 2)
        testEval("spendMath", n1, listOf(1), listOf(2,3), 4)
    }

    @Test fun testAtomicSecretCommunication()
    {
        val libnexaVersion = libnexa.version()
        println("libnexa version is: $libnexaVersion")
        val nftSecret = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 1})),0)
        val n1 = Nexa("testASC")
        {
            val holder = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holder.pubKey.weight = -1
            val secretSig = NSig("secretSig")
            val secretCommitment = NCAddress(nftSecret,"secretCommitment")
            // We'll use OP_RETURN at output 0 for the agreed public nonce
            val priorAgreedNonce = NBytes("publicNonce")

            group("A") {
                face {
                    // build the
                    rule("AscConstraint")
                    {
                        templateArgs(secretCommitment.pubKey)
                        // The NFT recipient will construct a transaction with an output that pays the sender and specifies an agreed upon nonce
                        // that the sender must use in the signature of the challenge.  So this nonce must be specified somewhere that the recipient
                        // can sign.
                        // Propose that this nonce is placed in an OP_RETURN in output 0.
                        // With the new script changes, this will be a lot easier
                        holderArgs(holder.pubKey)
                        spenderArgs(secretSig, holderSig)
                        script {
                            val challenge = inputUtxoHash(0.nx)                               //  grab input 0 as the data to sign
                            checkDataSigVerify(secretSig, challenge, secretCommitment.pubKey) //  check that the signature is correct
                            val signingNonce = secretSig.split(32.nx)                         //  split the signature into R and s
                            val dataScript = constraintScriptForOutputN(0.nx)                 //  output 0 must be an OP_RETURN holding the public nonce
                            val agreedNonce = dataScript.split(2.nx)                          //  OP_RETURN, (length byte) <SPLIT> 32 bytes of public nonce
                            signingNonce.first equalVerify agreedNonce.second                 //  Make sure that R is what we agreed upon
                            checkSigVerify(holderSig, holder.pubKey)                      //  Make sure that the holder agrees to this spend
                        }
                    }
                }
            }
        }

        n1.compile()

        for (ruleName in listOf("AscConstraint"))
        {
            val rule = n1.findRule(ruleName)
            println("Rule $ruleName is:\n${rule}")

            val holder = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 1})),0)
            val recipient = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 2})),0)
            val nftSecret = UnsecuredSecret(ByteArray(32, { 1}))
            val secretCommitment = libnexa.getPubKey(nftSecret.getSecret())

            // TODO make a real sig: Note that the sig needs to use the public nonce committed to in the op_return so I fake such a sig here
            val spendingThisOutpoint = NexaTxOutpoint(Hash256(ByteArray(32, {3})))  // the utxo won't be stored anywhere so the prevout hash is fake
            //val fakeNftSecretSig = rawPublicNonce + ByteArray(32, { 4})

            val fakeNftHolderSig = ByteArray(64, { 5})

            val script = rule.script.script()

            val CovenantedGroupFlags = 1.toByte()
            val nft = GroupId(script.chainSelector, ByteArray(32, { if (it ==31) CovenantedGroupFlags  else it.toByte() }))
            nft.isFenced()
            println("template script hex: ${script.toHex()}")
            // println("satisfier stack: 0x${oracleSig.toHex()} 0x${pdp.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")

            // populate this with actual values for the test

            // build up a prevout
            val priorOut:iTxOutput = txOutputFor(script.chainSelector)
            priorOut.amount = 1000
            val fakesp = Spendable(script.chainSelector)
            fakesp.amount = 1000
            fakesp.priorOutScript = P2T(script,NexaArgs(listOf(holder.pubkey!!))).groupedLockingScript(nft, 1)
            fakesp.outpoint = spendingThisOutpoint // the utxo won't be stored anywhere so the prevout hash is fake

            val firstInputOutpointHash = libnexa.sha256(fakesp.outpoint!!.toByteArray())
            val nonceSecret = UnsecuredSecret(ByteArray(32, { 3}))
            //val nonceSecret = UnsecuredSecret(firstInputOutpointHash)
            val publicNonce = libnexa.getPubKey(nonceSecret.getSecret())
            val rawPublicNonce = publicNonce.drop(1).toByteArray()
            assert(rawPublicNonce.size == 32)
            val nftSig = libnexa.signHashSchnorrWithNonce(firstInputOutpointHash, nftSecret.getSecret(), nonceSecret.getSecret())
            val nonceMatch = nftSig.slice(0 until 32).toByteArray() contentEquals rawPublicNonce
            assert(nonceMatch)

            val (utxo, tx) = txBuilder(script.chainSelector) {
                input(fakesp) { tx, idx ->
                    val flatTx = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
                    val nftHolderSig = libnexa.signTxOneInputUsingSchnorr(flatTx, NexaSigHashType().all().build(), idx.toLong(), tx.inputs[idx].spendable.amount, script.toByteArray(), holder.secret.getSecret() )
                    //val nftHolderSig = holder.unlockingScript(flatTx, idx, NexaSigHashType().all().build(), tx.inputs[idx].spendable.amount)
                    script.satisfier(NexaArgs(holder.pubkey!!),
                        NexaArgs(nftSig, nftHolderSig)
                        )
                }
                data(rawPublicNonce)
                pay(dust(script.chainSelector), script, listOf(recipient.pubkey!!), null, nft, 1)
            }

            print(tx)

            //val (utxo, tx) = setupUtxoTx(fakesp, script, listOf(holder.pubkey!!)) { tx ->
            //    // TODO is the right script being passed?
            //    val holderSig = libnexa.signTxOneInputUsingSchnorr(tx.toByteArray(),NexaSigHashType().all().build(), 0, fakesp.amount, script.toByteArray(), holder.secret.getSecret() )
            //    listOf(nftSig, holderSig)
            //}

            // Try a correct transaction
            try
            {
                val result = rule.eval(listOf<Any>(holder.pubkey!!), listOf(nftSig, fakeNftHolderSig)) {
                    checkDataSigResult = true
                    checkSigResult = true
                    this.tx = tx
                    utxos = arrayListOf(utxo[0] as iTxOutput)
                }
                // TODO check(result.stack.size == 0)  // good end stack should be empty
                println("Simulated Evaluation Succeeded!")
            }
            catch(e:NSL.VerifyException)
            {
                println("Simulated Evaluation Failed! ${e}")
            }

            // on the script virtual machine
            println("\nRun script machine evaluation with real data")

            //val tmp = tx.inputs[0].script.parseTemplateSpend(utxo)
            println("NFT secret nonce is ${nonceSecret.getSecret().toHex()}")
            println("NFT public nonce is ${rawPublicNonce.toHex()}")
            println("NFT signed data is ${firstInputOutpointHash.toHex()}")
            println("NFT secret sig is ${nftSig.toHex()}")
            println("")
            println("utxo & tx: ${utxo[0].toHex()} ${tx.toHex()}")

            val ntx = tx as NexaTransaction
            val sm = ScriptMachine(ntx, 0, utxo[0] as NexaTxOutput)
            // println("template start machine state: ${sm.getState()}")
            var smresult = sm.next(true)
            println(smresult)
            println("status: ${sm.status} script position: ${sm.pos}")
            println(sm.getState())
            //sm.loadStacks(listOf(oracleSig, pdp), listOf())
            //println("starting machine state: ${sm.getState()}")
            //val ok = sm.eval(rule.script.script())

            if (smresult.second == "No error(0)")
            {
                println("eval success")
                check(true)
            }
            else
            {
                println("Eval failed: ${sm.status} script position: ${sm.pos}")
                println("machine state: ${sm.getState()}")
                check(false)
            }
            sm.delete()

            /*
            try
            {
                //val pdp = PriceDataPoint("NEXA", "USDT", Instant.now().toEpochMilli(), 90000).toByteArray()
                var result = rule.eval(listOf(), listOf("010203".fromHex(), pdp)) {
                    checkDataSigResult = false  // force failure
                }
                check(false) // should have thrown
            }
            catch (e: NSL.VerifyException)
            {
                check(true)
            }
            */
        }
    }

    @Test fun testDSV()
    {
        class PriceDataPoint(name: String? = null, _nsl: NSL? = null):PackedStructure(name, _nsl)
        {
            constructor(_tickerA: String, _tickerB:String, _epochSeconds: Long, _priceAinB: Long):this()
            {
                //val tmp =  NSL() // I'm building a concrete object, not a script to manipulate this object, so just create a dummy script and forget it
                //tmp.startVm = VmState(this)
                //nsl = tmp
                tickerA.curVal = _tickerA.toPaddedByteArray(8)
                tickerB.curVal = _tickerB.toPaddedByteArray(8)
                epochSeconds.curVal = _epochSeconds
                priceAinB.curVal = _priceAinB
            }

            val tickerA: NBytes by PBytes(8)
            val tickerB: NBytes by PBytes(8)
            val epochSeconds: NInt by PInt(8)
            // Since this is an integer, the B asset should be its finest unit (e.g. pennies rather than dollars)
            // Or for large granularity units, you could create an artificial division.
            // This finest unit is therefore implied by the name of asset B and does not need to be otherwise specified.
            val priceAinB: NInt by PInt(8)
        }


        val oracle = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 1})),0)

        val n1 = Nexa("testDSV")
        {
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1

            val oracleAddr = NCAddress(oracle,"oracleAddress")
            val oracleSig = NSig("oracleSig")
            val NexaTicker = NCBytes("NEXA",8)
            val UsdtTicker = NCBytes("USDT",8)
            val oracleMsg = PriceDataPoint("oracleMessage")
            val MinPrice = NCInt(10000)
            MinPrice.name = "MinPrice"
            group("A") {
                face {
                    // build the
                    rule("spendAtOrAbovePrice")
                    {
                        templateArgs(NexaTicker, UsdtTicker, MinPrice, oracleAddr.pubKey)
                        holderArgs()
                        spenderArgs(oracleSig, oracleMsg)

                        script {
                            checkDataSigVerify(oracleSig, oracleMsg, oracleAddr.pubKey)
                            oracleMsg.tickerA equalVerify NexaTicker
                            oracleMsg.tickerB equalVerify UsdtTicker
                            verify(oracleMsg.priceAinB gte MinPrice)
                        }
                    }
                    rule("spendAtOrAbovePrice1")
                    {
                        templateArgs(NexaTicker, UsdtTicker, MinPrice, oracleAddr.pubKey)
                        holderArgs()
                        spenderArgs(oracleSig, oracleMsg)

                        script {
                            oracleMsg.tickerA equalVerify NexaTicker
                            oracleMsg.tickerB equalVerify UsdtTicker
                            verify(oracleMsg.priceAinB gte MinPrice)
                            checkDataSigVerify(oracleSig, oracleMsg, oracleAddr.pubKey)
                        }
                    }
                    rule("spendAtOrAbovePrice2")
                    {
                        templateArgs(NexaTicker, UsdtTicker, MinPrice, oracleAddr.pubKey)
                        holderArgs()
                        spenderArgs(oracleSig, oracleMsg)

                        script {
                            oracleMsg.tickerB equalVerify UsdtTicker
                            oracleMsg.tickerA equalVerify NexaTicker
                            verify(oracleMsg.priceAinB gte MinPrice)
                            checkDataSigVerify(oracleSig, oracleMsg, oracleAddr.pubKey)
                        }
                    }
                    rule("spendAtOrAbovePrice3")
                    {
                        templateArgs(NexaTicker, UsdtTicker, MinPrice, oracleAddr.pubKey)
                        holderArgs()
                        spenderArgs(oracleSig, oracleMsg)

                        script {
                            verify(oracleMsg.priceAinB gte MinPrice)
                            oracleMsg.tickerB equalVerify UsdtTicker
                            oracleMsg.tickerA equalVerify NexaTicker
                            checkDataSigVerify(oracleSig, oracleMsg, oracleAddr.pubKey)
                        }
                    }
                }
            }
        }
        n1.compile()

        for (ruleName in listOf("spendAtOrAbovePrice", "spendAtOrAbovePrice1","spendAtOrAbovePrice2","spendAtOrAbovePrice3"))
        {
            val rule = n1.findRule(ruleName)
            println("$ruleName is:\n${rule}")

            // All of these messages won't work
            print("Bad message test: ")
            for (test in 0..10)
            {
                print(" $test")
                try
                {
                    val pdp = when (test)
                    {
                        // Broken price too low
                        0    -> PriceDataPoint("NEXA", "USDT", Instant.now().toEpochMilli(), 1000).toByteArray()
                        // Broken wrong tickers
                        1    -> PriceDataPoint("NEXA", "xUSDT", Instant.now().toEpochMilli(), 100000).toByteArray()
                        2    -> PriceDataPoint("xNEXA", "USDT", Instant.now().toEpochMilli(), 100000).toByteArray()
                        else -> Random.nextBytes(Random.nextInt(0, 200)) // throw garbage at it
                    }

                    // Try this script, and configure the sig check to always work, forcing the failure to be one of the above
                    rule.eval(listOf(), listOf("010203".fromHex(), pdp)) {
                        checkDataSigResult = true
                    }
                    check(false) // should have thrown
                } catch (e: IndexOutOfBoundsException)
                {
                    // The randomly generated bytearray can't be loaded into the PriceDataPoint structure.
                    // In this case the dataflow processor is not really testing the script
                } catch (e: NSL.VerifyException)
                {
                    // yay script validator properly failed this case
                    check(true)
                }
            }


            // Try a correct transaction
            val now = 1685821732L  // Instant.now().toEpochMilli()/1000
            val oracleMsg = PriceDataPoint("NEXA", "USDT", now, 90000)
            val pdp = oracleMsg.toByteArray()

            // dataflow evaluation should work
            var result = rule.eval(listOf(), listOf("010203".fromHex(), pdp)) {
                checkDataSigResult = true
            }
            check(result.stack.size == 0)  // good end stack should be empty

            // on the script virtual machine
            println("\nRun script machine evaluation with real data")
            val omsgHash = libnexa.sha256(pdp)
            val oracleSig = libnexa.signHashSchnorr(omsgHash, oracle.secret.getSecret())
            //break the sig:
            //oracleSig[2] = 0x0
            println("sig: ${oracleSig.toHex()}")
            println("msg: ${pdp.toHex()}")
            println("msgHash: ${omsgHash.toHex()}")
            println("pub: ${oracle.pubkey!!.toHex()}")

            val script = rule.script.script()
            println("template script hex: ${script.toHex()}")
            println("satisfier stack: 0x${oracleSig.toHex()} 0x${pdp.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")
            val (utxo, tx) = setupUtxoTx(script, listOf(), listOf(oracleSig, pdp))

            //val tmp = tx.inputs[0].script.parseTemplateSpend(utxo)
            println("utxo & tx: ${utxo.toHex()} ${tx.toHex()}")

            val sm = ScriptMachine(tx, 0, utxo)
            // println("template start machine state: ${sm.getState()}")
            var smresult = sm.next(true)
            println(smresult)
            println("status: ${sm.status} script position: ${sm.pos}")
            println(sm.getState())
            //sm.loadStacks(listOf(oracleSig, pdp), listOf())
            //println("starting machine state: ${sm.getState()}")
            //val ok = sm.eval(rule.script.script())

            if (smresult.second == "No error(0)")
            {
                println("eval success")
                check(true)
            }
            else
            {
                println("Eval failed: ${sm.status} script position: ${sm.pos}")
                println("machine state: ${sm.getState()}")
                check(false)
            }
            sm.delete()

            try
            {
                val pdp = PriceDataPoint("NEXA", "USDT", Instant.now().toEpochMilli(), 90000).toByteArray()
                var result = rule.eval(listOf(), listOf("010203".fromHex(), pdp)) {
                    checkDataSigResult = false  // force failure
                }
                check(false) // should have thrown
            } catch (e: NSL.VerifyException)
            {
                check(true)
            }
        }
    }

    /** Try checkSequenceVerify scripts */
    @Test fun testCSV()
    {
        val n1 = Nexa("testCSV") {
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1
            group("g") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("spendAfterDate", null, listOf(holderAddr.pubKey), listOf(holderSig)) {
                        script {
                            checkSigVerify(holderSig, holderAddr.pubKey)
                            checkSequenceVerify(NCInt(0x12345))
                        }
                    }
                }

            }

        }

        n1.compile()

        val ruleName = "spendAfterDate"
        val pgm = n1?.groups!!.get(0)!!.contract.interfaces[0]?.rules!!.get("spendAfterDate")!!
        println("$ruleName is:\n${pgm}")
        check(pgm.script.bin() contentEquals "6cad03452301b275".fromHex())
    }

    /** Try check lock time verify scripts */
    @Test fun testCLTV()
    {
        val n1 = Nexa("testCLTV") {
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1
            group("Chartie") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("spendAfterDate", null, listOf(holderAddr.pubKey), listOf(holderSig)) {
                        script {
                            checkLockTimeVerify(NCInt(0x12345))
                            checkSigVerify(holderSig,holderAddr.pubKey)
                        }
                    }
                }

            }

        }

        n1.compile()

        val ruleName = "spendAfterDate"
        val pgm = n1.findRule("spendAfterDate")
        println("$ruleName is:\n${pgm} -> ${pgm.script.bin().toHex()}")
        check((pgm.script.bin() contentEquals "03452301b16c77ad".fromHex()) ||
            (pgm.script.bin() contentEquals "03452301b1756cad".fromHex()))
    }

    /** Try check if statements */
    @Test fun testIfStmt()
    {
        val n1 = Nexa("testIf") {
            val spenderA = NInt("spenderA")
            //val spenderB = NInt("spenderB")
            val holderA = NInt("holderA")
            //val holderB = NInt("holderB")

            group("g") {
                flags = DefaultGroupFlags u GroupFlag.COVENANT

                face {
                    rule("if0", null, listOf(holderA), listOf(spenderA)) {
                        script {
                            val tmp = holderA+spenderA
                            if_(tmp gte 10.nx,
                                { verify(tmp gte 100.nx)},
                                { verify((holderA - spenderA) lte 5.nx)})
                        }
                    }
                }
            }

        }

        n1.compile()

        val ruleName = "if0"
        val pgm = n1.findRule(ruleName)
        println("$ruleName is:\n${pgm}")
        println("Script hex: ${pgm.script.toHex()}")
        println("Script ASM: ${pgm.script.toString()}")

        expectVerifyFail { pgm.eval(listOf(10), listOf(10)) }
        expectVerifyFail { pgm.smeval(listOf(10), listOf(10)) }

        var result = pgm.eval(listOf(90), listOf(20))
        pgm.smeval(listOf(90), listOf(20))


        // Other side of the if
        pgm.eval(listOf(6), listOf(3))
        pgm.smeval(listOf(6), listOf(3))

        expectVerifyFail { pgm.eval(listOf(8), listOf(1)) }
        expectVerifyFail { pgm.smeval(listOf(8), listOf(1)) }
    }



    @Test fun testSplitPush()
    {
        val n1 = Nexa("testSplitPush")
        {
            val scriptBin = NScript("scriptBin")

            group("A")
            {
                face()
                {
                    rule("splitPush", listOf(), listOf(scriptBin), listOf()) {
                        script {
                            val (first, rest) = scriptBin.splitPush()
                            first.size() numEqualVerify 10.nx
                            rest.size() numEqualVerify 0.nx
                        }
                    }

                }
            }
        }

        n1.compile()

        for (ruleName in listOf("splitPush"))
        {
            val rule = n1.findRule(ruleName)
            println("$ruleName is:\n${rule}")

            // dataflow evaluation should work
            var result = rule.eval(listOf(NexaScript(OP.push(ByteArray(10,{ it.toByte() }))).toByteArray()), listOf())
            check(result.stack.size == 0)  // good end stack should be empty

            try
            {
                result = rule.eval(listOf(NexaScript(OP.push(ByteArray(11, { it.toByte() }))).toByteArray()), listOf())
                mycheck(false)  // should throw
            }
            catch (e: VerifyError)
            {
                mycheck(true)
            }

            try
            {
                result = rule.eval(listOf(NexaScript(OP.push(ByteArray(10, { it.toByte() })), OP.C1).toByteArray()), listOf())
                mycheck(false)  // should throw
            }
            catch (e: VerifyError)
            {
                mycheck(true)
            }


            // on the script virtual machine
            println("\nRun script machine evaluation with real data")

            val script = rule.script.script()
            println("template script hex: ${script.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")

            if (true)
            {
                val (utxo, tx) = setupUtxoTx(script, listOf(NexaScript(OP.push(ByteArray(10, { it.toByte() }))).toByteArray()), listOf())

                println("utxo & tx: ${utxo.toHex()} ${tx.toHex()}")
                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                println(smresult)
                println("status: ${sm.status} script position: ${sm.pos}")
                println(sm.getState())

                if (smresult.second == "No error(0)")
                {
                    println("eval success")
                    check(true)
                }
                else
                {
                    println("Eval failed: ${sm.status} script position: ${sm.pos}")
                    println("machine state: ${sm.getState()}")
                    check(false)
                }
                sm.delete()
            }

            if (true)
            {
                // should fail because the pushed array is too big (the script checks that its size 10
                val (utxo, tx) = setupUtxoTx(script, listOf(NexaScript(OP.push(ByteArray(11, { it.toByte() }))).toByteArray()), listOf())
                println("utxo & tx: ${utxo.toHex()} ${tx.toHex()}")
                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                println(smresult)
                println("status: ${sm.status} script position: ${sm.pos}")
                println(sm.getState())
                check(smresult.second != "No error(0)")
                sm.delete()
            }


        }

    }


    // All rules that are entirely populated with constants should be optimized to no script at all
    @Test fun constRule()
    {
        val cdata = NCBytes(ByteArray(100, {it.toByte()}))
        val n = Nexa("testConstRule")
        {
            group("g")
            {
                face()
                {
                    // This entirely constant rule is always true so the generated script will be empty
                    rule("r") {
                        templateArgs(cdata)
                        script {
                            cdata.size() numEqualVerify 100.nx
                            val (a,b) = cdata.split(0.nx)
                            b equalVerify cdata
                        }
                    }
                }
            }
        }

        n.compile()
        for (ruleName in listOf("r"))
        {
            val rule = n.findRule(ruleName)
            val script = rule.script.script()
            check(script.size == 0)  // Size of the script should be 0 because this script evaluates to success
        }

    }

    @Test fun bigInstantiable()
    {
        //val cdata = NCBytes(ByteArray(20, {it.toByte()}))
        val cdata = NCBytes("010201020102".fromHex())
        val n = Nexa("bigInstantiable")
        {
            group("g")
            {
                face()
                {
                    val sp1 = NInt("sp1")
                    val sp2 = NInt("sp2")
                    rule("r") {
                        templateArgs(cdata)
                        holderArgs(sp1, sp2)
                        script {
                            val (a,b) = cdata.split(sp1)
                            val (c,d) = cdata.split(sp2)
                            a equalVerify d
                            b equalVerify c
                        }
                    }
                }
            }
        }

        n.compile()
        for (ruleName in listOf("r"))
        {
            val rule = n.findRule(ruleName)
            println("$ruleName is:\n${rule}")
            val script = rule.script.script()
            println("template script hex: ${script.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")
            rule.smeval(listOf(2,4), listOf())
            // We are really looking for only one copy of cdata in this script
            // The more efficient of these scripts uses the altstack, the other does not
            // (it depends on the depth of your stack program search
             check(
                 script.toHex() == "060102010201026c7c766c7f727c7f537a7c8888" ||
                 script.toHex() == "060102010201026c787b6c7f727c7f6b7b6c8888" ||
                 script.toHex() == "060102010201026c6c7b7b787c7f727c7f537a7c8888")
        }

    }

    @Test fun testReturnGroup()
    {
        val returnAddress = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 2 })),0)


        val grp = NCGroupId("nexareg:tqczktvxhd450m6szh87nuqwk8deflpwlk9n7d3vj00m6j4yygqqqhwsxmz0z")

        val n1 = Nexa("testReturnGroup")
        {
            val returnToAddr = NCAddress(returnAddress,"returnToAddr")
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1

            group("A")
            {
                face()
                {
                    rule("return", listOf(returnToAddr.argsHash), listOf(holderAddr.pubKey), listOf(holderSig)) {

                        script {
                            checkSigVerify(holderSig, holderAddr.pubKey)
                            // TODO
                            // in a real deployment you'd use the ThisGroup object gain the group id after it gets created
                            // mustSpendGroupToP2pkt(ThisGroup, returnToAddr)
                            mustSpendGroupToP2pkt(grp, returnToAddr)
                        }
                    }
                    rule("return2", listOf(returnToAddr.argsHash), listOf(holderAddr.pubKey), listOf(holderSig)) {
                        script {
                            checkSigVerify(holderSig, holderAddr.pubKey)
                            mustSpendLowQuantityGroupToP2pkt(grp, { it equalVerify returnToAddr.argsHash })
                        }
                    }
                    rule("returnOrKeep", listOf(returnToAddr.argsHash), listOf(holderAddr.pubKey), listOf(holderSig)) {
                        script {
                            checkSigVerify(holderSig, holderAddr.pubKey)
                            mustSpendLowQuantityGroupToP2pkt(grp, {
                                it equalVerify returnToAddr.argsHash
                                // TODO my template and args hash
                            })
                        }
                    }

                }
            }
        }

        n1.compile()

        for (ruleName in listOf("return", "return2"))
        {
            val rule = n1.findRule(ruleName)
            println("$ruleName is:\n${rule}")

            // on the script virtual machine
            println("\nRun script machine evaluation with real data")

            val holderDest = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 1 })),0)
            val script = rule.script.script()
            println("template script hex: ${script.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")

            // Working case
            if (true)
            {
                val (utxo, tx) = setupGroupedUtxoTx(grp.gid!!, holderDest, script, listOf(holderDest.pubkey!!), { tx ->
                    val flatTx = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
                    val sig = libnexa.signTxOneInputUsingSchnorr(flatTx, byteArrayOf(), 0, tx.inputs[0].spendable.amount, script.toByteArray(), holderDest.secret.getSecret())
                    listOf(sig)
                }) {
                    val output: iTxOutput = GroupedTxOutput(123, grp.gid!!, returnAddress.address)
                    it.add(output)
                }

                println("return address is: ${returnAddress.address} argsHash: ${returnAddress.constraintArgsHash().toHex()}")
                println("utxo & tx: ${utxo.toHex()} ${tx.toHex()}")

                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                println(smresult)
                println("status: ${sm.status} script position: ${sm.pos}")
                println(sm.getState())
                check(smresult.second == "No error(0)")
                sm.delete()
            }

            // Broken sig
            if (true)
            {
                val (utxo, tx) = setupGroupedUtxoTx(grp.gid!!, holderDest, script, listOf(holderDest.pubkey!!), { tx ->
                    val flatTx = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
                    val sig = libnexa.signTxOneInputUsingSchnorr(flatTx, byteArrayOf(), 0, tx.inputs[0].spendable.amount, script.toByteArray(), holderDest.secret.getSecret())
                    sig[10] = 0
                    sig[11] = 0
                    listOf(sig)
                }) {
                    val output: iTxOutput = GroupedTxOutput(123, grp.gid!!, returnAddress.address)
                    it.add(output)
                }

                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                //println(smresult)
                //println("status: ${sm.status} script position: ${sm.pos}")
                //println(sm.getState())
                check("Signature must be zero for failed" in smresult.second)
                sm.delete()
            }

            // send to some other address
            if (true)
            {
                val anotherReturnAddress = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 3 })),0)
                val (utxo, tx) = setupGroupedUtxoTx(grp.gid!!, holderDest, script, listOf(holderDest.pubkey!!), { tx ->
                    val flatTx = tx.BCHserialize(SerializationType.NETWORK).toByteArray()
                    val sig = libnexa.signTxOneInputUsingSchnorr(flatTx, byteArrayOf(), 0, tx.inputs[0].spendable.amount, script.toByteArray(), holderDest.secret.getSecret())
                    listOf(sig)
                }) {
                    val output: iTxOutput = GroupedTxOutput(123, grp.gid!!, anotherReturnAddress.address)
                    it.add(output)
                }

                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                println(smresult)
                check(smresult.second == "Script failed an OP_EQUALVERIFY operation(16)")
                sm.delete()
            }
        }

    }


    @Test fun testThisTemplateAndArgsHash()
    {
        val address = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 2 })),0)

        val grp = NCGroupId("nexareg:tqczktvxhd450m6szh87nuqwk8deflpwlk9n7d3vj00m6j4yygqqqhwsxmz0z")

        val n1 = Nexa("testThisTemplateAndArgsHash")
        {
            val matchHolderAddr = NAddress("mholderAddress")
            val matchTemplate = NBytes("matchTemplate")
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1

            group("A")
            {
                face()
                {
                    rule("test") {
                        spenderArgs(matchHolderAddr.argsHash, matchTemplate)
                        holderArgs(holderAddr.argsHash)

                        script {
                            val (tmpl, args) = thisTemplateAndArgsHash()
                            // This is a very contrived example to just check that we get something
                            // tmpl.size() numEqualVerify 20.nc

                            // The spender will pass in the template and args hashes that the UTXO should have
                            verify(tmpl.size() eq 20.nx)
                            verify(tmpl eq matchTemplate)
                            args equalVerify matchHolderAddr.argsHash
                        }
                    }
                    rule("test2") {
                        spenderArgs(matchHolderAddr.argsHash, matchTemplate)
                        holderArgs(holderAddr.argsHash)

                        script {
                            val tmpl = thisTemplateHash()
                            // The spender will pass in the template hash that the UTXO should have
                            verify(tmpl.size() eq 20.nx)
                            verify(tmpl eq matchTemplate)
                        }
                    }
                    rule("test3") {
                        spenderArgs(matchHolderAddr.argsHash, matchTemplate)
                        holderArgs(holderAddr.argsHash)

                        script {
                            val args = thisArgsHash()
                            // The spender will pass in the args hash that the UTXO should have
                            verify(args.size() eq 20.nx)
                            verify(args eq matchHolderAddr.argsHash)
                        }
                    }
                    /*
                    rule("test2") {
                        spenderArgs(matchHolderAddr.argsHash, matchTemplate)
                        holderArgs(holderAddr.argsHash)

                        script {
                            val (tmpl, args) = thisTemplateAndArgsHash()
                            // This is a very contrived example to just check that we get something
                            // tmpl.size() numEqualVerify 20.nc
                            verify(tmpl.size() eq 20.nc)  // We cannot test the actual template hash for a constant because that code change changes the hash
                            verify(tmpl eq matchTemplate)
                            args equalVerify matchHolderAddr.argsHash  // The spender will pass in the args hash that the UTXO should have
                            val t2 = thisTemplateHash()
                            verify(t2 eq matchTemplate)
                            val a2 = thisArgsHash()
                            verify(a2 eq args)
                        }
                    }

                     */
                }
            }
        }

        n1.compile()

        for (ruleName in listOf("test", "test2"))
        {
            val rule = n1.findRule(ruleName)
            println("$ruleName is:\n${rule}")

            val holderDest = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 1 })),0)
            val nextHolderDest = Pay2PubKeyTemplateDestination(ChainSelector.NEXA, UnsecuredSecret(ByteArray(32, { 2 })),0)
            val script = rule.script.script()
            println("template script hex: ${script.toHex()}")
            println("script length: ${script.size} hex: ${script.toHex()} asm: ${script.toAsm(" ")}")

            // Working case
            if (true)
            {
                val (utxo, tx) = setupGroupedUtxoTx(grp.gid!!, holderDest, script, listOf(holderDest.pubkey!!), { tx ->
                    //val flatTx = tx.BCHserialize(SerializationType.NETWORK).flatten()
                    //val sig = Wallet.signOneInputUsingSchnorr(flatTx, byteArrayOf(), 0, tx.inputs[0].spendable.amount, script.toByteArray(), holderDest.secret.getSecret())
                    listOf(holderDest.constraintArgsHash(), script.scriptHash160())
                }) {
                    val output: iTxOutput = GroupedTxOutput(123, grp.gid!!, nextHolderDest.address)
                    it.add(output)
                }

                println("address is: ${holderDest.address} pubkey:${holderDest.pubkey!!.toHex()} argsHash: ${holderDest.constraintArgsHash().toHex()}")
                println("utxo & tx: ${utxo.toHex()} ${tx.toHex()}")

                val sm = ScriptMachine(tx, 0, utxo)
                var smresult = sm.next(true)
                println(smresult)
                sm.delete()
                // rule.smeval(listOf(), listOf(holderDest.argsHash()))
            }

        }

    }


    /** The data carrier test demonstrates a group that requires the output to commit to an argument that is 1 bigger than the input
     * The test checks that there is 1 output, and that that output's argsHash commits to the holder's input arg + 1.
     * It does not need to check that the output template hash is the same as the input template hash because the group's COVENANT bit is set.
     */
    @Test fun testDataCarrier()
    {
        val n1 = Nexa("testDataCarrier")
        {
            val matchHolderAddr = NAddress("mholderAddress")
            val matchTemplate = NBytes("matchTemplate")
            val holderAddr = NAddress("holderAddress")
            val holderSig = NSig("holderSig")
            holderSig.weight = -1
            holderAddr.pubKey.weight = -1

            group("holderArgsHashedDataCarrier", GroupFlags.of(GroupFlag.COVENANT))
            {
                // The initial data is located in the holderArgs
                mint(1UL, 1) {
                    face {
                        rule("dataXform")
                        {
                            // To avoid the complexity of encoding script ints, we define our datacarrier integer as a 4 byte little-endian number
                            val counter = NBytes("counter")

                            holderArgs(counter)
                            script {
                                val gid = thisGroup()
                                val gout = groupedOutputN(gid, 0.nx)  // there can be only 1 output because the data carrier only has 1 token in the group
                                val cs = constraintScriptForOutputN(gout)
                                // The output MUST be: push(groupId) push(group_quantity) push(this_template) push(argsHash)
                                //                     33            3                     1     20             1   20 or 32
                                val (prefix: Nexa.npl.NBytes, argsHash: Nexa.npl.NBytes) = split(cs, (33 + 3 + 1 + 20 + 1).nx)
                                if (!cc.compiling) check(argsHash.curVal!!.size == 20)
                                val nextVal = counter.toInt() + 1.nx
                                val nextBytes = nextVal.toBytes(4.nx)
                                val nextArgs = "04".fromHex().nx + nextBytes
                                if (!cc.compiling) println(nextArgs.curVal?.toHex())
                                val nextArgsHash = nextArgs.hash160()
                                nextArgsHash equalVerify argsHash
                            }
                        }
                    }
                }
            }
        }

        n1.compile()
        val rule = n1.findRule("dataXform")

        // Create an evaluation config that supplies simulated transaction state which specifies an input count of 1, and an output count of 2
        val fakeTemplateHash = ByteArray(20, {0.toByte()})
        val ev = EvalConfig().apply {
            gid = GroupId(ChainSelector.NEXAREGTEST, ByteArray(32,{ it.toByte()}))
            thisIndex = {0.nx}
            inputConstraintScript = {
                check(it.curVal == 0L)  // it should be what we passed in groupOutputN
                val constraint = NexaConstraint(evalConfig.gid!!, 1UL, fakeTemplateHash, NexaArgs(1L.leSignMag(4)) )
                NScript(constraint)
            }
            constraintScriptForOutputN =
                {
                    check(it.curVal == 0L)  // it should be what we passed in groupOutputN
                    val constraint = NexaConstraint(evalConfig.gid!!, 1UL, fakeTemplateHash, NexaArgs(2L.leSignMag(4)) )
                    NScript(constraint)
                }
            groupedOutputN = { a,b -> 0.nx }
        }

        // Check that the script works for 1 -> 2
        var result = rule.eval(listOf(1), listOf(), ev)
        check(result.stack.size == 0)

        // Modify the evaluation config's output to a count of 100
        ev.constraintScriptForOutputN = {
            // This is incorrect for the input constraint script because the next data carrier is too large
            NScript(NexaConstraint(evalConfig.gid!!, 1UL, fakeTemplateHash, NexaArgs(100L.leSignMag(4)) ))
        }

        // Check that the script correctly fails for 1 -> 100
        try
        {
            result = rule.eval(listOf(1), listOf(), ev)
            check(false)  // This should fail because the constraintScriptForOutputN is wrong
        }
        catch(e:NslException)
        {
            check(true)
        }

    }

}