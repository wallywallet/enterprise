package Nexa.npl

import org.nexa.libnexakotlin.GetLog
import org.nexa.libnexakotlin.leSignMag
import org.nexa.libnexakotlin.toHex
import org.jetbrains.kotlin.utils.checkWithAttachment
import java.io.ByteArrayOutputStream
import kotlin.reflect.KProperty
private val LogIt = GetLog("nexa.nsl.struct")
class UnvaluedException(what: String?=null): NplException(what)


/** Base class for every field in a packed structure */
abstract class PObject
{
    var name: String = ""
    lateinit var container: PackedStructure

    open operator fun provideDelegate(_container: PackedStructure, prop: KProperty<*>): PObject
    {
        container = _container
        name = prop.name
        container.addField(this)
        return this
    }

    abstract val size:Int

    /** serialize this object into a bytearray */
    abstract fun toByteArray(): ByteArray

    /** deserialize this object from a bytearray */
    abstract fun fromByteArray(ba: ByteArray)


    fun extractInto(ret:NBytes, nsl: NSL)
    {
        val start = container.getfieldStart(this)
        val isLast = container.isLast(this)

        if (start != 0)
        {
            // If this is the last element, the suffix will not have any addtl elements in it so it is the return
            val suffix:NBytes = if (container.isLast(this)) ret else NBytes.unbound(container.name + "." + name + ".suffix")
            nsl.splitSuffixInto(container, NCInt(start), suffix)
            if (!isLast) nsl.splitPrefixInto(suffix, NCInt(size), ret)
            return
        }
        // This object starts the structure
        // if it also ends it, then it is the entire PackedStructure
        if (isLast) ret.assign(container)
        else
            nsl.splitPrefixInto(container, NCInt(size), ret)
    }

    fun extractInto(ret:NInt, nsl: NSL)
    {
        val start = container.getfieldStart(this)
        val isLast = container.isLast(this)

        if (start != 0)
        {
            val asBytes = NBytes()
            // If this is the last element, the suffix will not have any addtl elements in it so it is the return
            if (isLast)
            {
                nsl.splitSuffixInto(container, NCInt(start), asBytes)
            }
            else
            {
                val suffix:NBytes = NBytes.unbound(container.name + "." + name + ".suffix")
                nsl.splitSuffixInto(container, NCInt(start), suffix)
                nsl.splitPrefixInto(suffix, NCInt(size), asBytes)
            }
            asBytes.toInt(ret)
            return
        }
        // This object starts the structure
        if (isLast)  // if it also ends it, then it is the degenerate case of a single field in the PackedStructure
        {
            // Since no splitting is needed we can just convert directly to an Int
            container.toInt(ret)
            // asBytes.unused = false // artificially mark as used because we don't need it
            return
        }
        else
        {
            val asBytes = NBytes()
            nsl.splitPrefixInto(container, NCInt(size), asBytes)
            asBytes.toInt(ret)
            return
        }

    }

}

/** Packed Byte Array */
open class PBytes(override val size: Int):PObject()
{
    var v: NBytes? = null

    /** Return the current value of this object as a byte array */
    override fun toByteArray(): ByteArray = v?.curVal?.copyOf(size) ?: throw UnvaluedException("Error: no value provided for $name")

    /** deserialize this object from a bytearray */
    override fun fromByteArray(ba: ByteArray) { v!!.curVal = ba }

    //override fun setNsl(nsl: NSL?) { v?.let { it.nsl = nsl }}

    override operator fun provideDelegate(container: PackedStructure, prop: KProperty<*>): PBytes
        = super.provideDelegate(container, prop) as PBytes


    operator fun getValue(thisRef: PackedStructure, property: KProperty<*>): NBytes
    {
        assert(thisRef == container)
        val ret:NBytes = v ?: { val tmp = NBytes.unbound(thisRef.name + "." + name); v = tmp; tmp }()

        if (cc.compiling) extractInto(ret, cc.nsl)
        return ret
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: NBytes) {
        println("$value has been assigned to '${property.name}' in $thisRef.")
    }

}

/** Packed Integer.  If a size is provided, this is the size of the integer when packed.  When unpacked, it will be a normal ScriptInt (8 bytes) */
open class PInt(override val size: Int):PObject()
{
    var v :NInt? = null

    override operator fun provideDelegate(container: PackedStructure, prop: KProperty<*>): PInt
        = super.provideDelegate(container, prop) as PInt

    /** Return the current value of this object as a byte array */
    override fun toByteArray(): ByteArray
    {
        val amt = v?.curVal ?: throw UnvaluedException("Error: no value provided for $name")
        // Now I need to serialize amt as a little endian number
        val ret = amt.leSignMag()
        // println("encode $name ${ret.toHex()}")
        return ret
    }

    /** deserialize this object from a bytearray */
    override fun fromByteArray(ba: ByteArray)
    {
        // println("decode $name ${ba.toHex()}")
        if (v == null)  // time to bind to a constant
        {
            v = NInt(container.name + "." + name)
        }
        v!!.curVal = ba.leSignMag()

    }


    //override fun setNsl(nsl: NSL?) { v?.let { it.nsl = nsl }}

    operator fun getValue(thisRef: PackedStructure, property: KProperty<*>): NInt
    {
        assert(thisRef == container)
        val ret:NInt = v ?: { val tmp = NInt.unbound(thisRef.name + "." + name); v = tmp; tmp }()
        if (cc.compiling) extractInto(ret, cc.nsl)  // if its null we are not compiling
        return ret
    }

    /* It would be nice to make this instantiable.  That way, the code could choose to reuse or to instantiate, but
    I can't make this instantiable, because right now instantiables take 0 arguments
    operator fun getValue(thisRef: PackedStructure, property: KProperty<*>): NInt
    {
        var tmp =  v
        if (tmp != null) return tmp

        tmp = NInt(thisRef.name + "." + property.name, thisRef.nsl)

        // TODO make instantiables capable of returning with other stuff on the stack and handle it
        tmp.instantiable = {
            assert(thisRef == container)
            val nsl = NSL()
            nsl.startVm = VmState(container)  // We need a container object in order to extract this field
            nsl.endVm = VmState(tmp)  // At the end of this instantiable, only tmp should be on the main stack
            val ret = NBytes.unbound(thisRef.name + "." + name,nsl)
            extractInto(ret, nsl)
            nsl.toStep()
        }

        v = tmp
        return tmp
    }
     */

    operator fun setValue(thisRef: PackedStructure, property: KProperty<*>, value: NInt)
    {
        println("$value has been assigned to '${property.name}' in $thisRef.")
        TODO()
    }
}

/** A PackedStructure is a structure with multiple fields that appears in a single stack item within the Nexa script machine
 * All packed structures should derive from this class.
 * */
open class PackedStructure(name: String? = null, _nsl: NSL? = null): NBytes(name)
{
    protected val fields = mutableListOf<PObject>()

    /** Uncommonly used: Add a field into this packed structure, at the end.
     * Typically, you would simply derive from PackedStructure and declare members of Packed types children of [PObject], such as [PInt], [PBytes].
     * Those objects will automatically add themselves.
     */
    fun addField(field:PObject) = fields.add(field)


    /** Parse this byte array and set the fields properly */
    override fun setValue(v: Any)
    {
        mycheck(v is ByteArray)
        super.setValue(v)
        val ba = v as ByteArray

        var curpos = 0
        for (f in fields)
        {
            val sl = ba.sliceArray(curpos until (curpos + f.size))
            // println("unpack ${f.name} <- ${sl.toHex()}")
            f.fromByteArray(sl)
            curpos += f.size
        }
    }

    /** Return the byte offset that this field begins in within the packed structure */
    fun getfieldStart(field:PObject): Int
    {
        var offset = 0
        for (f in fields)
        {
            if (f == field) return offset
            offset += field.size
        }
        return offset
    }

    /** Return true if the passed field is the last one defined in this structure */
    fun isLast(field: PObject) = fields.getOrNull(fields.size-1) == field

    /** Pack this structure into a ByteArray (synonym for toByteArray) */
    fun pack():ByteArray
    {
        val ret = ByteArrayOutputStream()
        fields.forEach {
            val enc = it.toByteArray().copyOf(it.size)
            // println("pack ${it.name} -> ${enc.toHex()}")
            ret.write(enc)
        }
        return ret.toByteArray()
    }

    /** Return the packed representation of this structure */
    fun toByteArray() = pack()

    /** Return the hex string of the packed representation of this structure */
    fun toHex() = pack().toHex()
}