package Nexa.npl

import org.nexa.scriptmachine.ScriptMachine
import org.nexa.libnexakotlin.OP
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.NexaScript
import kotlinx.serialization.Serializable
import java.util.*
import java.util.StringJoiner
import kotlin.collections.ArrayList


var bindingCount = 0L
val bindingNames = mutableMapOf<String,NBinding>()

fun mycheck(b: Boolean)
{
    if (b == false)
    {
        println("CHECK FAILED")
    }
    check(b)
}


open class NslException(what: String?=null, var locations: MutableSet<Step>?=null): NplException(what)
{
    constructor(what: String?=null, location: Step): this(what, if (location != null) mutableSetOf(location) else null)

    override fun toString(): String
    {
        val ret = StringBuilder()
        ret.append(super.toString())
        val locs = locations
        if (locs != null)
        {
            ret.append("AT:\n")
            for (loc in locs)
            {
                ret.append(loc.toString())
            }
            ret.append("\n")
        }
        return ret.toString()
    }
}

fun<T> annotateExceptionsWith(location: Step, fn: ()->T ): T
{
    try
    {
        return fn()
    }
    catch (e: NslException)
    {
        if (e.locations == null) e.locations = mutableSetOf(location)
        else e.locations?.add(location)
        throw e
    }
    catch(e: Exception) { throw e }
}


val ignoreFiles = setOf("nsl.kt")
fun loc(ignoreCount:Int = 0): String
{
    val st = Exception().stackTrace
    var cnt = 0
    val ret = StringJoiner("\u2b9c") //("<-")
    var lastfn = ""
    var fileCount = 0
    for (fr in st)
    {
        if (cnt >= ignoreCount+1)
        {
            if (fr.fileName != null && fr.fileName.endsWith(".kt"))  // Ignore all java files as they are not from this project
            {
                if (lastfn == fr.fileName) ret.add(fr.lineNumber.toString())
                else
                {
                    ret.add(fr.fileName + ":" + fr.lineNumber)
                    if (!ignoreFiles.contains(fr.fileName)) fileCount++
                }
                lastfn = fr.fileName
            }
        }
        if (fileCount > 1) break
        cnt++
    }

    return ret.toString()
}



val LocationSkips = mapOf("org.nexa.nsl.SourceLocation" to null,
    "org.nexa.nsl.NBinding" to null, "org.nexa.nsl.NBytes" to null, "org.nexa.nsl.NScript" to null,
    "org.nexa.nsl.Step" to null, "org.nexa.nsl.NSL" to setOf("exec","exec\$default"))
val LocationStops = mapOf("org.nexa.nsl.RuleBuilder" to setOf("build"))

/** Holds the source code location, for display and use when back-tracking compiled code into source code
 *
 */
class SourceLocation()
{
    val loc = Exception().stackTrace

    override fun toString(): String
    {
        var cnt = 0
        val ret = StringJoiner("\u2b9c") //("<-")
        var lastfn = ""
        var fileCount = 0
        for (fr in loc)
        {
            if (LocationStops.containsKey(fr.className))
            {
                val fns = LocationStops[fr.className]
                if (fns == null) break
                if (fns.contains(fr.methodName)) break
            }
            if (LocationSkips.containsKey(fr.className))
            {
                val fns = LocationSkips[fr.className]
                if (fns == null) continue
                if (fns.contains(fr.methodName)) continue
            }


            if (fr.fileName != null && fr.fileName.endsWith(".kt"))  // Ignore all java files as they are not from this project
            {
                if (lastfn == fr.fileName) ret.add(fr.lineNumber.toString())
                else
                {
                    ret.add(fr.fileName + ":" + fr.lineNumber)
                    if (!ignoreFiles.contains(fr.fileName)) fileCount++
                }
                lastfn = fr.fileName
            }
            cnt++
        }

        return ret.toString()
    }
}

/*
fun MutableMap<K, V>.getsert(key: K, factory:()->V): V
{
    var ret = get(K)
    if (ret != null) return ret
    ret = factory()
    this[K] = ret
}

 */


enum class GroupFlag(val v: UInt)
{
    /** No special group functionality */
    NONE(0U),
    /** covenants/ encumberances -- output script template must match input */
    COVENANT(1U),
    /** group inputs and outputs must balance NEX, token quantity MUST be 0 */
    HOLDS_NEX(2U);
}

/** Defines properties of a Group */
typealias GroupFlags = EnumSet<GroupFlag>

/** convert these group flags into a bit field */
fun GroupFlags.toBitfield(): UInt
{
    var ret = 0U
    if (contains(GroupFlag.COVENANT)) ret += GroupFlag.COVENANT.v.toUInt()
    if (contains(GroupFlag.HOLDS_NEX)) ret += GroupFlag.HOLDS_NEX.v.toUInt()
    return ret
}

/** convert a bit field into group flags (loading this object) */
fun GroupFlags.fromBitfield(b: UInt)
{
    clear()
    if ((b and GroupFlag.COVENANT.v) != 0U) add(GroupFlag.COVENANT)
    if ((b and GroupFlag.HOLDS_NEX.v) != 0U) add(GroupFlag.HOLDS_NEX)
}


/** Check whether the LHS group has all of the RHS properties */
infix fun GroupFlags.has(other: GroupFlags) = this.containsAll(other)

/** Union of flags */
infix fun GroupFlags.u(other: GroupFlag) = GroupFlags.of(other, *this.toTypedArray())

/** Default group functionality (GroupFlag.NONE) */
val DefaultGroupFlags = GroupFlags.of(GroupFlag.NONE)


/** A spend rule is the top-level entry to spending.  In other words, if an interface contains
 * multiple spend rules, the spender can choose any one of them.  All the contents of the spend rules
 * are compiled to build a holder and spender args lists that defines this particular API in the interface */
/*
class SpendRule(
    val name: String,
    val spendConstraints: List<SpendConstraint>)
{
}

class SpendRuleBuilder(var name: String)
{

    fun build(): SpendRule = SpendRule(name, spendConstraints)
}

 */


/** When simulation evaluation occurs, this configures the evaluation */
data class EvalConfig(
    /** Cause checkdatasig and checkdatasigverify to always succeed (true) or always fail (false), or actually execute the signature check (null) */
    var checkDataSigResult: Boolean? = null,
    /** Cause checksig and checksigverify to always succeed (true) or always fail (false), or actually execute the signature check (null) */
    var checkSigResult: Boolean? = null,
    var chainSelector: ChainSelector = ChainSelector.NEXA)
{
    var compiling = false

    /** map group name to instantiated group.  Null if groups are not instantiated */
    var instantiations: Map<String, NCGroupId>? = null

    // Transaction state emulation
    var thisIndex: (NSL.()->NInt)? = null
    var inputConstraintScript: (NSL.(NInt)->NScript)? = null

    var inputCount: (NSL.()->NInt)? = null
    var outputCount: (NSL.()->NInt)? = null

    // The fields above can be used to override this transaction state (this tx provides introspection data)
    var tx: iTransaction? = null
    // The fields above can be used to override the incoming UTXOs
    var utxos: ArrayList<iTxOutput>? = null

    /** Simulate OP.CHECKLOCKTIMEVERIFY primitive instruction */
    var checkLockTimeVerify: (NSL.(locktime: NInt)->Unit)? = null
    /** Simulate OP.CHECKSEQUENCEVERIFY primitive instruction */
    var checkSequenceVerify: (NSL.(sequence: NInt)->Unit)? = null

    /** Simulate OP.PushTxState.GROUP_NTH_OUTPUT: Returns the grouped output indexed by n */
    var groupedOutputN: (NSL.(gid: NGroupId, n: NInt) -> NInt)? = null

    /** Simulate OP.PushTxState.GROUP_OUTGOING_COUNT: Return number of grouped outputs */
    var countOutputsByGroup: (NSL.(gid: NGroupId) -> NInt)? = null
    /** Simulate OP.PushTxState.GROUP_INCOMING_COUNT: Return number of grouped inputs */
    var countInputsByGroup: (NSL.(gid: NGroupId) -> NInt)? = null


    /** Simulate OP.OUTPUTBYTECODE: Returns the constraint (output) script specified for the passed output index */
    var constraintScriptForOutputN: (NSL.(idx: NInt) -> NScript)? = null

    /** Will be assigned to the current group id (if within a group context) */
    var gid: GroupId? = null
}


/** Nexa Scripting Language definition */
class NSL
{
    /** This function lets you put a breakpoint in one place and catch all thrown errors */
    fun Err(err: Exception): Exception
    {
        println("\n\n")
        return err
    }

    /** Capture all the warnings */
    fun Warn(s:String, locations:Set<Step>? = null)
    {
        println("\nWARNING: $s\n")
    }

    companion object
    {
        fun clause(_pgm: MutableList<Step>): NSL
        {
            val ret = NSL(_pgm)
            ret.evalConfig = cc.nsl.evalConfig  // inherit the evaluation behavior
            ret.unspecifiedEntryExit = true
            return ret
        }
        fun clause(builder: NSL.()->Unit): NSL
        {
            val ret = NSL()
            ret.evalConfig = cc.nsl.evalConfig  // inherit the evaluation behavior
            ret.unspecifiedEntryExit = true
            cc.with(ret) { ret.builder() }
            return ret
        }

        /** Create an evaluation-style NSL object */
        fun eval(): NSL
        {
            val ret = NSL()
            ret.evalConfig.compiling = false
            return ret
        }
    }

    /** Thrown if a verify fails during dataflow execution */
    class VerifyException(reason:String): NslException(reason)

    /** Thrown in the case of an illegal stack access.  This is a valid script failure mode. */
    class StackException(reason:String): NslException(reason)

    /** Thrown in the case of an opcode execution exception.  This is a valid script failure mode. */
    class OpcodeException(reason:String): NslException(reason)


    /** The script is not correct. */
    class ProgrammingException(reason: String):NslException(reason)


    /** modify how this script will be evaluated in simulation */
    var evalConfig = EvalConfig()

    val ENTRY_STEP = Step(-1, VM_NO_CHANGE, VM_NO_CHANGE)
    val EXIT_STEP = Step(Int.MAX_VALUE-1, VM_NO_CHANGE, VM_NO_CHANGE)

    /** If true, this entire subscript cannot be dead code, because it exports state out-of-the-VM.
     * But if false, the subscript may or may not be dead code, depending on whether endVm binding are used */
    var hasSideEffects = false

    /** If this rule needs to be recompiled after all groups have been instantiated (due, for example, its use of other group IDs), then set this to true */
    var recompileAfterInstantiation = false

    // All the steps in the program (unordered)
    val pgm = mutableListOf<Step>()
    var programCount = 0  // incremented in each step, so is used to remember the order each step appears in the source program

    /** The stack, altstack, and any other (future) state, as the program is entered */
    var startVm = VmState()
    /** The stack, altstack, and any other (future) state, as the program is exited */
    var endVm = VmState()

    /** The bindings that are the output when this script is completed
     */
    // val endBindings:MutableList<NBinding> = mutableListOf()
    /** The final compiled program */
    var endPgm:MutableList<Step>? = null
    /** The final flattened program, with inefficiencies removed */
    var refactored: MutableList<OP>? = null
    /** Set to true to indicate that analysis of the program should produce an entry stack (this is a clause -- if statement or subroutine)*/
    var unspecifiedEntryExit = false

    // Map every binding to every statement that uses it.
    val bindingUses = mutableMapOf<NBinding, MutableSet<Step> >()
    // Map the statement that created every binding, including the entry
    // So every binding that is used MUST be in this map
    val bindingCreated = mutableMapOf<NBinding, Step>()

    constructor()

    constructor(_pgm: MutableList<Step>)
    {
        pgm.addAll(_pgm)
    }

    /** Convert this script into a Step.
     * This script is effectively acting like a subroutine */
    fun toStep():Step
    {
        val ret = Step(programCount, startVm, endVm, false)
        ret.sub.add(this)
        programCount++
        compile(ret)
        return ret
    }

    override fun toString(): String = print(false)
    fun toHex() = script().toHex()

    fun dump(bytePos: Boolean = false, indent:Int = 0): String
    {
        val spaces = "  ".repeat(indent)
        val s = StringBuilder()
        val pgmstr = StringJoiner("")
        val ep = endPgm
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
        if (ep != null)
        {
            var pos = 0
            for (a in ep)
            {
                pgmstr.add(a.dump(bytePos, indent))
            }
            s.append(pgmstr)
        }
        else
        {
            for (a in pgm)
            {
                pgmstr.add(a.dump(bytePos, indent))
            }
            s.append(pgmstr)
        }

        return s.toString()
    }

    fun print(bytePos: Boolean = false): String
    {
        val s = StringBuilder()
        val pgmstr = StringJoiner("\n")
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())

        val ref = refactored
        if (ref != null)
        {
            var pos = 0
            for (op in ref)
            {
                if (bytePos) pgmstr.add("$pos: ${op.toAsm()}")
                else pgmstr.add(op.toAsm())
                bin.add(op)
                pos += op.v.size
            }
            s.append(""" "asm":"""" + "\n${pgmstr}" + """", "hex":"${bin.toHex()}" """)
        }
        else
        {
            val ep = endPgm

            if (ep != null)
            {
                var pos = 0
                for (a in ep)
                {
                    val code = a.fullOpcodes().forEach()
                    { it:OP ->
                        if (bytePos) pgmstr.add("$pos: ${it.toAsm()}")
                        else pgmstr.add(it.toAsm())
                        bin.add(it)
                        pos += it.v.size
                    }
                }
                s.append(""" "asm":"""" + "\n${pgmstr}" + """", "hex":"${bin.toHex()}" """)
            }
            else
            {
                for (a in pgm)
                {
                    pgmstr.add(a.toString())
                }
                s.append(""" dataflow="${pgmstr}" """)
            }
        }

        return s.toString()
    }

    /** returns the bytecode (compiled script binary) for the script associated with this rule.
     * You must compile the rule first
     */
    fun bin(): ByteArray
    {
        return script().toByteArray()
    }

    /** returns all the opcodes that make up this rule (you must compile the rule first) */
    fun fullOpcodes(): List<OP>
    {
        refactored?.let { return it.toList() }

        // otherwise get from the end program
        val opcodes = mutableListOf<OP>()
        val ep = endPgm
        if (ep == null) throw IllegalStateException("Not compiled yet")
        for (a in ep)
        {
            opcodes.addAll(a.fullOpcodes())
        }
        return opcodes
    }

    /** returns the SatoshiScript associated with this rule (you must compile the rule first) */
    fun satoshiscript(): SatoshiScript
    {
        val bin = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, byteArrayOf())
        for (op in fullOpcodes()) bin.add(op)
        return bin
    }

    /** returns the SatoshiScript associated with this rule (you must compile the rule first) */
    fun script(): NexaScript
    {
        val bin = NexaScript()
        for (op in fullOpcodes()) bin.add(op)
        return bin
    }

    /** VERIFY primitive: Fails the script if the item is false or 0 */
    fun verify(b:NBytes)
    {
        b.verify()
    }

    fun verify(b:NBool)
    {
        b.verify()
    }

    fun verify(b:NInt)
    {
        b.verify()
    }

    /** Fail validation */
    fun fail()
    {
        exec(listOf(), listOf(OP.PUSHFALSE, OP.VERIFY), listOf())
        endVm.failed = true
    }

    /** all of these constraints must be true for the script to succeed */
    fun and(vararg constraints: NSL): NSL
    {
        TODO()
    }

    /** one of these constraints must be true for the script to succeed (satisfier tells which to use) */
    fun or(vararg constraints: NSL): NSL
    {
        TODO()
    }

    fun result(vararg bindings: NBinding)
    {
        // mycheck(endBindings.isEmpty())  // you can only call ret once to specify the exit bindings
        for (b in bindings)
            endVm.stack.add(b)
    }

    enum class Stack
    {
        MainStack,
        AltStack
        ;
    }

    @Serializable
    class BindingLocation(val stack: Stack)
    {
    }



    /** Return the minimum of 2 script integers */
    fun min(a:NInt, b:NInt): NInt
    {
        val ret = NInt(a.name + "_" + b.name + "_min")
        // Do an evaluation if possible
        a.curVal?.let { me -> b.curVal?.let { ret.curVal = min(me,it); return@min ret }}

        cc.nsl!!.exec(setOf(a, b), OP.MIN, listOf(ret))
        return ret
    }

    /** Return the maximum of 2 script integers */
    fun max(a:NInt, b:NInt): NInt
    {
        val ret = NInt(a.name + "_" + b.name + "_max")
        // Do an evaluation if possible
        a.curVal?.let { me -> b.curVal?.let { ret.curVal = max(me,it); return@max ret }}

        cc.nsl!!.exec(setOf(a, b), OP.MAX, listOf(ret))
        return ret
    }




    /** bind an integer into a nexa const */
    // TODO find one that already exists (for possible reuse)
    val Int.nx
        get() = NCInt(this)

    val UInt.nx
        get() = NCInt(this.toLong())
    val Long.nx
        get() = NCInt(this)


    /** bind an integer into a nexa const */
    val Byte.nx
        get() = NCInt(this)

    /** bind an integer into a nexa const */
    val ByteArray.nx
        get() = NCBytes(this)


    /** Specifies opcode execution.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * An opcode expects certain parameters to be on the stack, and may output results.
     * @param entryStack Specify what parameters are expected to be on the stack before execution.  The END of this list is the TOP of the stack (so the list's order is the 'push' order).  It is assumed that these stack items are consumed.  To indicate an items is not consumed, it should be specified as part of the output stack
     * @param opcode The Nexa VM opcode
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     */
    fun exec(entryStack: List<NBinding>, opcode: OP, exitStack: List<NBinding>? = null) =
        exec(entryStack, listOf(opcode), exitStack)

    /** Specifies opcode execution.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * This variant expects a single parameter and returns 1 or 0 parameters
     * @param entryStack Specify what parameters are expected to be on the stack before execution.  The END of this list is the TOP of the stack (so the list's order is the 'push' order).  It is assumed that these stack items are consumed.  To indicate an items is not consumed, it should be specified as part of the output stack
     * @param opcode The Nexa VM opcode
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     */
    fun exec(entry: NBinding, opcode: OP, exit: NBinding? = null) =
        exec(listOf(entry), listOf(opcode), if (exit == null) null else listOf(exit))


    /** Specifies execution of a group of opcodes.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * The entry and exit stack should reflect the complete changes as if this group of opcodes was actually a single instruction.
     * @param entryStack Specify what parameters are expected to be on the stack before execution.  The END of this list is the TOP of the stack (so the list's order is the 'push' order).  It is assumed that these stack items are consumed.  To indicate an items is not consumed, it should be specified as part of the output stack
     * @param opcode The Nexa VM opcodes to be executed
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     */
    fun exec(entryStack: List<NBinding>, opcode: List<OP>, exitStack: List<NBinding>? = null)
    {
        for ((cnt,b) in entryStack.withIndex()) mycheck(b.isBound)
        // If an input is not popped, caller will have it in the entry and exit stacks.
        // then bind will be a no-op because the input is already bound
        if (exitStack != null) for(b in exitStack) b.bind()
        pgm.add(Step(programCount, VmState(*entryStack.toTypedArray()), opcode, if (exitStack==null) VM_NO_CHANGE else VmState(*exitStack.toTypedArray())))
        programCount++
    }

    /** Specifies opcode execution.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * If the opcode is commutative (that is the order of parameters on the stack does not matter), use this API
     * @param entryStack Set because the order of the entry parameters does not matter
     * @param opcode The Nexa VM opcodes to be executed
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     *
     * */
    fun exec(entryStack: Set<NBinding>, opcode: OP, exitStack: List<NBinding>? = null) =
        exec(entryStack, listOf(opcode), exitStack)


    /** Specifies execution of a group of opcodes.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * The entry and exit stack should reflect the complete changes as if this group of opcodes was actually a single instruction.
     * If the order of parameters on the stack does not matter, use this API
     * @param entryStack Specify what parameters are expected to be on the stack before execution.  The END of this list is the TOP of the stack (so the list's order is the 'push' order).  It is assumed that these stack items are consumed.  To indicate an items is not consumed, it should be specified as part of the output stack
     * @param opcode The Nexa VM opcodes to be executed
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     */
    fun exec(entryStack: Set<NBinding>, opcode: List<OP>, exitStack: List<NBinding>? = null)
    {
        for (b in entryStack) mycheck(b.isBound)
        // If an input is not popped, caller will have it in the entry and exit stacks.
        // then bindnew will be a no-op because the input is already bound
        if (exitStack != null) for(b in exitStack) b.bind()
        pgm.add(Step(programCount, VmState(*entryStack.toTypedArray()), opcode, if (exitStack==null) VM_NO_CHANGE else VmState(*exitStack.toTypedArray()), false))
        programCount++
    }

    /** Specifies execution of a group of opcodes.  This is a helper function; it is meant to be wrapped in a type-safe opcode specific function.
     * The entry and exit stack should reflect the complete changes as if this group of opcodes was actually a single instruction.
     * If the order of parameters on the stack does not matter, use this API
     * @param entryStack Specify what parameters are expected to be on the stack before execution.  The END of this list is the TOP of the stack (so the list's order is the 'push' order).  It is assumed that these stack items are consumed.  To indicate an items is not consumed, it should be specified as part of the output stack
     * @param opcode The Nexa VM opcodes to be executed
     * @param exitStack Specify what parameters are placed on the stack by the execution of this opcode.  The END of this list is the TOP of the stack (as if the implementation of this opcode executed "push" in the list order).
     */
    fun exec(entry: VmState, opcode: List<OP>, exit: VmState = VM_NO_CHANGE)
    {
        mycheck(entry.isBound)
        // If an input is not popped, caller will have it in the entry and exit stacks.
        // then bindnew will be a no-op because the input is already bound
        if (exit != null) exit.bind()
        pgm.add(Step(programCount, entry, opcode, if (exit==null) VM_NO_CHANGE else exit, false))
        programCount++
    }


    fun if_(condition: NBinding, then: NSL.()->Unit, else_: (NSL.()->Unit)? = null):List<NBinding>
    {
        val v = condition.value
        // execute this if statement right now
        if (v != null)
        {
            // VM script handling of Int verses ByteArray is weird.  Some opcodes handle them uniformly, but others require bytearrays to be transformed
            // into scriptnums via NUM2BIN.  So they need to be modeled as disjoint types (not derivation), and when an opcode can handle either one,
            // it needs to do so individually.
            val result: Boolean = when (v)
            {
                is Int -> if (v == 0) false else true
                is UInt -> if (v == 0) false else true
                is Long -> if (v == 0) false else true
                is ULong -> if (v == 0) false else true
                is ByteArray -> v.asVmBool()
                is Boolean -> v
                else -> throw NplException("Unrecognized primitive type ${v::class.simpleName}")
            }
            if (result)
            {
                val thenClause = NSL.clause(then)
                return thenClause.endVm.stack + thenClause.endVm.alt
            }
            else
            {
                if (else_ != null)
                {
                    val elseClause = NSL.clause(else_)
                    return elseClause.endVm.stack + elseClause.endVm.alt
                }
            }
        }
        // Generate the code needed to execute this statement
        else
        {
            val step = Step(programCount, VmState(condition), listOf(OP.IF), VM_NO_CHANGE, false)
            programCount++
            step.unifySubEntryStack = true

            val thenClause = NSL.clause(then)
            if (else_ == null)
            {
                check(thenClause.endVm == VM_NO_CHANGE)  // if the then clause returns something, the else must as well
                step.sub.add(thenClause)
                step.exit = VmState(thenClause.endVm)
                pgm.add(step)
                return step.exit.stack + step.exit.alt
            }
            else
            {
                val elseClause = NSL.clause(else_)
                step.sub.add(thenClause)
                step.sub.add(elseClause)


                // If either side causes script failure, then that side's exit stack does not matter.
                // (so set it to the exit of the other side to make this alg simpler)
                var tc = thenClause.endVm
                var ec = elseClause.endVm
                if (tc.failed) tc = ec
                if (ec.failed) ec = tc

                if (tc.failed)  // both failed
                {
                    // If both sides failed, we can eliminate this entire if statement and replace with fail
                    TODO()
                }

                // We need to make sure that upon exit the two code paths claim a structurally equivalent exit VmState
                mycheck(tc.stack.size == ec.stack.size)

                // And then bind these to a list of the bindings that will be returned
                // to do that we xxx need to create a fake operation that consumes the endVm bindings and produces the unified ones
                //               just need to output them in the step
                val exitStack = mutableListOf<NBinding>()
                for (i in 0 .. tc.stack.lastIndex)
                {
                    // If both paths produce exactly the same binding in the same location, then return it directly
                    // this won't work because you get the binding being created in 2 locations (the original creation, and this if statement)
                    if (false) //(tc.stack[i] == ec.stack[i])
                        //exitStack.add(tc.stack[i])
                    else // make a new binding for this stack location
                    {
                        // TODO create the same type as the then and else clauses
                        val tmp = NBinding(null)
                        tmp.bind()
                        exitStack.add(tmp)
                    }
                }

                check(tc.alt.size == ec.alt.size)
                val exitAlt = mutableListOf<NBinding>()
                for (i in 0 .. tc.alt.lastIndex)
                {
                    // If both paths produce exactly the same binding in the same location, then return it directly
                    if (tc.alt[i] == ec.alt[i])
                        exitAlt.add(tc.alt[i])
                    else // make a new binding for this stack location
                    {
                        // TODO create the same type as the then and else clauses
                        val tmp = NBinding(null)
                        tmp.bind()
                        exitAlt.add(tmp)
                    }
                }
                step.exit = VmState(exitStack, exitAlt)
                //for(b in exitStack) b.bind()
                //for(b in exitAlt) b.bind()
                pgm.add(step)
                // Give all the bindings back to the caller of the if statement
                return exitStack + exitAlt
            }

        }

        return listOf()
    }


    /** SPLIT primitive instruction */
    fun split(data: NBytes, location: NInt): Pair<NBytes, NBytes>
    {
        val prefix = NBytes()
        val suffix = NBytes()

        val d = data.curVal
        val l = location.curVal
        if (d != null && l != null)
        {
            if (l >= d.size)
                throw NplException("ERROR: Attempt to split '${data.name?:"unnamed variable"}' of length ${d.size} at position $l.  Data is: ${d.toHex()}")
            var tmp: ByteArray = d.slice(0 until l.toInt()).toByteArray()
            assert(tmp.size == l.toInt())
            prefix.setValue(tmp)

            tmp = d.slice(l.toInt() until d.size).toByteArray()
            suffix.setValue(tmp)
        }
        else
            exec(listOf(data, location), OP.SPLIT, listOf(prefix, suffix))
        return Pair(prefix, suffix)
    }

    /** This API allows you to specify the binding destinations, so that you can specify their (derived) types.
     * The passed prefix and suffix bindings MUST be unbound. */
    fun<A: NBinding, B: NBinding> splitInto(data: NBytes, location: NInt, prefix:A, suffix: B): Pair<A, B>
    {
        mycheck(!prefix.isBound)
        mycheck(!suffix.isBound)
        val d = data.curVal
        val l = location.curVal
        if (d != null && l != null)
        {
            var tmp: ByteArray = d.slice(0 until l.toInt()).toByteArray()
            prefix.setValue(tmp)

            tmp = d.slice(l.toInt() until d.size).toByteArray()
            suffix.setValue(tmp)
        }
        else exec(listOf(data, location), OP.SPLIT, listOf(prefix, suffix))
        return Pair(prefix, suffix)
    }

    /** This API allows you to specify the binding destinations, so that you can specify their (derived) types.
     * The passed prefix and suffix bindings MUST be unbound. */
    fun<B: NBinding> splitSuffixInto(data: NBytes, location: NInt, suffix: B): B
    {
        val d = data.curVal
        val l = location.curVal
        if (d != null && l != null)
        {
            val tmp:ByteArray = d.slice(l.toInt() until d.size).toByteArray()
            suffix.setValue(tmp)
        }
        else
        {
            var prefix = NBytes("unused_prefix")
            prefix.unused = true  // avoid a warning by explicitly acknowledging this will not be used
            exec(listOf(data, location), OP.SPLIT, listOf(prefix, suffix))
        }
        return suffix
    }


    /** This API allows you to specify the binding destinations, so that you can specify their (derived) types.
     * The passed prefix and suffix bindings MUST be unbound. */
    fun<A: NBinding> splitPrefixInto(data: NBytes, location: NInt, prefix:A): A
    {
        val d = data.curVal
        val l = location.curVal
        if (d != null && l != null)
        {
            var tmp: ByteArray = d.slice(0 until l.toInt()).toByteArray()
            prefix.setValue(tmp)
        }
        else
        {
            var suffix = NBytes("unused_suffix")
            suffix.unused = true
            exec(listOf(data, location), OP.SPLIT, listOf(prefix, suffix))
        }
        return prefix
    }

    fun splitLeSignMagInt(data: NBytes, location: NInt, pfxName:String? = null, suffixName: String? = null): Pair<NInt, NBytes>
    {
        val prefix = NInt(pfxName)
        val suffix = NBytes(suffixName)
        val d = data.curVal
        val l: Long? = location.curVal
        if (d != null && l != null) // Do an evaluation if possible
        {
            var tmp: ByteArray = d.slice(0 until l.toInt()).toByteArray()
            prefix.curVal = tmp.leSignMag()

            tmp = d.slice(l.toInt() until d.size).toByteArray()
            suffix.curVal = tmp
        }
        else
        {
            exec(listOf(data, location), OP.SPLIT, listOf(prefix, suffix))
        }

        return Pair(prefix, suffix)
    }


    /** The index of the input being evaluated */
    fun thisIndex():NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt("thisIndex")
            exec(listOf(), OP.INPUTINDEX, listOf(ret))
            return ret
        }
        else
        {
            if (evalConfig.thisIndex == null)
                throw NplException("Simulation execution requires context information: index of currently executing input (evalConfig.thisIndex)")
            return evalConfig.thisIndex!!.invoke(cc.nsl)
        }
    }

    /** The number of inputs in this transaction */
    fun inputCount():NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt("inputSize")
            exec(listOf(), OP.TXINPUTCOUNT, listOf(ret))
            return ret
        }
        else return evalConfig.inputCount!!.invoke(cc.nsl)
    }

    /** The number of outputs in this transaction */
    fun outputCount():NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt("outputSize")
            exec(listOf(), OP.TXOUTPUTCOUNT, listOf(ret))
            return ret
        }
        else return evalConfig.outputCount!!.invoke(cc.nsl)
    }

    /** The constraint script for a particular input (the script in the spent utxo) */
    fun inputUtxoHash(idx: NInt):NBytes
    {
        if (evalConfig.compiling)
        {
            val ret = NBytes("utxoId")
            exec(listOf(idx), OP.OUTPOINTTXHASH, listOf(ret))
            return ret
        }
        else
        {
            if (evalConfig == null)
                throw NplException("Simulation execution requires context information")
            val tx = evalConfig.tx
            if (tx != null) return NCBytes(tx.inputs[idx.curVal!!.toInt()].spendable.outpoint!!.toByteArray())
            else throw NplException("Simulation execution requires context information: the transaction")
        }
    }

    /** The constraint script for a particular input (the script in the spent utxo) */
    fun inputConstraintScript(idx: NInt):NScript
    {
        if (evalConfig.compiling)
        {
            val ret = NScript("inputScript")
            exec(listOf(idx), OP.UTXOBYTECODE, listOf(ret))
            return ret
        }
        else
        {
            val ics = evalConfig.inputConstraintScript
            if (ics != null)
            {
                val ret = ics.invoke(cc.nsl, idx)
                if (ret != null) return ret
            }
            val utxos = evalConfig.utxos
            if (utxos != null) return NScript(utxos[idx.curVal!!.toInt()].script)
            throw NplException("Simulation execution requires context information: the transaction or the constraint script AKA prevout scriptpubkey (evalConfig.inputConstraintScript)")
        }
    }

    /** The solution script for a particular input (the script that satisfies the utxo constraints) */
    fun inputSatisfierScript(idx: NInt):NScript
    {
        if (evalConfig.compiling)
        {
            val ret = NScript("inputScript")
            exec(listOf(idx), OP.INPUTBYTECODE, listOf(ret))
            return ret
        }
        else return evalConfig.inputConstraintScript!!.invoke(cc.nsl,idx)
    }

    /** CHECKSIGVERIFY primitive instruction */
    fun checkSigVerify(sig: NSig, pubkey: NPubKey)
    {
        val s = sig.curVal
        val p = pubkey.curVal
        if (s != null && p != null)
        {
            if (evalConfig.checkSigResult == false) throw VerifyException("Artificial (configured) failure of CHECKSIGVERIFY")
            else if (evalConfig.checkSigResult == null)
            {
                // TODO actually execute
                TODO("emulate checksigverify")
            }
            else
            {
                // forcing correct evaluation -- nothing to do
                // println("Artificial (configured) success of CHECKDATASIGVERIFY")
            }
        }
        else
            exec(listOf(sig, pubkey), OP.CHECKSIGVERIFY)
    }

    /** CHECKDATASIGVERIFY primitive instruction
     * @param sig oracle signature of the provided message
     * @param msg the message signed by the oracle
     * @param pubkey the public key of the oracle
     * */
    fun checkDataSigVerify(sig: NSig, msg: NBytes, pubkey: NPubKey)
    {
        // Do an evaluation if possible
        if ((sig.curVal != null)&&(msg.curVal != null)&&(pubkey.curVal != null))
        {
            if (evalConfig.checkDataSigResult == false) throw VerifyException("Artificial (configured) failure of CHECKDATASIGVERIFY")
            else if (evalConfig.checkDataSigResult == null)
            {
                // TODO actually execute a checkdatasig
                TODO()
            }
            else
            {
                // forcing correct evaluation -- nothing to do
                // println("Artificial (configured) success of CHECKDATASIGVERIFY")
            }

        }
        else
        {
            exec(listOf(sig, msg, pubkey), OP.CHECKDATASIGVERIFY)
        }

    }


    /** CHECKSEQUENCEVERIFY primitive instruction */
    fun checkSequenceVerify(seq: NInt)
    {
        if (evalConfig.compiling)
        {
            exec(seq, OP.CHECKSEQUENCEVERIFY, seq)
        }
        else
        {
            if (evalConfig.checkSequenceVerify == null)
                throw NplException("Simulation execution requires a simulated checkLockTimeVerify (evalConfig.checkLockTimeVerify)")
            else evalConfig.checkSequenceVerify!!.invoke(cc.nsl, seq)
        }
    }

    /** CHECKLOCKTIMEVERIFY primitive instruction */
    fun checkLockTimeVerify(locktime: NInt)
    {
        if (evalConfig.compiling)
        {
            exec(locktime, OP.CHECKLOCKTIMEVERIFY, locktime)
        }
        else
        {
            if (evalConfig.checkLockTimeVerify == null)
                throw NplException("Simulation execution requires a simulated checkLockTimeVerify (evalConfig.checkLockTimeVerify)")
            else evalConfig.checkLockTimeVerify!!.invoke(cc.nsl, locktime)
        }
    }

    /** 0NOTEQUAL primitive instruction */
    fun notEqualZero(b: NInt): NInt
    {
        val ret = NInt(null)
        if (b.curVal != null)
        {
            ret.curVal = if (b.curVal!! != 0.toLong()) 1L else 0L
        }
        else
        {
            exec(b, OP.NOTEQUAL0, ret)
        }
        return ret
    }

    /** Returns the group id of a named group that is part of this project */
    fun groupIdOf(name: String):NGroupId
    {
        val inst = evalConfig.instantiations
        if (inst != null)
        {
            return inst[name]!!
        }
        else
        {
            cc.nsl.recompileAfterInstantiation = true
            // Since we don't know the groupID yet, just use the name of the group as some bytes to act as a stand-in
            return NGroupId(name.toByteArray().copyOf(32).nx)
        }
    }


    /** returns how many inputs of group 'gid' exist in the transaction under analysis */
    fun countInputsByGroup(gid: NGroupId): NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt(null)
            exec(
                VmState(listOf(NCInt(OP.PushTxState.GROUP_INCOMING_COUNT.v), gid), listOf(), setOf(ThisTransaction)),
                listOf(OP.CAT, OP.PUSH_TX_STATE),
                VmState(listOf(ret), listOf(), setOf(ThisTransaction))
            )
            return ret
        }
        else
        {
            if (evalConfig.countInputsByGroup == null)
                throw NplException("Simulation execution requires context information: count of grouped inputs (evalConfig.countInputsByGroup)")
            else return evalConfig.countInputsByGroup!!.invoke(cc.nsl, gid)
        }
    }

    /** returns how many outputs of group 'gid' exist in the transaction under analysis */
    fun countOutputsByGroup(gid: NGroupId): NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt()
            exec(
                VmState(listOf(NCInt(OP.PushTxState.GROUP_OUTGOING_COUNT.v), gid), listOf(), setOf(ThisTransaction)),
                listOf(OP.CAT, OP.PUSH_TX_STATE),
                VmState(listOf(ret), listOf(), setOf(ThisTransaction))
            )
            return ret
        }
        else
        {
            if (evalConfig.countOutputsByGroup == null)
                throw NplException("Simulation execution requires context information: count of grouped outputs (evalConfig.countOutputsByGroup)")
            else return evalConfig.countOutputsByGroup!!.invoke(cc.nsl, gid)
        }
    }

    /** Given a group id and a zero-based index (n), returns the output index of the nth grouped output
     * @param gid enumerate only the outputs sent to this group
     * @param n   zero based index of the enumerated outputs
     * @return output index (typically for use with other introspection opcodes) */
    fun groupedOutputN(gid: NGroupId, n: NInt): NInt
    {
        if (evalConfig.compiling)
        {
            val ret = NInt()
            exec(
                VmState(listOf(NCInt(OP.PushTxState.GROUP_NTH_OUTPUT.v), gid, n), listOf(), setOf(ThisTransaction)),
                listOf(OP.C2, OP.NUM2BIN, OP.SWAP, OP.CAT, OP.CAT, OP.PUSH_TX_STATE),
                VmState(listOf(ret), listOf(), setOf(ThisTransaction))
            )
            return ret
        }
        else
        {
            if (evalConfig.groupedOutputN == null)
                throw NplException("Simulation execution requires context information: index of the Nth grouped Output (evalConfig.groupedOutputN)")
            else return evalConfig.groupedOutputN!!.invoke(cc.nsl, gid, n)
        }
    }

    /** Returns the constraint (output) script specified for the passed output index.
     * If the index is out of bounds, the script fails.
     * @param idx zero based index of the input whose prevout script you will receive
     * @return A Compiled script (i.e. script bytecode)
     * */
    fun constraintScriptForOutputN(idx: NInt): NScript
    {
        if (evalConfig.compiling)
        {
            val ret = NScript()
            exec(
                VmState(listOf(idx), listOf(), setOf(ThisTransaction)),
                listOf(OP.OUTPUTBYTECODE),
                VmState(listOf(ret), listOf(), setOf(ThisTransaction))
            )
            return ret
        }
        else
        {
            if (evalConfig.constraintScriptForOutputN != null)
                return evalConfig.constraintScriptForOutputN!!.invoke(cc.nsl, idx)
            val tx = evalConfig.tx
            if (tx != null)
            {
                val i = idx.curVal!!
                if (i >= tx.outputs.size) throw NplException("Provided transaction has only ${tx.outputs.size} outputs, but asking for locking script for output #${i}")
                return NScript(tx.outputs[i.toInt()].script, "lockingScriptO$i")
            }
            throw NplException("Simulation execution requires context information: Nth constraint script AKA Nth prevout script (evalConfig.constraintScriptForOutputN)")
        }
    }



    /** Fails the script if the items are not numerically equal */
    infix fun NBinding.numEqualVerify(b: NBinding)
    {
        val cv = value
        val bv = b.value
        if (cv != null && bv != null)
        {
            if (cv is Long)
            {
                if (cv as Long != bv as Long)
                    throw VerifyError("VERIFY FAILURE: numEqualVerify $this (${this.value})  != $b (${b.value})")
            }
            if (cv is ByteArray)
            {
                //if (cv != bv as ByteArray)
                if (cv.leSignMag() != (bv as ByteArray).leSignMag())
                    throw VerifyError("VERIFY FAILURE: numEqualVerify $this = ${this.value} with $b = ${b.value}")
            }
        }
        else
        {
            mycheck(this is NInt)  // Use equalVerify for non-numbers
            mycheck(b is NInt)  // Use equalVerify for non-numbers
            mycheck(cc.compiling == true)
            exec(setOf(this, b), OP.NUMEQUALVERIFY, listOf())
        }
    }

    /** returns true or false if the top two stack items are numerically equal */
    infix fun NBinding.numEqual(b: NBinding): NBool
    {
        val ret = NBool()
        val cv = value
        val bv = b.value
        if (cv != null && bv != null)
        {
            if (cv is Long)
            {
                ret.curVal = if (cv as Long == bv as Long) 1L else 0L
            }
            if (cv is ByteArray)
            {
                ret.curVal = if (cv.leSignMag() == (bv as ByteArray).leSignMag())  1L else 0L
            }
        }
        else
        {
            exec(setOf(this, b), OP.NUMEQUAL, listOf(ret))
        }
        return ret
    }
}


/** A rule is a specific constraint (contract) within an interface.  At the interface level, spending may occur if any one rule is satisfied */
class Rule(val name:String? = null,
           val templateArgs: List<NBinding>,
           val holderArgs: List<NBinding>,
           val spenderArgs: List<NBinding>,
           val script: NSL,
           val function: (NSL.() -> Unit)? = null)
{
    /** Clear (set to null) the current values of every binding used in this rule (in preparation for a new evaluation) */
    fun clearCurrentBindingValues()
    {
        // the bindingCreated map also contains all bindings because input bindings are considered "created" at entry
        for (b in script.bindingCreated)
        {
            b.key.resetValue()
        }
        // because re-compilation or re-evaluation will call "return" again and push new result values into these stacks
        script.endVm.stack.clear()
        script.endVm.alt.clear()
    }


    /** Evaluate this rule given the provided arguments (which must be the correct type of the template, holder and spender bindings) */
    fun eval(templateParams: List<Any>, holderParams: List<Any>, spenderParams: List<Any>):VmState
    {
        mycheck(templateArgs.size == templateParams.size)
        for (i in 0..templateParams.lastIndex)
        {
            templateArgs[i].setValue(holderParams[i])
        }
        return eval(holderParams, spenderParams)
    }

    fun smeval(holderParams: List<Any>, spenderParams: List<Any>, modifyConfig: EvalConfig.() -> Unit = {})
    {
        val sm = ScriptMachine()
        try
        {
            sm.loadStacks(spenderParams, holderParams)
            val ok = sm.eval(script.script() as SatoshiScript)
            if (!ok) throw NSL.VerifyException("${sm.scriptErr} at pos ${sm.pos}")
            val top = sm.mainStackAt(0)
            if (top != "") throw NSL.StackException("Main stack is not empty")
        }
        finally
        {
            sm.delete()
        }
    }

    /** Evaluate this rule within the script machine with a transaction and utxo context
     * Note that this template script OVERRIDES and is executed INSTEAD of the one provided in the transaction
     */
    fun smeval(tx: NexaTransaction, inputIdx: Int, utxo: NexaTxOutput)
    {
        val sm = ScriptMachine(tx, inputIdx, utxo)
        try
        {
            val ok = sm.eval(script.script())
            if (!ok) throw NSL.VerifyException("${sm.scriptErr} at pos ${sm.pos}")
            val top = sm.mainStackAt(0)
            if (top != "") throw NSL.StackException("Main stack is not empty")
        }
        finally
        {
            sm.delete()
        }
    }


    /** Evaluate this rule given the provided arguments (which must be the correct type of the holder and spender bindings)
     * @param holderParams A list of the holder's parameters (the arguments provided in the constraint script, either committed to as argsHash or provided directly)
     * @param spenderParams A list of the spender's parameters (provided in the spend script)
     * @param config Configuration of the evaluation, including functions that provide transaction context.
     * @throws NslException If the evaluation fails due to an error or VERIFY failure, a [NslException] is thrown
     * @return The final VmState.  Note that the main stack in this VmState must be empty for an actual ScriptTemplate validation to succeed.
     * */
    fun eval(holderParams: List<Any>, spenderParams: List<Any>, config: EvalConfig): VmState
    {
        val evalScript = NSL.eval()
        evalScript.evalConfig = config
        evalScript.evalConfig.compiling = false
        return eval(holderParams, spenderParams, evalScript)
    }

    /** Evaluate this rule given the provided arguments (which must be the correct type of the holder and spender bindings)
     * @param holderParams A list of the holder's parameters (the arguments provided in the constraint script, either committed to as argsHash or provided directly)
     * @param spenderParams A list of the spender's parameters (provided in the spend script)
     * @param modifyConfig You may pass a function that configures the evaluation.
     * @throws NslException If the evaluation fails due to an error or VERIFY failure, a [NslException] is thrown
     * @return The final VmState.  Note that the main stack in this VmState must be empty for an actual ScriptTemplate validation to succeed.
     * */
    fun eval(holderParams: List<Any>, spenderParams: List<Any>, modifyConfig: EvalConfig.() -> Unit = {}): VmState
    {
        val evalScript = NSL.eval()
        evalScript.evalConfig.modifyConfig()
        return eval(holderParams, spenderParams, evalScript)
    }


    /** Evaluate this rule given the provided arguments (which must be the correct type of the holder and spender bindings)
     * @param holderParams A list of the holder's parameters (the arguments provided in the constraint script, either committed to as argsHash or provided directly)
     * @param spenderParams A list of the spender's parameters (provided in the spend script)
     * @param evalContext What context this evaluation is executing in.
     * @throws NslException If the evaluation fails due to an error or VERIFY failure, a [NslException] is thrown
     * @return The final VmState.  Note that the main stack in this VmState must be empty for an actual ScriptTemplate validation to succeed.
     * */
    fun eval(holderParams: List<Any>, spenderParams: List<Any>, evalContext: NSL): VmState
    {
        val ret = cc.with(evalContext)
        {
            clearCurrentBindingValues()

            // Attach all the bindings to this script language instance,
            // and initialize their current values to what is passed to eval.
            mycheck(holderArgs.size == holderParams.size)
            mycheck(spenderArgs.size == spenderParams.size)

            for (i in 0..holderParams.lastIndex)
            {
                holderArgs[i].setValue(holderParams[i])
            }

            for (i in 0..spenderParams.lastIndex)
            {
                spenderArgs[i].setValue(spenderParams[i])
            }

            // Closest method to original definition
            // Call the construction function with args bound to values
            val f = function
            if (f != null)
            {
                val e = f(evalContext)

                evalContext.pgm.clear()
                return@with evalContext.endVm // script.endBindings
            }
            else  // if no construction function then go through the program executing
            {
                TODO()
                for (p in evalContext.pgm)
                {

                }
                return@with VmState()
            }
        }
        return ret
    }


    /** Evaluate this rule given the provided arguments (which must be the correct type of the holder and spender bindings) */
    /*
    fun eval(holderParams: List<Any>, spenderParams: List<Any>, modifyConfig: EvalConfig.() -> Unit = {}): VmState
    {
        val evalScript = NSL.eval()
        val ret = cc.with(evalScript)
        {
            clearCurrentBindingValues()

            // Attach all the bindings to this script language instance,
            // and initialize their current values to what is passed to eval.
            mycheck(holderArgs.size == holderParams.size)
            mycheck(spenderArgs.size == spenderParams.size)

            for (i in 0..holderParams.lastIndex)
            {
                holderArgs[i].setValue(holderParams[i])
            }

            for (i in 0..spenderParams.lastIndex)
            {
                spenderArgs[i].setValue(spenderParams[i])
            }

            // Closest method to original definition
            // Call the construction function with args bound to values
            val f = function
            if (f != null)
            {
                evalScript.evalConfig.modifyConfig()
                val e = f(evalScript)

                evalScript.pgm.clear()
                return@with evalScript.endVm // script.endBindings
            }
            else  // if no construction function then go through the program executing
            {
                TODO()
                for (p in evalScript.pgm)
                {

                }
                return@with VmState()
            }
        }
        return ret
    }
     */

    override fun toString(): String
    {
        // unfortunately a lot of customization would still be needed to print only what is wanted
        //val js = Json { prettyPrint = true }
        //val s = js.encodeToString(serializer(),this)

        val s = StringBuilder()
        val ha = StringJoiner(", ")
        val sa = StringJoiner(", ")
        for (a in holderArgs)
        {
            ha.add(a.toString())
        }
        for (a in spenderArgs)
        {
            sa.add(a.toString())
        }

        s.append("""{ "type": "constraint", "name" : "$name", "holderArgs" : [ $ha ], "spenderArgs": [ $sa ], "script" : {$script} } """)
        return s.toString()
    }

}



/** Specify a rule.  A rule is a specific constraint within an interface.  At the interface level, spending may occur if any rule is satisfied */
class RuleBuilder(var name: String? = null,
                  var templateArgs: List<NBinding>?,
                  var holderArgs: List<NBinding>?,
                  var spenderArgs: List<NBinding>?)
{
    protected var _script: (NSL.() -> Unit)? = null

    fun script(block: NSL.() -> Unit)
    {
        _script = block
    }

    /** Specify the template arguments.
     * These arguments are resolved at compile time
     */
    fun templateArgs(vararg bindings: NBinding) { templateArgs = bindings.toList() }

    /** Specify the holder arguments.
     * These arguments are supplied by the argsHash and args fields in a UTXO (or transaction output), in the script template style output.
     * They are located on the altstack when the template is executed.
     */
    fun holderArgs(vararg bindings: NBinding) { holderArgs = bindings.toList() }

    /** Specify the spender arguments.
     * These arguments are supplied by the input script in a transaction.
     * They are located on the main stack when the template is executed.
     */
    fun spenderArgs(vararg bindings: NBinding) { spenderArgs = bindings.toList() }

    /** When the rule is fully defined, build() is called to transform the ruleBuilder into a Rule */
    fun build(): Rule
    {
        val s = _script
        if (s == null) throw IllegalStateException("Spend Constraint Rule ${name} is not implemented")
        val nsl = NSL()
        nsl.evalConfig.compiling = true

        if (templateArgs == null) templateArgs = listOf()
        if (spenderArgs == null) spenderArgs = listOf()
        if (holderArgs == null) holderArgs = listOf()

        // Prepare the args for the dataflow compilation
        for ((i,v) in templateArgs!!.withIndex())
        {
            v.bind("templateArg$i")
        }
        for ((i,v) in spenderArgs!!.withIndex()) v.bind("spenderArg$i")
        for ((i,v) in holderArgs!!.withIndex()) v.bind("holderArg$i")

        // This actually executes the script transformation, effectively doing the code to dataflow compilation
        cc.with(nsl) {
            nsl.apply(s)
        }

        nsl.startVm = VmState(spenderArgs!!, holderArgs!!)
        return Rule(name, templateArgs!!, holderArgs!!, spenderArgs!!, nsl ?: throw IllegalArgumentException("All spend constraints need an associated script"), s)
    }
}


/** An interface is a group of top level spending rules that together capture some human-level concept.
 * The spender can satisfy the contract's constraints by executing any one of the rules
 * (which are defined by what needs to be pushed onto the stack to satisfy them -- effectively the parameters of the rule's function).
 * Groups implement 1 or more interfaces.  Different groups can contain the same interface,
 * in which case each group may implement all the rules in each interface, differently.  But a rule's parameters must remain the same
 * so that wallets can spend a group without code being written for that group specifically -- only one of the interfaces in that group.
 * */
class Interface(val name: String, val rules: Map<String, Rule>)
{

    val hashId: ByteArray
        get() {
            // TODO calculate a unique hash based on the spend rule interfaces
            return byteArrayOf()
        }

    override fun toString(): String
    {
        val s = StringBuilder()
        val rs = StringJoiner(", ")
        for (r in rules)
        {
            rs.add(""" "${r.key}":${r.value} """)
        }
        s.append("""{ "type": "Interface", "name" : "$name", "id" : "${hashId.toHex()}", "rules" : "{ ${rs} }" } """)
        return s.toString()

    }

    /** Compile all rules into Nexa script binary */
    fun compile(onlyIfNeeded: Boolean = false)
    {
        for (r in rules)
        {
            val rule = r.value
            val script = rule.script
            if ((r.value.script.recompileAfterInstantiation == false) && (script.endPgm != null))
            {
                if (onlyIfNeeded) continue
            }
            println("\n\nscript")
            println(rule.script.toString())
            println("done")
            script.breakIntoClauses()  // Splits the program by conditionals because we cannot sort across them
            println("\n\nscript broken into clauses")
            println(rule.script.toString())
            println("done")
            script.precompile()
            script.compile()
            script.compilationCleanup()
        }
    }
}

fun bind(prefix: String, bindingList:List<NBinding>?)
{
    if (bindingList != null)
        for ((cnt, b) in bindingList.withIndex())
            if (!b.isBound) b.bind("${prefix}${cnt}")
}

class InterfaceBuilder
{
    private var spendRules = mutableMapOf<String, Rule>()

    var name = "anonymous"

    fun rule(name: String, templateArgs:List<NBinding>?=null, holderArgs:List<NBinding>?=null, spenderArgs:List<NBinding>?=null, block: RuleBuilder.() -> Unit)  // top level rules must be named
    {
        bind("templateArg", templateArgs)
        bind("holderArgs", holderArgs)
        bind("spenderArgs", spenderArgs)
        spendRules[name] = RuleBuilder(name, templateArgs, holderArgs, spenderArgs).apply(block).build()
    }

    fun build(): Interface = Interface(name, spendRules)
}




class AddressBuilder(var name: String)
{
    fun build(): NAddress = NAddress(name)
}


