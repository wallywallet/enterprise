package Nexa.npl

/** Return the group id of the running script's input */
fun NSL.thisGroup(): NGroupId
{
    val myin = thisIndex()
    val utxo = inputConstraintScript(myin)
    val (first, rest) = utxo.splitPush()
    return NGroupId(first)
}

/** Returns the template hash of the running script */
fun NSL.thisTemplateHash(): NBytes
{
    val myin = thisIndex()
    val utxo = inputConstraintScript(myin)
    val (tmpl, args) = utxo.templateAndArgsHash()
    return(tmpl)
}

/** Returns the template hash of the running script */
fun NSL.thisArgsHash(): NBytes
{
    val myin = thisIndex()
    val utxo = inputConstraintScript(myin)
    val (tmpl, args) = utxo.templateAndArgsHash()
    return(args)
}

fun NSL.thisInputUtxoHash(): NBytes
{
    val myin = thisIndex()
    val utxo = inputUtxoHash(myin)
    return(utxo)
}

/** Returns the template hash and args hash of the constraint of the running script */
fun NSL.thisTemplateAndArgsHash(): Pair<NBytes, NBytes>
{
    val myin = thisIndex()
    val utxo = inputConstraintScript(myin)
    return utxo.templateAndArgsHash()
}


/** Check that a single output exists to this group, and that the output spends to the provided address */
fun NSL.mustSpendGroupToP2pkt(gid: NGroupId, addr: NAddress)
{
    val inCount = countOutputsByGroup(gid)
    inCount numEqualVerify 1.nx  // this group should be paid to just one output
    val gout = groupedOutputN(gid, 0.nx)  // So of course its group index 0
    val cs = constraintScriptForOutputN(gout)

    val (tmpl, args) = cs.templateAndArgsHash()
    NInt(tmpl) numEqualVerify 1.nx // Verify that this is the well-known p2pkt template type

    val argsVal:ByteArray = addr.argsHash.curVal!!
    args equalVerify addr.argsHash
}

/** Check that a single output exists to this group, and pass a function that further constrains that output's args hash */
fun NSL.mustSpendGroupToP2pkt(gid: NGroupId, argsCheck: (NBytes) -> Unit)
{
    val inCount = countOutputsByGroup(gid)
    inCount numEqualVerify 1.nx  // this group should be paid to just one output
    val gout = groupedOutputN(gid, 0.nx)  // So of course its group index 0
    val cs = constraintScriptForOutputN(gout)

    val (tmpl, args) = cs.templateAndArgsHash()
    NInt(tmpl) numEqualVerify 1.nx // Verify that this is the well-known p2pkt template type
    argsCheck(args)
}

/** do an operation over no more than N outputs of a given group.  Fail if there are more grouped outputs.
 * Since Nexa scripting does not have loops, this unrolls [doit] N times.
 * @param doit A function that accepts an output index (that will be grouped)
 * */
fun NSL.forGroupedOutputs(N: Int, gid: NGroupId, doit: (NInt) -> Unit)
{
    val outCount = countOutputsByGroup(gid)
    outCount numEqualVerify N.nx
    for(i in 0 until N)
    {
        if_ (i.nx lt outCount, {
            val gout = groupedOutputN(gid, i.nx)
            doit(gout)
        })
    }
}

/** tests that all the passed groups exist as outputs in this transaction.  */
fun NSL.groupsOut(vararg gids: NGroupId)
{
    for (gid in gids)
    {
        val inCount = countOutputsByGroup(gid)
        verify(inCount)
    }
}

/** tests that all the passed groups exist as inputs in this transaction.  */
fun NSL.groupsIn(vararg gids: NGroupId)
{
    for (gid in gids)
    {
        val inCount = countInputsByGroup(gid)
        verify(inCount)
    }
}


/** If the group amount field is constrained to a 2 byte push, this is very efficiently implemented */
fun NSL.mustSpendLowQuantityGroupToP2pkt(gid: NGroupId, argsCheck: (NBytes) -> Unit)
{
    val inCount = countOutputsByGroup(gid)
    inCount numEqualVerify 1.nx  // this group should be paid to just one output
    val gout = groupedOutputN(gid, 0.nx)  // So of course its group index 0
    val cs = constraintScriptForOutputN(gout)

    // The output MUST be: push(groupId) push(group_quantity) OP_1 push(argsHash)
    //                     33            3                     1     1   20 or 32
    val (opcode: NInt, argsHash: NBytes) = splitLeSignMagInt(cs, (33+3+1+1).nx, "unused", "argsHash")
    argsCheck(argsHash)
}


