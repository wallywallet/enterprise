// Copyright (c) 2023 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
@file:OptIn(ExperimentalFoundationApi::class)

package org.wallywallet.wew

import androidx.compose.foundation.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.key
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.key.utf16CodePoint
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import org.nexa.libnexakotlin.blockchains
import org.wallywallet.composing.*

var dashboardFontScale = 0.8

fun dashboardFontSize() = defaultTextFontSize.scale(dashboardFontScale)

@Composable
fun DashboardPanel(panelState: InterpreterPanelState)
{
    val cmdLine = enteredCmdLine.collectAsState()
    val scrollState = rememberScrollState(0)

    Row(Modifier.fillMaxSize().onKeyEvent {
        if (it.utf16CodePoint == 10) // enter codepoint is 10
        {
            org.nexa.libnexakotlin.launch(panelState.dw.coScope) { panelState.execute(cmdLine.value.text) }
        }
        true
    }) {
        SelectionContainer()
        {
            Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.Top)
            {
                val headerStyle = TextStyle(fontWeight = FontWeight.Bold, fontSize = defaultTextFontSize)
                val textStyle = TextStyle(fontSize = dashboardFontSize())
                Column(Modifier.fillMaxWidth(), verticalArrangement = Arrangement.Top)
                {
                    Text(_x_("Blockchains"), Modifier.fillMaxWidth(), style = headerStyle)
                    Text(panelState.blockchainInfo, style = textStyle, overflow = TextOverflow.Clip)

                    /*
                    for (b in panelState.blockchainDisplay)
                    {
                        b.value.compose()
                    }
                     */
                }
                Column(Modifier.fillMaxWidth(), verticalArrangement = Arrangement.Top)
                {
                    Text(_x_("Wallets"), style = headerStyle)
                    Text(panelState.walletInfo, style = textStyle, overflow = TextOverflow.Clip, softWrap = false)
                    //for (w in panelState.walletDisplay) w.value.compose()
                }

                if (panelState.completionChoices.value.isNotEmpty())
                {

                    Text(_x_("Completing") + " " + panelState.completionObject, style = headerStyle)
                    Column(Modifier.fillMaxWidth().weight(1f).verticalScroll(scrollState).padding(10.dp).pointerHoverIcon(PointerIcon.Hand)
                        , verticalArrangement = Arrangement.Top)
                    {
                        for(i in panelState.completionChoices.value)
                        {
                            val interactionSource = remember { MutableInteractionSource() }
                            val isHovered = interactionSource.collectIsHoveredAsState()
                            val objPath = if (panelState.completionObject.isBlank()) i else (panelState.completionObject + "." + i)

                            if (isHovered.value)
                            {
                                if (panelState.helpForObject != objPath)
                                {
                                    panelState.helpForObject = objPath
                                    val out = StringOutput()
                                    outputHelpFor(panelState.helpForObject, out)
                                    panelState.help = out.toString()
                                }
                            }

                            val col = if (i.startsWith(panelState.completing))
                                Color.Green
                            else
                                Color.Gray
                            Text(i, color = col, modifier = Modifier.clickable {
                                panelState.completionUpdater?.invoke(panelState.completionPrefix + objPath)
                            }.pointerHoverIcon(PointerIcon.Hand).hoverable(interactionSource))
                        }
                    }
                }

                if (panelState.helpForObject.isNotBlank())
                {
                    val helpscrollState = rememberScrollState(0)
                    Column(Modifier.fillMaxWidth().fillMaxHeight(0.3f), verticalArrangement = Arrangement.Top)
                    {
                        Text(_x_("Documentation for ") + " " + panelState.helpForObject, style = headerStyle)
                        Column(
                            Modifier.fillMaxWidth().verticalScroll(helpscrollState).padding(10.dp).pointerHoverIcon(PointerIcon.Hand), verticalArrangement = Arrangement.Top
                              )
                        {
                            Text(panelState.help)
                        }
                    }
                }
            }
        }
        VerticalScrollbar(modifier = Modifier.width(10.dp).fillMaxHeight(), adapter = rememberScrollbarAdapter(scrollState))
    }
}