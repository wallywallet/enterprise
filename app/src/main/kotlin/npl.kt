package Nexa.npl

import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.NexaArgs
import java.io.BufferedOutputStream
import java.io.FileOutputStream
import java.nio.file.Path
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import java.util.StringJoiner
import kotlin.NoSuchElementException
import kotlin.io.path.*
private val LogIt = GetLog("nexa.nsl.npl")
/** Base of all custom exceptions */
open class NplException(what: String?=null): Exception(what)

fun Path.load(): String = readText()

class GroupDescriptor(
    var ticker: String? = null,
    var name: String? = null,
    /** Where this document will be placed: if http or https is used, this connects this group with a domain */
    var docUri: String? = null,
    /** The token description document as a JSON string.   */
    var doc:String? = null,
    var decimals: Int? = null,
)
{
    /** Turn this group descriptor into a genesis opreturn script and a Token description document file.  Return that filename */
    @Synchronized
    fun buildGenesisData():SatoshiScript
    {
        val d = doc
        var tddHash: ByteArray? = null
        if (d != null)
        {
            val tdd = decodeTokenDescDoc(d, null)
            tddHash = tdd.tddHash

            // TODO sign the tdd and create the final file
            // val finalTdd = "[" + doc + ",\n\"" + sig.toString() + "\"\n]"
        }
        else
        {
            if (docUri != null) throw NplException("Token Description Document not provided, but a URI location ($docUri) was provided")
        }

        // See https://spec.nexa.org/blockchain/tokens/tokenDescription
        val genesisDesc = SatoshiScript(ChainSelector.NEXA, SatoshiScript.Type.SATOSCRIPT, OP.RETURN, OP.push(88888888),
            ticker?.let {OP.push(it.toByteArray(Charsets.UTF_8)) } ?: OP.PUSHFALSE,
            name?.let {OP.push(it.toByteArray(Charsets.UTF_8)) } ?: OP.PUSHFALSE,
            docUri?.let {OP.push(it.toByteArray(Charsets.UTF_8)) } ?: OP.PUSHFALSE,
            tddHash?.let {OP.push(it) } ?: OP.PUSHFALSE,
            decimals?.let { OP.push(it)} ?: OP.C0)

        return genesisDesc
    }

     @Synchronized
    fun buildTokenDescriptionDoc(wallet: Wallet, genesisAddr: PayAddress, outputFile: Path): Boolean
    {
        val d = doc
        if (d != null)
        {
            val tdd = decodeTokenDescDoc(d, null)

            // Sign the tdd and create the final file
            val sigBin = wallet.signMessage(tdd.signedSlice!!, genesisAddr)
            val sigStr = Codec.encode64(sigBin)
            val finalTdd = "[" + doc + ",\n\"" + sigStr + "\"\n]"
            outputFile.writeText(finalTdd)

            return true
        }
        return false
    }
}

fun Wallet.createGroup(groupFlags: GroupFlags, genesisAuthorityFlags: ULong, opRet: SatoshiScript?=null, genesisAddress: PayAddress? = null): Triple<iTransaction, PayAddress, GroupId>
{
    val cs = chainSelector
    val addr = genesisAddress ?: getNewAddress()

    val gaf = genesisAuthorityFlags or GroupAuthorityFlags.AUTHORITY

    val tx = txFor(cs)

    val genesis = txOutputFor(cs)
    genesis.amount = dust(cs)
    // Fill the output with dummy values to get the size right
    var gid = GroupId(cs, ByteArray(32, {0}))
    genesis.script = addr.groupedLockingScript(gid, 0)
    tx.add(genesis)

    if (opRet != null) tx.add(txOutputFor(cs,0,opRet))

    // We need to get an input, since that is entropy needed to compute a group ID
    txCompleter(tx, 0, TxCompletionFlags.FUND_NATIVE)
    val inpoint = tx.inputs[0]
    if (inpoint == null) throw WalletNotEnoughBalanceException("Wallet cannot fund this transaction")

    val groupFlagBits = groupFlags.toBitfield()

    var nonce = 0x10000UL
    var grpId: ByteArray? = null
    val inpointPrevout = inpoint.spendable.outpoint!!
    do
    {
        val ba = BCHserialized(SerializationType.HASH)
        ba.add(inpointPrevout)
        if (opRet != null && opRet.size != 0) ba.add(opRet)
        nonce = (nonce and (GroupAuthorityFlags.ALL_AUTHORITIES.inv())) or gaf
        ba.addUint64(nonce)
        val grpIdDerivationData = ba.toByteArray()
        grpId = libnexa.hash256(grpIdDerivationData)
        val foundGrpFlags = (grpId[30].toPositiveUInt() shl 8) or grpId[31].toPositiveUInt()
        if (foundGrpFlags == groupFlagBits)
        {
            LogIt.info("grp id data size: ${grpIdDerivationData.size} hex: ${grpIdDerivationData.toHex()}")
            break
        }
        nonce += 0x1UL
    } while(true)

    if (grpId == null) throw WalletException("Cannot calculate group id")
    gid = GroupId(cs, grpId)
    genesis.script = addr.groupedLockingScript(gid, nonce.toLong())
    signTransaction(tx)
    return(Triple(tx, addr, gid))
}

/** This function assumes that the wallet has an authority that allows it to create the specified subgroup */
fun Wallet.createSubgroup(gid: GroupId, nftQty: ULong, opRet: SatoshiScript?=null, genesisAddress: PayAddress? = null):Pair<iTransaction, PayAddress>
{
    val addr = genesisAddress ?: getNewAddress()
    val tx = txFor(chainSelector)

    val genesis = txOutputFor(chainSelector)
    genesis.amount = dust(chainSelector)
    genesis.script = addr.groupedLockingScript(gid, nftQty.toLong())
    tx.add(genesis)

    if (opRet != null) tx.add(txOutputFor(chainSelector,0,opRet))
    txCompleter(tx, 0, TxCompletionFlags.SIGN or TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.FUND_GROUPS or TxCompletionFlags.USE_GROUP_AUTHORITIES)
    if (tx.inputs.size == 0) throw NplException("Failed to fund transaction ${tx.toHex()}")
    return Pair(tx, addr)
}

/** This function assumes that the wallet has an authority that allows it to create the specified subgroup */
fun Wallet.createSubgroups(subgroupMinter: (iTransaction)->Unit):iTransaction
{
    val mint = txFor(chainSelector)
    subgroupMinter(mint)
    txCompleter(mint, 0, TxCompletionFlags.SIGN or TxCompletionFlags.FUND_GROUPS or TxCompletionFlags.USE_GROUP_AUTHORITIES)
    return mint
}

/** Definition of a group */
open class Group(
    var name: String,
    var flags: GroupFlags = DefaultGroupFlags,
    var contract: Contract,
    val authorities: List<AuthorityDefs>,
    val descriptor: GroupDescriptor?,
    val mints: List<InitialQuantities>)
{
    val subgroups = mutableMapOf<String, Subgroup>()

    /** If deployed this will be set to the actual group id of this group */
    var gid: GroupId? = null
    /** The transaction that deployed this group */
    var genesisTx: iTransaction? = null
    /** The address that received the group genesis Authority */
    var genesisAddr: PayAddress? = null

    /** NFT media .zip file bytes */
    var mediaContents: ByteArray? = null

    data class AuthorityDefs(
        /** Authority permission flags */
        val flags:ULong,
        /** Authorities can be constrained by contract */
        var contract: Contract?=null,
        /** Or just to an address.  Or if both are null, a wallet address will be used */
        val address: PayAddress?=null)

    data class InitialQuantities(
        /** Amount to pay */
        val quantity: ULong,
        /** Pay to a contract (if non-null) */
        val contract: Contract? = null,
        /** if paying this quantity to a contract, this is the holder arguments to that contract */
        val holderArgs: Array<out Any>? = null,
        /** Pay to an address (if non-null).  If both contract and address are null, these coins are paid to an address from the current wallet */
        var address: PayAddress? = null
    )


    override fun toString(): String
    {
        val s = StringBuilder()

        val cs = StringJoiner(",")
        for (i in contract.interfaces)
        {
            cs.add(""" "${i.name}":${i} """)
        }
        s.append("""{ "type": "Group", "name" : "$name", "flags": "$flags", "interfaces" : { ${cs} } } """)
        return s.toString()
    }

    /** Add a subgroup below this group */
    open fun add(g: Subgroup)
    {
        if (subgroups.containsKey(g.name)) throw NplException("Attempted to add 2 instances of subgroup ${g.name} to group $name")
        subgroups[g.name] = g
    }

    /** Group create creates this group and adds a genesis Authority into the wallet.  This binds an identifier to the group, so must
     * be called before any group deployments in this project, in case any rules hard-code group identifiers.
     */
    open fun create(wallet: Wallet):List<iTransaction>
    {
        var genesisAuthorityFlags = 0UL
        // Get all the authorities we are going to need to make all the defined final authorities
        for (a in authorities)
        {
            genesisAuthorityFlags = genesisAuthorityFlags or a.flags
        }

        // Even if no mint auth is wanted, I need one now to mint the specified # of tokens
        // I need to pass authorities to children for my own setup (even if no final authorities have BATON capability)
        // and of course I need this to be marked as an authority to create a group
        genesisAuthorityFlags = genesisAuthorityFlags or GroupAuthorityFlags.MINT or GroupAuthorityFlags.BATON or GroupAuthorityFlags.AUTHORITY

        // Even if no subgroup auth is wanted, I need one now to create the subgroups
        if (subgroups.size != 0)
        {
            genesisAuthorityFlags = genesisAuthorityFlags or GroupAuthorityFlags.SUBGROUP
        }

        val genesisData = descriptor?.buildGenesisData()
        val (tx, gAddr, grpId) = wallet.createGroup(flags, genesisAuthorityFlags, genesisData)
        LogIt.info("Created group $name:${grpId.toString()} to ${gAddr}.  tx: $tx")
        gid = grpId
        genesisAddr = gAddr
        genesisTx = tx

        // We need to commit this transaction, so the wallet can use this group
        wallet.send(tx, sync = true)

        return mutableListOf(tx)
    }

    open fun check()
    {
        // Check that subgroup IDs are not repeated
        val ids = mutableMapOf<String, String>()
        for (sg in subgroups)
        {
            val hexId = sg.value.id.toHex()  // Convert to a string because maps can't compare bytearray contents
            if (ids.contains(hexId))
            {
                throw NplException("In group $name, subgroup ${sg.value.name} has the same ID as subgroup ${ids[hexId]}.  This can happen if you accidentally use the same NFT media file for 2 different NFTs.")
            }
            ids[hexId] = sg.value.name
        }
    }

    /** Group deploy produces the configuration defined by this Group specification in this wallet and the blockchain it's connected to.
     * If objects are already created, do not recreate them. */
    open fun deploy(wallet: Wallet):List<iTransaction>
    {
        val tx = genesisTx
        if (tx == null) throw NplException("Group $name deployment: Must call create first")
        val finalFile = Path(name + ".tdd")
        val tdd = descriptor?.buildTokenDescriptionDoc(wallet, genesisAddr!!, finalFile)
        if (tdd == false) throw NplException("Group $name: Unspecified problem building token description document")

        val ret = mutableListOf<iTransaction>()

        for ((name,sg) in subgroups)
        {
            ret.addAll(sg.deploy(wallet))
        }

        ret.addAll(deployMints(wallet))
        deployMedia()
        return ret
    }

    fun deployMedia()
    {
        mediaContents?.let {
            val f = Path(gid!!.toStringNoPrefix())
            f.writeBytes(it)
        }
    }

    /** create transactions that fill all the specified coin creation (called by deploy) */
    fun deployMints(wallet: Wallet):List<iTransaction>
    {
        val ret = mutableListOf<iTransaction>()
        val mintTx = txFor(wallet.chainSelector)
        // Construct a single tx that produces all mints as outputs.
        // This is needed because if a mint pays to this same wallet, a subsequent mint might use that payment rather than the authority.
        // Its also more efficient to do it all in one tx.
        for (m in mints)
        {
            if ((m.contract != null) && (m.address != null)) throw NplException("Group ${name}: A single mint has both a contract and address destination")

            val outS = if (m.contract != null)
            {
                m.contract.groupedConstraint(gid, m.quantity, *(m.holderArgs ?: arrayOf()))
            }
            else
            {
                val addr: PayAddress = m.address ?: { val tmp = wallet.getNewAddress(); m.address = tmp; tmp }()
                addr.groupedLockingScript(gid!!, m.quantity.toLong())
            }
            val out = txOutputFor(wallet.chainSelector, dust(wallet.chainSelector), outS)
            mintTx.add(out)
            LogIt.info("Minting group $name:${gid}: $out")
        }

        wallet.txCompleter(mintTx, 0, TxCompletionFlags.SIGN or TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.FUND_GROUPS or TxCompletionFlags.USE_GROUP_AUTHORITIES)
        wallet.send(mintTx, sync = true)
        LogIt.info("Group $name:${gid}: mint transaction $mintTx hex: ${mintTx.toHex()}")
        ret.add(mintTx)
        return ret
    }

    /** Compile all the contracts in this group */
    fun compile(cfg: EvalConfig, onlyIfNeeded:Boolean = false)
    {
        for (itf in contract.interfaces)
        {
            for (r in itf.rules) r.value.script.evalConfig = cfg
            itf.compile(onlyIfNeeded)
        }

        for((sgname, sg) in subgroups)
        {
            for (itf in sg.contract.interfaces)
            {
                for (r in itf.rules) r.value.script.evalConfig = cfg
                itf.compile(onlyIfNeeded)
            }
        }

        for (m in mints)
        {
            m.contract?.interfaces?.forEach {
                for (r in it.rules) r.value.script.evalConfig = cfg
                it.compile(onlyIfNeeded)
            }
        }
    }

    /** Shortcut to find a named rule */
    fun findRule(name:String):Rule?
    {
        for (itf in contract.interfaces)
        {
            for (r in itf.rules)
            {
                if (r.key == name) return r.value
            }
        }

        for ((name,sg) in subgroups)
        {
            for (itf in sg.contract.interfaces)
            {
                for (r in itf.rules)
                {
                    if (r.key == name) return r.value
                }
            }
        }

        for (m in mints)
        {
            m.contract?.interfaces?.forEach {
                for (r in it.rules)
                {
                    if (r.key == name) return r.value
                }
            }
        }
        return null
    }
}

class Subgroup(val parent: Group, name: String, contract: Contract, descriptor: GroupDescriptor?, mints: List<InitialQuantities>,
    val id: ByteArray):
    Group(name, parent.flags, contract, listOf(), descriptor, mints)
{

    override fun add(g: Subgroup)
    {
       throw NplException("Subgroups may not have their own sub-subgroups: Subgroup $name tried to add ${g.name} as a sub-subgroup.")
    }

    // subgroup deploy
    override fun deploy(wallet: Wallet): List<iTransaction>
    {
        val genesisData = descriptor?.buildGenesisData()

        var qty = 0UL
        for (m in mints)
        {
            qty += m.quantity
        }
        if (qty <= 0UL) throw NplException("Subgroup ${parent.name}.${name} has quantity 0.  Use 'mint' to specify quantities, or 'authority' to produce a subgroup authority")
        // TODO subgroup authorities rather than mint

        gid = parent.gid!!.subgroup(id)
        val (tx, genesisAddr) = wallet.createSubgroup(gid!!, qty, genesisData)
        val finalFile = Path(name + ".tdd")
        val tdd = descriptor?.buildTokenDescriptionDoc(wallet, genesisAddr, finalFile)
        // its ok to have no descriptor
        if (tdd == false) throw NplException("Subgroup ${parent.name}.$name: Unspecified problem building token description document")

        val ret = mutableListOf(tx)
        genesisTx = tx
        wallet.send(tx, sync = true)

        val mintTxs = deployMints(wallet)
        ret.addAll(mintTxs)
        deployMedia()
        return ret
    }
}

open class SubgroupBuilder(val parent: GroupBuilder, name: String, val block: SubgroupBuilder.() -> Unit): GroupBuilder(name, parent.flags)
{
    override var built: Group? = null
    /** Specify the data that makes this subgroup unique */
    var id: ByteArray? = null


    override fun build(buildInto: Group?): Subgroup
    {
        built?.let { return (built as Subgroup)} // Only build once

        block()

        val parent = parent.built!!
        val c = contract.build()
        if ((id == null)&&(mediaHash==null)) throw NplException("Specify subgroup unique data (part of the subgroup ID)")
        if (id == null) id = mediaHash  // If no id override given use the NFT hash as the unique data
        val ret = Subgroup(parent, name, c, gDescriptor, mints, id!!)
        parent.add(ret)
        super.build(ret)
        built = ret
        return ret
    }
}

/** A contract is simply a list of implemented interfaces.  Any satisfied interface unlocks the contract */
open class Contract(val interfaces: List<Interface>)
{
    var templateScript: SatoshiScript? = null
    var templateHash: ByteArray? = null

    var argsScript: SatoshiScript? = null
    var argsHash: ByteArray? = null

    /** Get the output constraint defined by this contract and group information */
    fun constraint(vararg holderArgs:Any): SatoshiScript = groupedConstraint(null, null, holderArgs)


    /** Get the output constraint defined by this contract and group information */
    fun groupedConstraint(gid: GroupId?, grpQty: ULong?, vararg holderArgs:Any): SatoshiScript
    {
        val cs = gid?.blockchain ?: ChainSelector.NEXA
        val ret = SatoshiScript(cs)
        if (interfaces.size > 1) TODO()
        val face = interfaces[0]
        if (face.rules.size > 1) TODO()
        val rule = face.rules.get(face.rules.keys.first())
        templateScript = rule!!.script.script()
        templateHash = templateScript!!.scriptHash160()

        argsScript = NexaArgs(holderArgs, cs)
        argsHash = argsScript!!.scriptHash160()

        if (gid != null)
        {
            ret.add(OP.push(gid.data), OP.push(grpQty!!))
        }
        else
        {
            ret.add(OP.PUSHFALSE)  // no group
        }
        ret.add(OP.push(templateHash!!))
        if (holderArgs.size > 0) ret.add(OP.push(argsHash!!))
        else ret.add(OP.PUSHFALSE)
        // TODO holder visible args
        return ret
    }
}

/** Define a contract.  A contract is simply a list of implemented interfaces */
open class ContractBuilder()
{
    val interfaces = mutableListOf<Interface>()
    /** Specify a contract interface implementation for this group */
    fun face(block: InterfaceBuilder.() -> Unit)
    {
        interfaces.add(InterfaceBuilder().apply(block).build())
    }

    /** Shortcut when you have only a single interface and specify args as parameters */
    fun rule(name: String, templateArgs:List<NBinding>?=null, holderArgs:List<NBinding>?=null, spenderArgs:List<NBinding>?=null, block: NSL.() -> Unit)  // top level rules must be named
    {
        bind("templateArg", templateArgs)
        bind("holderArgs", holderArgs)
        bind("spenderArgs", spenderArgs)

        val ib = InterfaceBuilder().apply {
            rule(name, templateArgs, holderArgs, spenderArgs) {
                script(block)
            }
        }
        interfaces.add(ib.build())
    }

    /** Shortcut when you have only a single interface and no args */
    fun rule(block: NSL.() -> Unit)  // top level rules must be named
    {
        val ib = InterfaceBuilder().apply {
            rule(name, null, null, null) {
                script(block)
            }
        }
        interfaces.add(ib.build())
    }

    @Synchronized
    open fun build(): Contract
    {
        return Contract(interfaces)
    }

}


open class MediaBuilder(val name: String, val npl: NPL, val dir: String)
{
    protected var requiredGroups: Array<out String> = arrayOf()

    data class Subst(val subst: Array<out Pair<String, String>>)

    protected var substmap = mutableMapOf<Path, Subst>()

    init {
        val d = Path(dir)
        if (!d.isDirectory()) throw NplException("In media $name, expected directory named $dir.")
    }

    fun requires(vararg groups: String)
    {
        requiredGroups = groups
    }

    /** Indicate a file to add to the media zip and a list of text substitutions */
    fun substitute(file: String, vararg subst: Pair<String,String>)
    {
        substmap[Path(file)] = Subst(subst)
    }
    fun groupIdOf(name: String): GroupId = npl.groupIdOf(name)

    fun deploy()
    {
        val zout = ZipOutputStream(BufferedOutputStream(FileOutputStream(Path(name+".zip").toFile())))
        val d = Path(dir)
        if (!d.isDirectory()) throw NplException("In media $name, expected directory named $dir.")
        // add all the static files
        d.forEachDirectoryEntry {
            // TODO subdirectories are not supported
            if (!substmap.contains(it))
            {
                val entry = ZipEntry(it.name)
                zout.putNextEntry(entry)
                zout.write(it.readBytes())
                zout.closeEntry()
            }
        }
        // add any string template files
        for (s in substmap)
        {
            val data = s.key.readText()
            val xform = data.format(s.value.subst)
            val entry = ZipEntry(s.key.name)
            zout.putNextEntry(entry)
            zout.write(xform.toByteArray())
            zout.closeEntry()
        }
        zout.finish()
        zout.close()
    }
}

/** Define a group within this project */
open class GroupBuilder(var name: String, var flags:GroupFlags = DefaultGroupFlags)
{
    open var built: Group? = null
    protected var contract = ContractBuilder()
    protected var gDescriptor: GroupDescriptor? = null
    protected var mediaFile: String? = null
    protected var mediaHash: ByteArray? = null
    protected var mediaContents: ByteArray? = null
    protected val authorities = mutableListOf<Group.AuthorityDefs>()
    protected val subgroups = mutableListOf<SubgroupBuilder>()
    protected val mints = mutableListOf<Group.InitialQuantities>()

    /** Specify a contract interface implementation for this group */
    fun face(block: InterfaceBuilder.() -> Unit)
    {
        contract.interfaces.add(InterfaceBuilder().apply(block).build())
    }

    /** specify information about this group that is placed into the group genesis transaction and into the token description document */
    fun descriptor(block: GroupDescriptor.() -> Unit): GroupDescriptor
    {
        if (gDescriptor != null) throw IllegalStateException("Only one descriptor allowed in group $name")
        val ret =  GroupDescriptor()
        ret.block()
        gDescriptor = ret
        return ret
    }

    /** Define an authority for this group within the provided wallet.
     * Once this group is created, this function will not create a new authority UTXO or wallet address.
     * You may specify this field multiple times to get multiple authorities.
     * If the union of all authorities, does not include a capability flag, then that capability will not be available!
     * If this is never used, the group will be created with all authorities.
     * If this is used exactly once, it will define the genesis authority.
     * @flags The desired capabilities for this authority.
     * @address The destination address for this authority.  This field should only be used if the authority is targeted to some external wallet.  Set to null or leave blank if an address in the creating wallet should gain this authority.
     */
    fun authority(flags:ULong, address: PayAddress?=null)
    {
        authorities.add(Group.AuthorityDefs(flags, null, address))
    }

    /** Define an authority for this group within the provided wallet.
     * Once this group is created, this function will not create a new authority UTXO or wallet address.
     * You may specify this field multiple times to get multiple authorities.
     * If the union of all authorities, does not include a capability flag, then that capability will not be available!
     * If this is never used, the group will be created with all authorities.
     * If this is used exactly once, it will define the genesis authority.
     * @flags The desired capabilities for this authority.
     * @address The destination address for this authority.  This field should only be used if the authority is targeted to some external wallet.  Set to null or leave blank if an address in the creating wallet should gain this authority.
     */
    fun authority(flags:ULong, block: ContractBuilder.() -> Unit)
    {
        val tmp = ContractBuilder()
        tmp.block()
        authorities.add(Group.AuthorityDefs(flags, tmp.build(), null))
    }


    /** Issue some tokens immediately.
     * flag set.
     *
     */
    fun mint(qty: Int, block: (ContractBuilder.()->Unit)? = null)
    {
        if (qty <= 0) throw NplException("Mint quantity for group $name is 0 or negative")
        mint(qty.toULong(), block)
    }

    /** Issue some tokens immediately.
     */
    fun mint(qty: ULong, address: String) = mint(qty, PayAddress(address))
    fun mint(qty: Long, address: String) = mint(qty.toULong(), PayAddress(address))
    fun mint(qty: Int, address: String) = mint(qty.toULong(), PayAddress(address))
    fun mint(qty: UInt, address: String) = mint(qty.toULong(), PayAddress(address))


    fun mint(qty: ULong, address: PayAddress)
    {
        if (qty <= 0UL) throw NplException("Mint quantity for group $name is 0 or negative")
        mints.add(Group.InitialQuantities(qty, address = address))
    }

    /** Issue these tokens immediately.
     */
    fun mint(qty: ULong, block: (ContractBuilder.()->Unit)? = null)
    {
        if (qty == 0UL) throw NplException("Mint quantity for group $name is 0 ")
        val cb = ContractBuilder()
        if (block != null)
        {
            cb.block()

            mints.add(Group.InitialQuantities(qty, cb.build()))
        }
        else mints.add(Group.InitialQuantities(qty))
    }

    /** Issue these tokens immediately.
     */
    fun mint(qty: ULong, vararg holderArgs: Any, block: (ContractBuilder.()->Unit)? = null)
    {
        if (qty == 0UL) throw NplException("Mint quantity for group $name is 0 ")
        val cb = ContractBuilder()
        if (block != null)
        {
            cb.block()

            mints.add(Group.InitialQuantities(qty, cb.build(), holderArgs))
        }
        else mints.add(Group.InitialQuantities(qty))
    }

    /** Specify a child group */
    fun subgroup(name: String, block: SubgroupBuilder.() -> Unit): SubgroupBuilder
    {
        val ret = SubgroupBuilder(this, name, block)
        subgroups.add(ret)
        return ret
    }


    /** Specify an associated NFT/SFT file.  Or provide a directory and the system will produce the .nft file out of all files in that
     * directory
     * */
    fun media(fileOrDir:String, block: (MediaBuilder.() -> Unit)? = null)
    {
        if (mediaFile != null) throw NplException("In group $name: Cannot have multiple media blocks.")
        val path = Path(fileOrDir)
        var contents: ByteArray? = null
        if (path.isDirectory())
        {
            TODO("pack a directory into an .nft file")
        }
        else if (path.exists())
        {
            mediaFile = path.absolutePathString()
            contents = path.readBytes()
        }
        else
        {
            // See if we can grab it as a resource
            val res = this::class.java.classLoader.getResource(fileOrDir)
            if (res != null) contents = res.readBytes()

            if (contents == null) throw NplException("In group ${name}: Media file or directory ${fileOrDir} does not exist (searched ${path.absolutePathString()} and resource directory).")
        }

        if (contents != null)
        {
            // TODO check valid NFT
            mediaHash = libnexa.hash256(contents)
            mediaContents = contents
        }
    }


    /** Add another interface implementation */
    fun unaryPlus(face: Interface): GroupBuilder
    {
        contract.interfaces.add(face)
        return this
    }

    @Synchronized
    open fun build(buildInto: Group? = null): Group
    {
        built?.let { return it }

        val c = contract.build()

        var ret = buildInto ?: Group(name, flags, c, authorities, gDescriptor, mints)
        ret.name = name
        ret.flags = flags
        ret.contract = c
        ret.mediaContents = mediaContents
        built = ret
        for (sgb in subgroups)
        {
            val sg = sgb.build()  // Automatically adds to parent group's list of subgroups
        }

        return ret
    }
}


/** Start defining a Nexa project
 * @param name  A name that you choose for this project.  After deployment, a similarly named file will be created containing the deployment info
 * @param initializer Your project definition
 */
fun Nexa(name: String, initializer: NplBuilder.() -> Unit): NPL
{
    return NplBuilder(name).apply(initializer).build()
}


/** Extend an already-defined Nexa project
 * @param npl A reference to the original project
 * @param initializer Your project extension definition
 */
fun NexaExtend(npl: NPL, initializer: NplExtensionBuilder.() -> Unit): NPL
{
    return NplExtensionBuilder(npl).apply(initializer).build()
}


/** Define a Nexa project */
class NplBuilder(val name: String)
{
    val groups = mutableListOf<Group>()

    fun build(): NPL = NPL(name, groups)

    /** Define a group within this project */
    fun group(name: String, flags: GroupFlags = DefaultGroupFlags, block: GroupBuilder.() -> Unit): Group
    {
        val g = GroupBuilder(name, flags).apply(block).build()
        groups.add(g)
        return g
    }

    /** Define a group that holds NEXA.  This is a shorthand for setting a group's HOLDS_NEX bit, and so this bit
     * will be set within the group flags, regardless of the passed flags parameter */
    fun groupOfNexa(name: String, flags: GroupFlags = GroupFlags.of(GroupFlag.HOLDS_NEX), block: GroupBuilder.() -> Unit): Group
    {
        flags.add(GroupFlag.HOLDS_NEX)
        return group(name, flags, block)
    }

    fun NPL.address(name: String, block: (AddressBuilder.() -> Unit)? = null): NAddress
    {
        return if (block != null) AddressBuilder(name).apply(block).build() else AddressBuilder(name).build()
    }

}

/** Define a Nexa project */
class NplExtensionBuilder(val npl: NPL)
{
    val groups = mutableListOf<Group>()

    fun build(): NPL
    {

        return npl
    }

    /** Define a group within this project */
    fun group(name: String, flags: GroupFlags = DefaultGroupFlags, block: GroupBuilder.() -> Unit): Group
    {
        val g = GroupBuilder(name, flags).apply(block).build()
        groups.add(g)
        return g
    }

}

/** Nexa Project Language */
class NPL(val name: String, val groups: List<Group>)
{
    /** return the group with the passed name */
    fun group(name: String) = groups.find { it.name == name }

    /** return the group id with the passed name.
     * @throws [NplException] If the group does not exist or is not yet instantiated, NplException is thrown.
     * @return [GroupId] the group's identifier */
    fun groupIdOf(name: String): GroupId
    {
        val g:Group?= group(name)
        if (g == null) throw NplException("Group named $name is not found")
        val gid = g.gid
        if (gid == null) throw NplException("Group $name is not instantiated yet (you are calling this function too soon)")
        return gid
    }

    /** return the group at the passed index (in declaration order) */
    fun group(index: Int) = groups[index]

    var groupInstantations: Map<String, NCGroupId>? = null

    /** compile all scripts defined in this system */
    fun compile()
    {
        val cfg = EvalConfig()
        cfg.instantiations = groupInstantations
        for (g in groups)
        {
            g.compile(cfg)
        }
    }

    /** See if this project is internally consistent
     *
     */
    fun check()
    {
        for (g in groups)
        {
            if (g.subgroups.size > 0)
            {
                // check that we have a subgroup authority
            }
            g.check()
        }

    }

    /** Deploy this project into a blockchain.
     * If the project has already been deployed into this blockchain (as evidenced by a json data file), it will not be redeployed.
     * Instead, the previous deployed state will be loaded for your use.
     * @param wallet Use this wallet (and its attached blockchain) as the deployment target
     * @param forceRedeploy If you have already deployed to this blockchain, redo the deployment.
     * @param dryRun If true, do not issue any transactions, just create them
     * @return All created transactions (needed for deployment)
     */
    fun deploy(wallet: Wallet, forceRedeploy:Boolean = false, dryRun:Boolean = false): List<iTransaction>
    {
        val deploymentFilename = name + "_" + wallet.chainSelector.toString() + ".json"
        val deploymentFile = Path(deploymentFilename)
        if ((forceRedeploy != true) && deploymentFile.exists())
        {
            TODO("load deployment from file")
            return listOf()  // TODO need to deploy any changes
        }

        val txes = mutableListOf<iTransaction>()
        val gInst = mutableMapOf<String, NCGroupId>()
        for (g in groups)
        {
            txes.addAll(g.create(wallet))
            gInst[g.name] = NCGroupId(g.gid!!)  // gid must be nonnull because I just created it in [create]
        }
        groupInstantations = gInst.toMap()

        // Compile all the code after all groups are instantiated so that group id constants are properly resolved
        val ec = EvalConfig()
        ec.compiling = true
        ec.instantiations = groupInstantations
        for (g in groups)
        {
            LogIt.info("Compiling: ${g.name}:${g.gid}")
            g.compile(ec)
        }

        for (g in groups)
            txes.addAll(g.deploy(wallet))

        if (!dryRun)
        {
            // I need to issue these in order and sync each time because later tx may depend on earlier
            for (tx in txes)
            {
                println("tx: ${tx.toHex()}")
            }
        }

        return txes
    }


    /** Shortcut to find a named rule */
    fun findRule(name:String):Rule
    {
        for (g in groups)
        {
            g.findRule(name)?.let { return it }
        }
        throw NoSuchElementException("$name not found")
    }

}