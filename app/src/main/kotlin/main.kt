// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.wallywallet.wew

import org.nexa.libnexakotlin.*

import kotlinx.serialization.json.Json
// import org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngine
import kotlin.script.experimental.jsr223.KotlinJsr223DefaultScriptEngineFactory

// is eng: kotlin.script.experimental.jvmhost.jsr223.KotlinJsr223ScriptEngineImpl?
// YOU forgot create the file src/main/resources/META-INF/services/javax.script.ScriptEngineFactory with contents "org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngineFactory"

import java.io.File
import java.util.*
import java.util.concurrent.Executors
import kotlin.coroutines.CoroutineContext

import kotlinx.coroutines.*
import kotlinx.serialization.*
import kotlinx.serialization.json.*
import Nexa.npl.calcStackX
import Nexa.npl.loadCalcStackX
import Nexa.npl.stackX
import kotlinx.coroutines.flow.MutableStateFlow
import java.io.OutputStream
import javax.script.*
import javax.script.ScriptException
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.Path
import kotlin.system.exitProcess

import org.nexa.threads.*
import org.wallywallet.composing.Composing
import org.wallywallet.wew.WallyEnterpriseWallet.injectSymbol
import org.wallywallet.wew.WallyEnterpriseWallet.invisibleEval
import org.wallywallet.wew.WallyEnterpriseWallet.resolveToNearestObject
import org.wallywallet.wew.cli.help
import org.wallywallet.wew.cli.resultOf
import java.lang.reflect.Method
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.full.*

private val LogIt = GetLog("wew.main")

var scriptSearchPath:List<String> = mutableListOf<String>(".")

val runTests = false

val DEFAULT_NEXA_REGTEST_IP = "127.0.0.1"
val DEFAULT_NEXA_REGTEST_PORT = NexaRegtestPort

var FULL_NODE_IP = "127.0.0.1"

var PLUGIN_DIR = "app/build/plugins/"

// Search the first N addresses in a particular derivation path during wallet recovery.
// Since wallets give out addresses in order, this will tend to find the activity.  The only reason we search a bunch of addresses
// is because maybe some addresses were given out but payments weren't made.

val WALLET_RECOVERY_NON_INCREMENTAL_ADDRESS_HEIGHT = 300000  // This works around a bug in early wallets that accidentally would take addresses out of order

// Translation wrapper function
fun _x_(s: String?):String {
    return s ?: ""
}

val WelcomeMessage = "Wally Enterprise Wallet (type 'help' for help)"
val WallyTitle = "Wally Enterprise Wallet"

// val hardCodedElectrumClients = mutableListOf<IpPort>(IpPort("nexa.wallywallet.org", DEFAULT_NEXA_TCP_ELECTRUM_PORT))  // , IpPort("electrumserver.seed.bitcoinunlimited.net", DEFAULT_TCP_ELECTRUM_PORT), IpPort("bitcoincash.network", DEFAULT_TCP_ELECTRUM_PORT))

/* parameter display styles */
var pDisplayStyles = EnumSet.of(DisplayStyle.FieldPerLine, DisplayStyle.ShortTypes)
/* display level */
var DisplayLevel = Display.Dev

val perThreadOutput = mutableMapOf<Long, Output>()

fun getThreadOutput(): Output?
{
    val threadId = Thread.currentThread().getId()
    return perThreadOutput[threadId]
}
fun setThreadOutput(out: Output?)
{
    val threadId = Thread.currentThread().getId()
    if (out == null) perThreadOutput.remove(threadId)
    else perThreadOutput[threadId] = out
}

// Unfortunately we can't ALWAYS know what blockchain an object is associated with
// in particular the block header, so track the last used one to make a guess
var lastBlockchainUsed:ChainSelector = ChainSelector.NEXA

val uiDisplayCtxt = newFixedThreadPoolContext(2, "ui")
val uiScope = CoroutineScope(uiDisplayCtxt)

open class Error(what: String): LibNexaException(what,"")

open class Output()
{
    open fun stdout(s:Any?) { if (s is String) print(s)}
    open fun error(s:String?) { print(s)}
    open fun info(s:String?) { print(s)}

    open fun displayException(e:Throwable, cmdcount: Int, engine: ScriptEngine?=null, fileSpec: String?=null)
    {
        if (engine != null)
        {

            val os = StringBuilder()
            val stack = e.stackTrace
            // The stack contains both the user's code and this CLI code.  Remove everything but the user's stack
            for (s in stack)
            {
                if (s.fileName == "KotlinJsr223JvmScriptEngineBase.kt") break
                os.append("  " + s.toString() + "\n")
            }
            error("Exception: ${e.message}\n")
            if (fileSpec != null) print("Located at $fileSpec\n")
            else
            {
                print("Executed from console command # $cmdcount\n")
            }
            os.toString().let { if (it.length > 0) error("Stack:\n$it\n") }
            injectSymbol("lastException", e)
            injectSymbol("_result", e)
            // deadlock invisibleEval("""resultOf[$cmdcount] = result""")
        }
    }
}

/*
fun calcSig(tx: iTransaction, inputIdx: Int, sigHashType: ByteArray, serializedTx: ByteArray? = null): ByteArray
{
    val inp = tx.inputs[inputIdx]
    val spendable = inp.spendable
    val secret = spendable.secret

    val flatTx = serializedTx ?: tx.BCHserialize(SerializationType.NETWORK).flatten()

    if (secret == null) throw IllegalArgumentException("Input spendable must have secret set")
    if (secret.getSecret().size == 0) throw IllegalArgumentException("Input spendable must have a nonempty secret")
    val sigSchnorr = Wallet.signOneInputUsingSchnorr(flatTx, sigHashType, inputIdx.toLong(), inp.spendable.amount, inp.spendable.priorOutScript.flatten(), secret.getSecret())
    return sigSchnorr
}
*/

fun OverrideElectrumServerOn(chain: ChainSelector): IpPort
{
    return when (chain)
    {
        ChainSelector.BCH -> IpPort("electrum.seed.bitcoinunlimited.net", DEFAULT_BCH_TCP_ELECTRUM_PORT)
        ChainSelector.BCHREGTEST -> IpPort(FULL_NODE_IP, 60401)
        ChainSelector.NEXAREGTEST -> IpPort(FULL_NODE_IP, DEFAULT_NEXAREG_TCP_ELECTRUM_PORT)
        // TODO: point these IPs to a seeder
        ChainSelector.NEXA -> IpPort(FULL_NODE_IP, DEFAULT_NEXA_TCP_ELECTRUM_PORT)
        ChainSelector.NEXATESTNET -> IpPort(FULL_NODE_IP, DEFAULT_NEXATEST_TCP_ELECTRUM_PORT)
        //IpPort("electrumserver.seeder.nexa.org", 7229)
        else -> throw Exception()//BadCryptoException()
    }
}

class CoScript(var output: Output) : AbstractCoroutineContextElement(CoScript) {
    companion object Key : CoroutineContext.Key<CoScript>
}

object GenerateXlat
{
    @JvmStatic
    fun main(args: Array<String>)
    {
        calcStackX()
        libNexaJobPool.stop()
    }
}


open class FifoOutput(val out: OutputStream):Output()
{
    val writer = out.bufferedWriter()
    override fun stdout(s: Any?)
    {
        if (s is String) writer.write(s)
        else writer.write(s.toString())
    }

    override fun error(s: String?)
    {
        writer.write(s)
    }

    override fun info(s: String?)
    {
        writer.write(s)
    }
}

open class StringOutput():Output()
{
    val writer = StringBuilder()
    override fun stdout(s: Any?)
    {
        if (s is String) writer.append(s)
        else writer.append(s.toString())
    }

    override fun error(s: String?)
    {
        writer.append(s)
    }

    override fun info(s: String?)
    {
        writer.append(s)
    }

    override fun toString(): String
    {
        return writer.toString()
    }
}


object MainFifo
{
    @JvmStatic
    fun main(args: Array<String>)
    {
        WallyEnterpriseWallet.run(WallyEnterpriseWallet.CliType.Fifo)
    }
}


fun main(args: Array<String>)
{
    WallyEnterpriseWallet.main(args)
}

/*
fun showFn(p: KFunction<*>, obj: Any): String
{

    val d = p.findAnnotation<cli>()
    val help = if (d != null)
    {
        d.help
    } else ""

    val ret = StringBuilder()
    ret.append(p.name)
    ret.append("(")
    var first = true
    for (arg in p.parameters)
    {
        if (first) first = false
        else ret.append(",")
        if (arg.isOptional) ret.append("[")
        ret.append(arg.name).append(":").append(arg.type)
        if (arg.isOptional) ret.append("]")
    }
    ret.append(" -> ").append(p.returnType)
    if (help.isNotBlank())
    {
        ret.append("\n  ").append(help)
    }
    return ret.toString()
}
 */

fun showMember(obj:Any?, member: String, _cls: KClass<*>? = null):String?
{
    if (obj == null) return null
    var cls = _cls ?: obj::class

    for (p in cls.memberProperties)
    {
        if (p.name == member)
        {
            return showPropertyOf(obj,p, Display.Dev, setOf(DisplayStyle.Help, DisplayStyle.FieldPerLine),5,0)
        }
    }
    for (p in cls.memberFunctions)
    {
        if (p.name == member)
        {
            return showFunction(p, Display.Dev, setOf(DisplayStyle.Help, DisplayStyle.FieldPerLine, DisplayStyle.ShortTypes),0).second
        }
    }
    for (p in cls.memberExtensionFunctions)
    {
        if (p.name == member) return showFunction(p, Display.Dev, setOf(DisplayStyle.Help, DisplayStyle.FieldPerLine, DisplayStyle.ShortTypes),0).second
    }
    if (_cls == null) for (p in cls.superclasses)
    {
        if (!(p.qualifiedName?.contains("kotlin") ?: true))
            showMember(obj, member, p)?.let { return it }
    }
    return null
}

fun outputHelpFor(expression: String, output: Output)
{
    val result = invisibleEval(expression)

    when (result)
    {
        is Long -> {
            resolveToNearestObject(expression, output)
        }
        is Int -> {
            resolveToNearestObject(expression, output)
        }
        is Unit -> {
            resolveToNearestObject(expression, output)
        }
        is String    -> {
            resolveToNearestObject(expression, output)
        }
        is Composing -> {
            uiScope.launch {
                result.highlight(true, "help")
                delay(5000)
                result.highlight(false, "help")
            }
            resolveToNearestObject(expression, output)
            output.stdout("Contains:" + help(result) + "\n")
        }
        else         -> {
            resolveToNearestObject(expression, output)
            //output.stdout("Contains:" + help(result) + "\n")
        }
    }
}

object WallyEnterpriseWallet
{
    var done = false
    var initialPanel = true
    var engine: ScriptEngine? = null
    var scriptContext: ScriptContext? = null
    var scriptTopLevelSyms = mutableMapOf<String, Any?>("help" to ::help)

    var engThread: iThread? = null
    var engineClassLoader: DynamicUrlClassLoader? = null

    // lock before using any of the following vars
    var engCtrl: iGate = Gate("script engine gate")
    var engCmd: String? = null
    var engOut: Output? = null
    var engResult: Any? = null
    var engResultReady = false
    @Volatile
    var engRestarts = 0L
    var engResetActions = mutableListOf<() -> Unit>()
    val processing: MutableStateFlow<String?> = MutableStateFlow(null)
    var rawEval = false // set to true before the eval

    var accounts: MutableMap<String, Wallet> = mutableMapOf()
    //var blockchains: MutableMap<ChainSelector, Blockchain> = mutableMapOf()

    val inputLines = mutableListOf<String>()

    var lastFile: String = ""

    val coCtxt: CoroutineContext = Executors.newFixedThreadPool(4).asCoroutineDispatcher()
    val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

    val output: Output
        get()
        {
            val threadId = Thread.currentThread().getId()
            val threadOutput = perThreadOutput[threadId]
            if (threadOutput != null)
            {
                return threadOutput
            } else
            {
                return scriptContext!!.getBindings(ScriptContext.ENGINE_SCOPE)!!.get("_output")!! as Output
            }
        }


    fun connectBlockchain(cs: ChainSelector): Blockchain
    {
        try
        {

            blockchains[cs]?.let { return it }

            val cm = GetCnxnMgr(cs, start = true)
            if (cs == ChainSelector.NEXAREGTEST)
                cm.add(DEFAULT_NEXA_REGTEST_IP, DEFAULT_NEXA_REGTEST_PORT)
            val bc = GetBlockchain(cs, cm, start = true)
            (bc.net as MultiNodeCnxnMgr).getElectrumServerCandidate = { chain, excl, pref -> OverrideElectrumServerOn(chain) }
            blockchains[cs] = bc
            return bc
        }
        catch(e:Exception)
        {
            handleThreadException(e, "connecting blockchain", sourceLoc())
            throw e
        }
    }

    enum class CliType { None, Console, Graphical, Fifo }
    @JvmStatic
    fun run(shell: CliType = CliType.None, walName: String? = null, cs:ChainSelector? = null)
    {
        LogIt.warning("Starting Wally Enterprise Wallet")
        Init()
        guiIni()

        // Open a wallet if one was passed
        if (walName != null)
        {
            if (cs != null)
                openOrNewWallet(walName, cs)
            else openWallet(walName)
        }

        startScriptEvaluator()

        if (shell == CliType.Graphical)
        {
            guiNewPanel(this)
        }
        else if (shell == CliType.Console)
        {
            println()
            println(_x_(WelcomeMessage))
            commandLoop()
            for ((_, w) in accounts)
            {
                (w as CommonWallet).stop()
                w.save(true)
            }
            for ((_, b) in blockchains)
            {
                b.stop()
            }
        }
        else if (shell == CliType.Fifo)
        {
            val fifoIn = File("in")
            val fifoOut = File("out")
            LogIt.info("FIFO MODE: Control fifo is at ${fifoIn.absolutePath} and ${fifoOut.absolutePath}")
            while(true) try
            {
                val read = fifoIn.inputStream()
                val write = fifoOut.outputStream()
                val output = FifoOutput(write)
                while (true)
                {
                    val buffer = ByteArray(1024*100)
                    val dataSz = read.read(buffer)  // is capable of partial reads
                    // TODO handle partial lines
                    val data = String(buffer.sliceArray(0 until dataSz))
                    if (data.length > 0)
                    {
                        // LogIt.info("fifo received '$data'")
                    }
                    else
                    {
                        Thread.sleep(500)
                        continue
                    }

                    if (data.startsWith("gui") || data.startsWith("!gui"))
                    {
                        guiNewPanel(WallyEnterpriseWallet)
                    }
                    else if (data.startsWith("gui ") || data.startsWith("!gui "))
                    {
                        // GraphicsEnvironment
                        //setEnv("DISPLAY", data.drop(4).trim())
                        val disp = data.drop(4).trim()
                        System.setProperty("DISPLAY", disp)
                        guiNewPanel(WallyEnterpriseWallet)
                    }
                    else if (data.startsWith("exit"))
                    {
                        LogIt.info("control sent exit")
                        return
                    }
                    else
                    {
                        var str: String? = data
                        do
                        {
                            str = execute(str, output)
                        } while((str != null)&&(str != ""))
                        output.writer.write("\n|>>\n")  // fifo command output end indicator
                        output.writer.flush()
                    }

                }
            }
            catch(e:Exception)  // wait for the fifo pipe to be created
            {
                println("Waiting for fifos. Create them with 'mkfifo ${fifoIn.absolutePath};mkfifo ${fifoOut.absolutePath}'")
                Thread.sleep(4000)
            }
        }
        else
        {
            // while (true) Thread.sleep(100000)
        }
    }

    @JvmStatic
    fun main(args: Array<String>)
    {
        //System.setProperty("java.util.logging.SimpleFormatter.format", "%1\$tY-%1\$tm-%1\$td %1\$tH:%1\$tM:%1\$tS %4$-6s %2\$s %5\$s%6\$s%n")
        //System.setProperty("java.util.logging.FileHandler.pattern", "dwally.log")

        LogIt.warning("Starting Wally Enterprise Wallet")
        Init()
        guiIni()

        //runBlocking { TestRequestMgr("bu", "bu", "localhost", 18332) }

        //? Currently selected fiat currency code
        var fiatCurrencyCode: String = "USD"

        //testSimpleEval(wallet)
        //testEngineFactory()
        if (!runTests)
        {
        }
        else
        {
            GlobalScope.launch {
                // TODO missing reference: allTests("bu", "bu", "localhost", 18332)
            }
        }

        startScriptEvaluator()

        if (initialPanel)
        {
            guiNewPanel(this)
            exitProcess(0)
        }
        else
        {
            println()
            println(_x_(WelcomeMessage))
            commandLoop()
            for ((_, w) in accounts)
            {
                (w as CommonWallet).stop()
                w.save(true)
            }
            for ((_, b) in blockchains)
            {
                b.stop()
            }
            exitProcess(0)
        }
    }

    fun checkWalletFilename(name: String, allowNonExistent: Boolean = false, mustNotExist: Boolean = false):File
    {
        var f = File(name)
        val fNoExt:File = if (f.extension == ".db")
        {
            File(f.nameWithoutExtension)  // opening is going to add it on
        }
        else
        {
            f
        }
        if (mustNotExist)
        {
            val fWithExt = File(fNoExt.absolutePath + ".db")
            if (fWithExt.exists())
            {
                throw Error("Wallet file already exists: " + fWithExt.absolutePath)
            }
        }
        if (allowNonExistent) return fNoExt
        val fWithExt = File(fNoExt.absolutePath + ".db")
        if (!fWithExt.exists())
        {
            throw Error("File does not exist: " + fWithExt.absolutePath)
        }
        return fNoExt
    }


    fun recoverWallet(name: String, recoveryKey: String, bc:Blockchain): Bip44Wallet
    {
        val w = runBlocking {
            val f = checkWalletFilename(name, true)
            val wdb: WalletDatabase = openWalletDB(f.absolutePath, bc.chainSelector)!!  // One DB file per wallet for the desktop
            val w = Bip44Wallet(wdb, name, bc.chainSelector, recoveryKey)
            val earliestHeight = 2  // TODO get from rostrum
            w.addBlockchain(bc, 2, 0)  // Since this is a recovered wallet, look for old tx (start as early as possible)
            if (earliestHeight < 300000)  // Work around a bug in early wallets that would use addresses out-of-order
            {
                w.prepareDestinations(900)
            }
            accounts[name] = w
            w
        }
        return w
    }

    fun recoverWallet(name: String, recoveryKey: String, cs:ChainSelector): Bip44Wallet
    {
        var bc = blockchains[cs]
        if (bc == null) bc = connectBlockchain(cs)
        return recoverWallet(name, recoveryKey, bc)
    }

    /** Open this wallet if it exists, or create a new wallet */
    /**
    fun openOrNewWallet(name: String, cs:ChainSelector): Bip44Wallet
    {
        try
        {
            return openWallet(name)
        }
        catch(e:Exception)
        {
            // println("Creating new wallet: $name")
            return newWallet(name, cs)
        }
    }
    */

    fun displayObject(x:Any?, style: DisplayStyles = DEFAULT_STYLE): String
    {
        val ret = StringBuilder()

        if (x != null)
        {
            if (x is JsonElement)
            {
                val format = Json { prettyPrint = true }
                return format.encodeToString(x)
            }
            else if (x is String)
            {
                try  // See if it is json data and if so, pretty print
                {
                    val format = Json { prettyPrint = true }
                    val jsp = Json.parseToJsonElement(x)
                    return format.encodeToString(jsp)
                }
                catch (e: Exception)
                {
                }
            }
            ret.append(cliDump(x, level=DisplayLevel, style=style))
            // cliDump is already printing these: if (!isPrimitive(x)) ret.append(getApis(x, style =EnumSet.of(DisplayStyle.OneLine)))
        }
        return ret.toString()
    }

    fun cmdCount(): Long = inputLines.size.toLong()
    var prompt:()->String = {
        var ret = ""
        for ((name,wal) in accounts)
        {
            ret += "(" + name + "@" + wal.syncedHeight + "/" + wal.blockchain.curHeight + " " + wal.balance.toString() + ":" + wal.balanceUnconfirmed.toString() + ")"
        }
        ret += " " + inputLines.size.toString() + "> "
        ret
    }

    fun anotherReadLine(): String
    {
        val scanner = Scanner(System.`in`)
        return scanner.nextLine()
    }

    fun resolveToNearestObject(cmd:String, output: Output)
    {
        val lastDot = cmd.lastIndexOf(".")
        var objName = cmd
        if (lastDot != -1)
        {
            var unrelatedPrefix = ""
            var objName = cmd.take(lastDot)
            val beginTokenizer = objName.lastIndexOfAny(neverInSymbol)
            if (beginTokenizer != -1)
            {
                unrelatedPrefix = objName.take(beginTokenizer + 1)
                objName = objName.drop(beginTokenizer + 1)
            }

            val rest = cmd.drop(lastDot + 1) // don't want the dot
            val result = invisibleEval(objName)
            val text = showMember(result, rest)
            output.stdout(text ?: "not found")
        }
    }

    fun plugin(filename: String, output: Output)
    {
        val f = File(filename)
        if (!f.exists())
        {
            org.wallywallet.wew.cli.print("Plugin $filename (${f.absolutePath}) does not exist!")
            return
        }
        val pluginName = f.nameWithoutExtension.split("-")[0]
        WallyEnterpriseWallet.engineClassLoader!!.add(f.toURI().toURL())

        org.wallywallet.wew.cli.print("Launching plugin $filename installer...  ")
        // I can't restart the script engine within the thread that's processing a command.
        invisibleEval("!restartEngineKeepSymbols")
        val classToLoad = Class.forName("org.wallywallet.wew.$pluginName.Initializer", true, engineClassLoader!!)
        val method: Method = classToLoad.getDeclaredMethod("CliInstaller", WallyEnterpriseWallet::class.java, Output::class.java)
        val instance = classToLoad.getDeclaredConstructor().newInstance()
        method.invoke(instance, WallyEnterpriseWallet, output)
        org.wallywallet.wew.cli.println("Plugin $filename installed.")
    }

    fun executeScript(script: String, output: Output): String?
    {
        val sl = splitByBeginningBang(script)
        var last:String? = null
        for(cmd in sl)
        {
            last = execute(cmd, output)
        }
        return last
    }

    fun execute(s: String?, output: Output): String?
    {
        // set up our chosen output
        injectSymbol("_output", output)

        var historyOverride: String? = null
        var str:String? = s
        if (str != null)
        {
            str = str.trimEnd() // chop off any ending whitespace
            if (str == "?" || str == "help")
            {
                output.stdout(help(null))
                str = null  // consumed it
            }
            else if (str == "!exit") { done = true; return null }
            else if (str == "!reset")
            {
                resetScriptEngine()
                str = null
            }
            else if (str.startsWith("!path"))
            {
                val expression = str.removePrefix("!path").trim({it.isWhitespace()})
                if (expression != "") scriptSearchPath = expression.split(':').map { it.trim()}
                output.stdout(scriptSearchPath.toString() + "\n")
                str = null
            }
            else if (str.startsWith("!plugin"))
            {
                val expression = str.removePrefix("!plugin").trim({it.isWhitespace()})
                if (File(expression).exists()) plugin(expression, output)
                else
                {
                    val noout = StringOutput()
                    val x = eval(expression, noout)
                    x?.let { plugin(it.toString(), output) }
                }
                str = null
            }
            else if (str.startsWith("!help") || (str.length > 0 && str[0] == '?'))
            {
                val expression = str.substringAfter(' ').trim({it.isWhitespace()})
                outputHelpFor(expression, output)
                str = null
            }
            else if (str.length > 0 && str[0] == '!')
            {
                var cmd = str.drop(1)
                var num:Int? = null
                str = null  // This isn't a normal kts line to be executed so clear it out

                // pick out the digits in the string and convert to a number, leaving the rest to be appended to the retrieved history
                if (cmd.length > 0)
                {
                    val numEnd = cmd.indexOfFirst { c -> (c == '-') or !(c >= '0' && c <= '9') }
                    num = if (numEnd != -1)
                    {
                        val numpart = cmd.substring(0, numEnd)
                        cmd = cmd.substring(numEnd)
                        numpart.toIntOrNull()
                    }
                    else
                    {
                        val n = cmd.toIntOrNull()
                        cmd = ""
                        n
                    }
                }

                if (num != null)
                {
                    if (num < 0) num = inputLines.size + num
                    if (num < inputLines.size)
                    {
                        output.stdout("--> " + inputLines[num] + cmd + "\n")
                        // Inject this command into another run
                        return inputLines[num] + cmd
                    }
                    else
                    {
                        output.error("history out of range\n")
                    }
                }
                else if (cmd.length == 0)  // display the history
                {
                    inputLines.forEachIndexed { index, line -> output.stdout(index.toString() + ": " + line + "\n") }
                    str = null  // consumed
                }
                else if (cmd[0] == 'p') // new panel
                {
                    //val nexa = blockchains["NEXAREGTEST"]
                    //if (nexa != null)
                    //    guiBlockchainPanel(nexa)
                    //else println("initialize Nexa blockchain first")
                    guiNewPanel(this)
                    str = null // consumed it
                }
                else if (cmd[0] == 't')
                {
                    str = null
                    var expression = cmd.drop(1).trim({it.isWhitespace()})
                    expression += "::class.simpleName"
                    output.info("--> executing ${expression}\n")
                    eval(expression, output=output)
                }
                else if (cmd[0] == 'x')
                {
                    str = null
                    var fileName = cmd.drop(1).trim({it.isWhitespace()})
                    if (fileName.length == 0) fileName = lastFile

                    if (fileName[0] == '/')  // Don't search the path
                    {
                        var f = File(fileName)
                        if (!f.isFile())
                        {
                            fileName = fileName + ".kts"
                            f = File(fileName)
                        }
                        if (!f.isFile()) output.error("cannot find ${fileName.dropLast(3)} or ${fileName}\n")
                        else
                        {
                            str = f.readText()
                            output.info("--> executing ${fileName} last modified on ${Date(f.lastModified())}\n")
                        }
                    }
                    else
                    {
                        val (file, contents) = resolveScript(fileName)
                        if (file != null)
                        {
                            val fileName = file.toString()
                            output.info("--> executing ${fileName} last modified on ${Date(file.lastModified())}\n")
                            str = contents
                        }

                    }
                    if (str == null) output.error("cannot find ${fileName.dropLast(3)} or ${fileName} in path ${scriptSearchPath}\n")
                    else
                    {
                        historyOverride = "!x ${fileName}"
                    }
                }
            }

            if (str != null)
            {
                eval(str, output, historyOverride)
                str = null
            }
        }

        if (s != null && s != "") inputLines.add(s)
        return str
    }

    fun resolveScript(fileName: String):Pair<File?, String>
    {
        for (p in scriptSearchPath)
        {
            var fpath = Path(p)
            fpath = fpath.resolve(fileName)

            var f = fpath.toFile()
            if (!f.isFile())
            {
                fpath = Path(p).resolve(fileName + ".kts")
                f = fpath.toFile()
            }
            if (f.isFile())
            {
                val str = f.readText()
                return Pair(f, str)
            }
        }
        return Pair(null, "")
    }

    @OptIn(ExperimentalPathApi::class)
    fun commandLoop()
    {
        val output = Output()
        var str: String? = null
        while(!done)
        {
            if (str == null)
            {
                print(prompt())
                str = anotherReadLine()
            }
            str = execute(str, output)
        }
    }

    fun scriptSessionSetup(eng: ScriptEngine)  // KotlinJsr223JvmLocalScriptEngine
    {
        val sco = scriptContext!!
        val sbi = sco.getBindings(ScriptContext.ENGINE_SCOPE)
        // val sbi = sco.getBindings(ScriptContext.GLOBAL_SCOPE)
        //val sbi = scriptContext.getBindings()
        // ret.eval("""@file:Import("testData/import-common.main.kts")""")
        eng.eval("import org.nexa.libnexakotlin.*", sco)
        eng.eval("import org.wallywallet.wew.*", sco)
        // eng.eval("import org.wallywallet.wew.WewButton", sco)
        eng.eval("import org.nexa.libnexakotlin.simpleapi.*", sco)
        eng.eval("import org.wallywallet.wew.cli.*", sco)
        eng.eval("import org.wallywallet.composing.*", sco)
        eng.eval("import androidx.compose.ui.unit.dp", sco)
        eng.eval("import kotlin.coroutines.CoroutineContext", sco)
        eng.eval("import kotlinx.serialization.json.*", sco)  // For JsonElement and related classes
        eng.eval("import java.util.EnumSet", sco)

        sbi["""_connect"""] =::connectBlockchain
        eng.eval("""fun connect(cs:ChainSelector): Blockchain { return (bindings["_connect"] as (ChainSelector) -> Blockchain)(cs) }""", sco)
        scriptTopLevelSyms["connect"] = ::connectBlockchain

        //sbi["_result"] = null
        eng.eval("""val result: Any? get() = bindings["_result"]""", sco)
        eng.eval("""val resultType: kotlin.reflect.KClass<*> get() = bindings["_resultType"] as kotlin.reflect.KClass<*>""", sco)
        scriptTopLevelSyms["result"] = { sbi["_result "] }
        sbi["_coCtxt"] = coCtxt
        sbi["_coScope"] = coScope

        // WEIRDNESS: If you set it up like this:
        // sbi["wallets"] = wallets
        // then when you access in the CLI a field (like this wallets["w"].name) you get this:
        // Exception: ERROR Unresolved reference: name (ScriptingHost46399077_Line_15_gen_2.kts:1:17)
        // Because the interpreter does not know the type.

        sbi["_cnxns"] = cnxnMgrs
        val result = eng.eval("""val cnxns: MutableMap<ChainSelector,CnxnMgr> get() = (bindings["_cnxns"]!! as MutableMap<ChainSelector,CnxnMgr>)""", sco)
        // println(result)

        sbi["_wallets"] = accounts
        eng.eval("""val wallets: MutableMap<String, Wallet> get() = (bindings["_wallets"] as MutableMap<String, Wallet>)""", sco)
        sbi["_blockchains"] = object: MutableMap<ChainSelector,Blockchain> {
            override val entries: MutableSet<MutableMap.MutableEntry<ChainSelector, Blockchain>>
                get() {
                    return blockchains.entries
                }
            override val keys: MutableSet<ChainSelector>
                get() {
                    return blockchains.keys
                }
            override val size: Int
                get() {
                    return blockchains.size
                }
            override val values: MutableCollection<Blockchain>
                get() {
                    return blockchains.values
                }

            override fun clear()
            {
                blockchains.clear()
            }

            override fun isEmpty(): Boolean
            {
                return blockchains.isEmpty()
            }

            override fun remove(key: ChainSelector): Blockchain?
            {
                lastBlockchainUsed = key
                return blockchains.remove(key)
            }

            override fun putAll(from: Map<out ChainSelector, Blockchain>)
            {
                return blockchains.putAll(from)
            }

            override fun put(key: ChainSelector, value: Blockchain): Blockchain?
            {
                lastBlockchainUsed = key
                return blockchains.put(key,value)
            }

            override fun get(key: ChainSelector): Blockchain?
            {
                lastBlockchainUsed = key
                return blockchains.get(key)
            }

            override fun containsValue(value: Blockchain): Boolean
            {
                return blockchains.containsValue(value)
            }

            override fun containsKey(key: ChainSelector): Boolean
            {
                return blockchains.containsKey(key)
            }

        }

            //blockchains
        eng.eval("""val blockchains: MutableMap<ChainSelector,Blockchain> get() = (bindings["_blockchains"] as MutableMap<ChainSelector,Blockchain>)""", sco)
        scriptTopLevelSyms["cnxns"] = cnxnMgrs
        scriptTopLevelSyms["wallets"] = accounts
        scriptTopLevelSyms["blockchains"] = blockchains
        //(sbi["kotlin.script.state"] as AggregatedReplStageState<*,*>).history[12].item.first as LazyScriptDescriptor
    }

    fun injectSymbol(s:String, v: Any?)
    {
        engCtrl.waitfor({ engine != null })
        {
            val b = scriptContext!!.getBindings(ScriptContext.ENGINE_SCOPE)
            b[s] = v
        }
    }

    fun symbols():Bindings
    {
        return engCtrl.waitfor({ engine != null })
        {
            val b = scriptContext!!.getBindings(ScriptContext.ENGINE_SCOPE)
            b
        }
    }

    fun getSymbol(s:String): Any?
    {
        return engCtrl.waitfor({ engine != null })
        {
            val b = scriptContext!!.getBindings(ScriptContext.ENGINE_SCOPE)
            b[s]
        }
    }

    fun<T> getTypedSymbol(s:String): T
    {
        return engCtrl.waitfor({ engine != null })
        {
            val b = scriptContext!!.getBindings(ScriptContext.ENGINE_SCOPE)
            b[s] as T
        }
    }


    fun resetScriptEngine()
    {
        var r = 0L
        engCtrl.lock {
            engine = null
            scriptContext = null
            r = engRestarts
            engCtrl.wake()
        }
        engCtrl.waitfor({ engRestarts != r}) {}
        for (e in engResetActions) e()
    }

    fun restartScriptEngine()
    {
        var r = 0L
        engCtrl.lock {
            engine = null
            r = engRestarts
            engCtrl.wake()
        }
        engCtrl.waitfor({ engRestarts != r}) {}
    }

    fun startScriptEvaluator()
    {
        engThread = org.nexa.threads.Thread("scriptEval")
        {
            while(true)
            {
                engCtrl.lock {  // Recreate the engine if its null
                    if (engine == null)
                    {
                        if (engineClassLoader == null)
                            engineClassLoader = DynamicUrlClassLoader(arrayOf())
                        Thread.currentThread().contextClassLoader = engineClassLoader
                        val engineFactory = KotlinJsr223DefaultScriptEngineFactory()
                        val eng = engineFactory.getScriptEngine()

                        if (eng == null)
                        {
                            throw Exception("No kotlin script engine -- you probably need to fall back to a prior version of kotlin")
                        }
                        engine = eng

                        if (scriptContext==null)
                        {
                            scriptContext = SimpleScriptContext()
                            scriptSessionSetup(eng)
                        }
                        engRestarts++
                        engCtrl.wake()
                    }
                }
                // Wait for the prior command to be consumed and the next one to be ready
                engCtrl.waitfor({ engResult == null && engCmd != null || engine==null})
                {
                    assert(engResult == null)
                    var eng = engine
                    if (eng != null)  // loop around and create a new one if removed
                    {
                        // Process the next command if one is given to us
                        val c = engCmd
                        if (c == "!restartEngineKeepSymbols")
                        {
                            engine = null
                            engResult = null
                            engResultReady = true
                        }
                        else if (c != null)
                        {
                            // Raw eval means skip all CLI processing, just hand the command to the interpreter
                            engResult = if (rawEval)
                            {
                                rawEval = false
                                try
                                {
                                    LogIt.info("executing $c (hidden)")
                                    eng.eval(c, scriptContext)
                                }
                                catch(e:Exception)
                                {
                                    handleThreadException(e)
                                    //LogIt.error("error during raw eval of $c.  Error is $e")
                                    e
                                }
                            }
                            else _eval(c, engOut!!)
                            engResultReady = true
                        }
                        engCmd = null
                        engCtrl.wake()  // Wake everybody who is waiting for a result
                    }
                }
            }

        }
    }

    fun eval(cmd:String, output: Output, inputLineOverride: String? = null):Any?
    {
        var state = 0
        var ret:Any? = null
        while(state < 2)
        {
            engCtrl.waitfor({ (state == 0 && !engResultReady && engCmd == null) || (state == 1 && engResultReady) }) {
                if (state == 0)
                {
                    assert(engResult == null)
                    assert(engCmd == null)
                    engCmd = cmd
                    processing.value = cmd
                    engOut = output
                    state++
                    engCtrl.wake()
                }
                else if (state == 1)
                {
                    state++
                    ret = engResult
                    engCmd = null
                    processing.value = null
                    engResult = null
                    engResultReady = false
                }
            }
        }
        return ret
    }

    fun invisibleEval(cmd:String):Any?
    {
        var state = 0
        var ret:Any? = null
        while(state < 2)  // Actually just passes the request off to the interpreter thread
        {
            engCtrl.waitfor({ (state == 0 && !engResultReady && engCmd == null) || (state == 1 && engResultReady) }) {
                if (state == 0)
                {
                    assert(engResult == null)
                    assert(engCmd == null)
                    rawEval = true
                    engCmd = cmd
                    state++
                    engCtrl.wake()
                }
                else if (state == 1)
                {
                    state++
                    ret = engResult
                    engResult = null
                    engResultReady = false
                    rawEval = false
                }
            }
        }
        return ret
    }


    /** Actually do the evaluation (inside the thread) */
    fun _eval(cmd:String, output: Output, inputLineOverride: String? = null):Any?
    {
        var recmd = cmd
        val cmdcount = inputLines.size
        val eng = engine
        if (eng != null)
        {
            var trycount = 0
            while(trycount < 10)
            {
                trycount++
                try
                {
                    //print("Cmd: $recmd")
                    //print(" Eng: $eng\n")
                    val ret: Any? = eng.eval(recmd, scriptContext) //, bindings!!)
                    if (recmd != cmd)
                    {
                        output.info("Fixing: $cmd -> $recmd\n")
                    }
                    if (ret != null)
                    {
                        output.stdout(ret)
                    }
                    injectSymbol("_result", ret)
                    if (ret != null) injectSymbol("_resultType", ret::class)
                    else injectSymbol("_resultType", null)
                    //eng.eval("""resultOf[$cmdcount] = result""", scriptContext)
                    resultOf[cmdcount] = ret
                    return ret
                }
                catch (e: ScriptException)
                {
                    //println("scriptException $e")
                    var realException: Throwable = e.cause ?: e

                    val msg = e.message
                    if (msg != null)
                    {
                        if ("error: no value passed for parameter" in msg)
                        {
                            // TODO  figure out how to provide help for that item
                        }
                        if ("ERROR Only safe" in msg)
                        {
                            try
                            {
                                // example: ERROR Only safe (?.) or non-null asserted (!!.) calls are allowed on a nullable receiver of type Wallet? (ScriptingHost142ca6b9_Line_17.kts:1:14)
                                val colons = msg.split(":")
                                val line = colons[colons.size - 2].toInt()
                                val char = colons[colons.size - 1].dropLast(1).toInt() - 1
                                recmd = recmd.substring(0, char) + "!!" + recmd.substring(char)
                                continue
                            }
                            catch(e: Exception)
                            {
                                // if anything goes wrong with the fixup, give up
                                LogIt.info("fixup error $e")
                            }
                        }
                    }

                    output.displayException(realException, cmdcount, eng, e.fileName?.let { "${e.fileName}:${e.lineNumber}.${e.columnNumber}" })
                    resultOf[cmdcount] = e
                    return realException

                }
                catch (e: Exception)
                {
                    output.displayException(e, cmdcount, eng)
                    resultOf[cmdcount] = e
                    return e
                }
            }
        }
        return null
    }

}

fun Init()
{
    //val netCoroutineContext = newFixedThreadPoolContext(3, "net")
    //org.nexa.libnexakotlin.NetExecContext = netCoroutineContext
    initializeLibNexa()
    appI18n = { i18nLbc[it] ?: "LibnexaError$it"}
    val searchPath = System.getenv("WEW_SCRIPT_PATH")
    if (searchPath != null)
    {
        scriptSearchPath = searchPath.split(":")
    }
    else
    {
        scriptSearchPath = mutableListOf(System.getProperty("user.dir"), System.getProperty("user.home"))
    }
}

/*
class EarlyExitException:Exception()

fun SearchAllActivity(secretWords: String, chainSelector: ChainSelector, ecCnxn: ElectrumClient? = null,  maxGap: Int?=null, progress :((AccountSearchResults)->Unit)?=null): Pair<AccountSearchResults, iBlockHeader>
{
    val gap = maxGap ?: WALLET_FULL_RECOVERY_DERIVATION_PATH_MAX_GAP
    val net = connectBlockchain(chainSelector).net
    var ec: ElectrumClient? = null

    fun getEc():ElectrumClient
    {
        val start = org.nexa.threads.millinow()
        return retry(10) {
            val tmp = ec
            if (tmp != null && tmp.open) ec
            else
            {
                progress?.invoke(AccountSearchResults(AccountSearchResultsStatus.CONNECTION_LOST, "Reconnecting to Electrum", mutableMapOf(),listOf(), 0,0,0,0))
                ec = net.getElectrum()
                ec
            }
        }
    }


    try
    {
        val (tip, tipHeight) = getEc().getTip()

        val passphrase = "" // TODO: support a passphrase
        val secret = generateBip39Seed(secretWords, passphrase)

        val addressDerivationCoin = Bip44AddressDerivationByChain(chainSelector)

        LogIt.info("Searching in ${addressDerivationCoin} (Nexa path)")
        var activity = searchDerivationPathActivity(::getEc, chainSelector, gap, {
                val key = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, addressDerivationCoin, 0, false, it)
                key
            }, progress)

        LogIt.info("Searching in ${addressDerivationCoin} change (Nexa change path)")
        var activity1 = searchDerivationPathActivity(::getEc, chainSelector, WALLET_FULL_RECOVERY_DERIVATION_PATH_MAX_GAP, {
            val key = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, addressDerivationCoin, 0, true, it)
            key
        }, progress)

        LogIt.info("Searching in 0 (legacy BIP44 path)")
        var activity2 = searchDerivationPathActivity(::getEc, chainSelector, WALLET_FULL_RECOVERY_DERIVATION_PATH_MAX_GAP, {
            val key = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, 0, 0, true, it)
            key
        }, progress)

        LogIt.info("Searching in 0 (legacy BIP32 path)")
        var activity3 = searchDerivationPathActivity(::getEc, chainSelector, WALLET_FULL_RECOVERY_DERIVATION_PATH_MAX_GAP, {
            val key = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP32, 0, 0, true, it)
            key
        }, progress)

                    // TODO need to explicitly push nonstandard addresses into the wallet, by explicitly returning them.
        // otherwise the transactions won't be noticed by the wallet when we jam them in.
        LogIt.info("Searching in ${AddressDerivationKey.ANY} (multi-chain path")
        // Look for activity in the identity and common location
        var activity4 = searchDerivationPathActivity(::getEc, chainSelector, WALLET_FULL_RECOVERY_NONSTD_DERIVATION_PATH_MAX_GAP, {
                val key = libnexa.deriveHd44ChildKey(secret, AddressDerivationKey.BIP44, AddressDerivationKey.ANY, 0, false, it)
                key
            }, progress)

        val act = activity + activity1 + activity2 + activity3 + activity4
        return Pair(act,tip)
    }
    finally
    {
        ec?.let { net.returnElectrum(it) }
    }
    LogIt.info("Account search is complete")
}

enum class xxAccountSearchResultsStatus
{
    CONNECTION_LOST,
    SEARCHING,
    COMPLETED
}

data class AccountSearchResults(
    val status: AccountSearchResultsStatus,
    val details: String,
    val txh: MutableMap<Hash256, TransactionHistory>,
    val dests: List<PayDestination>,
    val addrCount: Long,
    val lastAddressIndex: Int,
    val balance: Long,
    val lastCheckedIndex: Int)
{
    operator fun plus(asr: AccountSearchResults): AccountSearchResults
    {
        val nmap = txh.toMutableMap()
        asr.txh.forEach({ k,v -> nmap[k] = v })
        val ret = AccountSearchResults(status, details, nmap, dests + asr.dests, addrCount + asr.addrCount, max(lastAddressIndex, asr.lastAddressIndex), balance + asr.balance, max(lastCheckedIndex, asr.lastCheckedIndex))
        return ret
    }
}
*/
/*
fun xxSearchDerivationPathActivity(getEc: () -> ElectrumClient, chainSelector: ChainSelector, maxGap:Int, secretDerivation: (Int) -> Pair<ByteArray,String>, ongoingResults: ((AccountSearchResults)->Unit)?=null): AccountSearchResults
{
    var addrsFound = 0L
    var index = 0
    var gap = 0
    var ret = mutableMapOf<Hash256, TransactionHistory>()
    val retDests = mutableListOf<PayDestination>()
    var hdrs = mutableMapOf<Int, iBlockHeader>()
    var bal = 0L
    var lastAddressIndex = index
    var gapMultiplier = 1  // Works around an error in early wallets where they did not use addresses in order
    while (gap < maxGap * gapMultiplier)
    {
        val (newSecret, pathString) = secretDerivation(index)
        val us = UnsecuredSecret(newSecret)

        val dests = mutableListOf<PayDestination>()  // Note, if multiple destination types are allowed, the wallet load/save routines must be updated
        if (chainSelector.hasTemplates)
            dests.add(Pay2PubKeyTemplateDestination(chainSelector, us, index.toLong()))
        else dests.add(Pay2PubKeyHashDestination(chainSelector, us, index.toLong()))

        var found = false
        for (dest in dests)
        {
            retDests.add(dest)
            var replied = false
            while(!replied) try
            {
                val script = dest.lockingScript()
                val history = getEc().getHistory(script, 10000)
                if (history.size > 0)
                {
                    LogIt.info("Found activity at address $index path $pathString")
                    found = true
                    lastAddressIndex = index
                    addrsFound++
                    //retDests.add(dest)
                    for (h in history)
                    {
                        // Its easy to get repeats because a wallet sends itself change, spends 2 inputs, etc.
                        // But I don't need to investigate any repeats
                        if (!ret.containsKey(h.second))
                        {
                            val tx = getEc().getTx(h.second)
                            val txh: TransactionHistory = TransactionHistory(chainSelector, tx)
                            //  Load the header at this height from a little cache we keep, or from the server
                            val header = hdrs[h.first] ?: run {
                                val headerBytes = getEc().getHeader(h.first)
                                blockHeaderFor(chainSelector, BCHserialized(SerializationType.NETWORK, headerBytes))
                            }
                            if (header.validate(chainSelector))
                            {
                                if (txh.confirmedHeight < WALLET_RECOVERY_NON_INCREMENTAL_ADDRESS_HEIGHT) gapMultiplier=3
                                txh.confirmedHeight = h.first.toLong()
                                txh.confirmedHash = header.hash
                                txh.date = header.time
                                ret[h.second] = txh
                                replied = true
                            }
                            else
                            {
                                LogIt.error("Electrum header validation failure!!!")
                                assert(false)
                            }
                        }
                        else
                        {
                            replied = true
                        }
                    }
                    val unspent = getEc().listUnspent(dest, 10000)
                    for (u in unspent)
                    {
                        found = true
                        bal += u.amount
                    }
                }
                else
                {
                    // LogIt.info("No activity at address $index")
                    replied = true
                }
                ongoingResults?.invoke(AccountSearchResults(AccountSearchResultsStatus.SEARCHING, "", ret, retDests, addrsFound, lastAddressIndex, bal, index))
            }
            catch (e: ElectrumNotFound)
            {
                LogIt.info("Electrum connection problem, retrying")
                assert(false)
            }
        }
        if (found) gap = 0
        else gap++
        index++
    }
    return AccountSearchResults(AccountSearchResultsStatus.COMPLETED, "", ret, retDests, addrsFound, lastAddressIndex, bal, index)
}
*/