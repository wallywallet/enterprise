package Nexa.npl

import org.nexa.libnexakotlin.*
import kotlin.experimental.and
import kotlin.experimental.or
import kotlin.experimental.xor

/** Interpret a bytearray as a boolean in the same manner the Nexa VM would
 * **Note an array of hex: 0000..0080 is seen as -0, or false!!!!**
 * */
fun ByteArray.asVmBool():Boolean
{
    val bzero = 0.toByte()
    if (size == 0) return false
    for (i in 0 until size-1)
    {
        if (this[i] != bzero) return true
    }
    // Everything prior was 0, now look at the last byte
    val last = this[size-1]

    // As per the VM, this buffer interpreted as a LE negative zero number counts as a zero
    if (last == bzero || last == 0x80.toByte()) return false
    // ok wasn't 0 or -0, so true
    return true
}

/** base class for all classes that you want to have member functions that can access the base NSL APIs */
open class PgmEnvironment
{
    fun if_(condition: NBinding, then: NSL.()->Unit, else_: (NSL.()->Unit)? = null):List<NBinding> = cc.nsl!!.if_(condition, then, else_)

    fun splitInt(data: NBytes, location: NInt, pfxName:String? = null, suffixName: String? = null): Pair<NInt, NBytes> =
        cc.nsl!!.splitLeSignMagInt(data, location, pfxName, suffixName)

    /** The index of the input being evaluated */
    fun thisIndex() = cc.nsl!!.thisIndex()

    /** The constraint script for a particular input (the script in the spent utxo) */
    fun inputConstraintScript(idx: NInt):NScript = cc.nsl!!.inputConstraintScript(idx)

    /** The solution script for a particular input (the script that satisfies the utxo constraints) */
    fun inputSatisfierScript(idx: NInt):NScript = cc.nsl!!.inputSatisfierScript(idx)
}


/** Bind some data on the VM stack to an object */
open class NBinding(_name: String? = null): PgmEnvironment()
{
    init {
        if (_name != null) bind(_name)
    }
    var instantiable: (() -> Step)? = null  // if not null, this is the code to instantiate this binding on the stack
    var weight = Int.MAX_VALUE  // used in partial ordering algorithms
    var lastUse:Step? = null  // What step is this binding last used in
    var name: String?=null
    var bound: Boolean = false

    /** The source code location where this binding was created */
    var srcLoc = SourceLocation()

    val locations = mutableListOf<NSL.BindingLocation>()

    /** Set this to true to indicate that this binding was created but will never be used.
     *  This avoids a dead code warning, and is necessary in a few cases where an opcode produces several outputs but all of them are not needed.
     * */
    var unused: Boolean = false

    /** Returns true if this object is already bound */
    open val isBound: Boolean
        get() = bound

    /** Return the value primitive if undergoing dataflow evaluation (if the binding is bound to a real value, as opposed to a stack).
     * Return null if compiling
     * */
    open val value: Any? = null

    /** Bind this object now, if it is not yet bound
     * @param _name Propose a name for this binding.  If the name is already used, _{monotonically increasing number} will be appended */
    open fun bind(_name: String? = null)
    {
        // If I rebind, I may be connecting this binding to a new script.
        // But I still want to keep its original binding data to give to the script developer for debugging
        // So the only thing I should do in a rebind is switch the script.
        // TODO: reuse won't work in multithreaded compilation! (we'll have to copy the binding)
        synchronized(bindingNames)
        {
            if (bound) return
            bound = true
            srcLoc = SourceLocation()
            if (name == null)     // Do not overwrite a name that's already been given
            {
                name = _name ?: "t${bindingCount}"
                bindingCount++
            }
            else if (bindingNames.containsKey(name)) // name collision
            {
                name = name + "_${bindingCount}"
                bindingCount++
            }
            // Name must be set at this point
            bindingNames[name!!] = this
        }
    }

    open fun assign(b: NBinding)  // Set the contents of this binding to b
    {
        // Create a dummy statement that transforms the input into the output (except it is the same underlying bytes)
        if (cc.compiling)
            cc.nsl.exec(listOf(b),listOf(), listOf(this))
        if (b.value != null)
            this.setValue(b)
        if (!bound) this.bind()
    }

    override fun toString(): String
    {
        return """ "$name" """
    }

    /** reset the current value of this binding to no value (for evaluation not compilation) */
    open fun resetValue() {}

    open fun setValue(v: Any) {}

}


open class NBytes(name: String? = null): NBinding(name)
{
    /** Explicit binding type transformation */
    constructor(b: NBinding): this()
    {
        srcLoc = SourceLocation() // + "(NScript of " + b.srcloc + ")"
        name = b.name + "_as_NBytes"
        this.assign(b)
    }

    companion object
    {
        /** Create an unbound object, but name it.
         * This is done internally sometimes when the binding will happen soon thereafter
         * and we want to provide a meaningful name.
         */
        fun unbound(name: String? = null):NBytes
        {
            val ret = NBytes(name)
            ret.bound=false
            return ret
        }
    }
    /** When evaluating (not compiling), this is the current value of this binding */
    var curVal: ByteArray? = null

    /** Return the value primitive if undergoing dataflow evaluation (if the binding is bound to a real value, as opposed to a stack).
     * Return null if compiling
     * */
    override val value: Any?
        get() = curVal

    override fun toString(): String
    {
        val tmp = curVal
        return if (tmp == null) """$name:Bytes"""
        else """$name:Bytes=${tmp.toHex()}h"""
    }

    /** reset the current value of this binding to no value (for evaluation not compilation) */
    override fun resetValue() { curVal = null }

    override fun setValue(v: Any)
    {
        if (v is NInt)
            curVal = v.curVal?.leSignMag()
        else if (v is Long)
            curVal = v.leSignMag()
        else if (v is Int)
            curVal = v.toLong().leSignMag()
        else if (v is ByteArray)
           curVal = v as ByteArray
        else if (v is NBytes)
            curVal = v.curVal!!
        else
            mycheck(false)  // Can't convert this type into a NBytes
        if (!bound) bind()
    }

    /** concatenation primitive */
    infix operator fun plus(other:NBytes): NBytes
    {
        val ret = NBytes(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me + it; return@plus ret }}

        cc.nsl!!.exec(setOf(this, other), OP.CAT, listOf(ret))
        return ret
    }

    /** OP.BIN2NUM primitive: Convert these bytes into an integer */
    fun toInt(dest: NInt? = null): NInt
    {
        val ret = dest ?: NInt()
        val tmp = curVal
        if (tmp == null)
        {
            val _nsl: NSL = cc.nsl!!
            _nsl.exec(this, OP.BIN2NUM, ret)
        }
        else
        {
            ret.curVal = tmp.leSignMag()
        }
        return ret
    }

    /** Fails the script if the item is false, 0, or -0 */
    fun verify()
    {
        if (curVal == null)
        {
            cc.nsl!!.exec(setOf(this), OP.VERIFY, listOf())
        }
        else
        {
            curVal!!.let { if (!it.asVmBool()) throw NSL.VerifyException("verify of $name failed") }
        }
    }

    /** Fails the script if the items are not bytewise equal */
    infix fun equalVerify(other: NBytes)
    {
        val oth = other.curVal
        val ths = curVal
        if ((oth != null) && (ths != null))
        {
            if (!(oth contentEquals ths)) throw NSL.VerifyException("verify of $name == ${other.name} failed because ${oth.toHex()} != ${ths.toHex()}")
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            _nsl.exec(setOf(this, other), OP.EQUALVERIFY, listOf())
        }
    }

    /** Returns 1 if this item is bytewise equal to the passed value */
    infix fun eq(other:NBytes): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me contentEquals it) 1 else 0; return@eq ret }}

        cc.nsl!!.exec(setOf(this, other), OP.EQUAL, listOf(ret))
        return ret
    }


    /** Return the length of this object */
    fun size(): NInt
    {
        val ths = curVal
        if (ths != null)
        {
            return NCInt(ths.size)
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NInt(name + "_size")
            _nsl.exec(setOf(this), OP.SIZE, listOf(this, ret))
            return ret
        }
    }

    /** reverse PRIMITIVE: Reverse the bytes of this object */
    fun reverse(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            return NCBytes(ths.reversedArray())
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_reversed")
            _nsl.exec(setOf(this), OP.REVERSE, listOf(ret))
            return ret
        }
    }

    /** OP.HASH160 PRIMITIVE: Return the SHA256 then the RIPEMD160 hash of this object */
    fun hash160(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            return NCBytes(libnexa.hash160(ths))
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_hash160")
            _nsl.exec(setOf(this), OP.HASH160, listOf(ret))
            return ret
        }
    }

    /** OP.HASH256 PRIMITIVE: Return the double SHA256 of this object */
    fun hash256(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            return NCBytes(libnexa.hash256(ths))
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_hash256")
            _nsl.exec(setOf(this), OP.HASH256, listOf(ret))
            return ret
        }
    }

    /** OP.SHA256 PRIMITIVE: Return the SHA256 of this object */
    fun sha256(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            return NCBytes(libnexa.sha256(ths))
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_sha256")
            _nsl.exec(setOf(this), OP.SHA256, listOf(ret))
            return ret
        }
    }

    /** OP.RIPEMD160 PRIMITIVE: Return the RIPEMD160 hash of this object */
    fun ripemd160(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            TODO()
            return NCBytes(libnexa.sha256(ths))
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_ripemd160")
            _nsl.exec(setOf(this), OP.RIPEMD160, listOf(ret))
            return ret
        }
    }

    /** OP.SHA1 PRIMITIVE: Return the SHA1 hash of this object */
    fun sha1(): NBytes
    {
        val ths = curVal
        if (ths != null)
        {
            TODO()
            return NCBytes(libnexa.sha256(ths))
        }
        else
        {
            val _nsl: NSL = cc.nsl!!
            val ret = NBytes(name + "_sha1")
            _nsl.exec(setOf(this), OP.SHA1, listOf(ret))
            return ret
        }
    }

    infix fun or(other:NBytes): NBytes
    {
        val ret = NBytes(null)
        // Do an evaluation if possible
        curVal?.let {
            me -> other.curVal?.let {
                if (me.size != it.size) throw NSL.OpcodeException("Cannot OR different length stack items")
                ret.curVal = ByteArray(it.size, { i -> me[i] or it[i] })
            }
        }

        cc.nsl!!.exec(setOf(this, other), OP.OR, listOf(ret))
        return ret
    }

    infix fun xor(other:NBytes): NBytes
    {
        val ret = NBytes(null)
        // Do an evaluation if possible
        curVal?.let {
                me -> other.curVal?.let {
            if (me.size != it.size) throw NSL.OpcodeException("Cannot OR different length stack items")
            ret.curVal = ByteArray(it.size, { i -> me[i] xor it[i] })
        }
        }

        cc.nsl!!.exec(setOf(this, other), OP.XOR, listOf(ret))
        return ret
    }

    infix fun and(other:NBytes): NBytes
    {
        val ret = NBytes(null)
        // Do an evaluation if possible
        curVal?.let {
                me -> other.curVal?.let {
            if (me.size != it.size) throw NSL.OpcodeException("Cannot OR different length stack items")
            ret.curVal = ByteArray(it.size, { i -> me[i] and it[i] })
        }
        }

        cc.nsl!!.exec(setOf(this, other), OP.AND, listOf(ret))
        return ret
    }

    /** Split these bytes into 2 pieces
     * @param location Where to split.  The first byte array will be this many bytes long */
    fun split(location: NInt): Pair<NBytes, NBytes> = cc.nsl.split(this, location)
}

/** Nexa Constant bytes binding */
open class NCBytes(val v:ByteArray, val size:Int?=null):NBytes()
{
    constructor(s: String, size: Int?) : this(s.toByteArray().copyOf(if (size != null) size else s.length), size)

    init
    {
        var name = "${v.toHex()}h"
        if (name.length > 66) name = name.take(8) + "..." + name.takeLast(8)
        bind("$name") // constants can be created "magically", so immediately bind
        // and set the code to indicate how it is done
        weight = -1  // It can be instantiated at any time so use a weight that implies its existed since the beginning of the script
        instantiable = {
            Step(-1, VM_NO_CHANGE, listOf(OP.push(v)), VmState(this), false)
        }
        // The current value of a constant is always the constant
        curVal = v
    }

    override fun toString(): String
    {
        return """ "$name:NCBytes" """
    }

    /** reset the current value of this binding to no value (for evaluation not compilation) */
    override fun resetValue()
    {
        // The reset value of a constant is the constant.
        // It better not have changed, but just in case set it
        curVal = v
    }

    override fun setValue(v: Any)
    {
        // You cannot set a constant
        mycheck(false)
    }

}


/** A group identifier */
open class NGroupId(name: String? = null) : NBytes(name)
{

    /** Type transformation: Create an NGroupId from some bytes */
    constructor(b: NBytes):this(b.name)
    {
        assign(b)
    }

}

/** A signature.
 * These wrappers around NBytes allow type checking at the NSL layer.
 * If you have NBytes that you know are a signature, you can create an NSig using that object
 */
open class NSig(name: String? = null) : NBytes(name)
{
    /** The value to "reset" this object to, if restarting evalutation or compilation */
    var origVal: ByteArray? = null

    /** Type transformation: Create an NSig from some bytes */
    constructor(b: NBytes):this(b.name)
    {
        assign(b)
    }

    /** Construct a constant sig */
    constructor(b: ByteArray):this()
    {
        bind("0x${b.toHex()}") // constants can be created "magically", so immediately bind
        weight = -1  // It can be instantiated at any time so use a weight that implies its existed since the beginning of the script
        instantiable = {
            Step(-1, VM_NO_CHANGE, listOf(OP.push(curVal!!)), VmState(this), false)
        }
        origVal = b
        curVal = origVal
    }

    override fun resetValue()
    {
        curVal = origVal
    }

    override fun toString(): String = """$name:NSig"""
}

/** A pubkey.
 * These wrappers around NBytes allow type checking at the NSL layer
 * */
open class NPubKey(name: String? = null) : NBytes(name)
{
    /** The value to "reset" this object to, if restarting evalutation or compilation */
    var origVal: ByteArray? = null

    /** Type transformation: Create an NPubKey from some bytes */
    constructor(b: NBytes):this(b.name)
    {
        assign(b)
    }

    /** Construct a constant PubKey */
    constructor(b: ByteArray):this()
    {
        bind("0x${b.toHex()}") // constants can be created "magically", so immediately bind
        weight = -1  // It can be instantiated at any time so use a weight that implies its existed since the beginning of the script
        instantiable = {
            Step(-1, VM_NO_CHANGE, listOf(OP.push(curVal!!)), VmState(this), false)
        }
        origVal = b
        curVal = origVal
    }

    override fun toString(): String = """$name:NPubKey"""
}



/** Nexa Constant Group Id */
open class NCGroupId(val gid:GroupId):NGroupId()
{
    constructor(s: String) : this(GroupId(s))

    init
    {
        bind(gid.toString()) // constants can be created "magically", so immediately bind
        // and set the code to indicate how it is done
        weight = -1  // It can be instantiated at any time so use a weight that implies its existed since the beginning of the script
        instantiable = {
            Step(-1, VM_NO_CHANGE, listOf(OP.push(gid.toByteArray())), VmState(this), false)
        }
        // The current value of a constant is always the constant
        curVal = gid.toByteArray()
    }

    override fun toString(): String
    {
        return """ "$name:NCGroupId" """
    }

    /** reset the current value of this binding to no value (for evaluation not compilation) */
    override fun resetValue()
    {
        // The reset value of a constant is the constant.
        // It better not have changed, but just in case set it
        curVal = gid.toByteArray()
    }

    override fun setValue(v: Any)
    {
        // You cannot set a constant
        mycheck(false)
    }
}

object ThisGroup: NGroupId()
{
    // TODO actual group id
    var gid = org.nexa.libnexakotlin.GroupId(ChainSelector.NEXA, ByteArray(32, { 0 }))
    init {
        bind("ThisGroup")
        weight = -1
        instantiable = {
            TODO("get instantiated group id")
            Step(-1, VM_NO_CHANGE, listOf(OP.push(this.gid.data)), VmState(this), false)
        }
    }
}

/** Any introspection opcode implicitly uses the state of this transaction.  This binding captures that idea.
 * The transaction state is a binding that's always available
 * Its instantly instantiable with no opcodes needed to do so :-), and already instantiated.
 */
object ThisTransaction: NBinding()
{
    init {
        bind("TX")
        weight = -1
        instantiable = { Step(-1, VM_NO_CHANGE, listOf(), VM_NO_CHANGE, false)}
    }
}


open class NHashData: NBytes()
{}


/** A Nexa Address */
open class NAddress(name: String? = null) : NBinding(name)
{
    protected var _pubkey : NPubKey? = null
    protected var _argshash : NBytes? = null


    /** return a binding for this address in args hash format (with just this one address as the arg) */
    val argsHash: NBytes
        get()
        {
            var t = _argshash  // Create a binding for this if we use it
            if (t == null)
            {
                t = NBytes()
                t.name = name + ".argsHash"
                _argshash = t
            }
            return t
        }

    /** return a binding for the pubkey of this address */
    val pubKey: NPubKey
        get()
        {
            var t = _pubkey  // Create a binding for this if we use it
            if (t == null)
            {
                t = NPubKey()
                t.name = name + ".pubKey"
                _pubkey = t
            }
            return t
        }

}

open class NCAddress(val address: PayDestination, name: String? = null, nsl: NSL? = null):NAddress(name)
{
    init
    {
        // If this PayDestination has certain properties, populate the field with the appropriate constant
        val pk = address.pubkey
        if (pk != null)
        {
            _pubkey = NPubKey(pk)
            if (name != null) _pubkey!!.name = name + ".pubkey"
        }
        // TODO: else populate with a Binding that creates an error if used

        val ah = address.constraintArgsHash()
        if (ah != null)
        {
            _argshash = NCBytes(ah)
            if (name != null) _pubkey!!.name = name + ".argshash"
        }
        // TODO: else populate with a Binding that creates an error if used
    }

}



open class NScript(name: String? = null, nsl: NSL? = null) : NBytes(name)
{
    /** Explicit binding type transformation */
    constructor(b: NBytes) : this()
    {
        srcLoc = SourceLocation()  // + "(NScript of " + b.srcloc + ")"
        name = b.name
    }

    /** Explicit binding type transformation */
    constructor(b: NBinding) : this()
    {
        srcLoc = SourceLocation() // + "(NScript of " + b.srcloc + ")"
        name = b.name + "_as_NScript"
        this.assign(b)
    }

    constructor(b: SatoshiScript, name: String? = null): this(name)
    {
        this.curVal = b.toByteArray()
    }

    override fun toString(): String
    {
        return """ "$name:NScript" """
    }

    fun templateAndArgsHash(): Pair<NBytes, NBytes>
    {
        val (gid, rest) = splitPush()

        val ifResult = if_(gid.size(), // if there's a real group, ignore the amount
            {
                val gamt = NBytes("gamt")
                val rest2 = NScript("rest2")
                rest.splitPushInto(gamt, rest2)
                result(rest2)
            },
            {  // otherwise the next parameter is the amount
                result(rest)
            })
        val noGrpOrAmt = NScript(ifResult[0])

        val (tmpl, rest3) = noGrpOrAmt.splitPush()  // peel off the script template
        val (argsHash, rest4) = rest3.splitPush()  // TODO this might be a bug if nothing left in the script?
        return Pair(tmpl, argsHash)
    }

    /** Splits the first operation (which must be a data push) off of this script */
    fun splitPush(): Pair<NBytes, NScript>
    {
        val data = this
        val n = cc.nsl
        if (n == null) throw NslException("NSL script context is null")

        val (opcode: NInt, rest) = splitInt(data, 1.nx, "opcode", "rest")

        val opNotZero = opcode neq NCInt(OP.PUSHFALSE.v[0])
        val ifResult = n.if_(opNotZero,
            {
                // handle const push opcodes
                val r2 = if_(opcode lt OP.PUSHDATA1.v[0].nx,
                {
                    val (pushdata, rest2) = splitInto(rest, opcode, NBytes(), NScript())  // The first 75 opcodes can be interpreted as push that amount of data to the stack
                    result(pushdata, rest2)
                },
                {
                    // handle constant numbers
                    val r3 = if_(opcode lte OP.C16.v[0].nx,
                        {
                            val pushdata = opcode - (OP.C1.v[0]-1).nx
                            result(pushdata, rest)
                        },
                        {
                            // TODO -1 pushed onto the stack
                            fail()
                        })
                    result(r3[0], r3[1])
                })
                result(r2[0], r2[1])
            },
            {
                // If its zero, that's what was pushed
                result(opcode, rest)
            }
            )
                // TODO bigger push amounts

        return Pair(NBytes(ifResult[0]), NScript(ifResult[1]))
    }

    fun splitPushInto(car: NBytes, cdr: NScript)
    {
        val (a, b) = splitPush()
        car.assign(a)
        cdr.assign(b)
    }

}


open class NInt(name: String?): NBinding(name)  // While ultimately in the VM Ints are bytes, we want to keep these separate, so don't derive from NBytes
{
    companion object
    {
        /** Create an unbound object, but name it.
         * This is done internally sometimes when the binding will happen soon thereafter, and we want to provide a meaningful name.
         */
        fun unbound(name: String? = null):NInt
        {
            val ret = NInt(name)
            ret.bound=false
            return ret
        }
    }

    /** When evaluating (not compiling), this is the current value of this binding */
    var curVal: Long? = null

    /** Return the value primitive if undergoing dataflow evaluation (if the binding is bound to a real value, as opposed to a stack).
     * Return null if compiling
     * */
    override val value: Any?
        get() = curVal

    constructor(): this(null)

    constructor(b: NBytes):this() // Explicit binding type transformation
    {
        // use the original binding information for debugging
        srcLoc = b.srcLoc
        name = b.name
        assign(b)
    }

    override fun toString(): String
    {
        return """ "$name:NInt" """
    }

    /** OP.NUM2BIN primitive: Convert this integer into leSignMag bytes */
    fun toBytes(size:NInt, dest: NBytes? = null): NBytes
    {
        val ret = dest ?: NBytes()
        val tmp = curVal
        if (tmp == null)
        {
            val _nsl: NSL = cc.nsl!!
            _nsl.exec(listOf(this, size), OP.NUM2BIN, listOf(ret))
        }
        else
        {
            ret.curVal = tmp.leSignMag(4)
        }
        return ret
    }

    infix fun or(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me or it; return@or ret }}

        cc.nsl!!.exec(setOf(this, other), OP.OR, listOf(ret))
        return ret
    }

    infix fun xor(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me xor it; return@xor ret }}

        cc.nsl!!.exec(setOf(this, other), OP.OR, listOf(ret))
        return ret
    }

    infix fun and(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me and it; return@and ret }}

        cc.nsl!!.exec(setOf(this, other), OP.OR, listOf(ret))
        return ret
    }

    /*  Bit shift is disabled
    infix fun shr(other:NInt): NInt
    {
        val _nsl: NSL = (nsl ?: other.nsl)!!
        val ret = NInt(null, _nsl)
        _nsl.exec(listOf(this, other), OP.RSHIFT, listOf(ret))

        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me shr it }}  // TODO is Kotlin shr sign-preserving?
        return ret
    }

    infix fun shl(other:NInt): NInt
    {
        val _nsl: NSL = (nsl ?: other.nsl)!!
        val ret = NInt(null, _nsl)
        _nsl.exec(listOf(this, other), OP.LSHIFT, listOf(ret))

        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me shl it }}  // TODO is Kotlin shr sign-preserving?
        return ret
    }
     */


    /* DO NOT USE:  Use [eq] instead
    override fun equals(other: Any?): Boolean
    {
        if (other is NInt) throw NSL.ProgrammingException("""Due to a limitation of Kotlin, use 'eq' e.g. 'if (a eq b)' rather than '=='.""")
        return super.equals(other)
    }

     */

    /** NUMNOTEQUAL primitive: check if a != b, where a and be are script integers */
    infix fun neq(other:NInt): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me != it) 1 else 0; return@neq ret }}

        cc.nsl!!.exec(setOf(this, other), OP.NUMNOTEQUAL, listOf(ret))
        return ret
    }

    /** NUMEQUAL primitive: check if a == b, where a and be are script integers */
    infix fun eq(other:NInt): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me == it) 1 else 0; return@eq ret }}

        cc.nsl!!.exec(setOf(this, other), OP.NUMEQUAL, listOf(ret))
        return ret
    }


    /** GREATERTHAN primitive: check if a > b */
    infix fun gt(other:NInt): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me > it) 1 else 0; return@gt ret }}

        cc.nsl!!.exec(setOf(this, other), OP.GREATERTHAN, listOf(ret))
        return ret
    }

    /** GREATERTHANOREQUAL primitive: check if a > b */
    infix fun gte(other:NInt): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me >= it) 1 else 0; return@gte ret }}

        cc.nsl!!.exec(setOf(this, other), OP.GREATERTHANOREQUAL, listOf(ret))
        return ret
    }

    /** LESSTHAN primitive: check if a > b */
    infix fun lt(other:NInt): NBool
    {
        val ret = NBool(null)
        // Only do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me < it) 1 else 0; return@lt ret }}

        cc.nsl!!.exec(setOf(this, other), OP.LESSTHAN, listOf(ret))
        return ret
    }

    /** LESSTHANOREQUAL primitive: check if a > b */
    infix fun lte(other:NInt): NBool
    {
        val ret = NBool(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = if (me <= it) 1 else 0; return@lte ret }}

        cc.nsl!!.exec(setOf(this, other), OP.LESSTHANOREQUAL, listOf(ret))
        return ret
    }

    /** ADD primitive: add these 2 numbers */
    infix operator fun plus(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me + it; return@plus ret }}

        cc.nsl!!.exec(setOf(this, other), OP.ADD, listOf(ret))
        return ret
    }

    infix operator fun minus(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me - it; return@minus ret }}

        cc.nsl!!.exec(listOf(this, other), OP.SUB, listOf(ret))
        return ret
    }

    infix operator fun times(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me * it;  return@times ret }}

        cc.nsl!!.exec(setOf(this, other), OP.MUL, listOf(ret))
        return ret
    }

    infix operator fun div(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me / it; return@div ret }}

        cc.nsl!!.exec(listOf(this, other), OP.DIV, listOf(ret))
        return ret
    }

    infix operator fun rem(other:NInt): NInt
    {
        val ret = NInt(null)
        // Do an evaluation if possible
        curVal?.let { me -> other.curVal?.let { ret.curVal = me % it; return@rem ret }}

        cc.nsl!!.exec(listOf(this, other), OP.MOD, listOf(ret))
        return ret
    }

    /** increment this number */
    fun inc(): NInt
    {
        val ret = NInt(name + "++")
        // Do an evaluation if possible
        curVal?.let { ret.curVal = it+1; return@inc ret}

        cc.nsl!!.exec(this, OP.ADD1, ret)
        return ret
    }

    /** Decrement this number */
    fun dec(): NInt
    {
        val ret = NInt(name + "--")
        // Do an evaluation if possible
        curVal?.let { ret.curVal = it-1; return@dec ret}

        cc.nsl!!.exec(this, OP.SUB1, ret)
        return ret
    }

    infix operator fun plus(other:Int): NInt = plus(NCInt(other))
    infix operator fun minus(other:Int): NInt = minus(NCInt(other))
    infix operator fun times(other:Int): NInt = times(NCInt(other))
    infix operator fun div(other:Int): NInt = div(NCInt(other))
    infix operator fun rem(other:Int): NInt = rem(NCInt(other))
    // DISABLED OPCODE infix fun shl(other:Int): NInt = shl(NCInt(other, this.nsl))
    // DISABLED OPCODE infix fun shr(other:Int): NInt = shr(NCInt(other, this.nsl))

    /** Fails the script if the item is false, 0, or -0 */
    fun verify()
    {
        if (curVal != null)
        {
            if (curVal!! == 0L) throw NSL.VerifyException("verify of $name failed")
        }
        else
            cc.nsl!!.exec(setOf(this), OP.VERIFY, listOf())
    }


    /** reset the current value of this binding to no value (for evaluation not compilation) */
    override fun resetValue() { curVal = null }

    override fun setValue(v: Any)
    {
        mycheck(v is Int || v is Long || v is UInt || v is ULong)
        when(v)
        {
            is Int -> curVal = v.toLong()
            is Long -> curVal = v
            is UInt -> curVal = v.toLong()
            is ULong -> curVal = v.toLong()
        }
    }
}

    /** Nexa Boolean Binding */

open class NBool(name: String?=null): NInt(name)
    {

    }

/** Nexa Constant Int Binding */
open class NCInt(val v: Long): NInt()
{
    constructor(b: Byte): this(b.toLong())
    constructor(b: Int): this(b.toLong())

    init{
        bind("C$v") // constants can be created "magically", so immediately bind
        // and set the code to indicate how it is done
        weight = -1  // It can be instantiated at any time so use a weight that implies its existed since the beginning of the script
        instantiable = {
            Step(-1, VM_NO_CHANGE, listOf(OP.push(v)), VmState(this), false)
        }
        // The current value of a constant is always the constant
        curVal = v
    }

    override fun toString(): String
    {
        return """ "$name:NCInt" """
    }

    /** reset the current value of this binding to no value (for evaluation not compilation) */
    override fun resetValue()
    {
        // The reset value of a constant is the constant.
        // It better not have changed, but just in case set it
        curVal = v.toLong()
    }

    override fun setValue(v: Any)
    {
        // You cannot set a constant
        mycheck(false)
    }
}

/** bind an integer into a nexa const */
// TODO find one that already exists (for possible reuse)
val Int.nx
    get() = NCInt(this)

val ByteArray.nx
    get() = NCBytes(this)


/** When a string is converted to a Nexa constant, it is converted to bytes without the null terminator (since Nexa VM stack items are sized) */
val String.nx
    get() = NCBytes(this.toByteArray())
