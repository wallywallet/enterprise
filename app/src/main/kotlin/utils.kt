package org.wallywallet.wew

import org.nexa.libnexakotlin.*
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.net.URL
import java.net.URLClassLoader
import java.util.jar.JarFile

private val LogIt = GetLog("wewutils")

/** Make some type (probably a primitive type) into an object that holds one of them */
class Objectify<T>(var obj: T)
{
}

val i18nLbc = mapOf<Int, String>(
    RinsufficentBalance to "insufficentBalance",
    RbadWalletImplementation to "badWalletImplementation",
    RdataMissing to "PaymentDataMissing",
    RwalletAndAddressIncompatible to "chainIncompatibleWithAddress",
    RnotSupported to "notSupported",
    Rexpired to "expired",
    RsendMoreThanBalance to "sendMoreThanBalance",
    RbadAddress to "badAddress",
    RblankAddress to "blankAddress",
    RblockNotForthcoming to "blockNotForthcoming",
    RheadersNotForthcoming to "headersNotForthcoming",
    RbadTransaction to "badTransaction",
    RfeeExceedsFlatMax to "feeExceedsFlatMax",
    RexcessiveFee to "excessiveFee",
    Rbip70NoAmount to "badAmount",
    RdeductedFeeLargerThanSendAmount to "deductedFeeLargerThanSendAmount",
    RwalletDisconnectedFromBlockchain to "walletDisconnectedFromBlockchain",
    RsendDust to "sendDustError",
    RnoNodes to "NoNodes",
    RwalletAddressMissing to "badAddress",
    RunknownCryptoCurrency to "unknownCryptoCurrency",
    RsendMoreTokensThanBalance to "insufficentTokenBalance"
)


fun splitByBeginningBang(input:String):List<String>
{
    val ret = mutableListOf<String>()
    val lines = input.lines()
    var lastLines = StringBuilder()
    for(l in lines)
    {
        if (l.startsWith("!"))
        {
            ret.add(lastLines.toString())
            ret.add(l)
            lastLines.clear()
        }
        else lastLines.append(l + "\n")
    }
    ret.add(lastLines.toString())
    return ret.toList()
}

/*
fun ChainSelector.explorer(s:String):String
{
    return when(this)
    {
        ChainSelector.NEXA -> "https://explorer.nexa.org" + s
        ChainSelector.NEXATESTNET -> "https://testnet-explorer.nexa.org" + s
        ChainSelector.NEXAREGTEST -> "http://localhost" + s
        ChainSelector.BCH -> "https://explorer.bitcoinunlimited.info" + s
        ChainSelector.BCHTESTNET -> "https://testnet-explorer.bitcoinunlimited.info" + s
        ChainSelector.BCHREGTEST -> "http://localhost" + s
    }
}
 */

class DynamicUrlClassLoader(urls: Array<URL>):URLClassLoader(urls)
{
    fun add(url: URL)
    {
        addURL(url)
    }
}

/*
class DynamicClassLoader(dir:String, parent: ClassLoader? = Thread.currentThread().contextClassLoader): ClassLoader(parent)
{
    var plugins= mutableListOf<JarFile>() //JarFile(File("/ssd/git/wew/app/build/plugins/niftyWew-1.0.jar")))

    fun add(f: File)
    {
        if (!f.exists()) throw FileNotFoundException(f.absolutePath)
        plugins.add(JarFile(f))
    }
    fun add(f: String) = add(File(f))

    override fun findClass(name: String): Class<*>
    {
        val classFileName = name.replace('.', '/') + ".class"
        LogIt.info("dynCL: Searching $name ($classFileName)")
        for (p in plugins)
        {
            val jarEntry = p.getJarEntry(classFileName)
            if (jarEntry != null)
            {
                val classBytes = p.getInputStream(jarEntry).use(InputStream::readBytes)
                LogIt.info("  Found in ${p.name}")
                return defineClass(name, classBytes, 0, classBytes.size)
            }
        }
        LogIt.info("  Not Found in ${plugins.map { it.name}}")
        val ret = super.findClass(name)
        return ret
    }
}

 */

/** Take all the items provided by the iterator function and turn them into a list */
fun<T> take(qty: Int, iterFn: ((T) -> Boolean) ->Unit ):MutableList<T>
{
    val ret = mutableListOf<T>()
    var count = 0
    if (qty == 0) return ret
    iterFn {
        ret.add(it)
        count++
        (count >= qty)
    }
    return ret
}

/** Take all the items provided by the iterator function and turn them into a list */
fun<T> take(iterFn: ((T) -> Boolean) ->Unit ):MutableList<T>
{
    val ret = mutableListOf<T>()
    iterFn {
        ret.add(it)
        false
    }
    return ret
}
