package org.wallywallet.composing
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.*
import androidx.compose.ui.unit.*

import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlin.properties.Delegates
import kotlin.reflect.KProperty
import androidx.compose.ui.platform.LocalDensity
import org.nexa.threads.Mutex
import org.wallywallet.wew.Output
import org.wallywallet.wew.TextFace
import org.wallywallet.wew.consoleTextStyle

@Composable
fun Float.pxToDp(): Dp
{
    val density = LocalDensity.current.density
    return (this / density).dp
}

interface Composing
{
    @Composable
    abstract fun compose()

    open fun highlight(push: Boolean, purpose: String? = null) {}

    open fun id():String? = null
}

interface ComposingContainer: Composing
{
    abstract var items:MutableList<MutableList<Composing>>
}

interface Refreshing
{
    abstract fun refresh()
}

/** You can drop ComposeFace things into this object */
interface DropTarget: Composing
{
    /** If you pass null for offset, the object is not considered to have been dropped into any particular spot */
    abstract fun add(obj: Composing, pos: Offset? = null)

    /** Returns true if this object was there to be removed */
    abstract fun remove(obj: Composing): Boolean
}

/** convert from these composing classes to @Composables */
class Composer(var run: @Composable () -> Unit):Composing
{
    @Composable
    override fun compose()
    {
        run()
    }
}


class Nothing:Composing
{
    @Composable()
    override fun compose()
    {}
}

val nothing = Nothing()

// Used as a "newline" stands for "compose new line"
val CNL = Nothing()

open class TriggeredRefresh(val dofirst:()->Unit = {} ):Refreshing
{
    protected val trigger = MutableStateFlow(0)
    fun<T> changed(a: KProperty<*>, b: T, c: T)
    {
        refresh()
    }

    @Composable
    fun onRefresh(fn:(@Composable () -> Unit))
    {
        key(trigger.collectAsState().value) { fn() }
    }

    override fun refresh()
    {
        dofirst()
        trigger.update { it+1 }
    }
}

open class Sizeable(val refresh: TriggeredRefresh,width: Dp? = null, height: Dp? = null)
{
    var width by Delegates.observable(width, refresh::changed)
    var height by Delegates.observable(height, refresh::changed)

    /** Change both the width and height (if not null) by the provided fraction */
    fun scale(amt: Double)
    {
        width?.let { width = it * amt.toFloat() }
        height?.let { height = it * amt.toFloat() }
    }
}

/** handler for the output (text, errors) of any commands -- this class gets plugged into the bottom of various
 * print functions in the interpreter.
 *
 * This class just forwards all the output calls into the interpreter state object (which is tracking the current state of the console and interpreter)
 */

fun String.splitKeepDelimiter(delimiter: String): List<String>
{
    val result = mutableListOf<String>()
    var startIndex = 0

    while (true)
    {
        val index = indexOf(delimiter, startIndex)
        if (index == -1) break // No more matches
        if (startIndex != index)
            result.add(substring(startIndex, index))
        result.add(delimiter)
        startIndex = index + delimiter.length
    }

    if (startIndex < length) result.add(substring(startIndex))
    return result
}

class ComposingOutput(): ComposingContainer, Refreshing, Output()
{
    var colMod = Modifier
    var rowMod = Modifier
    var rowValign = Alignment.CenterVertically
    val accessLock = Mutex()
    override var items:MutableList<MutableList<Composing>> = mutableListOf(mutableListOf<Composing>())
    protected val trigger = MutableStateFlow(0)
    //public val lst: MutableStateFlow<SnapshotStateList<Composing>> = MutableStateFlow<SnapshotStateList<Composing>>(SnapshotStateList<Composing>())
    override fun stdout(s:Any?)
    {
        if (s != null)
        {
            if (s is String)
            {
                var curLine = items.last()  // Get the last row
                val sLines = s.splitKeepDelimiter("\n")
                accessLock.lock {
                    for (line in sLines)
                    {
                        if (line == "\n")
                        {
                            curLine = mutableListOf<Composing>()
                            items.add(curLine)
                        }
                        else if (line != "")
                        {
                            var appended = false
                            // For efficiency append text into the prior text if its just standard boring text
                            curLine.lastOrNull()?.let {
                                if (it is TextFace)
                                {
                                    if (it.textStyle == consoleTextStyle)  // No fancy formatting
                                    {
                                        it.text.value = it.text.value + line
                                        appended = true
                                    }
                                }
                            }
                            if (!appended) curLine.add(cify(line))
                        }
                    }
                    refresh()
                }
            }
            else
            {
                if (s == CNL)  // New line
                {
                    accessLock.lock {
                        items.add(mutableListOf())
                    }
                }
                else
                {
                    accessLock.lock {
                        items.last().add(cify(s))
                    }
                }
                refresh()
            }
        }
    }
    override fun error(s:String?)
    {
        if (s != null)
        {
            accessLock.lock { items.add(mutableListOf(cify(s))) }
            refresh()
        }
    }
    override fun info(s:String?)
    {
        if (s != null)
        {
            accessLock.lock { items.add(mutableListOf(cify(s))) }
            refresh()
        }
    }

    @Composable override fun compose()
    {
        key(trigger.collectAsState().value) {
            val cpy = accessLock.lock { items.toList() }

            Column(modifier = colMod) {
                for (row in cpy)
                {
                    Row(modifier = rowMod, verticalAlignment = rowValign) {
                        for (item in row)
                        {
                            item.compose()
                        }
                    }
                }
            }
        }
    }

    override fun refresh()
    {
        trigger.update { it+1 }
    }
}
