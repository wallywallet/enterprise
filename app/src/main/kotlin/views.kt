package org.wallywallet.wew

import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.toPainter
import androidx.compose.ui.input.pointer.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow
import org.nexa.libnexakotlin.*
import org.nexa.threads.Mutex
import org.wallywallet.composing.*
import org.wallywallet.composing.Composer
import org.wallywallet.wew.*
import java.io.ByteArrayInputStream
import java.io.File
import java.time.Instant.ofEpochSecond
import javax.imageio.ImageIO

private val LogIt = GetLog("BU.enterpriseWallet.views")

fun ByteArrayPainter(imageData: ByteArray): Painter
{
    val inputStream = ByteArrayInputStream(imageData)
    val bufferedImage = ImageIO.read(inputStream) // Read the byte array as an image
    return bufferedImage.toPainter()
}

class ImageFace(val fp: Painter): Composing
{
    constructor(filename: String, width:Int?=null, height:Int?=null):this(FilePainter(filename, width, height))

    constructor(ba: ByteArray):this(ByteArrayPainter(ba))

    @Composable()
    override fun compose()
    {
        Image(fp, "", Modifier.wrapContentSize())
    }

    fun savePng(f: File)
    {
        val bmp = (fp as? FilePainter)?.bmp
        if (bmp != null)
        {
            bmp.savePng(f)
        }
        else
        {
            TODO()
        }
    }
}

class ResourcePainterFace(val filename: String, var width:Int?=null, var height:Int?=null): Composing
{
    var fp: Painter? = null
    @Composable()
    override fun compose()
    {
        if (fp == null)
        {
            fp = painterResource(filename)
        }

        var mod = width?.let { Modifier.width(it.dp)} ?: Modifier.wrapContentWidth()
        mod = height?.let { mod.height(it.dp)} ?: mod.wrapContentHeight()
        fp?.let { Image(it, "", mod) }
    }
}

class TextFace(startText: String, var textStyle: (@Composable ()-> TextStyle) = consoleTextStyle):
    Composing
{
    var maxLines = 1
    var text = MutableStateFlow<String>(startText)
    @Composable
    override fun compose()
    {
        Text(
            text.collectAsState().value,
            Modifier.wrapContentSize().clipToBounds(),
            style = textStyle(),
            overflow = TextOverflow.Clip,
            maxLines = maxLines
        )
    }
}

class FieldFace(var obj: Any?,
                /** How to resolve this object from various contexts (string or ()->String) */
                var path: List<Any>?=null,
                /** Where to send proposed resolutions (when user alt-clicks) */
                var dataSink: ((Any?, DataRole) -> Boolean)? = defaultDataSink,
                var textStyle: (@Composable ()-> TextStyle) = fieldTextStyle,
                var dataDisplayStyle: DataDisplayStyle = DataDisplayStyle.SIMPLE):
    Composing
{
    var maxLines = 1
    @Composable
    override fun compose()
    {
        var lastPath by remember { mutableStateOf(0) }
        var lastObj = remember { mutableStateOf(obj) }
        val mod = Modifier.wrapContentSize().clipToBounds().pointerInput(Unit) {
            while (true)
            {
                val event = awaitPointerEventScope {
                    awaitPointerEvent()
                }
                if (event.type == PointerEventType.Press)
                {

                    if (event.keyboardModifiers.isCtrlPressed)
                    {

                    }
                    else if (event.keyboardModifiers.isAltPressed)
                    {
                        // Multiple clicks will move through all offered paths
                        var p = path
                        if (lastObj.value != obj)  // reset if we go to another obj
                        {
                            lastPath = 0
                            lastObj.value = obj
                        }
                        p?.let {
                            if (p.size > 0)
                            {
                                if (lastPath >= p.size) lastPath = 0
                                dataSink?.invoke(p[lastPath], DataRole.DATAPATH)
                                lastPath += 1
                            }
                        }
                    }
                    else if (event.keyboardModifiers.isShiftPressed)
                    {

                    }
                    else
                    {
                        dataSink?.invoke(obj, DataRole.DATA)
                    }

                }
            }
        }
        composeAnything(obj, mod, textStyle, dataDisplayStyle, maxLines)
    }

}
typealias FF = FieldFace

class ExpanderFace(var title: Composing, var contents: Composing, var expanded: Boolean = false):
    Composing
{
    var color : Color = defaultBoxColor
    var background: Color = Color.Unspecified
    var shape : Shape = _root_ide_package_.androidx.compose.foundation.shape.RoundedCornerShape(25)
    var borderWidth : Int = 3
    var padding: Int = 2

    @Composable()
    override fun compose()
    {
        var exp by remember { mutableStateOf(expanded) }
        var mod = Modifier.background(background, shape).border(
            BorderStroke(
                borderWidth.dp,
                color
            ), shape).padding(padding.dp)

        var clickMod = Modifier.clickable { exp = !exp; expanded = exp }.pointerHoverIcon(PointerIcon(java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR)))
        Column {
            Row(verticalAlignment = _root_ide_package_.androidx.compose.ui.Alignment.CenterVertically) {
                if (exp) Text(" ▽ ", clickMod, fontSize = _root_ide_package_.androidx.compose.ui.unit.TextUnit(1.75f, _root_ide_package_.androidx.compose.ui.unit.TextUnitType.Em)) else Text(" ▷ ", clickMod,fontSize = _root_ide_package_.androidx.compose.ui.unit.TextUnit(1.75f, _root_ide_package_.androidx.compose.ui.unit.TextUnitType.Em))
                title.compose()
            }
            if (exp) contents.compose()
        }
    }
}


class IndentFace(var amt: Int, var item: Composing): Composing
{
    @Composable()

    override fun compose()
    {
        var mod = Modifier.padding(amt.dp,0.dp,0.dp,0.dp)
        Box(mod) {
            item.compose()
        }
    }
}

class BlockFace(var cs: ChainSelector, var height: Long?, var hash: Hash256, var heightPath: String?, var hashPath: String?): Composing,
    Refreshing
{
    var e = build()

    fun build(): Composing
    {
        return RowCol(listOf(listOf(
            FieldFace(
                { height ?: "unconfirmed" },
                listOf({ heightPath },
                    { height?.let { "blockchains[${cs}]!!.blockHeader(${height})" } },
                    { "open(\"" + cs.explorer("/block-height/") + height + "\")" })
            ),
            TextFace(":"),
            FieldFace({ hash.toHex() },
                listOf(
                    { hashPath },
                    { """blockchains[${cs}]!!.blockHeader(Hash256("${hash.toHex()}"))""" },
                    { "open(\"" + cs.explorer("/block/") + hash.toHex() + "\")" }
                )
            ))))
    }

    @Composable
    override fun compose()
    {
        e.compose()
    }

    override fun refresh()
    {
        e = build()
    }
}

class TokenFace(var data: GroupInfo?): Composing, TriggeredRefresh()
{
    val b = build()
    @Composable
    override fun compose() = key(trigger.collectAsState().value) { b.compose() }
    override fun id():String = data?.groupId.toString()
    override fun refresh() { trigger.value += 1 }

    fun build(): Composing
    {
        val g = data
        if (g == null) return rowOf()
        val d = mutableListOf<Composing>()
        if (g.isAuthority())
        {
            d.add(TextFace("  Token authority: "))
            d.add(FF(GroupAuthorityFlags.toString(g.authorityFlags)))
            d.add(TextFace(" of "))
            d.add(FF(g.groupId.toString()))
        }
        else
        {
            d.add(TextFace("  Token: "))
            d.add(FF(g.tokenAmt))
            d.add(TextFace(" of "))
            d.add(FF(g.groupId.toString()))
        }
        return rowOf(d)
    }
}

class SpendableFace(var spendable: Spendable, var w: Wallet?=null): Composing, TriggeredRefresh()
{
    val e = buildContents()
    @Composable
    override fun compose()
    {
        key(trigger.collectAsState().value) {
            e.compose()
        }
    }
    override fun refresh()
    {
        // Reload the tx from the wallet if possible to get any changes
        trigger.value += 1
    }

    fun cmd():String
    {
        w?.let {
            return ("""wallets["${it.name}"]!!.getTxo(outpointFor(ChainSelector.${spendable.chainSelector},"${spendable.outpoint?.toHex()}"))""")
        }
        return ""
    }

    fun buildContents(): Composing
    {
        val spent = if (spendable.isUnspent) alignedRow(TextFace("Unspent"))
        else alignedRow(
            TextFace("Spent: "),
            BlockFace(spendable.chainSelector, spendable.spentHeight, spendable.spentBlockHash, cmd() + "!!.spentHeight", cmd() + "!!.spentBlockHash")
        )

        return colOf(
            alignedRow(FieldFace(spendable.amount), TextFace(" sats  "), TokenFace(spendable.groupInfo)),
            alignedRow(
                TextFace("Address: "),
                FieldFace(spendable.addr.toString()),
                TextFace("  Id: "),
                FieldFace(spendable.outpoint?.toHex())
            ),
            alignedRow(
                TextFace("Confirmed: "),
                BlockFace(spendable.chainSelector, spendable.commitHeight, spendable.commitBlockHash, cmd() + "!!.commitHeight", cmd() + "!!.confirmedHash")
            ),
            spent
        )
    }
}

class TxHistoryFace(var txh: TransactionHistory, var w: Wallet?=null): Composing,
    Refreshing
{
    // Keep a hold of TxFace objects we create so that their state in the GUI is not lost
    val cachedTxFaces = mutableMapOf<String, TxFace>()
    val trigger = MutableStateFlow(0)
    val cs = w?.chainSelector ?: lastBlockchainUsed ?: ChainSelector.NEXA
    val e = expander(buildTitle(), buildContents())
    @Composable
    override fun compose()
    {
        key(trigger.collectAsState().value) {
            e.compose()
        }
    }

    override fun id():String
    {
        return txh.tx.idem.hash.toHex()
    }

    fun cmd():String
    {
        w?.let {
            return ("""wallets["${it.name}"]!!.getTx(Hash256("${txh.tx.idem.toHex()}"))""")
        }
        return ""
    }
    fun open():String
    {
        val tmp = cs ?: lastBlockchainUsed ?: ChainSelector.NEXA

        return("""open("${tmp.explorer("/tx/" + txh.tx.idem.toHex())}")""")
    }

    fun buildTitle(): Composing
    {
        val r = mutableListOf(TextFace( dateTimeFormatter.format(_root_ide_package_.java.time.Instant.ofEpochSecond(txh.date/1000))), TextFace("  Idem: "), FieldFace(txh.tx.idem.toHex(), listOf(cmd(), open())))

        if (txh.incomingAmt > 0)
        {
            r.add(TextFace("  Received: "))
            r.add(FieldFace(txh.incomingAmt, listOf(cmd() + """!!.incomingAmt""")))
        }
        if (txh.outgoingAmt > 0)
        {
            r.add(TextFace("  Sent: "))
            r.add(FieldFace(txh.incomingAmt, listOf(cmd() + """!!.outgoingAmt""")))
        }
        return RowCol(listOf(r)).alignStart()
    }

    fun buildContents(): Composing
    {
        val contents = mutableListOf<MutableList<Composing>>()
        if (txh.note.isNotBlank())
        {
            val row = mutableListOf<Composing>(TextFace("Note: "), FieldFace(txh.note, listOf(cmd() + "!!.note")))
            contents.add(row)
        }
        if (txh.relatedTo.isNotEmpty())
        {
            val subjs = txh.relatedTo.keys.joinToString(", ")
            val row = mutableListOf<Composing>(TextFace("Related To: " + subjs))
            contents.add(row)
        }

        val c = txh.confirmedHash
        val row = if (c == null)
        {
            mutableListOf<Composing>(TextFace("Unconfirmed"))
        }
        else
        {
            mutableListOf<Composing>(TextFace("Confirmed: "), BlockFace(cs, txh.confirmedHeight, c, cmd()+"!!.confirmedHeight", cmd()+"!!.confirmedHash"))
        }
        contents.add(row)
        // Reuse existing faces if possible so gui state is not lost
        val face = cachedTxFaces.getOrPut(txh.tx.idem.toHex(), { TxFace(txh.tx) })
        contents.add(mutableListOf(face))

        return IndentFace(defaultExpanderIndent, RowCol(contents).alignStart())
    }

    override fun refresh()
    {
        // Reload the tx from the wallet if possible to get any changes
        w?.getTx(txh.tx.idem)?.let { txh = it }
        e.title = buildTitle()
        e.contents = buildContents()
        trigger.value += 1
    }

}

class TxFace(val tx: iTransaction): Composing
{
    val b = build()

    fun buildInputs(): Composing
    {
        val rows = mutableListOf<MutableList<Composing>>(mutableListOf())

        for (i in tx.inputs)
        {
            //r1.add(TextFace("Address:"))
            //r1.add(FF(i.spendable.addr.toString()))
            rows.last().add(TextFace("Amount: "))
            rows.last().add(FF(i.spendable.amount))
            rows.last().add(TextFace("  Outpoint: "))
            rows.last().add(FF(i.spendable.outpoint?.toHex()))
            rows.add(mutableListOf())  // new line
        }
        if (rows.size < 20) return RowCol(rows).alignStart()
        else
        {
            val tmp = CCLazyColumn { rows }
            tmp.mod = Modifier.heightIn(50.dp, 600.dp)
            return tmp
        }
    }

    fun buildOutputs(): Composing
    {
        val d = mutableListOf<MutableList<Composing>>(mutableListOf())

        for (i in tx.outputs)
        {
            d.last().add(TextFace("Amount: "))
            d.last().add(FF(i.amount))
            val s = i.script
            d.last().add(TextFace("  Address: "))
            d.last().add(FF(s.address))
            val g = s.groupInfo(i.amount)
            if (g != null)
            {
                if (g.isAuthority())
                {
                    d.last().add(TextFace("  Token authority: "))
                    d.last().add(FF(GroupAuthorityFlags.toString(g.authorityFlags)))
                    d.last().add(TextFace(" of "))
                    d.last().add(FF(g.groupId.toString()))
                }
                else
                {
                    d.last().add(TextFace("  Token: "))
                    d.last().add(FF(g.tokenAmt))
                    d.last().add(TextFace(" of "))
                    d.last().add(FF(g.groupId.toString()))
                }
            }
            d.add(mutableListOf())  // new line
        }
        if (tx.outputs.size < 20) return RowCol(d).alignStart()
        else
        {
            val tmp = CCLazyColumn { d }
            tmp.mod = Modifier.heightIn(50.dp, 600.dp)
            return tmp
        }
    }

    fun build(): ComposingContainer
    {
        val row1 = mutableListOf<Composing>()
        row1.add(TextFace("Idem:  "))
        row1.add(FF({tx.idem.toHex()},listOf("open(\"" + tx.chainSelector.explorer("/tx/") + tx.idem.toHex() + "\")")))

        val row2 = mutableListOf<Composing>()
        row2.add(TextFace("Version: "))
        row2.add(FF(tx.version))
        row2.add(TextFace("  Size: "))
        row2.add(FF(tx.size))
        row2.add(TextFace("  Fee: "))
        row2.add(FF(tx.fee))
        row2.add(TextFace(" ("))
        row2.add(FF(tx.feeRate))
        row2.add(TextFace(" sat/byte)"))
        row2.add(TextFace("  LockTime: "))
        row2.add(FF(tx.lockTime))

        val row3 = mutableListOf<Composing>()
        row3.add(TextFace("Inputs: "))
        row3.add(FF(tx.inputs.size))
        row3.add(TextFace("  Outputs: "))
        row3.add(FF(tx.outputs.size))

        val row4 = mutableListOf<Composing> (expander(TextFace("Inputs"), buildInputs()))
        val row5 = mutableListOf<Composing> (
            expander(
                TextFace("Outputs"),
                buildOutputs()
            )
        )
        val ret = RowCol(
            listOf(
                row1.toList(),
                row2.toList(),
                row3.toList(),
                row4.toList(),
                row5.toList()
            )
        ) //, mod = Modifier.fillMaxWidth().align(Alignment.Start))
        ret.alignStart()
        return ret
    }

    @Composable
    override fun compose()
    {
        b.compose()
    }
}


fun cmdAccessHeader(height: Long, cs: ChainSelector) = "blockchains[${cs}]!!.blockHeader(${height})"
fun cmdAccessHeader(hash: Hash256, cs: ChainSelector) =  "blockchains[${cs}]!!.blockHeader(Hash256(\"${hash.toHex()}\"))"

class BlockHeaderFace(val hdr: iBlockHeader, var cs: ChainSelector? = null):
    Composing
{
    // TODO cs is set to the blockchain based on the type in toComposing, but how to figure out mainnet,testnet,regtest?
    val b = build()

    fun build(): ComposingContainer
    {
        val chain = cs ?: run {
            if (hdr is NexaBlockHeader)
            {
                if (lastBlockchainUsed.isNexaFamily) lastBlockchainUsed
                else ChainSelector.NEXA
            }
            else
            {
                if (lastBlockchainUsed.isBchFamily) lastBlockchainUsed
                else ChainSelector.BCH
            }
        }
        val row = mutableListOf<Any?>()
        row.add("Id:  ")
        row.add(BlockFace(chain, hdr.height, hdr.hash, cmdAccessHeader(hdr.height, chain),cmdAccessHeader(hdr.hash, chain)))
        row.add(" Size:  ")
        row.add(FF(hdr.size))
        row.add(" Chain Work:  ")
        row.add(FF(hdr.chainWork))
        row.add(" Num Transactions:  ")
        row.add(FF(hdr.txCount))
        val row2 = mutableListOf<Any?>()
        row2.add("Previous Block:  ")
        row2.add(BlockFace(chain, hdr.height-1, hdr.hashPrevBlock,cmdAccessHeader(hdr.height-1, chain),cmdAccessHeader(hdr.hashPrevBlock, chain)))

        val row3 = mutableListOf<Any?>()
        val nexaHeader = (hdr as? NexaBlockHeader)
        if (nexaHeader != null)
        {
            row3.add("Ancestor Block:  ")
            val ancestorHeight = GetAncestorHeight(hdr.height.toInt())
            row3.add(BlockFace(chain, ancestorHeight.toLong(), hdr.hashAncestor,cmdAccessHeader(0, chain),cmdAccessHeader(hdr.hashAncestor, chain)))
        }

        val row4 = mutableListOf<Any?>()
        row4.add("Merkle Root:  ")
        row4.add(FF(hdr.hashMerkleRoot))

        return colOf(row, row2, row3, row4)
    }

    @Composable
    override fun compose()
    {
        b.compose()
    }
}

class BlockchainFace(val b: Blockchain, noFreeze:Boolean=false): Composing
{
    val trigger = TriggeredRefresh({})
    val live = if (noFreeze) null else object: UiCtrlRefreshing(28,0)
    {
        override fun refresh()
        {
            trigger.refresh()
        }
    }

    var r = build()
    var chainExpander: CCExpander? = null

    fun cmd() = "blockchains[${b.chainSelector}]"

    fun build(): Composing
    {
        val lead = mutableListOf("Blockchain: ", FF(b.name, listOf("blockchains[${b.chainSelector}]")), "on: ", FF(b.chainSelector, listOf("open(\"${b.chainSelector.explorer("")}\")")))
        val tip = b.nearTip
        if (tip != null)
        {
            lead.add("at: ")
            lead.add(BlockFace(b.chainSelector, tip.height, tip.hash, cmd()+"!!.neartip!!.height",cmd()+"!!.neartip!!.hash"))
        }
        val netinfo:MutableList<Any?> = mutableListOf("Connections: ", FF(b.net.p2pCnxns.size), CNL, "Peers: ")
        val sep = if (b.net.p2pCnxns.size > 1) CNL else null
        for (p in b.net.p2pCnxns)
        {
            sep?.let { netinfo.add(sep) }
            netinfo.add(FF(p.logName))
            netinfo.add("  Latency: ")
            netinfo.add(FF(p.aveLatency))
            netinfo.add("  Sent: ")
            netinfo.add(FF(p.bytesSent))
            netinfo.add("  Received: ")
            netinfo.add(FF(p.bytesReceived))
            netinfo.add(CNL)
        }
        val blocks = mutableListOf<()-> Composing>()
        if (tip != null)
        {
            for (i in 0 until 100)
            {
                val height = tip.height - i
                if (height >= 0)
                {
                    blocks.add({
                        try
                        {
                            val blk = b.blockHeader(height)
                            BlockFace(b.chainSelector, blk.height, blk.hash, cmd() + "!!.blockHeader(${blk.height})", cmd() + "!!.blockHeader(Hash256(\"${blk.hash.toHex()}\"))")
                        } catch (e: HeadersNotForthcoming)
                        {
                            CCText("loading headers")
                        }
                    })
                }
            }
        }
        val blockinfo = expander(
            rowOf("Blocks"),
            indent(
                defaultExpanderIndent,
                colOf(*(blocks.toTypedArray()))
            )
        )

        /*
        return rowOf(live.refreshView(), expander(rowOf(*lead.toTypedArray()),
            org.wallywallet.composing.indent(defaultExpanderIndent, colOf(
                rowOf(*netinfo.toTypedArray()),
                rowOf(blockinfo)))))
*/

        val contents = indent(
            defaultExpanderIndent, colOf(
                rowOf(*netinfo.toTypedArray()),
                rowOf(blockinfo)
            )
        )

        val tmp = chainExpander
        val ce = if (tmp!=null)
        {
            tmp.contents = contents
            tmp
        }
        else
        {
            val ce = expander(rowOf(*lead.toTypedArray()), contents)
            chainExpander = ce
            ce
        }

        if (live != null)
        {
            val rc = RowCol(listOf(listOf(live.refreshView(), chainExpander!!)))
            rc.rowValign = _root_ide_package_.androidx.compose.ui.Alignment.Top
            return rc
        }
        else
        {
            return chainExpander!!
        }
    }

    fun refresh()
    {
        r = build()
        trigger.refresh()
    }

    @Composable
    override fun compose()
    {
        trigger.onRefresh { r.compose() }
    }

}

class WalletFace(var w: Wallet, noFreeze: Boolean=false):
    Composing, UiCtrlRefreshing(28,0)
{
    val trigger = MutableStateFlow(0)
    val lock = Mutex()
    val txhListState = LazyListState()
    val utxoListState = LazyListState()
    val txHist = MutableStateFlow(mutableStateListOf<TxHistoryFace>())
    var subBlockHeight = 800.dp
    protected val needsRefresh = mutableListOf<Refreshing>()
    val exp = expander(
        rowOf(
            TextFace(
                "Wallet " + w.name + " on " + w.blockchain.chainSelector,
                walletExpanderTitleStyle
            )
        ), buildContent()
    )
    val e = if (noFreeze) exp else rowOf(refreshView(), exp)

    fun buildContent(): Composing
    {
        return lock.lock {
            val cw = (w as CommonWallet)
            val hash = cw.chainstate?.syncedHash?.toHex()
            val time = cw.chainstate?.syncedDate
            val timeStr = time?.let { " Date: " + timeFormatter.format(ofEpochSecond(time)) + "UTC (" + time + ")" } ?: ""

            needsRefresh.clear()

            val utxo = CCScroll(utxoListState, CCLazyColumn(listState = utxoListState) {
                // get a list of the current items in the row
                val s = mutableMapOf<String, Composing>()
                for (c in items) for (r in c)
                {
                    s[r.id()!!] = r
                }

                val toadd = mutableListOf<Spendable>()
                val tokeep = mutableListOf<Composing>()
                w.forEachUtxo {
                    val key = it.outpoint?.toHex()
                    val cur = s[key]
                    if (cur == null) toadd.add(it)
                    else tokeep.add(cur)
                    false
                }
                items.clear()
                tokeep.forEach { items.add(mutableListOf(it)) }
                for (t in toadd)
                {
                    items.add(mutableListOf(SpendableFace(t, w)))
                }

                // Sorts unspent native first then tokens
                val cc: Comparator<MutableList<Composing>> = Comparator<MutableList<Composing>> { a, b ->
                    var agrp: GroupId? = null
                    var aamt:Long = 0L
                    var bgrp: GroupId? = null
                    var bamt:Long = 0L
                    var spA: Spendable? = (a[0] as? SpendableFace)?.spendable
                    if (spA != null)
                    {
                        agrp = spA.groupInfo?.groupId
                        aamt = spA.groupInfo?.tokenAmt ?: spA.amount
                    }
                    var spB: Spendable? = (b[0] as? SpendableFace)?.spendable
                    if (spB != null)
                    {
                        bgrp = spB.groupInfo?.groupId
                        bamt = spB.groupInfo?.tokenAmt ?: spB.amount
                    }

                    if ((agrp == null)&&(bgrp == null)) bamt.compareTo(aamt)  // native to native compare (reverse a and b to do a descending comparision)
                    else if ((agrp != null)&&(bgrp != null))
                    {
                        if (agrp == bgrp) aamt.compareTo(bamt)
                        else agrp.toHex().compareTo(bgrp.toHex())
                    }  // group to group compare -- doesn't make much sense
                    else if (agrp == null) -1  // pick native over group
                    else 1 // pick native over group
                }
                items.sortWith(cc)
                items
            })
            utxo.modifier = utxo.modifier.height(subBlockHeight).fillMaxWidth()

            fun onLoadMore(cnt: Int):Boolean
            {
                // -1 means scrolling down
                val count = if (cnt == -1) 2 else cnt
                val more = if (txHist.value.size == 0)  // start with 20
                {
                    take(20) { cb -> w.forEachTxByDate(cb) }.map { TxHistoryFace(it) }
                }
                else
                {
                    take { cb -> w.forEachTxByDate(txHist.value.last().txh.date - 1, count.toLong(), cb) }.map { TxHistoryFace(it) }
                }
                if (more.size == 0) return false
                txHist.value.addAll(more)
                return true
            }

            val hist = CCScroll(txhListState, Composer {
                val listState = remember { txhListState }
                val txes = txHist.collectAsState()
                /*
                val txes2: SnapshotStateList<TxHistoryFace> = remember {
                    val tmp = take(2) { cb -> w.forEachTxByDate(cb) }
                    val tmp2 = tmp.map { TxHistoryFace(it) }.toTypedArray()
                    mutableStateListOf(*tmp2)
                }
                 */
                val scrollState = rememberScrollState()
                LazyColumn(
                    modifier = Modifier.height(subBlockHeight)
                        .scrollable(scrollState, Orientation.Vertical),
                    state = listState
                ) {
                    items(txes.value) { item ->
                        //Text(item.date.toString() + ": " + item.tx.idem.toHex())
                        item.compose()
                    }
                }

                InfiniteListHandler(listState = listState, buffer = 1) { count ->
                    onLoadMore(count)
                }

            }, { onLoadMore(it) })


            /*
            Need to replace with infinite here

            val hist = org.wallywallet.composing.CCLazyRow {
                val s = mutableSetOf<String>()
                for (c in items) for (r in c) {
                    r.id()?.let { s.add(it) }
                }
                val toadd = mutableListOf<TransactionHistory>()
                w.forEachTxByDate {
                    if (!s.contains(it.tx.idem.hash.toHex())) {
                        toadd.add(it)
                        false
                    }
                    else true // abort once I get one I know about
                }
                for (t in toadd) {
                    val tmp = TxHistoryFace(t, w)
                    items.add(0, mutableListOf(tmp))
                }
                null
            }
            needsRefresh.add(hist)

             */

            //cw.allAddresses
            val unusedD = cw.unusedAddresses.map {
                val dest = cw.walletDestination(it)
                if (dest == null)
                {
                    LogIt.info("missing unused address ${it.toString()}")
                }
                dest
            }
            val unusedDs = unusedD.filterNotNull().sortedBy { it.index }

            val unusedFF = unusedDs.map {
                FieldFace(it.index.toString() + "  " + it.address.toString())
            }
            val unused=unusedFF.tween(CNL) // TextFace(", "))

            val usedA = cw.allAddresses.toMutableSet()
            for (i in cw.unusedAddresses) usedA.remove(i)
            val usedDs = usedA.map { cw.walletDestination(it)!! }.sortedBy { it.index }
            val usedFF = usedDs.map { FieldFace(it.index.toString() + "  " + it.address.toString())  }
            val used=usedFF.tween(CNL) // TextFace(", "))


            val c = listOf(listOf(
                TextFace("At block: "), FieldFace({ w.syncedHeight },
                    listOf({ """wallets["${w.name}"]!!.syncedHeight""" }, { "blockchains[${w.chainSelector}]!!.blockHeader(${w.syncedHeight})" },
                        { "open(\"" + w.chainSelector.explorer("/block-height/") + w.syncedHeight + "\")" })
                ),
                TextFace("  hash: "), FieldFace({
                    (w as CommonWallet).chainstate?.syncedHash
                }, listOf(
                    { """wallets["${w.name}"]!!.syncedHash""" },
                    { """blockchains[${w.chainSelector}]!!.blockHeader(Hash256("${w.syncedHash.toHex()}"))""" },
                    { "open(\"" + w.chainSelector.explorer("/block/") + w.syncedHash.toHex() + "\")" }
                )),
                TextFace("  date: "), FieldFace({
                    val time1 = (w as CommonWallet).chainstate?.syncedDate
                    val timeStr1 = time1?.let { timeFormatter.format(_root_ide_package_.java.time.Instant.ofEpochSecond(time1)) + " UTC (" + time1 + ")" } ?: ""
                    timeStr1
                })
            ), listOf(
                TextFace("Balance: "), FieldFace({ w.balance }, listOf({
                    """wallets["${w.name}"]!!.balance"""
                })),
                TextFace("  Confirmed: "), FieldFace({ w.balanceConfirmed }, listOf(
                    { """wallets["${w.name}"]!!.balanceConfirmed""" }
                )),
                TextFace("  Unconfirmed: "), FieldFace({ w.balanceUnconfirmed }, listOf(
                    { """wallets["${w.name}"]!!.balanceUnconfirmed""" }
                ))
            ),
                listOf(
                    expander(
                        TextFace("usedAddresses", walletExpanderSubtitleStyle),
                        IndentFace(
                            defaultExpanderIndent,
                            RowCol(listOf(used)).alignStart()
                        )
                    )
                ),
                listOf(
                    expander(
                        TextFace("unusedAddresses", walletExpanderSubtitleStyle),
                        IndentFace(defaultExpanderIndent, colOf(unused))
                    )
                ),
                listOf(
                    expander(
                        TextFace("Unspent", walletExpanderSubtitleStyle),
                        IndentFace(defaultExpanderIndent, utxo)
                    )
                ),
                // listOf(expander(TextFace("Unconfirmed", walletExpanderSubtitleStyle), IndentFace(defaultExpanderIndent, unconf))),
                listOf(
                    expander(
                        TextFace("History", walletExpanderSubtitleStyle),
                        IndentFace(defaultExpanderIndent, hist)
                    )
                )
            )

            val d = IndentFace(50, RowCol(c).alignStart())

            val ret = object : Composing
            {
                @Composable
                override fun compose()
                {
                    key(trigger.collectAsState().value)  // forces recomposition so all the wallet state does not have to be "remember"-ed individually
                    {
                        d.compose()
                    }
                }

            }
            ret
        }

    }


    @Composable()
    override fun compose()
    {
        e.compose()
    }

    override fun refresh()
    {
        lock.lock {
            for (r in needsRefresh) r.refresh()
        }
        trigger.value += 1
    }

}