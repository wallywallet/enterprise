package org.wallywallet.wew.cli
// This module is imported within the CLI shell to define globals
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager
import kotlin.script.experimental.jsr223.KotlinJsr223DefaultScriptEngineFactory
import kotlin.script.experimental.jsr223.KotlinJsr223DefaultScript


import Nexa.npl.*
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import org.nexa.libnexakotlin.*
import org.nexa.libnexakotlin.simpleapi.defaultChain
import org.nexa.libnexakotlin.simpleapi.ofToken
import org.nexa.libnexakotlin.simpleapi.payTo
import org.nexa.nft.*
import org.nexa.threads.millisleep
import org.wallywallet.composing.*
import org.wallywallet.wew.*
import org.wallywallet.wew.WallyEnterpriseWallet.checkWalletFilename
import org.wallywallet.wew.WallyEnterpriseWallet.displayObject
import org.wallywallet.wew.WallyEnterpriseWallet.accounts
import org.wallywallet.wew.WallyEnterpriseWallet.initialPanel
import java.awt.Color
import java.awt.Desktop
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileNotFoundException
import java.lang.reflect.Method
import java.net.URI
import java.net.URL
import java.net.URLClassLoader
import java.net.URLEncoder
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.util.*
import javax.imageio.ImageIO
import kotlin.io.path.*
import kotlin.reflect.KClass
import kotlin.reflect.full.*

private val LogIt = GetLog("cli")

// try resultType.cast(result)
// val (r,t) = Pair(result,resultType)
// 9> t.simpleNameString10> r.trim()  (fails)
// (r as String).trim() (success)
// I need a way to tell the interpreter to eval something right now and stringify it
// (r as !*t*!).trim()
fun <T : Any> cast(value: Any?, targetType: kotlin.reflect.KClass<T>): T?
{
    if (value == null) return null
    return if (targetType.isInstance(value))
    {
        targetType.cast(value)
    }
    else
    {
        null
    }
}

// Put in libnexakotlin
const val TDPP_FLAG_NOFUND = 1
const val TDPP_FLAG_NOPOST = 2
const val TDPP_FLAG_NOSHUFFLE = 4
const val TDPP_FLAG_PARTIAL = 8
const val TDPP_FLAG_FUND_GROUPS = 16

// Intended to be symbols in the cli

val NEXA=org.nexa.libnexakotlin.ChainSelector.NEXA
val BCH=org.nexa.libnexakotlin.ChainSelector.BCH
val NEXAREGTEST=org.nexa.libnexakotlin.ChainSelector.NEXAREGTEST
val NEXATESTNET=org.nexa.libnexakotlin.ChainSelector.NEXAREGTEST
val RNEX=org.nexa.libnexakotlin.ChainSelector.NEXAREGTEST
val TNEX=org.nexa.libnexakotlin.ChainSelector.NEXATESTNET

fun millisleep(millis: ULong) = org.nexa.threads.millisleep(millis)

val resultOf: MutableMap<Int,Any?> = mutableMapOf()

fun window(c:Any?, title: String, width: Int=defaultFloatingWindowWidth, height:Int=defaultFloatingWindowHeight):CCWindow = org.wallywallet.composing.window(cify(c),title,width,height)

fun indent(amt: Dp, item: Composing) = org.wallywallet.composing.indent(amt, item)
fun indent(amt: Int, item: Composing) = org.wallywallet.composing.indent(amt.dp, item)

/** Create rows of lots of things.  Calls cify to resolve things into composables.  You may go to the next row using the CNL object */
fun rowOf(vararg things: Any?):ComposingContainer = org.wallywallet.composing.rowOf(things)

/** Create a column of lots of things.  Calls cify to resolve things into composables. */
fun colOf(vararg things: Any?):ComposingContainer = org.wallywallet.composing.colOf(things)

val nothing = org.wallywallet.composing.nothing

val Int.nexa
    get():Long = this.toLong()*100
val UInt.nexa
    get():Long = this.toLong()*100
val Long.nexa
    get():Long = this.toLong()*100

val Int.sat
    get():Long = this.toLong()
val UInt.sat
    get():Long = this.toLong()
val Long.sat
    get():Long = this.toLong()

val Int.mexa
    get():Long = this.toLong()*100L*1000000L
val UInt.mexa
    get():Long = this.toLong()*100L*1000000L
val Long.mexa
    get():Long = this.toLong()*100L*1000000L



fun QR(contents:String, size: Int=512): ImageBitmap?
{
    return generateQRCode(contents, size)
}

fun qr(contents:String, size: Int=512) = QR(contents, size)

fun image(file: String, width:Int?=null, height:Int?=null): Composing
{
    return ImageFace(file, width, height)
}

fun res(file:String): Composing
{
    return ResourcePainterFace(file)
}

fun refresh()
{
    val ips = WallyEnterpriseWallet.getTypedSymbol("_panel") as InterpreterPanelState
    ips.updateUI()
    ips.updateCommandOutput()
}

fun text(text: String): TextFace = TextFace(text)

fun row(vararg items: Any?): Composing
{
    val cf = mutableListOf<Composing>()
    for (i in items)
    {
        when(i)
        {
            is Composing -> cf.add(i)
            is String    -> cf.add(TextFace(i))
            is ByteArray -> cf.add(TextFace(i.toHex()))
            is Int       -> cf.add(TextFace(i.toString()))
            else -> cf.add(TextFace(i.toString()))
        }
    }
    return RowCol(listOf(cf))
}

fun open(uri: String)
{
    var u = URI(uri)
    if (u.scheme == null) u=URI("https://" + uri)

    if (Desktop.isDesktopSupported())
    {
        Desktop.getDesktop().browse(u)
    }
    else
    {
        println("Unavailable")
    }
}


fun newWallet(name: String, bc: Blockchain): Bip44Wallet
{
    val f = checkWalletFilename(name, allowNonExistent = true, mustNotExist = true)
    val w = org.nexa.libnexakotlin.newWallet(name, bc)
    accounts[name] = w
    return w
}
fun newWallet(name: String, cs: ChainSelector): Bip44Wallet
{
    val f = checkWalletFilename(name, allowNonExistent = true, mustNotExist = true)
    val w = org.nexa.libnexakotlin.newWallet(name, cs)
    accounts[name] = w
    return w
}

fun openWallet(name: String): Bip44Wallet
{
    val f = checkWalletFilename(name, false)
    val w = org.nexa.libnexakotlin.openWallet(name)
    accounts[name] = w
    return w
}

fun closeWallet(name: String)
{
    val w = accounts.remove(name)
    if (w!=null)
    {
        (w as? CommonWallet)?.finalize()
    }
}
fun closeWallet(w: Wallet)
{
    accounts.remove(w.name)
    (w as? CommonWallet)?.finalize()
}

/** Get a dictionary of all tokens and their Spendable UTXOs */
fun CommonWallet.assets(unspent: Boolean=true):MutableMap<GroupId, MutableList<Spendable>>
{
    val ret = mutableMapOf<GroupId, MutableList<Spendable>>()
    forEachAsset {
        if (!unspent || it.isUnspent)
        {
            val asset = it.groupInfo()
            if (asset != null)
            {
                val entry = ret.getOrPut(asset.groupId, { mutableListOf() })
                entry.add(it)
            }
        }
        false // keep going
    }
    return ret
}

fun SendAllAssetsTo(w: CommonWallet, addr: PayAddress): MutableList<iTransaction>
{
    val ret = mutableListOf<iTransaction>()
    val toks = w.assets()
    for ((k, v) in toks.toList())
    {
        var qty = 0L
        for (u in v)
        {
            if (u.isUnspent && !u.spentUnconfirmed)
                qty += u.groupInfo?.tokenAmt ?: 0
        }
        if (qty == 0L) continue
        println("Sending $qty of $k to $addr:")
        try
        {
            val tx = w.send(qty ofToken k payTo addr)
            println(tx)
            ret.add(tx)
        }
        catch(e:WalletNotEnoughTokenBalanceException)
        {
            println("insufficient token balance")
        }
        catch(e:WalletNotEnoughBalanceException)
        {
            println("insufficient balance")
        }
    }
    return ret
}

fun openOrNewWallet(name: String, cs: ChainSelector): Bip44Wallet
{
    val w = org.nexa.libnexakotlin.openOrNewWallet(name, cs)
    accounts[name] = w
    return w
}

/*
fun recoverWalletFastForward(name: String, recoveryKey: String, cs:ChainSelector): Bip44Wallet
{
    var bc = blockchains[cs]
    if (bc == null) bc = WallyEnterpriseWallet.connectBlockchain(cs)
    return recoverWalletFastForward(name, recoveryKey, bc)
}

fun recoverWalletFastForward(name: String, recoveryKey: String, bc: Blockchain): Bip44Wallet
{
    val f = checkWalletFilename(name, true)
    val (activity, tip) = searchAllActivity(recoveryKey, bc.chainSelector) {
        print(it.lastAddressIndex.toString() + " ")
        // print(it.addrCount.toString() + " ")  // only displays the number of discovered
    }
    val txhist = activity.txh
    if (txhist.isEmpty()) throw org.nexa.libnexakotlin.DataMissingException("no activity found")  // refuse to fastforward a new wallet; user should have, and still can, call new wallet
    var earliestHeight = Long.MAX_VALUE
    var earliestDate = Long.MAX_VALUE
    for (t in txhist.values)
    {
        if (t.confirmedHeight < earliestHeight)
        {
            earliestHeight = t.confirmedHeight
            earliestDate = t.date
        }
    }

    val addrs: MutableMap<String, PayDestination> = mutableMapOf()
    for (a in activity.dests)
    {
        addrs[a.address!!.toString()] = a
    }

    val wdb: org.nexa.libnexakotlin.WalletDatabase = org.nexa.libnexakotlin.openWalletDB(f.absolutePath, bc.chainSelector)!!  // One DB file per wallet for the desktop
    val w = Bip44Wallet(wdb, name, bc.chainSelector, recoveryKey)
    w.usesChain(bc)
    try
    {
        // w.injectReceivingAddresses(activity.dests)
    }
    catch (e: Exception)
    {
        println(e)
    }
    w.startChain(earliestHeight, earliestDate)  // Since this is a recovered wallet, look for old tx (start as early as possible)

    w.prepareDestinations(activity.lastAddressIndex, activity.lastAddressIndex)
    w.fastforward(tip.height, tip.time, tip.hash, txhist.values.toList())
    accounts[name] = w
    w.save(true)
    println("Finished!")
    return w
}
*/
/*
fun fastForward(w: Wallet, maxGap:Int=WALLET_FULL_RECOVERY_DERIVATION_PATH_MAX_GAP)
{
    val b44 = w as Bip44Wallet
    val (activity, tip) = searchAllActivity(b44.secretWords, b44.chainSelector, maxGap = maxGap) {
        if (it.status == AccountSearchResultsStatus.SEARCHING)
           print(it.lastCheckedIndex.toString() + " ")
        else
            print(it.details)
    }
    val txhist = activity.txh
    if (txhist.isEmpty()) throw org.nexa.libnexakotlin.DataMissingException("no activity found")  // refuse to fastforward a new wallet; user should have, and still can, call new wallet
    w.prepareDestinations(activity.lastAddressIndex, activity.lastAddressIndex)
    w.fastforward(tip.height, tip.time, tip.hash, txhist.values.toList())
    w.save(true)
    println("\nFinished, found ${txhist.size} entries, with largest address ${activity.lastAddressIndex} and ${activity.addrCount} active addresses.  Balance ${activity.balance}!")
}
*/


fun help(operation: Any? = null): String
{
    if (operation == null)
    {
        return """
Welcome to Wally Enterprise Wallet CLI!
This CLI executes Kotlin code, so you are expected to know the Kotlin programming language.
Since this is Kotlin, you can access any API in the libnexakotlin library.
In addition, there are a few shortcuts you should know about:

!exit: quit
!reset: restart the script session
!help <expression>: Show help on whatever object type this expression resolves to
!: show command history
!<number>[append this text]: execute this history item.  You can add more script to execute by placing it after the number.
!x <file>: execute the specified file
!t <expression>: return the type of expression
!path <colon separated path>: Set the script search path

Any command beginning with ! is not Kotlin code.  All other commands must be valid Kotlin statements.

Common Starting Commands:
val chain = connect(X):  Instantiates blockchain X, where is NEXA, BCH, RNEX, TNEX, RBCH, or TBCH.
addNode(ip, X): add a node to blockchain X.
newWallet(name, blockchain): Create a new wallet.
openWallet(name): open a wallet.  ex. openWallet("foo")
openOrNewWallet(name, blockchain): open a wallet if it exists, otherwise create it
recoverWalletFastForward(name, recoveryKey, blockchain): recover a wallet from secret key via "fast-forward"
recoverWallet(name, recoveryKey, blockchain): recover a wallet from secret key


p(object): Print object details

Implicit Variables:
"result" is always the result of the last command run
"resultOf[<n>]" is the result of the nth command run

"""
    }
    else
    {
        var styles = EnumSet.of(org.nexa.libnexakotlin.DisplayStyle.Help, org.nexa.libnexakotlin.DisplayStyle.FieldPerLine)
        val obj = displayObject(operation, styles)
        val api = org.nexa.libnexakotlin.getApiHelp(operation, DisplayLevel, styles)
        //val result = getApis(operation, style=style)
        //return result ?: "No information available\n"
        val apiStr = if (api != null && api!="[]") ("\n" + api) else ""
        return obj + apiStr
    }
}


fun easy(je: JsonElement): EJ
{
    return EJ(je)
}

/*
fun button(text: String, fn: ()->Any): CCButton
{
    val but = CCButton(CCText(text), null, fn)
    return but
}
 */

fun button(text: String, dropTarget: DropTarget, fn: ()->Any): CCButton
{
    val but = CCButton(CCText(text), null, fn)
    //val ips = DesktopWallet.getTypedSymbol("_panel") as InterpreterPanelState
    //ips.topBar.add(but)
    dropTarget?.add(but)
    return but
}

fun button(draw: Composing, height: Int? = null, dropTarget: DropTarget? = null, fn: ()->Any): CCButton
{
    val but = CCButton(draw, height?.dp, fn)
    //val ips = DesktopWallet.getTypedSymbol("_panel") as InterpreterPanelState
    //ips.topBar.add(but)
    dropTarget?.add(but)
    return but
}

fun button(draw: Composing, fn: ()->Any): CCButton
{
    val but = CCButton(draw, null, fn)
    return but
}

fun textentry(hint: String, initialValue: String = "", changed: CCTextEntry.(String) -> Boolean = { true }):CCTextEntry
{
    return CCTextEntry(initialValue, label = hint, changed = changed)
}

fun p(x:Any?):String
{
    return WallyEnterpriseWallet.displayObject(x, pDisplayStyles)
}

fun split(x: () -> Unit): Thread
{
    val t = kotlin.concurrent.thread(true, true, null, "split") { x() }
    perThreadOutput[t.id] = WallyEnterpriseWallet.output
    return t
}

fun script(s:String): SatoshiScript
{
    return SatoshiScript.fromAsm(s.split("\\s+".toRegex()).toTypedArray(), defaultChain)
}

/** Explicitly add a node that we should try connecting to.
 * If the blockchain isn't open on this client, this call will also open it
 */
fun addNode(ipOrName: String, cs: ChainSelector)
{
    val bc = WallyEnterpriseWallet.connectBlockchain(cs)
    bc.net.add(ipOrName, -1, 10)
}


fun print(vararg lo:Any?)
{
    //if (lo.size == 1) WallyEnterpriseWallet.output.stdout(lo[0])
    //else WallyEnterpriseWallet.output.stdout(rowOf(lo))
    // the above is an workaround.  But the output.stdout() really needs to drop objects in horizontal rows
    for(o in lo)
    {
        WallyEnterpriseWallet.output.stdout(o)
    }
}

fun pr(vararg o:Any?) = print(*o)

fun println(vararg lo:Any?): Unit
{
    print(*lo, "\n")
}

fun oprint(out: Output?, vararg lo:Any?)
{
    for(o in lo)
    {
        (out ?: WallyEnterpriseWallet.output).stdout(o)
    }
}

fun oprintln(out: Output?, vararg lo:Any?): Unit
{
    oprint(out, *lo, "\n")
}

fun run(x :() -> Unit)
{
    //return kotlinx.coroutines.runBlocking(DesktopWallet.coCtxt,x)
    split(x)
}


fun Import(x:String)
{
    val (file, contents) = WallyEnterpriseWallet.resolveScript(x)
    if (file != null)
    {
        WallyEnterpriseWallet.execute(contents,  WallyEnterpriseWallet.output)
    }
    else throw FileNotFoundException(x)
}

fun colaunch(x: suspend () -> Unit): Unit
{
    WallyEnterpriseWallet.coScope.launch(CoScript(WallyEnterpriseWallet.output)) { x() }
}

fun corun(x: suspend () -> Unit): Unit
{
    runBlocking(CoScript(WallyEnterpriseWallet.output)) { x() }
}


object FieldFunction {}
object FieldContinues {}
data class Nvh(val name: String, val value: Any?, val help: String)

typealias FieldDict = MutableMap<String, Nvh?>

@OptIn(kotlin.time.ExperimentalTime::class)
fun dictify(
    obj: Any,
    level: Display = Display.User,
    depth: Int = 10,
    resolve: Boolean = true,
    help: Boolean = false,
    toString: Boolean = true):Any?
{
    when (obj)
    {
        is String    -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is ByteArray -> return if (resolve) { if (toString) obj.toHex() else obj } else null
        is Int       -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Long      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Float     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Double    -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Char      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Boolean   -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Byte      -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is Short     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UInt     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is ULong     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UByte     -> return if (resolve) { if (toString) obj.toString() else obj } else null
        is UShort     -> return if (resolve) { if (toString) obj.toString() else obj } else null
    }

    return dictifyType(obj::class, obj, level, depth, superTypes = true)
}

fun dictifyType(kc: KClass<out Any>, obj: Any, level: Display = Display.User,depth: Int = 10,
                superTypes:Boolean=true,
                resolve: Boolean = true,
                help: Boolean = false,
                toString: Boolean = true
               ):FieldDict?
{
    if (depth == 0) return null
    val ret = mutableMapOf<String, Nvh?>()
    try
    {
        for (p in kc.declaredMemberProperties)
        {
            val d = p.findAnnotation<cli>()
            if (d != null)
            {
                if (d.display.v <= level.v)
                {
                    val subObj = p.getter.call(obj)
                    // decide whether to display details of subobjects
                    if ((subObj != null) && (d.delve + depth > 0))
                    {
                        val v = dictify(subObj, level, depth - 1, resolve, help, toString)
                        ret[p.name] = Nvh(p.name, v, d.help)
                    } else FieldContinues
                }
            }
        }
    }
    catch (e: IllegalStateException)
    {
        // typically: Cannot infer visibility for inherited open fun...
        logThreadException(e)
    }

    for (p in kc.memberFunctions)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                ret[p.name] = Nvh(p.name, FieldFunction, d.help)
            }
        }
    }

    for (p in kc.memberExtensionFunctions)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                ret[p.name] = Nvh(p.name, FieldFunction, d.help)
            }
        }
    }

    if (superTypes) for (st in kc.allSuperclasses)
    {
        val name = st.qualifiedName ?: ""
        if (!(name.contains("internal") && name.contains("kotlin")))
        {
            val sd = dictifyType(st, obj, level, depth, superTypes = false)  // every grandparent is also one of our supertypes so no need to recursively supertype
            sd?.let { ret.putAll(it) }
        }
    }

    return ret
}

class WalletProtocol
{
    companion object
    {
        fun reverseClipboard(fqdn: String, cookie: String, topic: String): String
        {
            return "tdpp://$fqdn/share?cookie=$cookie&topic=$topic"
        }

        fun reverseAddress(fqdn: String, cookie: String, topic: String): String
        {
            return "tdpp://$fqdn/share?cookie=$cookie&info=address&topic=$topic"
        }

        fun signMessage(fqdn: String, cookie: String, msg: String, addr: String?=null, replyprotocol:String="http"): String
        {
            val encText = URLEncoder.encode(msg, "utf-8")
            val addrparam = if (addr == null) "" else "&addr=" + URLEncoder.encode(addr,"utf-8")
            val uri = "nexid://$fqdn/_identity?op=sign&cookie=$cookie&proto=$replyprotocol$addrparam&sign=" + URLEncoder.encode(msg, "utf-8")
            return uri
        }

        fun signMessage(fqdn: String, cookie: String, bytes: ByteArray, addr: String?=null, replyprotocol:String="http"): String
        {
            val b = bytes.toHex()
            val addrParam = if (addr == null) "" else "&addr=" + URLEncoder.encode(addr,"utf-8")
            val uri = "nexid://$fqdn/_identity?op=sign&cookie=$cookie&proto=$replyprotocol$addrParam&signhex=$b"
            return uri
        }
    }
}



// CLASSES AND FUNCTIONS BELOW ARE FOR HANDLING ORACLE RESPONSES

data class ParsedOracleData(val tickerA: String,
                            val tickerB: String,
                            val epochSeconds: Long,
                            val price: Long)

@Serializable
data class OracleMsg(val data: String,
                     val signature: String)

@Serializable
data class OracleObject(val type: String,
                        val msg: OracleMsg,
                        val epochSeconds: Long,
                        val price: String,
                        val pairPriceUnit: String)

/** Convert this string to a ByteArray of size [zeroPadTo], padding it out with 0s */
fun String.toPaddedByteArray(zeroPadTo: Int): ByteArray = this.toByteArray().copyOf(zeroPadTo)

class PriceDataPoint : PackedStructure() {

    val tickerA: NBytes by PBytes(8)
    val tickerB: NBytes by PBytes(8)
    val epochSeconds: NInt by PInt(8)
    val priceAinB: NInt by PInt(8)

    fun setVals(_tickerA: String, _tickerB: String, _epochSeconds: Long, _priceAinB: Long)
    {
        tickerA.curVal = _tickerA.toPaddedByteArray(8)
        tickerB.curVal = _tickerB.toPaddedByteArray(8)
        epochSeconds.curVal = _epochSeconds
        priceAinB.curVal = _priceAinB
    }
}

/** NEED TO REVISE BYTE VALUES (TICKERS 4 BYTES) UPON !22 FOR WALLYWALLET.ORG **/
fun parseOracleData(data: ByteArray): ParsedOracleData
{
    require(data.size == 24) { "Data must be exactly 24 bytes" }

    val string1 = String(data.sliceArray(0..3)).trimEnd { it.toInt() == 0 }
    val string2 = String(data.sliceArray(4..7)).trimEnd { it.toInt() == 0 }

    val longBuffer1 = ByteBuffer.wrap(data.sliceArray(8..15)).order(ByteOrder.LITTLE_ENDIAN)
    val unsignedLongValue1 = longBuffer1.long

    val longBuffer2 = ByteBuffer.wrap(data.sliceArray(16..23)).order(ByteOrder.LITTLE_ENDIAN)
    val unsignedLongValue2 = longBuffer2.long

    return ParsedOracleData(string1, string2, unsignedLongValue1, unsignedLongValue2)
}

/**
 * Needed this for executing local file from cli, due to lack of serialization functionality
 */
/**
fun parseOraclePrice(jsonString: String): OraclePrice {

fun extractValue(json: String, key: String): String {
val keyIndex = json.indexOf("\"$key\"")
val colonIndex = json.indexOf(":", keyIndex) + 1
val commaIndex = json.indexOf(",", colonIndex)
val endIndex = if (commaIndex == -1) json.indexOf("}", colonIndex) else commaIndex
return json.substring(colonIndex, endIndex).trim().trim('"')
}

val type = extractValue(jsonString, "type")
val epochSeconds = extractValue(jsonString, "epochSeconds").toLong()
val price = extractValue(jsonString, "price")
val pairPriceUnit = extractValue(jsonString, "pairPriceUnit")

val msgStartIndex = jsonString.indexOf("\"msg\":") + 6
val msgEndIndex = jsonString.indexOf("}", msgStartIndex) + 1
val msgJson = jsonString.substring(msgStartIndex, msgEndIndex)

val data = extractValue(msgJson, "data")
val signature = extractValue(msgJson, "signature")

val msg = OracleMsg(data, signature)

return OraclePrice(type, msg, epochSeconds, price, pairPriceUnit)
}
 **/

fun parseOracleObject(response: String): OracleObject
{
    val oracleObject = Json.decodeFromString<OracleObject>(response)
    return oracleObject
}



// NFT Creation and handling

val NftLegal = """NiftyArt Terms and Conditions V1.0
Definitions:
NiftyArt NFT: A unique piece of data that is described by a record in a blockchain.  This record contains a probabilistically unique identifier of a data file containing and/or describing the represented Work.
Owner/Ownership: (of the NiftyArt NFT)  Modifying, destroying, or transferring an NiftyArt NFT record (called a transaction) is defined by rules specified by the blockchain and by each NiftyArt NFT record.  Successfully following these rules requires pieces of data that are provided as part of the transaction.  Ownership of an NiftyArt NFT is defined as possessing the ability to provide the data required to confirm a transaction on the blockchain.  Proving ownership occurs solely by one of two mechanisms.  First, by executing and confirming said transaction on the blockchain.  This confirms Ownership for the exact duration of the transaction.  Second, by producing a unique transaction upon demand with the correct required data but that is not admissible onto the blockchain for some other reason.  This confirms Ownership until the NFT blockchain record is modified or destroyed.  Every use of the word 'Owner' in these Terms and Conditions implies cryptographic verification of ownership as described.
Creator: (of the NiftyArt NFT) The entity that originally created the NiftyArt NFT on the blockchain.
Work: The artwork referenced by this NiftyArt NFT.  The Work may be fully embodied within files named 'public' and/or 'private'.  Additionally, the Work may include a physical and/or external components as specified by the 'info' field within the 'info.json' file.
Owner Work:  The portion of the Work visible only by the Owner, as described below.
Public Work:  The portion of the Work visible to anyone, as authorized by the Owner and these Terms and Conditions.
Service: A third party entity that facilitates the use, inclusion, participation, management, storage, display or trade of NiftyArt NFTs.
    
Inseparability of Work from the NiftyArt NFT:
Ownership of this NiftyArt NFT confers all licenses to the Work described herein.  Transfer of this NiftyArt NFT transfers the same.  The licenses described below are forever conferred by and inseparable from Ownership of this NiftyArt NFT.
The Owner does NOT have the right to create or enter into any contract, agreement or document, electronic, physical or otherwise that changes or overrides the NiftyArt NFT's licenses to the Work as described herein.
    
Visibility of the Work:
Third parties may reproduce and publish the media files located within this NiftyArt NFT prefixed by 'cardf' and 'cardb' (with various suffixes and extensions) and the data within the 'info.json' file within the context of identifying this NiftyArt NFT.  The fair use of the title and author of a book shall act as precedent for when this data may be published.
If media files or directories prefixed by 'owner' (with various suffixes and extensions) exist, this data is the Owner Work.   No Service may use, copy, display, or publish this file except to a proven current Owner.  Services that manage, store, use, or trade NiftyArt NFTs may store this file on behalf of and for use by the Owner.  If the NiftyArt NFT data file contains any Owner Work, the entire NiftyArt NFT data file has these same constraints.
If media files or directories prefixed by 'public' (with various suffixes and extensions) exist, this data is the Public Work.  'cardf' and 'cardb' prefixed files are also within the Public Work.  Control of the Public Work is specific to each NFT and described in the following sections.

Creator Copyright Ownership:
The Creator of the NiftyArt NFT asserts that they had sole ownership of exclusive copyright to the Work, had the right to transfer that ownership, and that the creation of this NiftyArt NFT inseparably binds the copyright and/or licenses described herein to Ownership of this NiftyArt NFT.

Owner:
The Owner has exclusive ownership of the copyright to the Work, constrained only by these Terms and Conditions.  Transfer of this NiftyArt NFT constitutes transfer of copyright ownership of the Work.
    
A Service may display or use of the Public Work as an avatar and/or icon associated with the Owner for the purpose of identifying the Owner to other participants.   
The service may display or use the Public Work on behalf of the Owner while the Owner is using the service.  When the Owner leaves the service, the use or display must cease.
For example, the Work may be used within Services by the Owner as the Owner's avatar or as an ancillary item that unlocks additional functionality or abilities.  
Once the Public Work has been displayed or used in the manner described above, the historical record of that use may continue to be displayed, even if the Owner transfers the NFT, provided that the display or use clearly indicates the time and date of the original use.
""".trimIndent()

@OptIn(ExperimentalPathApi::class)
fun createNftFiles(dir: Path, out:Path, creationData: NFTCreationData, seriesStart:Int=0)
{
    val items = dir.walk(PathWalkOption.BREADTH_FIRST)
    var count = seriesStart
    items.sorted().forEach {
        val f = it.toFile()
        if (f.isFile && f.exists() && org.nexa.nft.NFTY_SUPPORTED_MEDIA.contains("." + f.extension.lowercase()))
        {
            if (creationData.cardBackFile?.contains(f.name) == true) return@forEach  // skip the card back
            val cd = creationData.copy()
            cd.dataFile = f.canonicalPath
            cd.mediaType = f.extension
            cd.title = creationData.title + "#" + count.toString()
            cd.license = NftLegal
            println("Processing ${f.name} -- ${cd.title}")
            val finalFile = makeNftyZip(out, cd)
            println("Created" + finalFile.toAbsolutePath())
            count++
        }
    }
}

fun Wallet.repeatSend(amt:Long, addr:String, count:Int=100, delay:Int=2000)
{
    for (i in 0 until count)
    {
        val tx = send(amt, addr)
        println(tx)
        println("\n")
        millisleep(delay.toULong())
    }
}


fun utxos(w:Wallet)
{
    var utxoCount = 0
    var unconfirmed = 0
    w.forEachUtxo {
        utxoCount++
        // LogIt.info(it.toString())
        if (it.commitUnconfirmed > 0)
        {
            unconfirmed++
        }
        println(it)
        false
    }
    println("Completed $utxoCount UTXOs. $unconfirmed are unconfirmed")
    //LogIt.info("Completed $utxoCount UTXOs. $unconfirmed are unconfirmed")
}

fun dumpUtxos(w:Wallet, filename: String)
{
    var utxoCount = 0
    val file = File(filename).bufferedWriter().use { writer ->
        w.forEachUtxo {
            utxoCount++
            LogIt.info(it.toString())
            //println(it)
            writer.write(it.toString())
            false
        }
    }
    println("Completed $utxoCount UTXOs")
    //LogIt.info("Completed $utxoCount UTXOs")
}

fun unconf(w:Wallet)
{
    w.forEachTx {
        if (it.isUnconfirmed())
        //LogIt.info(it.toString())
        println(it)
        false
    }
    //LogIt.info("Completed")
}

//fun gti(b: Blockchain):TokenInfoMsg? {
    //return b.req.getTokenInfo(GroupId("nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqq9cmgs352g2ukx2cl270gctudc0t3jenpvd93e93d2chgut65f40lurge786"))
    //b.req.getTokenInfo()
//}

fun CommonWallet.analyze(address: String)
{
    val addr = PayAddress(address)

    val dest = walletDestination(addr)
    if (dest == null)
    {
        print("Address not known to the wallet")
        return
    }
    print("Address $addr is number ${dest.index}\n\n")

    forEachUtxo { sp ->
        if ((sp.addr != null)&&(sp.addr == addr))
        {
            print("UTXO: $sp")
        }
        false
    }
}

fun CommonWallet.txPaying(d: PayDestination): List<TransactionHistory>
{
    val ret = mutableListOf<TransactionHistory>()
    forEachTx { txh ->
        for (out in txh.tx.outputs)
        {
            if (out.script == d.lockingScript())
            {
                ret.add(txh)
            }
        }
        false
    }
    return ret
}

fun CommonWallet.gapAnalysis()
{
    val addrs = allAddresses
    val dests = mutableListOf<PayDestination>()
    for (a in addrs)
    {
        val dest = walletDestination(a)
        dest?.let { dests.add(dest)}
    }
    dests.sortBy { it.index }
    var gapStart = 0L
    var gapEnd = 0L
    var gapSize = 0L

    var prevIdx=0L
    for (d in dests)
    {
        // I have to receive something before sending so its sufficient to just check receipt for use
        if (txPaying(d).isNotEmpty())
        {
            if (d.index - prevIdx > gapSize)
            {
                gapStart = prevIdx
                gapEnd = d.index
                gapSize = gapEnd - gapStart
            }
            prevIdx = d.index
        }
    }
    print("Num Addresses: ${dests.size}  Last: ${dests[dests.size-1].index}  Max Gap: $gapStart -> $gapEnd = $gapSize")
}



fun CommonWallet.reissue()
{
    val unconf = getUnconfirmedTx()
    val rostrum = blockchain.net.getElectrum()
    println("${unconf.size} unspent transactions:")
    for (u in unconf)
    {
        val txh = getTx(u.commitTxIdem)
        if (txh!=null)
        {
            var spent = false
            for (i in txh.tx.inputs)
            {
                val outpt = i.spendable.outpoint
                if (outpt==null) spent=true
                else
                {
                    try
                    {
                        val inStat = rostrum.getUtxo(outpt)
                        if (inStat.spent.tx_hash != null)
                        {
                            spent = true
                        }
                    }
                    catch(e: ElectrumNotFound)
                    {
                        spent = true
                    }
                }
            }
            if (!spent)
            {
                print(txh)
                print("Sending: ", rostrum.sendTx(txh.tx.toByteArray()))
            }
        }
    }
    println("Analysis done")
}

fun CommonWallet.forEachAsset(doit: (org.nexa.libnexakotlin.Spendable) -> kotlin.Boolean): Unit
{
    forEachUtxo {
        val gi = it.groupInfo
        if (gi != null)
        {
            doit(it)
        }
        else false
    }
}

fun CommonWallet.numAssets(): Long
{
    var count = 0L
    forEachAsset {
        count++
        false
    }
    return count
}



/** Places all NFT files that exist in the wallet in [existsDir]  */
fun moveCreatedNfts(w:CommonWallet, dir: File, existsDir: File, confirmed:Boolean = true)
{
    existsDir.mkdirs()

    //val titleMap = mutableMapOf<String, MutableSet<File>>()
    val hashMap = mutableMapOf<String, File>()
    for (f in dir.listFiles()!!)
    {
        if (f.isFile)
        {
            //println("analyzing ${f.name}")
            val ba = f.readBytes()
            //println("  size: ${ba.size}")
            val hash = libnexa.hash256(ba)
            hashMap[hash.toHex()] = f
            /*
            val data = nftData(EfficientFile(ba))
            if (data != null)
            {
                titleMap.getOrPut(data.title, { mutableSetOf<File>() }).add(f)
            }
             */
        }
    }
    println("Assessing ${hashMap.size} files.")

    var numUtxos = 0
    w.forEachAsset {
        if (!confirmed || it.commitHeight > 0)  // confirmed
        {
            numUtxos++
            val gi = it.groupInfo
            if ((gi != null) && gi.isSubgroup())
            {
                val sg = gi.groupId.subgroupData()
                val f = hashMap[sg.toHex()]
                if (f != null)
                {
                    println("Confirmed NFT: ${f.name} in ${it}")
                    Files.move(f.toPath(), existsDir.resolve(f.name).toPath(), StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING)
                }
            }
        }
        false
    }
    println("Compared with ${numUtxos} outputs.")
}

@cli(Display.User, "Split the nexa in a wallet so there are at least [num] chunks of [amt] quantity.  If an output is already [amt] and [amt] + [delta] then count it as already split.")
fun SplitNexa(w: CommonWallet, num: Int, amt: Long, overridePayAddress: PayAddress?=null, delta: Long = 10000.nexa)
{
    val MAX_SPLITS = 250  // extra room for nexa change
    val chain = ChainSelector.NEXA
    val cashUtxo = mutableListOf<Spendable>()
    val splittableUtxo = mutableListOf<Spendable>()

    val payAddress = overridePayAddress ?: w.getnewaddress()

    if (amt < dust(chain)) throw WalletDustException("split amount is too small: ${amt} satoshis")

    w.forEachUtxo {
        val grp = it.groupInfo
        if (grp == null)
        {
            if (it.amount >= amt && it.amount <= amt+delta)
            {
                cashUtxo.add(it)
            }
            else
            {
                splittableUtxo.add(it)
            }
        }
        false
    }

    var cur = cashUtxo.size
    if (cur >= num)
    {
        println("Nothing to do: already have ${cashUtxo.size} SFT UTXOs ready to go!")
        return
    }

    while(cur < num)
    {
        val minTxCreate = amt*10
        var tx = txFor(chain)

        fun splitSpend()
        {
            val inAmt = tx.inputTotal
            // spend into a bunch of chunks
            while((tx.outputs.size < MAX_SPLITS) && (tx.outputs.size + cur < num) && (inAmt > tx.outputTotal+amt))
            {
                tx.add(txOutputFor(chain,amt,payAddress.lockingScript()))
            }
            val numSplits = tx.outputs.size
            try
            {
                tx.addChange(w, payAddress)
                // If the surplus was < the fee and even < the min fee, then forget about it!
                // TODO: the optimal thing to do would be to delete the change output and add another input
                if (tx.fee >= w.minFeeForSize(tx.size))
                {
                    // done
                    w.signTransaction(tx)
                    w.send(tx)
                    println("Nexa splitter transaction:")
                    println(tx)
                    println(tx.toHex())
                    cur += numSplits
                }
            }
            catch(e: WalletFeeException)
            {
                // TODO: the optimal thing to do would be to delete the change output and add another input
            }
            // start the tx over
            tx = txFor(chain)
        }

        for (u in splittableUtxo)
        {
            tx.add(txInputFor(u))
            if (tx.inputTotal >= minTxCreate)  // Got enough inputs, time to spend them
            {
                splitSpend()
            }
            if (cur >= num) break
        }
        if (cur<num) splitSpend()
    }
}

fun SplitSFT(w:CommonWallet, numQrs: Int, sftGroup: GroupId, overridePayAddress: PayAddress? = null)
{
    // globals to be commented out when script-ified
    //val w = openWallet("nftFaucet")
    //val SftGroup: GroupId = GroupId("nexa:tr9v70v4s9s6jfwz32ts60zqmmkp50lqv7t0ux620d50xa7dhyqqq3dnu5tvxdzgnhf0qlysek3flfgw7w4hx8tq9qdz6cuuc07sez69leula2dq")
    //val addr = "nexa:nqtsq5g5t52ae5236ylhzevlfmndrcw7ls96tusnpy8kdhaw"
    // end globals

    val payAddress = overridePayAddress ?: w.getnewaddress()

    val chain = ChainSelector.NEXA
    val singleSftUtxo = mutableListOf<Spendable>()
    val multiSftUtxo = mutableListOf<Spendable>()

    w.forEachUtxo {
        val grp = it.groupInfo
        if (grp != null)
        {
            if ((!grp.isAuthority()) && (grp.groupId == sftGroup))
            {
                if (grp.tokenAmt == 1L)
                    singleSftUtxo.add(it)
                else
                    multiSftUtxo.add(it)
            }
        }
        false
    }

    if (singleSftUtxo.size >= numQrs)
    {
        println("Nothing to do: already have ${singleSftUtxo.size} SFT UTXOs ready to go!")
        return
    }

    val MAX_SPLITS = 250  // extra room for nexa change
    var cur = singleSftUtxo.size
    for (utxo in multiSftUtxo)
    {
        val tokInAmt = utxo.groupInfo!!.tokenAmt
        val tx = txFor(chain)
        while(true)
        {
            val sft = txOutputFor(chain, dust(chain), payAddress.groupedLockingScript(utxo.groupInfo!!.groupId, 1))
            tx.add(sft)
            if ((tx.outputs.size > MAX_SPLITS) || (tx.outputs.size >= tokInAmt) || (tx.outputs.size + cur >= numQrs) ) break
        }
        if (tx.outputs.size < tokInAmt)  // spend left over tokens to myself in 1 output
        {
            val sft = txOutputFor(chain, dust(chain), payAddress.groupedLockingScript(utxo.groupInfo!!.groupId, tokInAmt-tx.outputs.size))
            tx.add(sft)
        }
        // If for some reason we didn't split anything then just skip this split
        if (tx.outputs.size < 1) continue
        cur += tx.outputs.size
        tx.add(txInputFor(utxo))
        w.txCompleter(tx, 0, TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.SIGN)
        print("Splitter transaction:")
        print(tx)
        print(tx.toHex())
        w.send(tx)
        if (cur >= numQrs) break // done
    }
}


fun NftCashQR(w: CommonWallet, dir: File, NUM_QRS:Int, TdppReason:String, SftGroup: Set<GroupId>, CASH_AMT: Long = 1.mexa, NFTY_SERVER_FQDN:String = "niftyart.cash"): List<Pair<iTransaction, ImageBitmap>>?
{
    val sftOffered = (SftGroup.size > 0)
    dir.mkdirs()
    val QR_SIZE = 1024
    val chain = w.chainSelector

    val MIN_FEE = 500.sat

    var count = 0

    val changeAddr = w.getnewaddress()

    // Find cash for each tx
    val cashUtxo = mutableListOf<Spendable>()
    w.forEachUtxo {
        val grp = it.groupInfo
        // We can use this output as an input because it has enough cash
        if ((grp == null)&&(it.amount >= CASH_AMT + MIN_FEE))
        {
            cashUtxo.add(it)
            count+=1
        }
        (count >= NUM_QRS)
    }

    if (cashUtxo.size < NUM_QRS)
    {
        println("ERROR: you only have ${cashUtxo.size} available cash prevouts.  Do some splitting!")
        return null
    }

    // Now find NFTs
    val nftUtxo = mutableListOf<Spendable>()
    val sftUtxo = mutableListOf<Spendable>()
    var unsplitSfts = 0L
    w.forEachUtxo {
        val grp = it.groupInfo
        if (grp != null)
        {
            if ((!grp.isAuthority()) && SftGroup.contains(grp.groupId))
            {
                if (grp.tokenAmt == 1L)
                    sftUtxo.add(it)
                else
                    unsplitSfts += grp.tokenAmt
            }
            else if ((!grp.isAuthority()) && (grp.tokenAmt == 1L))  // you should only have what you want given out in this wallet
            {
                nftUtxo.add(it)
            }
        }
        ( (!sftOffered) || (sftUtxo.size >= NUM_QRS)) && (nftUtxo.size > NUM_QRS)
    }

    if (nftUtxo.size < NUM_QRS)
    {
        println("ERROR: you only have ${nftUtxo.size} available NFT prevouts.  Load up more NFTs!")
        return null
    }
    if (sftOffered && (sftUtxo.size < NUM_QRS))
    {
        if (unsplitSfts + sftUtxo.size >= NUM_QRS)
        {
            println("ERROR: you need to split your SFT prevouts.  call SplitSFT()")
        }
        else println("ERROR: you only have ${sftUtxo.size} available SFT prevouts.  Load up more copies of your SFT!")
        return null
    }
    if (!sftOffered)
    {
        println("Note: no SFT is being offered")
    }

    val ret = mutableListOf<Pair<iTransaction, ImageBitmap>>()
    // I've collected all the needed inputs.  Now generate the partial transactions
    for (i in 0 until NUM_QRS)
    {
        val tx = txFor(chain)
        val cashIn = cashUtxo[i].amount
        tx.add(txInputFor(cashUtxo[i]))
        tx.add(txInputFor(nftUtxo[i]))
        if (sftOffered) tx.add(txInputFor(sftUtxo[i]))

        val changeAmt = cashIn-MIN_FEE-CASH_AMT
        val change = txOutputFor(chain, changeAmt, changeAddr.lockingScript())
        val cash = txOutputFor(chain, CASH_AMT, SatoshiScript(chain, SatoshiScript.Type.TEMPLATE, OP.C0, OP.TMPL_SCRIPT) )
        val nft = txOutputFor(chain, nftUtxo[i].amount, SatoshiScript.grouped(chain, nftUtxo[i].groupInfo!!.groupId,nftUtxo[i].groupInfo!!.tokenAmt).add(OP.TMPL_SCRIPT) )
        if (changeAmt > 800)
            tx.add(change)
        tx.add(cash)
        if (sftOffered)
        {
            val sft = txOutputFor(chain, sftUtxo[i].amount, SatoshiScript.grouped(chain, sftUtxo[i].groupInfo!!.groupId,sftUtxo[i].groupInfo!!.tokenAmt).add(OP.TMPL_SCRIPT) )
            tx.add(sft)
        }
        tx.add(nft)

        // Sign all the inputs I created and my change output, giving the scanner the rest
        val stx = if (changeAmt > 800) w.signTransaction(tx, NexaSigHashType().firstNIn(tx.inputs.size).firstNOut(1).build())
        else w.signTransaction(tx, NexaSigHashType().firstNIn(tx.inputs.size).firstNOut(0).build())

        // TDPP_FLAG_NOPOST
        val tdppFlags = TDPP_FLAG_NOFUND or TDPP_FLAG_NOSHUFFLE or TDPP_FLAG_PARTIAL
        val reason = URLEncoder.encode(TdppReason,"utf-8")
        val chainName = chainToURI[chain]
        val qrString = "tdpp://" + NFTY_SERVER_FQDN + "/tx?chain=$chainName&tx=${stx.toHex()}&flags=$tdppFlags&reason=$reason"
        val q:ImageBitmap = qr(qrString, QR_SIZE)!!
        ret.add(Pair(stx,q))
        println("TX: " + stx.debugDump())
        println("TX HEX: " + stx.toHex())
        //println(q)
        val file = dir.resolve("$i.png")
        q.savePng(file)
    }
    println("COMPLETED $NUM_QRS")
    return ret
}


fun NftHistoryCheck(w: CommonWallet)
{
    var redemption = 0
    w.forEachTxByDate {
        var nft = 0
        var nexa = 0L
        if (it.tx.outputs.size >= 3)
        {
            for (out in it.tx.outputs)
            {
                val gi = out.script.groupInfo(out.amount)
                if (gi == null) nexa += out.amount
                if (gi?.isSubgroup() == true) nft++
            }
        }
        if (nft == 2 && nexa == 300000000L) redemption++
        false
    }
    print("${redemption} redemptions!")
}

fun PositionAinB(aDir:File, b:File, x:Int, y:Int, width:Int, height: Int, outDir:File)
{
    val files = aDir.listFiles()?.sorted()
    outDir.mkdirs()
    val background = ImageIO.read(b)
    if (files != null)
    {
          for (af in files)
          {
              val a = ImageIO.read(af)
              val combined = BufferedImage(background.width, background.height, BufferedImage.TYPE_INT_ARGB)
              val g: Graphics2D = combined.createGraphics()
              g.drawImage(background, 0, 0, null)
              g.drawImage(a, x, y, width, height, null)
              g.dispose()
              val outfile = outDir.resolve(af.name)
              ImageIO.write(combined, "png", outfile)
              println("Read: ${af.name}  Wrote: ${outfile.name}")
          }
    }
}

fun amsterdam(wal:CommonWallet, FEE: Long=600)
{
    print("Amsterdam Web3 2025 builder: ")
    print(button("Split NEXA") {
        laterJob {
            println(ScrollableOutput("Split Output", 600.dp, { SplitNexa(wal, 305, 5000000.nexa + FEE) }))
        }
    })

    print(button ("Make QRs") {
        laterJob {
            println(ScrollableOutput("Make QR Output", 600.dp, {
                NftCashQR(wal, File("astrobaraQR"), 305, "Web3 Amsterdam Giveaway", setOf(), 5000000.nexa, "192.168.2.11:8988")
            }))
        }
    })

    println(button("Make Flyers") {
        laterJob {
            println(ScrollableOutput("Make Flyers Output", 600.dp) {
                PositionAinB(File("astrobaraQR"), File("astrobara/QRframe.png"), 670, 1245, 1950, 1950, File("astrobaraFlyers"))
            })
        }
    })
}