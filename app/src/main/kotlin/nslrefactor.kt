package Nexa.npl

import org.nexa.libnexakotlin.OP

class Trie<T, C: Comparable<C> > {
    data class Node<T,C>(val children: MutableMap<T, Node<T,C>> = mutableMapOf(), var value:C? = null)

    protected val root: Node<T,C> = Node()

    fun add(item: List<T>, value: C)
    {
        var cur = root
        for (i in item)
        {
            cur = cur.children.getOrPut(i) { Node() }
        }

        val curvalue = cur.value
        if (curvalue == null) cur.value = value
        else
        {
            if (curvalue > value) cur.value = value
        }
    }

    fun get(item: List<T>): C?
    {
        var cur:Node<T,C>? = root
        for (i in item)
        {
            if (cur == null) return null
            cur = cur.children[i]
        }
        return cur?.value
    }

    fun getprefix(item: List<T>): Pair<Int, C?>
    {
        var cur:Node<T,C>? = root
        val prefix = mutableListOf<T>()
        var bestIdx = 0
        var value:C? = null
        for ((idx, i) in item.withIndex())
        {
            if (cur == null) return Pair(bestIdx, value)
            cur = cur.children[i]
            if (cur != null)
            {
                if (cur.value != null)
                {
                    value = cur.value
                    bestIdx = idx
                }
            }
        }
        return Pair(bestIdx, value)
    }
}

class ComparableOpList(val v: List<OP>):Comparable<ComparableOpList>
{
    override fun compareTo(other: ComparableOpList): Int
    {
        return v.size.compareTo(other.v.size)
    }

}


val refactorTrie = Trie<OP, ComparableOpList>()

fun initRefactor()
{
    refactorTrie.add(listOf(OP.SHA256, OP.RIPEMD160), ComparableOpList(listOf(OP.HASH160)))
    refactorTrie.add(listOf(OP.SHA256, OP.SHA256), ComparableOpList(listOf(OP.HASH256)))

    refactorTrie.add(listOf(OP.SWAP, OP.LESSTHAN), ComparableOpList(listOf(OP.GREATERTHANOREQUAL)))
    refactorTrie.add(listOf(OP.SWAP, OP.GREATERTHAN), ComparableOpList(listOf(OP.LESSTHANOREQUAL)))
    refactorTrie.add(listOf(OP.SWAP, OP.LESSTHANOREQUAL), ComparableOpList(listOf(OP.GREATERTHANOREQUAL)))
    refactorTrie.add(listOf(OP.SWAP, OP.GREATERTHANOREQUAL), ComparableOpList(listOf(OP.LESSTHANOREQUAL)))

    // Commutative operations
    refactorTrie.add(listOf(OP.SWAP, OP.EQUAL, OP.VERIFY), ComparableOpList(listOf(OP.EQUALVERIFY)))
    refactorTrie.add(listOf(OP.SWAP, OP.EQUAL), ComparableOpList(listOf(OP.EQUAL)))
    refactorTrie.add(listOf(OP.SWAP, OP.EQUALVERIFY), ComparableOpList(listOf(OP.EQUALVERIFY)))
    refactorTrie.add(listOf(OP.SWAP, OP.NUMEQUALVERIFY), ComparableOpList(listOf(OP.NUMEQUALVERIFY)))
    refactorTrie.add(listOf(OP.SWAP, OP.MIN), ComparableOpList(listOf(OP.MIN)))
    refactorTrie.add(listOf(OP.SWAP, OP.MAX), ComparableOpList(listOf(OP.MAX)))
    refactorTrie.add(listOf(OP.SWAP, OP.ADD), ComparableOpList(listOf(OP.ADD)))
    refactorTrie.add(listOf(OP.SWAP, OP.MUL), ComparableOpList(listOf(OP.MUL)))

    refactorTrie.add(listOf(OP.NOP), ComparableOpList(listOf()))
    refactorTrie.add(listOf(OP.EQUAL, OP.VERIFY), ComparableOpList(listOf(OP.EQUALVERIFY)))
    refactorTrie.add(listOf(OP.NUMEQUAL, OP.VERIFY), ComparableOpList(listOf(OP.NUMEQUALVERIFY)))
    refactorTrie.add(listOf(OP.SWAP, OP.DROP), ComparableOpList(listOf(OP.NIP)))
    refactorTrie.add(listOf(OP.SWAP, OP.SWAP), ComparableOpList(listOf()))
    refactorTrie.add(listOf(OP.DROP, OP.DROP), ComparableOpList(listOf(OP.DROP2)))
    refactorTrie.add(listOf(OP.C0, OP.EQUAL), ComparableOpList(listOf(OP.NOT)))  // not -> 1 if input is 0, otherwise 0
}

/** Look for statement patterns and replace with shorter ones that do the same thing */
fun NSL.refactor(): MutableList<OP>
{
    var changed = true
    var refactored = fullOpcodes().toMutableList()
    while(changed)
    {
        changed = false
        val ops = refactored
        refactored = mutableListOf<OP>()

        while(ops.isNotEmpty())
        {
            val (pos, replace) = refactorTrie.getprefix(ops)
            if (pos == 0) refactored.add(ops.removeFirst())
            else
            {
                refactored.addAll(replace!!.v)  // it cannot be null if pos != 0
                for (i in 0..pos) ops.removeFirst()
                changed = true
            }
        }
    }
    return refactored
}
