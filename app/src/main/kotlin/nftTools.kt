// This should become a library
@file:OptIn(ExperimentalUnsignedTypes::class)

package org.nexa.nft

import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.Json
import kotlinx.serialization.encodeToString
import org.nexa.libnexakotlin.*
import org.wallywallet.wew.cli.res
import java.io.*
import java.nio.file.FileSystems
import java.nio.file.Path
import java.util.concurrent.TimeUnit
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream
import kotlin.io.path.*

import kotlin.text.toByteArray

private val LogIt = GetLog("nifty.nftTools")

val NFTY_MINT_MAX_FILE_SIZE = 1024*1024*50  // Note this is the max size that this web site's creation supports, not the max size of any Nifty file.
val NFTY_MINT_MAX_CARD_SIZE = 2*1024*1024

// Canonical file names that must appear in the .zip
val NFTY_CARD_FRONT_MEDIA = "cardf"
val NFTY_INFO_FILE = "info.json"   // Info.json must be UTF-8 encoded

// Canonical file names that may appear in the .zip
val NFTY_PUBLIC_MEDIA = "public"
val NFTY_OWNER_MEDIA = "owner"
val NFTY_CARD_BACK_MEDIA = "cardb"

val NFTY_SUPPORTED_VIDEO = listOf(".avif", ".webp", ".ogg", ".mp4", ".mpeg", ".mpg", ".webm").let { it + it.map { it.uppercase()}}
val NFTY_SUPPORTED_AUDIO = listOf(".mp3", ".wav", ".pcm", ".m4a", ".flac",".wma",".aac",".ac3", ".aiff").let { it + it.map { it.uppercase()}}
val NFTY_SUPPORTED_IMAGE = listOf(".svg", ".gif", ".png", ".apng", ".jpg", ".jpeg").let { it + it.map { it.uppercase()}}
val NFTY_SUPPORTED_MEDIA = NFTY_SUPPORTED_VIDEO + NFTY_SUPPORTED_IMAGE + NFTY_SUPPORTED_AUDIO

val NFTY_FILE_EXT = ".zip"

// To use the image and video resizing functionality, you need to point to these programs on your system
var FFMPEG = "/usr/bin/ffmpeg"
var IMMAG = "/usr/bin/convert"

var BASE_DIR = "."
var NFTY_TMP_PATH = Path(BASE_DIR + "/tmp")
var NFTY_TMP_PREFIX = "nifty"

// If we have to transcode, this is what we convert to
var NFTY_DEFAULT_VIDEO_SUFFIX = ".mp4"
var NFTY_MINT_CARD_PIX = 300
var NFTY_PREFERRED_BITMAP_EXTENSION = "png" // convert svg card images that are too large into this format

val NIFTY_ART_IP = mapOf(
    ChainSelector.NEXA to "niftyart.cash",
    // Enable manually for niftyart development: ChainSelector.NEXAREGTEST to "192.168.1.5:8988"
    ChainSelector.NEXATESTNET to "192.168.2.11:8988"
)
val NIFTY_ART_WEB = mapOf(
    ChainSelector.NEXA to "https://niftyart.cash",
    // Enable manually for niftyart development: ChainSelector.NEXAREGTEST to "192.168.1.5:8988"
    ChainSelector.NEXATESTNET to "http://192.168.2.11:8988"
)

/** Searches for a file with the provided name but with a supported extension */
fun File.resolveNftMedia(relPath: String=""): File?
{
    for (ext in NFTY_SUPPORTED_MEDIA)
    {
        val possibility = resolve(relPath + ext)
        if (possibility.exists()) return possibility
    }
    return null
}

/** Searches for a file with the provided name but with a supported extension */
fun File.resolveNftMedia(dir: String, pat: Regex, firstOfMany:Boolean=false): File?
{
    val d = resolve(dir)
    val result = d.listFiles()?.filter { it.isFile && it.nameWithoutExtension.matches(pat) }?.filter { ("." + it.canonicalExtension) in NFTY_SUPPORTED_MEDIA}
    if (result == null) return null
    if (result.size == 0) return null
    if ((result.size > 1)&&(!firstOfMany)) throw Exception("Too many matching files")
    return result.first()
}




/** Searches for a file with the provided name but with a supported extension */
fun File.resolveExists(relPath: String=""): File?
{
    val possibility = resolve(relPath)
    if (possibility.exists()) return possibility
    return null
}

// Run an external executable in a new process
// https://stackoverflow.com/questions/35421699/how-to-invoke-external-command-from-within-kotlin-code
fun String.runCommand(): String
{
    // val parts = this.split("\\s".toRegex())
    /*
        val proc = ProcessBuilder(*parts.toTypedArray())
            .directory(workingDir.toFile())
            //.redirectOutput(ProcessBuilder.Redirect.PIPE)
            //.redirectError(ProcessBuilder.Redirect.PIPE)
            .start()
        proc.waitFor(1, TimeUnit.MINUTES)
        return proc.inputStream.bufferedReader().readText()

     */

    val proc = Runtime.getRuntime().exec(this)
    proc.waitFor(1, TimeUnit.MINUTES)
    return proc.inputStream.bufferedReader().readText()
}

// Escapes double quotes and backslashes so this string is a valid json string
fun String.jsonString(): String
{
    // encodeToString wraps the string in quotes
    return Json.encodeToString(this).dropLast(1).drop(1)
    //var s = replace("\\","\\\\")
    //s = s.replace("\n"," ")  // new lines are not acceptable in json strings
    //return s.replace(""""""","""\"""")
}

fun isVideo(name:String): Boolean
{
    val n = name.lowercase()
    for (ext in NFTY_SUPPORTED_VIDEO)
    {
        if (n.endsWith(ext)) return true
    }
    return false
}

fun isAudio(name:String): Boolean
{
    val n = name.lowercase()
    for (ext in NFTY_SUPPORTED_AUDIO)
    {
        if (n.endsWith(ext)) return true
    }
    return false
}

fun isImage(name:String): Boolean
{
    val n = name.lowercase()
    for (ext in NFTY_SUPPORTED_IMAGE)
    {
        if (n.endsWith(ext)) return true
    }
    return false
}

fun canonicalExtension(s: String?): String?
{
    if (s == null) return null
    val r = s.split('.').last().lowercase()
    if (r == "jpg") return "jpeg"
    return r
}

val File.canonicalExtension: String
    get()
{
    val r = extension.lowercase()
    if (r == "jpg") return "jpeg"
    return r
}

fun canonicalSplitExtension(s: String?): Pair<String,String>?
{
    if (s == null) return null
    var r = s.substringAfterLast('.').lowercase()
    if (r == "jpg") r = "jpeg"
    return Pair(s.substringBeforeLast('.'), r)
}

@Serializable
data class NexaNFTv2(
    val niftyVer:String,
    val title: String,
    val series:String? = null,
    val author: String,
    val keywords: List<String>,
    val appuri: String? = null,
    val category: String? = null,
    val info: String,
    val bindata: String? = null,
    val data: JsonObject? = null,
    val license: String,
)

/** return filename and data of the public card front */
fun nftCardFront(nftyZip: ByteArray):Pair<String?, ByteArray?>
{
    val zipIn = ZipInputStream(ByteArrayInputStream(nftyZip))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        if (entry.name.startsWith("cardf"))
        {
            val data = zipIn.readBytes()
            return Pair(entry.name,data)
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }
    zipIn.close()
    return Pair(null,null)
}

fun nftCardBack(nftyZip: ByteArray):Pair<String?, ByteArray?>
{
    val zipIn = ZipInputStream(ByteArrayInputStream(nftyZip))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        if (entry.name.startsWith("cardb"))
        {
            val data = zipIn.readBytes()
            return Pair(entry.name,data)
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }
    zipIn.close()
    return Pair(null,null)
}

fun nftPublicMedia(nftyZip: ByteArray):Pair<String?, ByteArray?>
{
    val zipIn = ZipInputStream(ByteArrayInputStream(nftyZip))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        if (entry.name.startsWith("public"))
        {
            val data = zipIn.readBytes()
            return Pair(entry.name,data)
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }
    zipIn.close()
    return Pair(null,null)
}

fun nftOwnerMedia(nftyZip: ByteArray):Pair<String?, ByteArray?>
{
    val zipIn = ZipInputStream(ByteArrayInputStream(nftyZip))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        if (entry.name.startsWith("owner"))
        {
            val data = zipIn.readBytes()
            return Pair(entry.name,data)
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }
    zipIn.close()
    return Pair(null,null)
}


fun nftData(nftyZip: ByteArray): NexaNFTv2?
{
    val zipIn = ZipInputStream(ByteArrayInputStream(nftyZip))
    var entry = zipIn.nextEntry

    while (entry != null)
    {
        if (entry.name.lowercase() == "info.json")
        {
            val data = zipIn.readBytes()
            val s = String(data, Charsets.UTF_8)
            val js = Json { ignoreUnknownKeys = true }
            val nftInfo = js.decodeFromString<NexaNFTv2>(s)
            zipIn.close()
            return nftInfo
        }
        zipIn.closeEntry()
        entry = zipIn.nextEntry
    }
    zipIn.close()
    return null
}

// dataFile includes the extension, and mediaType is the "canonicalExtension()" so they are a little redundant
data class NFTCreationData(var dataFile: String, var mediaType: String,
                           var ownerFile: String?, var ownerMediaType: String?,
                           var cardFrontFile: String?, var cardfMediaType: String?,
                           var cardBackFile: String?, var cardbMediaType: String?,
                           var license: String,
                           var bindata: ByteArray?,
                           var title: String,
                           var series: String,
                           var author: String,
                           var keywords: List<String>,
                           var info: String,
                           var appuri: String,
                           var category: String?,
                           var data: String,
                           var quantity: Long)
{
    constructor(title: String, author: String, _series: String="" ):this("","",null, null, null, null, null, null,
        "", null, title, _series, author, listOf(), "", "", "NFT", "", 1L)
}

fun checkNftyZip(inFile:File, outputPrefix: String, parentGroupId: GroupId): GroupId
{
    // TODO verify some stuff about this zip file.

    val zipFile = FileInputStream(inFile)
    val zipBytes = zipFile.readBytes()

    var nftHash = libnexa.hash256(zipBytes)
    val nftGroup = parentGroupId.subgroup(nftHash)

    val finalName = Path(outputPrefix + "/" + nftGroup.toHex() + ".zip")

    try
    {
        inFile.copyTo(finalName.toFile())
    }
    catch (e: FileAlreadyExistsException)
    {
        // If the file already exists no reason to copy it because its name is its cryptographic hash so it must be the same file
    }
    inFile.delete()
    return nftGroup
}


fun makeNftyZip(outDir: Path, data: NFTCreationData): Path
{
    NFTY_TMP_PATH.createDirectories()
    val tmpfile = kotlin.io.path.createTempFile(NFTY_TMP_PATH, NFTY_TMP_PREFIX, ".zip")
    tmpfile.deleteIfExists()

    if (true)
    {
        val zout = ZipOutputStream(BufferedOutputStream(FileOutputStream(tmpfile.toFile())))

        val metaDataEntry = ZipEntry("info.json")
        zout.putNextEntry(metaDataEntry)

        val keywordsAsJson = data.keywords.map { "\"" + it.jsonString() + "\"" }.joinToString(",")

        val nftSpecificData = if (data.data == "") "{}" else
        {
            // Validate that its JSON by parsing it
            val je = Json.parseToJsonElement(data.data)
            data.data
        }

        // NFT author puts any JSON in the data dictionary (presumably for use by a custom app that interacts with this NFT)

        val series = data.series.let { if (it != "") """"series":"${it.jsonString()}",""" else "" }
        val bindata = data.bindata.let { if (it != null && it.size > 0) """"bindata":"${it.toHex()}",""" else "" }
        val category = data.category.let { if (it != null && it.length > 0) """"category":"${it.jsonString()}",""" else "" }

        zout.write("""{
  "niftyVer":"2.0",
  "title": "${data.title.jsonString()}",
  $series 
  "author": "${data.author.jsonString()}",
  "keywords": [ $keywordsAsJson ],
  $category
  "appuri": "${data.appuri}",
  "info": "${data.info.jsonString()}",
  $bindata
  "data" : $nftSpecificData,
  "license": "${data.license.jsonString()}"
}
""".toByteArray())

        zout.closeEntry()

        // If the file is too big to show as a card, then convert it to a smaller one
        val cardFrontFile = generateCardFile(data.cardFrontFile, data.dataFile)
        val differentFront = cardFrontFile != File(data.dataFile)
        val frontIsTmpFile = (cardFrontFile != File(data.dataFile)) && (cardFrontFile != File(data.cardFrontFile))

        val cardBackFile = generateCardFile(data.cardBackFile, null)

        if (cardFrontFile != null)
        {
            val cardFrontEntry = ZipEntry("cardf." + canonicalExtension(cardFrontFile.extension))
            var fi = FileInputStream(cardFrontFile)
            var from = BufferedInputStream(fi)
            zout.putNextEntry(cardFrontEntry)
            from.copyTo(zout)
            zout.closeEntry()
            if (frontIsTmpFile) cardFrontFile.delete()  // Clean it up
        }

        // These don't have to exist if they are no different than cardf
        if (cardBackFile != null)
        {
            val cardBackEntry = ZipEntry("cardb." + canonicalExtension(cardBackFile.extension))
            var fi = FileInputStream(cardBackFile)
            var from = BufferedInputStream(fi)
            zout.putNextEntry(cardBackEntry)
            from.copyTo(zout)
            zout.closeEntry()
        }

        if (differentFront)
        {
            val dFile = File(data.dataFile)
            val zippedFile = ZipEntry("public." + canonicalExtension(dFile.extension))
            var fi = FileInputStream(dFile)
            var from = BufferedInputStream(fi)
            zout.putNextEntry(zippedFile)
            from.copyTo(zout)
            zout.closeEntry()
        }

        if (data.ownerFile != null)
        {
            val dFile = File(data.ownerFile)
            val zippedFile = ZipEntry("owner." + canonicalExtension(dFile.extension))
            var fi = FileInputStream(dFile)
            var from = BufferedInputStream(fi)
            zout.putNextEntry(zippedFile)
            from.copyTo(zout)
            zout.closeEntry()
        }

        zout.finish()
        zout.close()
    }

    val zipFile = FileInputStream(tmpfile.toFile())
    val zipBytes = zipFile.readBytes()

    var nftHash = libnexa.hash256(zipBytes)

    val finalName = outDir.resolve(Path(nftHash.toHex() + ".zip"))
    tmpfile.copyTo(finalName)
    tmpfile.toFile().delete()

    LogIt.info("created: " + finalName)
    return finalName
}

fun generateCardFile(preferred: String?, backup: String?): File?
{
    val origfname = preferred ?: backup
    if (origfname == null) return null

    // If the file is too big to show as a card, then convert it to a smaller one
    val ret = if (File(origfname).length() > NFTY_MINT_MAX_CARD_SIZE)
    {
        // So createTempFile is carefully made to not allow it to overlap with some other newly created file
        // But we NEED that to happen because ffmpeg is going to generate the file
        // so delete the file, holding on to the file name
        var fname:String
        if (isVideo(origfname))
        {
            val tfile = kotlin.io.path.createTempFile(NFTY_TMP_PATH, NFTY_TMP_PREFIX, NFTY_DEFAULT_VIDEO_SUFFIX)
            tfile.deleteIfExists()
            fname = tfile.absolutePathString()
            // -an drops audio
            // size - 1024 because a few bytes is needed to close out the file
            // -2 means keep ratio, but be a multiple of 2
            val exec:String = FFMPEG + " -y -i " + origfname + " -vf scale=$NFTY_MINT_CARD_PIX:-2 -an " + " -fs " + (NFTY_MINT_MAX_CARD_SIZE-1024).toString() + " " + fname
            LogIt.info("Running: " + exec)
            val result = exec.runCommand()
            LogIt.info(result ?: "no result")
        }
        else if (isImage(origfname))
        {
            val newExt = if (File(origfname).extension.lowercase() == "svg") NFTY_PREFERRED_BITMAP_EXTENSION else canonicalExtension(File(origfname).extension)
            val tfile = kotlin.io.path.createTempFile(NFTY_TMP_PATH, NFTY_TMP_PREFIX, "." + newExt)
            tfile.deleteIfExists()
            fname = tfile.absolutePathString()

            val exec:String = IMMAG + " " + origfname + " -auto-orient -resize ${NFTY_MINT_CARD_PIX}x${NFTY_MINT_CARD_PIX} " + fname
            //val exec:String = FFMPEG + " -y -i " + data.dataFile + " -vf scale=$NFTY_MINT_CARD_X_PIX:-2 " + fname
            LogIt.info("Running: " + exec)
            val result = exec.runCommand()
            LogIt.info(result ?: "no result")
        }
        else if (isAudio(origfname))
        {
            return null
        }
        else
        {
            return null
        }
        File(fname)
    }
    else File(origfname)

    return ret
}

fun nftUrl(s: String?, groupId: GroupId):String?
    {
        if (s == null)
        {
            return("http://" + NIFTY_ART_IP[groupId.blockchain] + "/_public/" + groupId.toStringNoPrefix())
        }
        // TODO many more ways to get it
        return null
    }

private fun getFile(nftyZip: EfficientFile, fname:String):Pair<String?, ByteArray?>
{
    var data:ByteArray? = null
    var name:String? = null

    zipForeach(nftyZip) { header, d ->
        if (header.fileName == fname)
        {
            name = header.fileName
            if (d != null) data = d.readByteArray()
            true
        }
        else false
    }
    return Pair(name, data)
}

private fun getFileByPrefix(nftyZip: EfficientFile, namePrefix:String):Pair<String?, ByteArray?>
{
    var data:ByteArray? = null
    var name:String? = null

    zipForeach(nftyZip) { header, d ->
        if (header.fileName.startsWith(namePrefix))
        {
            name = header.fileName
            if (d != null) data = d.readByteArray()
            true
        }
        else false
    }
    return Pair(name, data)
}

fun nftCardFront(nftyZip: EfficientFile):Pair<String?, ByteArray?>
{
    return getFileByPrefix(nftyZip, "cardf")
}

fun nftCardBack(nftyZip: EfficientFile):Pair<String?, ByteArray?>
{
    return getFileByPrefix(nftyZip, "cardb")
}

fun nftPublicMedia(nftyZip: EfficientFile):Pair<String?, ByteArray?>
{
    return getFileByPrefix(nftyZip, "public")
}

fun nftOwnerMedia(nftyZip: EfficientFile):Pair<String?, ByteArray?>
{
    return getFileByPrefix(nftyZip, "owner")
}

fun nftDataFromInfoFile(infoFile: ByteArray): NexaNFTv2?
{
    val s = infoFile.decodeUtf8()
    val js = Json { ignoreUnknownKeys = true }
    val nftInfo = js.decodeFromString<NexaNFTv2>(NexaNFTv2.serializer(), s)
    return nftInfo
}

fun nftData(nftyZip: EfficientFile): NexaNFTv2?
{
    val (_, contents) = getFile(nftyZip, "info.json")
    if (contents == null) return null
    val s = contents.decodeUtf8()
    val js = Json { ignoreUnknownKeys = true }
    val nftInfo = js.decodeFromString<NexaNFTv2>(NexaNFTv2.serializer(),s)
    return nftInfo
}

fun getTokenDesc(chain: Blockchain, groupId: GroupId, getEc: () -> ElectrumClient): TokenDesc
{
    // first load the token description doc (TDD)
    try
    {
        val td = getTokenInfo(groupId.parentGroup(), getEc, chain.net)
        LogIt.info(sourceLoc() + ": Got token info for ${groupId} (${groupId.toHex()})")
        val tg = td.genesisInfo
        if (tg == null)  // Should not happen except network
        {
            LogIt.info(sourceLoc() + ": No token info available")
            throw ElectrumRequestTimeout()
        }

        // If the genesis commitment to the doc matches the actual document, then we can use it
        val tddHash = td.tddHash
        if ((tddHash != null) && (tg.document_hash == tddHash.toHex()))
        {
            LogIt.info(sourceLoc() + ": Saving token info for ${groupId}")
            return td
        }
        else
        {
            LogIt.info(sourceLoc() + ": Incorrect or non-existent token desc document for ${groupId}")
            return td
        }
    }
    catch(e: Exception)  // Normalize exceptions
    {
        handleThreadException(e, "getting token info (assetmanager.getTokenDesc) for ${groupId}")
        throw ElectrumRequestTimeout()
    }
}

/** Checks various sources for the NFT file, and returns it, only if it's hash properly matches the subgroup.
 * This means that a source cannot lie about what the NFT file is, so we can check untrusted sources. */
fun getNftFile(td: TokenDesc?, groupId: GroupId):Pair<String, EfficientFile>?
{
    var url = td?.nftUrl ?: nftUrl(td?.genesisInfo?.document_url, groupId)
    LogIt.info(sourceLoc() + "${groupId} NFT URL: " + url)

    var zipBytes:ByteArray? = null
    if (url != null)
    {
        try
        {
            LogIt.info(sourceLoc() + "${groupId} trying NFT URL: " + url)
            zipBytes = Url(url).readBytes(context = tokenCoCtxt)
        }
        catch(e:Exception)
        {
            logThreadException(e, "(from token doc location) NFT not loaded ")
        }
    }

    // Try well known locations
    if (zipBytes == null || zipBytes.size == 0)
    {
        try
        {
            url = "${NIFTY_ART_WEB[groupId.blockchain]}/_public/${groupId.toStringNoPrefix()}"
            LogIt.info(sourceLoc() + "${groupId} trying NFT URL: " + url)
            zipBytes = Url(url).readBytes(context = tokenCoCtxt)
            LogIt.info(sourceLoc() + "${groupId} received NFT URL: " + url)
        }
        catch(e: Exception)
        {
            logThreadException(e, "(trying niftyart) NFT not loaded ")
        }
    }

    if (zipBytes != null && zipBytes.size > 0)
    {
        LogIt.info("NFT file loaded for ${groupId.toStringNoPrefix()}")

        val hash = libnexa.hash256(zipBytes)
        if (groupId.subgroupData() contentEquals hash)
        {
            LogIt.info(sourceLoc() + "nft zip file matches hash for ${groupId.toStringNoPrefix()}")
        }
        else
        {
            // check another typical but nonstandard hash
            val hash = libnexa.sha256(zipBytes)
            if (groupId.subgroupData() contentEquals hash)
            {
                LogIt.info(sourceLoc() + "nft zip file matches sha256 hash for ${groupId.toStringNoPrefix()}")
            }
            else return null
        }
        val ef = EfficientFile(zipBytes)
        val nftData = nftData(ef)  // Sanity check the file
        if (nftData == null)
        {
            LogIt.info("but is NOT an NFT file")
            return null
        }
        return Pair(url!!, ef)
    }

    return null
}
