package Nexa.npl

import org.nexa.libnexakotlin.OP
import org.nexa.libnexakotlin.simpleapi.NexaScript
import java.util.*


class VmState
{
    /** Provide bindings to create a VM state with those binding on the main stack */
    constructor(vararg stackPushes: NBinding)
    {
        stack.addAll(stackPushes)
    }
    constructor(other: VmState)
    {
        stack.addAll(other.stack)
        alt.addAll(other.alt)
        global.addAll(other.global)
    }

    /** Provide 2 lists of bindings to put those bindings on the main and alt stacks */
    constructor(stackPushes:List<NBinding>, altPushes:List<NBinding>, _global:Set<NBinding>? = null)
    {
        stack.addAll(stackPushes)
        alt.addAll(altPushes)
        if (_global!=null) global.addAll(_global)
    }


    // This is the set of VM state that is not part of a stack
    var global = mutableSetOf<NBinding>()
    // The 2 stacks
    var stack = mutableListOf<NBinding>()
    var alt = mutableListOf<NBinding>()

    /** true if the state of this VM is failed (tx validation failed */
    var failed = false

    /** set to true if the stack defined above only specifies the stack top -- you are ok if other stuff is deep in the stack.
     * This is used when this data structure is used to define desired stack states
     * */
    var allowStackPrefix = true
    /** set to true if the stack defined above only specifies the stack top -- you are ok if other stuff is deep in the altstack.
     * This is used when this data structure is used to define desired stack states
     * */
    var allowAltPrefix = true

    /* Set to true if the defined bindings on the main stack must be ordered (this is the common case).
     * Some operations, such as addition, are commutative so the order of items on the stack does not matter.
     * If you set this value to false in these cases, the compiler may be able to produce smaller code.
     */
    var orderedStack:Boolean = true

    /* Set to true if the defined bindings on the alt stack must be ordered (this is the common case).
     * Some operations, such as addition, are commutative so the order of items on the stack does not matter.
     * If you set this value to false in these cases, the compiler may be able to produce smaller code.
     */
    var orderedAlt:Boolean = true

    /** Return true if every binding in this state is bound */
    val isBound: Boolean
        get()
        {
            stack.forEach({ if (!it.isBound) return false })
            alt.forEach({ if (!it.isBound) return false })
            return true
        }

    infix fun contentEquals (other: VmState): Boolean
    {
        // the global state is implicitly part of every VmState: if (global != other.global) return false
        if (stack != other.stack) return false
        if (alt != other.alt) return false
        return true
    }

    /** Bind every unbound object in this VmState */
    fun bind()
    {
        stack.forEach({ if (!it.isBound) it.bind() })
        alt.forEach({ if (!it.isBound) it.bind() })
    }

    /** Count the number of true returns of the filter func run over all bindings in this VmState */
    fun count(filter: (NBinding) -> Boolean): Int
    {
        return stack.count { filter(it) } + alt.count { filter(it) }
    }


    /** Count the number of times this binding appears in this VmState */
    fun count(match: NBinding): Int = count({ it == match})


    /** Remove every binding in b from this state */
    operator fun minus(b: VmState):Set<NBinding>
    {
        // I wonder if this will fail if stuff is in entryStack but not in exitStack?
        return (stack.toSet() + alt - b.stack - b.alt)
    }

    override fun toString(): String
    {
        val s = StringJoiner(",")
        val a = StringJoiner(",")
        stack.forEach( { s.add(it.toString())})
        alt.forEach( { a.add(it.toString())})
        return "$stack | $alt"
    }

    /** pop the passed items off of the end (top) of the main stack */
    fun popstack(lst: List<NBinding>)
    {

        for (i in lst.asReversed())
        {
            check (!stack.isEmpty())
            val mlast = stack.last()
            // make sure that what we are popping off is really what's on the end of the list
            // if this check fails, the stack is not formed correctly to dispatch the instruction
            mycheck(i == mlast)
            stack.removeLast()
        }
    }

    /** pop the passed items off of the end (top) of the alt stack */
    fun popalt(lst: List<NBinding>)
    {

        for (i in lst.asReversed())
        {
            check(alt.isNotEmpty())
            val mlast = alt.last()
            // make sure that what we are popping off is really what's on the end of the list
            // if this check fails, the stack is not formed correctly to dispatch the instruction
            mycheck(i == mlast)
            alt.removeLast()
        }
    }

    /** modify this VmState as directed by the passed step */
    fun apply(s: Step)
    {
        // remove all the stack items consumed by this step
        popstack(s.entry.stack)
        popalt(s.entry.alt)
        s.entry.global.forEach( { global.remove(it)})

        // place the items created by this step
        s.exit.stack.forEach({ stack.add(it)})
        s.exit.alt.forEach({ alt.add(it)})
        global.addAll(s.exit.global)
    }

}

val VM_NO_CHANGE = VmState()

open class Step(val programOrder: Int, var entry: VmState, var opcode: List<OP>, var exit: VmState,
           /** true if the order of the items in the entry VmState matters, false if the params are commutative */
           val orderedEntry:Boolean = true)
{
    //constructor(programOrder: Int, entry: VmState, opcode: List<OP>, exit: VmState, orderedEntry:Boolean = true):
    //    this(programOrder, entry, opcode, null, exit, orderedEntry)

    //constructor(programOrder: Int, entry: VmState, substeps: List<Step>, exit: VmState, orderedEntry:Boolean = true):
    //    this(programOrder, entry, null, substeps, exit, orderedEntry)

    constructor(programOrder: Int, entry: VmState, exit: VmState, orderedEntry:Boolean = true):
        this(programOrder, entry, listOf(), exit, orderedEntry)

    /** If True, this step does not contribute to the output.  See [NSL.markDeadCode] */
    var deadcode:Boolean? = null

    /** Weights specify the dataflow-based sort order of each step */
    var weight = Int.MAX_VALUE

    /** Eventually steps are proposed in a particular order.  This enumerates that order
        and is used to determine the last step a binding is used.
    */
    var ordinal = Int.MAX_VALUE

    /** subroutines or clauses associated with this step (e.g. contents of IF or ELSE clauses) */
    var sub:MutableList<NSL> = mutableListOf()

    /** Subroutines expect the exact same entry stack, so we need to take all the foreign bindings
     used in all subs and put them on the entry stack of all subs.
    */
    var unifySubEntryStack = false

    /** For statements the close and open a subclause (e.g. ELSE), this is true if the "close" portion is consumed already */
    var closed = false

    /** The source code location where this step ultimately comes from */
    var srcLoc = SourceLocation()

    val createdBindings: Set<NBinding>
        get()
        {
            return exit - entry
        }

    /** Returns the size in bytes of this step (and any substeps) */
    val size: Int
        get()
        {
            val scr = NexaScript(*fullOpcodes().toTypedArray())
            return scr.size
        }

    fun fullOpcodes(): List<OP>
    {
        val opcodes = mutableListOf<OP>()

        // TODO refactor:  Depending on how much customization is needed, derive subclasses of Step and overload
        if (opcode.size > 0 && opcode[0] == OP.IF)
        {
            opcodes.add(OP.IF)
            if (sub.size == 0) return opcodes  // probably just dumping the object prematurely
            opcodes.addAll(sub[0].fullOpcodes())  // there must be a then clause, unless we are asking for the opcode list prematurely
            if (sub.size == 2)  // if there is another clause, it must be the else
            {
                opcodes.add(OP.ELSE)
                opcodes.addAll(sub[1].fullOpcodes())
            }
            opcodes.add(OP.ENDIF)
        }
        else
        {
            // Default behavior is to include the opcode and then all subs in order
            if (opcode != null) for (a in opcode)
            {
                opcodes.add(a)
            }
            sub.forEach { clause ->
                for (s in clause.endPgm!!)  // If you haven't compiled this sub, we can't provide the full opcodes yet
                {
                    opcodes.addAll(s.fullOpcodes())
                }
            }
        }

        return opcodes
    }

    fun dump(bytePos: Boolean = false, indent:Int = 0): String
    {
        val spaces = "  ".repeat(indent)
        val s = StringBuilder()
        s.append(spaces + toString() + "\n")
        // TODO refactor:  Depending on how much customization is needed, derive subclasses of Step and overload
        if (opcode.size > 0 && opcode[0] == OP.IF)
        {
            s.append(spaces + "THEN" + "\n")
            if (sub.size > 0) s.append(sub[0].dump(indent = indent+1))
            s.append(spaces + "ELSE" + "\n")
            if (sub.size > 1) s.append(sub[1].dump(indent = indent+1))
            s.append(spaces + "ENDIF" + "\n")
        }
        return s.toString()
    }

    override fun toString(): String
    {
        val s = StringBuilder()

        val opcodes = opcodeString()

        //s.append(""" "(${entry}) ${opcodes} (${exit})" w:${weight}""")
        val deadmarker = if (deadcode == null) " " else if (deadcode == true) "~" else "+"
        s.append("""$deadmarker ${opcodes} (${entry}) -> ${exit} w:${weight} [${srcLoc}]""")
        return s.toString()
    }

    fun opcodeString(delimiter:String = " "): String
    {
        val opcodes = StringJoiner(delimiter)
        try
        {
            fullOpcodes().map { OP.toAsm(it.v) }.forEach { opcodes.add(it) }
        }
        catch (e: IllegalStateException)  // Subs not compiled into opcodes yet
        {
            opcode.map { OP.toAsm(it.v) }.forEach { opcodes.add(it) }
            opcodes.add("...")
        }
        return opcodes.toString()
    }

    /** Apply the stack transformation described in this step to the simulated VM state */
    fun applyToVmState(curState: VmState)
    {
        curState.apply(this)
    }
}
