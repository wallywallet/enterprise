// Copyright (c) 2021 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.wallywallet.wew
import org.nexa.libnexakotlin.*

class RamKvpDatabase:KvpDatabase
{
    val db = mutableMapOf<ByteArray, ByteArray>()
    override fun clear()
    {
        db.clear()
    }

    override fun close()
    {
        db.clear()
    }

    override fun delete(key: ByteArray)
    {
        db.remove(key)
    }

    override fun get(key: ByteArray): ByteArray
    {
        val v = db.get(key)
        if (v == null) throw DataMissingException(key.toHex())
        return v
    }

    override fun getOrNull(key: ByteArray): ByteArray?
    {
        return db.get(key)
    }

    override fun set(key: ByteArray, value: ByteArray): Boolean
    {
        db[key] = value
        return true
    }
}

/*
class RamWallet(name: String, chainSelector: ChainSelector) : CommonWallet(name, chainSelector)
{
    /** This secret data is used as entropy when generating seeded destinations (seeded destinations are a destination that, given a seed, always produces the
     * same destination. */
    val secretGen: ByteArray = libnexa.secureRandomBytes(32)
    val backingStore = RamKvpDatabase()

    init
    {
        walletDb = backingStore
        LONG_DELAY_INTERVAL = 500  // RamWallet is only used in a test environment so use quick delays
        prepareDestinations(100,10)  // put in at least one destination, because the non-ram wallets expect a bunch of pregenerated destinations

    }

    override fun generateDestination(): PayDestination
    {
        return Pay2PubKeyHashDestination(chainSelector, UnsecuredSecret(libnexa.secureRandomBytes(32)))
    }

    override fun destinationFor(seed: String): PayDestination
    {
        val generator: ByteArray = seed.toByteArray() + secretGen
        val secret = UnsecuredSecret(libnexa.hash256(generator))
        return Pay2PubKeyHashDestination(chainSelector, secret)
    }


    /** Save wallet transaction state to the database -- NO-OP for the RAM wallet */
    override fun saveWalletTo(db: KvpDatabase?)
    {
        return
    }

    /** Load wallet transaction state from the database */
    override fun loadWalletTx(db: KvpDatabase): Boolean
    {
        return true
    }
}
*/