// Copyright (c) 2023 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.wallywallet.wew


import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.*
import androidx.compose.ui.input.pointer.PointerEventType
import androidx.compose.ui.input.pointer.isTertiaryPressed
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.nexa.libnexakotlin.*
import org.wallywallet.composing.defaultTextFontSize
import org.wallywallet.composing.pxToDp
import org.wallywallet.wew.cli.FieldDict
import org.wallywallet.wew.cli.dictify
import kotlin.math.roundToInt
import kotlin.reflect.full.memberExtensionFunctions
import kotlin.reflect.full.memberExtensionProperties
import kotlin.reflect.full.memberFunctions
import kotlin.reflect.full.memberProperties
private val LogIt = GetLog("wew.guiConsole")
val neverInSymbol = charArrayOf('{','(',' ', '+', '-', '=', '*', '/', ':', '?',';','!','&','|',',')

val enteredCmdLine = MutableStateFlow(TextFieldValue(""))
val focusOnCmdLine = MutableStateFlow(FocusRequester())

val digits = setOf('0','1','2','3','4','5','6','7','8','9')

enum class StringContentType { ANY, FLOAT, UINT}
fun String.classify(): StringContentType
{
    return this.fold(StringContentType.UINT) { cur, v: Char ->
        if (cur==StringContentType.ANY) StringContentType.ANY
        else if (!(digits.contains(v) || v == '.')) StringContentType.ANY
        else if (v == '.') StringContentType.FLOAT
        else StringContentType.UINT
    }
}

fun maxf(a: Float, b:Float) = if (a>b) a else b

fun injectTextIntoCmdLine(data: Any?, role: DataRole):Boolean
{
    val ts = enteredCmdLine.value
    if (data != null)
    {
        var fo = if (data is Function<*>) (data as ()->Any?)?.invoke() else data  // Resolve a lambda
        var s = fo.toString() // stringify it

        if (role == DataRole.DATA)
        {
            if (fo is String || fo is ByteArray || fo is UByteArray) s = "\"" + s + "\""
            else if (fo is Char) s = "'" + s + "'"
            else
            {
                // If its not some kind of number, quote it
                val what = s.classify()
                if (what == StringContentType.ANY) s = "\"" + s + "\""
            }
        }

        val t = ts.text.replaceRange(ts.selection.start, ts.selection.end, s)  // stick it into our line
        enteredCmdLine.value = TextFieldValue(t, selection = TextRange(ts.selection.start, ts.selection.start+s.length))
        // focusOnCmdLine.value.requestFocus()

        return true
    }
    return false
}

val panelDragMode = MutableStateFlow<Boolean>(false)

@Composable
fun CommandEntry(panelState: InterpreterPanelState)
{
    var histItem by remember { mutableStateOf(0) }
    // This is what's been typed.  But it might not be what is showing right now in the command line (that is the enteredCmdLine global)
    // This is used to go back to the current line after the user browses the history
    var enteredText by remember { mutableStateOf(TextFieldValue("")) }

    // Collected state of the current command line field
    var cmdLine = enteredCmdLine.collectAsState()
    val clipboardManager: ClipboardManager = LocalClipboardManager.current

    fun proposeCompletions(prefix: String): List<String>
    {
        if (prefix.isBlank())
        {
            panelState.updateCompletionDashboard("", "", "", panelState.dw.scriptTopLevelSyms.keys.sorted(), {
                enteredCmdLine.value = TextFieldValue(it)
            })
            return listOf()
        }

        var lastDot = prefix.lastIndexOf(".")

        val result = panelState.dw.invisibleEval(prefix)
        if (!(result is javax.script.ScriptException))  // The current line resolves to something
        {
            when (result)
            {
                is Map<*, *> ->
                {
                    return listOf(prefix + "[")
                }

                is List<*> -> return listOf(prefix + "[")
            }

            if (result != null)
            {
                val type = result::class
                if (type.memberProperties.isNotEmpty() || type.memberFunctions.isNotEmpty() || type.memberExtensionProperties.isNotEmpty() || type.memberExtensionFunctions.isNotEmpty())
                {
                    return listOf(prefix + ".")
                }
            }
        } else
        {
            var unrelatedPrefix = ""
            var objName = if (lastDot == -1) "" else prefix.take(lastDot)
            val beginTokenizer = objName.lastIndexOfAny(neverInSymbol)
            if (beginTokenizer != -1)
            {
                unrelatedPrefix = objName.take(beginTokenizer + 1)
                objName = objName.drop(beginTokenizer + 1)
            }

            val rest = prefix.drop(lastDot + 1) // don't want the dot
            val result = if (objName.isBlank()) panelState.dw.scriptTopLevelSyms else panelState.dw.invisibleEval(objName)
            if (result == null) return listOf() // TODO say something
            val options = dictify(result)
            if (options != null)
            {
                val opDict = options as? FieldDict
                if (opDict != null)
                {
                    val sm = opDict.keys.sortedWith(object : Comparator<String>
                    {
                        override fun compare(p0: String?, p1: String?): Int
                        {
                            if (p0 == p1) return 0
                            if (p0 == null) return 1
                            if (p1 == null) return -1
                            val p0prefix = p0.startsWith(rest)
                            val p1prefix = p1.startsWith(rest)
                            if (p0prefix == p1prefix)  // if they are both true or both false we compare lexicagraphically
                            {
                                return p0.compareTo(p1)
                            }
                            // If one of them matches the prefix that one is preferred
                            if (p0prefix) return -1
                            if (p1prefix) return 1
                            return p0.compareTo(p1)  // should never be hit
                        }

                    })

                    panelState.updateCompletionDashboard(unrelatedPrefix, objName, rest, sm, {
                        enteredCmdLine.value = TextFieldValue(it)
                    })
                    return sm.map { unrelatedPrefix + objName + "." + it }
                }
            }
            // ok resolve this string to an object and then look up completions for it
        }
        return listOf()
    }


    fun histHelper(amt: Int)
    {
        var last = panelState.dw.inputLines.size
        if (last > 0)
        {
            if (histItem == 0) enteredText = enteredCmdLine.value // remember what's currently on the line
            if (amt == -1 || amt == 1) histItem += amt
            else histItem = amt

            if (last + histItem < 0) histItem = 0
            if (histItem > 0) histItem = 0
            val t = if (histItem >= 0) "" else panelState.dw.inputLines[last + histItem]
            enteredCmdLine.value = if (histItem >= 0) enteredText else TextFieldValue(
                text = t,
                selection = TextRange(t.length)
            )
        }

    }

    val ka = KeyboardActions(onDone = {}, onSend = {})


    TextField(value = cmdLine.value, onValueChange =
    {
        // check if a \n or \t was typed.  If so don't accept it (we will execute the entire line)
        if (cmdLine.value.text != it.text)  // First see if any text changed (as opposed to cursor movement)
        {
            try
            {
                // Ok 2nd see if \n is under the cursor
                val slice = it.text.slice(IntRange(it.selection.start - 1, it.selection.end - 1))
                if (slice != "\n" && slice != "\t")  // no so accept it
                {
                    enteredCmdLine.value = it
                }
                // otherwise refuse the change
            } catch (e: Exception)
            {
                // If something goes wrong with my analysis take the change
                enteredCmdLine.value = it
            }
        } else enteredCmdLine.value = it

    },
        colors = TextFieldDefaults.textFieldColors(backgroundColor = Color.LightGray),
        modifier = Modifier.background(Color.LightGray).fillMaxWidth().focusRequester(focusOnCmdLine.collectAsState().value).pointerInput(Unit) {
            awaitPointerEventScope {
                val event = awaitPointerEvent()
                if (event.type == PointerEventType.Press &&
                    event.buttons.isTertiaryPressed
                )
                {
                    event.changes.forEach { e -> e.consume() }
                    val pasteText = clipboardManager.getText()?.text
                    pasteText?.let {
                        val t = cmdLine.value.text.replaceRange(cmdLine.value.selection.start, cmdLine.value.selection.end, it)
                        enteredCmdLine.value = TextFieldValue(t, selection = TextRange(cmdLine.value.selection.start + 1, cmdLine.value.selection.end + 1))
                    }
                    if (pasteText == null)
                    {
                        // TODO displayError
                        println("NO PASTE CONTENTS")
                    }
                }
            }
        }.onKeyEvent {
            if (it.utf16CodePoint == 10) // enter codepoint is 10
            //if ((it.nativeKeyEvent as KeyEvent).keyChar == '\n')
            // ((it.nativeKeyEvent as InternalKeyEvent).nativeEvent as KeyEvent)
            {
                if (it.type == KeyEventType.KeyDown)
                {
                    /*
                    if (it.isCtrlPressed)
                    {
                        // this is in the wrong place (under key 10 pressed) and ctrl-v appears to work anyway by default
                        if (it.key == Key.V)
                        {
                            val pasteText = clipboardManager.getText()?.text
                            pasteText?.let {
                                val t = cmdLine.value.text.replaceRange(cmdLine.value.selection.start, cmdLine.value.selection.end, it)
                                enteredCmdLine.value = TextFieldValue(t, selection = TextRange(cmdLine.value.selection.start+1,cmdLine.value.selection.end+1))
                            }
                            if (pasteText == null)
                            {
                                // TODO displayError
                                println("NO PASTE CONTENTS")
                            }
                        }
                    }
                    */
                    if (it.isCtrlPressed || it.isShiftPressed)
                    {
                        val t = cmdLine.value.text.replaceRange(cmdLine.value.selection.start, cmdLine.value.selection.end, "\n")
                        enteredCmdLine.value = TextFieldValue(t, selection = TextRange(cmdLine.value.selection.start + 1, cmdLine.value.selection.end + 1))
                    } else  // plain enter: execute a command
                    {
                        val tmp = cmdLine.value.text  // Grab a copy before the async launch
                        launch(panelState.dw.coScope) { panelState.execute(tmp) }
                        enteredCmdLine.value = TextFieldValue("")
                        enteredText = TextFieldValue("")
                        histItem = 0
                    }
                }
                else if (it.type == KeyEventType.KeyUp)
                {
                    if (!it.isAltPressed)
                    {
                        panelDragMode.value = true
                    }
                }
                else if (it.type == KeyEventType.Unknown) /// Strangely pressing enter comes in like this
                {
                    if (!(it.isCtrlPressed || it.isShiftPressed))
                    {
                        val tmp = cmdLine.value.text  // grab a copy before async launch
                        launch(panelState.dw.coScope) { panelState.execute(tmp) }
                        enteredCmdLine.value = TextFieldValue("")
                        enteredText = TextFieldValue("")
                        histItem = 0
                    }
                }
                false
            }
            else
            {
                when (it.key.toString())
                {
                    "Key: Tab" ->
                    {
                        val p = proposeCompletions(cmdLine.value.text)
                        if (p.size > 0)
                        {
                            val choice = p[0]
                            val pfxLen = choice.commonPrefixWith(cmdLine.value.text).length
                            enteredCmdLine.value = TextFieldValue(choice, TextRange(pfxLen, choice.length))
                        }
                        true
                    }
                    // for MacOS
                    "Key: ⇥" ->
                    {
                        val p = proposeCompletions(cmdLine.value.text)
                        if (p.size > 0)
                        {
                            val choice = p[0]
                            val pfxLen = choice.commonPrefixWith(cmdLine.value.text).length
                            enteredCmdLine.value = TextFieldValue(choice, TextRange(pfxLen,choice.length))
                        }
                        true
                    }
                    "Key: Up"        ->
                    {
                        histHelper(-1)
                        true
                    }
                    // for MacOS
                    "Key: ↑"        ->
                    {
                        histHelper(-1)
                        true
                    }
                    "Key: Down"      ->
                    {
                        histHelper(1)
                        true
                    }
                    // for MacOS
                    "Key: ↓"      ->
                    {
                        histHelper(1)
                        true
                    }
                    "Key: Page Down" ->
                    {
                        histHelper(0)
                        true
                    }

                    "Key: Page Up" ->
                    {
                        histHelper(-(panelState.dw.inputLines.size))
                        true
                    }

                    "Key: Alt" ->
                    {
                        if (it.type == KeyEventType.KeyDown)
                        {
                            println("panel drag mode ON")
                            panelDragMode.value = true
                        } else if (it.type == KeyEventType.KeyUp)
                        {
                            println("panel drag mode OFF")
                            panelDragMode.value = false
                        }
                        false
                    }
                    // for MacOS
                    "Key: ⌃" ->
                    {
                        if (it.type == KeyEventType.KeyDown)
                        {
                            println("panel drag mode ON")
                            panelDragMode.value = true
                        }
                        else if (it.type == KeyEventType.KeyUp)
                        {
                            println("panel drag mode OFF")
                            panelDragMode.value = false
                        }
                        false  // I don't want to consume it
                    }
                    else             -> false
                }
            }

        },
        keyboardActions = ka,
        // maxLines = 50,
        textStyle = TextStyle(fontWeight = FontWeight.Bold, fontSize = defaultTextFontSize)
    )
}



@Composable
fun ConsolePanel(panelState: InterpreterPanelState)
{
    val cs = rememberCoroutineScope()
    val focusRequester = FocusRequester()
    val oldDragMode = remember { mutableStateOf(false)}
    val pdm by panelDragMode.collectAsState()

    LaunchedEffect(pdm)
    {
        if (oldDragMode.value != pdm)
        {
            for (i in panelState.console) i.dragMode(pdm)
            oldDragMode.value = pdm
        }
    }


    val s = panelState.consoleState

    var knobOffset by remember { mutableStateOf(0f) }
    var maxScroll by remember { mutableStateOf(1000f) }
    var knobHeight by remember { mutableStateOf(50f) }
    var listOffset by remember { mutableStateOf(0f) }


    LaunchedEffect(panelState.totalHeight)
    {
        if (panelState.totalHeight > 0)
        {
            var newFrac = listOffset / panelState.totalHeight
            if (newFrac > 1.0) newFrac = 1.0f
            knobOffset = maxf(0f, (maxScroll-knobHeight) * newFrac)
        }
    }

    Row(Modifier.fillMaxWidth()) {
        Box(Modifier.width(10.dp).fillMaxHeight().background(Color.LightGray).onGloballyPositioned {
            maxScroll = it.size.height.toFloat()
        })

        {
            Box(Modifier.padding(0.dp,knobOffset.roundToInt().dp,0.dp,0.dp).fillMaxWidth().height(knobHeight.pxToDp()).background(Color.DarkGray)
                .draggable(
                    orientation = Orientation.Vertical,
                    state = rememberDraggableState { delta ->
                        // LogIt.info("draggable by $delta")
                        val tmp = knobOffset + delta
                        // Keep it in bounds
                        knobOffset = if (tmp<0f) 0f else if (tmp > maxScroll - knobHeight) maxScroll - knobHeight else tmp
                        cs.launch {
                            val scrollRatio = delta/maxScroll

                            // println("ratio $scrollRatio  amt $scrollAmt")
                            s.scrollBy( panelState.totalHeight * scrollRatio)
                            listOffset += panelState.totalHeight * scrollRatio
                        }
                    }
                ))
        }

        LazyColumn(
            Modifier.fillMaxSize().padding(0.dp).pointerInput(Unit) {
                awaitPointerEventScope {
                    while (true)
                    {
                        val event = awaitPointerEvent()
                        if (event.type == PointerEventType.Release)
                        {
                            val cursorPosition = event.changes.first().position
                        }
                    }
                }
            }, contentPadding = PaddingValues(0.dp), verticalArrangement = Arrangement.spacedBy(0.dp, Alignment.Bottom), state = panelState.consoleState
        )
        {
            items(panelState.console) {
                it.compose()
            }
        }
    }
}
