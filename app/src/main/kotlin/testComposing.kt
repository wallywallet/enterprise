package org.wallywallet.wew

import androidx.compose.foundation.background
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import org.wallywallet.composing.*


class TestComposing: Composing
{
    var bcol = 1.0f
    val t = text("testComposing.kt test", defaultTextStyle)
    val t1:ComposingContainer = rowOf("list"," test", box(rowOf("boxed", CNL, "below")))

    val im1 = image("media/test.png", 50, 50)
    val im2 = image("media/test.svg", 50, 50)
    var row4 = rowOf(im1, im2)

    var row3 = rowOf(button("append")
    {
        t.text = t.text + " pressed"
        println("${t.text}")
    },
    button("grow")
    {
        t.style = t.style!!.copy(fontSize = t.style!!.fontSize.scale(1.1))
        im1.size.scale(1.1)

        println("${t.text}")
    },
        button("bkg")
        {
            bcol = bcol - 0.1f
            if (bcol < 0.0f) bcol = 1.0f
            t.modifier = Modifier.background(Color(bcol, 1.0f, 1.0f))
        },
        button(image("media/test.png", 50))
        {
            (t1.items[0] as? MutableCollection<Composing>)?.add(image("media/test.png",60))
            (t1 as? Refreshing)?.refresh()
        },
        )

    val sash = box(CCVerticalSash(im1, im2, row3), 500.dp, 300.dp)

    val all = colOf(t, t1, row3, row4, expander(rowOf("foo"), indent(50.dp, colOf("this ", " is ", "a test"))), sash)



    @Composable override fun compose()
    {
        initDefaults()
        all.compose()
    }
}