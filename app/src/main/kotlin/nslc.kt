package Nexa.npl

import org.nexa.libnexakotlin.GetLog
import org.nexa.libnexakotlin.OP
import java.lang.Integer.max
import java.util.*
import kotlin.Comparator
import kotlin.NoSuchElementException
import kotlin.collections.ArrayDeque

private val LogIt = GetLog("nexa.nsl.nslc")

const val STACK_SCRIPTS_FILE = "stackScripts.bin"


// basically the global variables of the compilation but not at global scope in case we multithread it
class CompilationContext
{
    /** return true if this is operating in compilation context.
     * If false, no opcodes will be generated to the script.
     * This normally happens in evaluation context.
     * */
    var compiling = false

    /** If the size of the code needed to instantiate this is greater than this value, we will attempt to save this object on the stack */
    var alwaysReinstantiateLimit = 4

    /** Get the current script or clause */
    val nsl: NSL
        get() = nslStack.last()


    protected val nslStack = ArrayDeque<NSL>()

    fun<T> with(n:NSL, fn:()->T):T
    {
        push(n)
        try
        {
            val ret = fn()
            return ret
        }
        finally
        {
            pop(n)
        }

    }

    fun push(n: NSL)
    {
        nslStack.addLast(n)
        compiling = n.evalConfig.compiling
    }
    fun pop(n: NSL)
    {
        check(nslStack.last() == n)
        nslStack.removeLast()
        if (nslStack.size == 0) compiling = false
        else compiling = nsl.evalConfig.compiling
    }
}
// use cc
var compCtxt = CompilationContext()

val cc:CompilationContext
    get() = compCtxt


/** Alias for byteArrayOf */
fun bao(vararg elements: Byte) = byteArrayOf(*elements)

fun ByteArray.toCoords(): String
{
    val s = StringJoiner(",")
    forEach { s.add(it.toString())}
    return "($s)"
}



const val UNUSED_INT = Int.MIN_VALUE+1

class OInt(var v: Int = UNUSED_INT)
class StackObjCvter
{
    val boMap = mutableMapOf<NBinding, OInt>()


    fun cvt(stack:List<NBinding>): MutableList<OInt>
    {
        val ret = mutableListOf<OInt>()
        for (i in stack)
        {
            ret.add(boMap.getOrPut(i, { OInt() }))
        }
        return ret
    }

    fun backConverter():Map<Int, NBinding>
    {
        val ret = mutableMapOf<Int, NBinding>()
        for (i in boMap)
        {
            // This is ok, it could me an unused variable that get chopped
            //mycheck(i.value.v != Int.MIN_VALUE)  // OInts need to be assigned real values before calling backConverter()
            ret[i.value.v] = i.key
        }
        return ret
    }
}

/** calculate the abstract transform needed to get from an initial state to the desired end state.
 *
 * TODO: consume Bindings that have reached their end of life, otherwise preserve them
 *
 * @param curVm: the current VM state (to transition FROM)
 * @param desiredVm: the VM state to transition TO
 */
fun NSL.calcStackXform(curVm: VmState, desiredVm: VmState):Pair<StateTransition,Map<Int, NBinding>>
{
    val cvter = StackObjCvter()
    val cs = cvter.cvt(curVm.stack)
    val ca = cvter.cvt(curVm.alt)
    val ds = cvter.cvt(desiredVm.stack)
    val da = cvter.cvt(desiredVm.alt)

    // Now chop the unneeded parts of the stack and alt stacks (stuff deeply buried)
    // find the earliest use of a desired end state in stack and alt
    val duses = ds.toSet() + da
    if (desiredVm.allowStackPrefix)
        while(cs.isNotEmpty() && !(cs[0] in duses)) cs.removeFirst()
    if (desiredVm.allowAltPrefix)
        while(ca.isNotEmpty() && !(ca[0] in duses)) ca.removeFirst()

    // Now assign numbers in the canonical manner (stack, then alt, bottom to top (aka start to end))
    var count=0
    for (i in cs)
    {
        if (i.v == UNUSED_INT)
        {
            i.v = count
            count++
        }
    }
    for (i in ca)
    {
        if (i.v == UNUSED_INT)
        {
            i.v = count
            count++
        }
    }

    // everything in the desired stack had better now be assigned because the transform can't create any elements
    for ((idx,i) in ds.withIndex())
    {
        if (i.v == UNUSED_INT)
        {
            val err = StringBuilder()
            err.append("ERROR: missing stack binding ${desiredVm.stack[idx]} index: $idx\n")
            err.append("current: $curVm\n")
            err.append("desired: $desiredVm\n")
            err.append("This often happens when you provided a non-constant template parameter (templates are resolved at compile time, so all parameters must be constant).\n")
            throw Err(NslException(err.toString()))
        }
    }

    // If I didn't make an assignment, I better not be using the binding
    cs.map{ mycheck(it.v != UNUSED_INT) }
    ca.map{ mycheck(it.v != UNUSED_INT) }
    ds.map{ mycheck(it.v != UNUSED_INT) }

    // Convert these object lists to bytearrays
    val sba = ByteArray(cs.size, { cs[it].v.toByte()})
    val aba = ByteArray(ca.size, { ca[it].v.toByte()})
    val dba = ByteArray(ds.size, { ds[it].v.toByte()})

    // TODO, BUG by specifying nothing in the altstack dest, I'm suggesting that everything from the first used param onwards be removed from the altstack, even stuff that's not used
    return Pair(StateTransition(SD(sba, aba), SD(dba, byteArrayOf())), cvter.backConverter())
}

/** find or calculate the code required to execute the proposed stack transformation */
fun NSL.findXformCode(xSpec: StateTransition): Clause
{
    // First, see if it's a no-op
    if (xSpec.begin == xSpec.end) return Clause(listOf(), xSpec)

    // Second, see if it is a primitive we already have
    val c = stackX.table.get(xSpec)
    if (c != null)
    {
        val t = c
        t.xSpec = xSpec  // Annotate this Clause with
        return c
    }
    // Try to split the xSpec into multiple StateTransitions and solve those

    // Try a random transition and then solve the result

    // Display this state transition for debugging
    println("Missing this state transition (in ${stackX.table.size} options): ")
    println(xSpec)
    throw Err(IllegalStateException("Cannot find state transition"))
}


/** This function looks at the current stack, the program, and all bindings to determine what a good stack would look like to enter execution of this step.
 * Obviously the top of the stacks must match that specified in the step.  However, this function needs to also determine:
 * A good place for constants
 * Whether bindings (that are already on the stack) should be consumed (if this is the last step or that data exists earlier in the stack), or duplicated
 * If duplicated, the desired VM state should reflect the duplication: e.g. [0,1] -> [0,1,0]  NOT [0,1] -> [0]
 * */
fun NSL.proposeEntryState(i : Step, curVm: VmState): Triple<VmState,List<Step>,List<Step>>
{
    // Since there can be instantiable items in the desired stack entry stack, we need to filter these out
    // because we can simply instantiate them.
    // There are 2 types:  Trailing instantiables' code can simply be executed in order.
    // But instantiables that need to go into the middle of existing stack items are harder.
    // We return code to instantiate them in advance and then add them in the correct spot to the vmstate.
    // The stack code then treats these as bindings that need to be permuted into the proper order.

    // Since nothing can be directly instantiated on the altstack, we can focus on the main stack

    val desiredStack = i.entry.stack
    val uninstDstack = mutableListOf<NBinding>()

    uninstDstack.addAll(desiredStack)

    // remove all the short trailing instantiables, and stick them in a list of steps that can simply be executed
    val postSteps = mutableListOf<Step>()
    while(uninstDstack.isNotEmpty())
    {
        val top = uninstDstack.last()
        val inst = top.instantiable
        if (inst == null) break
        val tmp = inst()
        if (tmp.size > cc.alwaysReinstantiateLimit) break
        uninstDstack.removeLast()
        postSteps.add(0,tmp)
    }

    // look at the bindings and determine whether items should be consumed or copied
    val ok2ConsumeBindings = mutableSetOf<NBinding>()
    if (true)
    {
        val possibleConsumedBindings = desiredStack // uninstDstack // ignore all the constants i.entry.stack.toSet() - i.exit.stack.toSet()
        for (b in possibleConsumedBindings)
        {
            // If this step "produces" it, it's ok to "consume" it (it's not really being consumed)
            if (i.exit.stack.contains(b)) ok2ConsumeBindings.add(b)
            else
            {
                val maxstep = try
                {
                    bindingUses[b]!!.filter { it.deadcode != true }.maxBy { it.ordinal } // !! every binding MUST have a bindingUses entry or the calc of that is broken
                } catch (e: NoSuchElementException)  // binding is only used in dead code
                {
                    i
                }

                if (maxstep.ordinal <= i.ordinal)  // this step or a prior one is the last use of this binding
                {
                    ok2ConsumeBindings.add(b)
                }
                // Its ok there's more than one of them in the VmState so we can consume this one.  BUT TODO try not consuming it to see which is more efficient
                else if (curVm.count(b) > 1) ok2ConsumeBindings.add(b)
                else // let's see if this is instantiable, and if so, if it's big
                {
                    val inst = b.instantiable
                    if (inst != null)
                    {
                        val instantiate = inst()
                        if (instantiate.size < cc.alwaysReinstantiateLimit)
                        {
                            ok2ConsumeBindings.add(b)
                        }
                    }
                }
            }
        }
    }
    val needsCopy = mutableSetOf<NBinding>()
    for(b in uninstDstack) // not i.entry.stack because nothing instantiable needs to be copied BUT TODO it might be more efficient to do so
    {
        if (!ok2ConsumeBindings.contains(b)) needsCopy.add(b)
    }

    //   First, find the earliest used binding
    var spos = 0
    val usedSet = i.entry.stack.toSet()  // Note we can cheat here because no normal operation uses the altstack
    while (spos < curVm.stack.size)
    {
        if (usedSet.contains(curVm.stack[spos])) break
        spos++
    }

    //   Now find any subsequent unused bindings
    val unusedButNeededLater = mutableSetOf<NBinding>()
    while (spos < curVm.stack.size)
    {
        with(curVm.stack[spos])
        {
            if (!usedSet.contains(this))
            {
                if (curVm.count(this) == 1)  // there's no other ref to this binding
                {
                    val steps = bindingUses[this]
                    if (steps == null)
                    {
                        // If this binding is meant to be unused, then fine ignore this warning.
                        if (!this.unused)
                        {
                            println("Warning binding unused ${this}")
                        }
                    }
                    else
                    {
                        val maxstep = steps!!.maxBy { it.ordinal }  // !! every binding MUST have a bindingUses entry or the calc of that is broken
                        if (maxstep.ordinal >= i.ordinal)  // something beyond this instruction uses this binding
                            unusedButNeededLater.add(this)
                    }
                }
            }
        }
        spos++
    }

    // Find the earliest used altstack binding
    var apos = 0
    while (apos < curVm.alt.size)
    {
        if (usedSet.contains(curVm.alt[apos])) break
        apos++
    }

    // Now find any subsequent unused altstack bindings
    // We'll put these on the unusedButNeededLater list, which will have the effect of pulling them off of the altstack.
    // Since you can't pick from the middle of the altstack, this is the best we can do.
    while (apos < curVm.alt.size)
    {
        with(curVm.alt[apos])
        {
            if (!usedSet.contains(this))
            {
                if (curVm.count(this) == 1)  // there's no other ref to this binding
                {
                    val steps = bindingUses[this]
                    if (steps == null)
                    {
                        // If this binding is meant to be unused, then fine ignore this warning.
                        if (!this.unused)
                        {
                            println("Warning binding unused ${this}")
                        }
                    }
                    else
                    {
                        val maxstep = steps!!.maxBy { it.ordinal }  // !! every binding MUST have a bindingUses entry or the calc of that is broken
                        if (maxstep.ordinal >= i.ordinal)  // something beyond this instruction uses this binding
                            unusedButNeededLater.add(this)
                    }
                }
            }
        }
        apos++
    }


    // TODO how should the unusedButNeededLater bindings be sorted?

    // Desired VM state stack should be:
    // 1. everything we need to save
    //       unusedButNeededLater
    //       not in consumedBindings (needsCopy)
    // 2. beginning instantiables  (this is a problem because its in the middle of the stack xform)
    // 3. uninstDstack
    // 3. ending instantiables
    val desiredVmState = VmState(i.entry)  // make a copy of the entry state
    val entryStack:MutableList<NBinding> = mutableListOf()
    // make a copy of the entry state but with a reworked entry stack
    entryStack.addAll(unusedButNeededLater)
    entryStack.addAll(needsCopy)
    // End with what is needed to call this function
    entryStack.addAll(uninstDstack)

    desiredVmState.stack = entryStack
    desiredVmState.orderedStack = i.orderedEntry

    // Find what instantiables should be created first
    var dpos = 0
    val preSteps = mutableListOf<Step>()

    // If we need one & its instantiable & we don't currently have one on the stack, then add to our presteps
    while(dpos < uninstDstack.size)
    {
        val binding = uninstDstack[dpos]
        val inst = binding.instantiable
        if (inst != null && !curVm.stack.contains(binding))
            preSteps.add(inst())
        dpos++
    }

    // Now we have all the data on the stack.  But we may need to reorder it to match what is needed (if instantiable stuff needs to go in the middle)
    return Triple(desiredVmState, preSteps, postSteps)
}


fun NSL.instantiateIntoNexaVm()
{
    var curVm = VmState(startVm)

    val outpgm = mutableListOf<Step>()
    for (i in pgm)
    {
        // println(i)
        mycheck(i.deadcode != null)  // I should know by now
        if (i.deadcode == true) continue

        // Figure out what we want the VM to look like going into this call
        val (desiredVmState, preInst, postInst) = proposeEntryState(i, curVm)

        // Now step through the desired stack and the uninstantiable desired stack issuing the proper instructions
        preInst.forEach{
            outpgm.add(it)
            curVm.apply(it)
        }

        // Now figure out how to get the current stack of uninstantiable data into the desired structure
        val (xSpec, backConverter) = annotateExceptionsWith(i, { calcStackXform(curVm, desiredVmState) })
        val xCode = try
        {
            findXformCode(xSpec)
        }
        catch (e: IllegalStateException)
        {
            for (i in backConverter)
            {
                println(backConverter)
            }
            throw Err(e)
        }

        //   Ok now issue the transformation that puts the uninstantiable data into the right order and positions on stack
        val tmpstep = xCode.toStep(backConverter)
        outpgm.add(tmpstep)
        curVm.apply(tmpstep)

        //   Instantiate trailing instantiable data
        postInst.forEach{
            outpgm.add(it)
            curVm.apply(it)
        }

        outpgm.add(i)
        curVm.apply(i)
    }


    // Now clean up the stack
    // We want the exact final stack (from the perspective of this subroutine anyway -- this whole sub may not "know" that the stack is actually deeper)

    if (!endVm.failed)  // There is no need to unify stacks if the code will never leave this clause
    {
        endVm.allowAltPrefix = false
        endVm.allowStackPrefix = false
        val (xSpec, backConverter) = calcStackXform(curVm, endVm)
        val xCode = try
        {
            findXformCode(xSpec)
        } catch (e: IllegalStateException)
        {
            for (i in backConverter)
            {
                println(backConverter)
            }
            throw Err(e)
        }

        val tmpstep = xCode.toStep(backConverter)
        outpgm.add(tmpstep)
        curVm.apply(tmpstep)
        check(curVm contentEquals endVm)
    }

    endPgm = outpgm
}


/** fills out bindingCreated based on the initial stack conditions */
fun NSL.calcStartBindingCreated()
{
    for (s in startVm.stack)
    {
        if (bindingCreated.containsKey(s) && bindingCreated[s] != ENTRY_STEP)
            throw Err(IllegalStateException("ERROR: binding ${s.name} created in two places\nOriginally: ${bindingCreated[s]}"))
        bindingCreated[s] = ENTRY_STEP
    }
    for (s in startVm.alt)
    {
        if (bindingCreated.containsKey(s) && bindingCreated[s] != ENTRY_STEP)
            throw Err(IllegalStateException("ERROR: binding created in two places"))
        bindingCreated[s] = ENTRY_STEP
    }
}

/** Calculates when every binding is created */
fun NSL.calcBindingCreated()
{
    bindingCreated.clear()
    // mycheck(bindingCreated.isEmpty())  // make sure we didn't call this already

    calcStartBindingCreated()

    for (s in pgm)
    {
        for (e in s.exit.stack)
        {

            // our indication that a binding is used but its instance on the stack is not destroyed is to see it in both the entry and exit params
            // so if we see the same binding in the entry stack, then it is not being created in this statement
            if (!(e in s.entry.stack))
            {
                if (bindingCreated.containsKey(e))
                {
                    throw Err(IllegalStateException("ERROR: binding ${e} created in two places\n  first:\n${bindingCreated[e]}\n  then:\n$s"))
                }
                bindingCreated[e] = s
            }
            else
            {
                println("untouched param")
            }
        }

        /*  You can only move stuff from the main stack to the alt stack, so its impossible to create a new binding directly onto the altstack */
        /*
        for (e in s.exit.alt)
        {

            // our indication that a binding is used but its instance on the stack is not destroyed is to see it in both the entry and exit params
            // so if we see the same binding in the entry stack, then it is not being created in this statement
            if (!(e in s.exit.alt))
            {
                if (bindingCreated.containsKey(e))
                {
                    throw Err(IllegalStateException("ERROR: binding created in two places"))
                }
                bindingCreated[e] = s
            }
            else
            {
                println("untouched param")
            }
        }
         */

    }
}


// A separate function because if we are discovering the entry stack, then we need to add these later (after discovery)
fun NSL.addEntryUseToBindingUseDict()
{

}

fun NSL.calcBindingUseDict()
{
    bindingUses.clear()
    //mycheck(bindingUses.isEmpty())
    for (s in pgm)
    {
        // Note: Any foreign bindings that the sub uses will be set as entry parameters by this point,
        // so I don't have to look into the sub's binding use.
        for (e in s.entry.stack)
        {
            bindingUses.getOrPut(e, { mutableSetOf<Step>() }).add(s)
        }
        for (e in s.entry.alt)
        {
            bindingUses.getOrPut(e, { mutableSetOf<Step>() }).add(s)
        }
    }

    // Add the bindings that must leave this script into the use dictionary as part of the last (exit) step
    for (e in endVm.stack)
    {
        bindingUses.getOrPut(e, { mutableSetOf<Step>() }).add(EXIT_STEP)
    }
    for (e in endVm.alt)
    {
        bindingUses.getOrPut(e, { mutableSetOf<Step>() }).add(EXIT_STEP)
    }
}


fun NSL.calcForeignBindings()
{
    var foreignBindings: Set<NBinding> = bindingUses.keys - bindingCreated.keys

    // Remove all instantiable bindings because we can just create them on demand
    foreignBindings = foreignBindings.filter {
        // TODO optimization: if instantiation is expensive, ask for it as a parameter
        it.instantiable == null
    }.toSet()

    // Are there any bindings that are used but not created?
    if (foreignBindings.isNotEmpty())
    {
        if (unspecifiedEntryExit)  // We can change the entry parameters to include these bindings
        {
            // TODO optimization: try different orderings
            for (b in foreignBindings)
            {
                startVm.stack.add(b)
            }
            calcStartBindingCreated()
        }
        else
        {
            val t = StringJoiner("\n   ")
            for (b in foreignBindings) t.add(b.toString() + "(" + b.srcLoc + ")")
            println("\n")
            println(this)
            println("WARNING: bindings are used but not bound in this context:\n   ${t}")
            throw Err(IllegalStateException("ERROR: bindings are used but not bound into this context (created or declared):\n${t}\nTO FIX: Declare these bindings as a template, constraint or satisfier parameter of the script.  And if this is declared as a template parameter, it needs to be bound to a constant!"))
        }
    }
}


// Else both closes the prior clause and opens another one
val clauseOpener:Set<OP> = setOf(OP.IF,OP.ELSE)
val clauseCloser = setOf(OP.ELSE, OP.ENDIF)

fun NSL.breakIntoClauses()
{
    val newpgm = doBreakIntoClauses(pgm)
    pgm.clear()
    pgm.addAll(newpgm)
}


fun NSL.doBreakIntoClauses(pgm:MutableList<Step>, depth:Int = 0): MutableList<Step>
{
    val newpgm = mutableListOf<Step>()
    while(pgm.isNotEmpty())
    {
        val s = pgm[0]

        if (s.opcode.size == 0)  // an empty instruction is a higher level operation such as type transformation.
        {
            pgm.removeFirst()  // consume it
            newpgm.add(s)
        }
        // Why opcode[0]?  If an opener does not begin this opcode phrase, we assume that it is self-contained
        else if ((s.opcode[0] == OP.IF)&&(s.sub.size == 0))  // an if statement and we haven't yet parsed out its subclauses
        {
            s.unifySubEntryStack = true
            pgm.removeFirst() // consume it
            val thenSubroutine = NSL.clause(doBreakIntoClauses(pgm, depth+1))
            s.sub.add(thenSubroutine)
            val nexts = pgm[0]
            if (nexts.opcode[0] == OP.ELSE)
            {
                pgm.removeFirst() // consume the else
                val elseSubroutine = NSL.clause(doBreakIntoClauses(pgm, depth+1))
                s.sub.add(elseSubroutine)
            }
            if (pgm.isEmpty() || pgm[0].opcode[0] != OP.ENDIF) throw Err(NslException("missing ENDIF", s))
            pgm.removeFirst() // consume the endif
            newpgm.add(s)
        }
        else if (s.opcode[0] == OP.ELSE)
        {
            if (depth == 0) throw Err(NslException("extra ELSE", s))
            // We want to leave the op on the program so the caller can see what it is because "else" is both a closer and an opener
            return newpgm
        }
        else if (s.opcode[0] == OP.ENDIF)
        {
            if (depth == 0) throw Err(NslException("extra ENDIF", s))
            // We want to leave the op on the program so the caller can see what it is because "else" is both a closer and an opener
            return newpgm
        }
        else
        {
            pgm.removeFirst()  // consume it
            newpgm.add(s)
        }
    }
    return newpgm
}

fun NSL.doBreakIntoClausesOld(pgm:MutableList<Step>): MutableList<Step>
{
    val newpgm = mutableListOf<Step>()
    while(pgm.isNotEmpty())
    {
        val s = pgm[0]
        // Why opcode[0]?  If an opener does not begin this opcode phrase, we assume that it is self-contained
        val closer = clauseCloser.contains(s.opcode[0]) && (s.closed == false)
        val opener = clauseOpener.contains(s.opcode[0])
        if (closer)
        {
            // If it is also an opener, leave it on the program for the outer processor to handle the opening part
            if (!opener) pgm.removeFirst()
            if (s.opcode[0].v contentEquals OP.ENDIF.v) newpgm.add(s)  // Add the closer into this NSL so the block is ended
            s.closed = true
            return newpgm
        }
        if (opener)
        {
            pgm.removeFirst()  // consume it
            val subroutine = NSL(doBreakIntoClauses(pgm))
            subroutine.unspecifiedEntryExit = true
            s.sub.add(subroutine)
            newpgm.add(s)
        }
        else
        {
            pgm.removeFirst()  // consume it
            newpgm.add(s)
        }
    }
    return newpgm
}


fun NSL.precompile()
{
    // Create some handy look up tables
    calcBindingCreated()
    calcBindingUseDict()
    calcForeignBindings()
}


// Discover the bindings used and created in subclauses, and specify them in the outer statement
fun NSL.compile(outerStep: Step? = null)
{
    cc.with(this) // point the context to this script
    {
        val script = this

        // promote in all children
        for (step in script.pgm)
        {
            // This analyzes the dataflow, producing look up tables.
            // In this context, we need to do this now to determine the foreign bindings, to produce the sub's entry/exit stack
            for (sub in step.sub) sub.precompile()

            if (step.unifySubEntryStack)
            {
                // use sets to eliminate repeats
                var estack = mutableSetOf<NBinding>()
                var ealt = mutableSetOf<NBinding>()

                for (sub in step.sub)
                {
                    estack.addAll(sub.startVm.stack)
                    ealt.addAll(sub.startVm.alt)
                }

                // We need to enter this instruction with the stack needed for this instruction on the top (end)
                // and the stack needed for any sub below (before) that.
                val newEntry = VmState()
                // Add in the combined stack needed for all subs
                newEntry.stack.addAll(estack)
                newEntry.alt.addAll(ealt)

                // Update the subs with this combined stack, so they know what's coming in
                val subState = VmState(newEntry)
                for (sub in step.sub) sub.startVm = subState

                // Add in the stack needed for this step
                newEntry.stack.addAll(step.entry.stack)
                newEntry.alt.addAll(step.entry.alt)

                step.entry = newEntry

                // TODO exit stuff if both the statement and sub produce an exit (does not happen with if)
            }
            else
            {
                check(step.sub.size == 0)  // Right now no nonunified subs.  And there's a question as to whether the initial op is repeated or not...
            }

            // Once the sub's entry/exit stack is defined, we can compile it
            for (sub in step.sub) sub.compile(step)
        }

        LogIt.info("clause\n${dump()}")
        precompile()
        val unweightedPgm = script.pgm.joinToString("\n")
        LogIt.info("\n\nUnweighted:\n${unweightedPgm}")

        // ok now analyze myself
        script.markSideEffect()
        script.markDeadCode()

        val annotatedPgm = script.pgm.joinToString("\n")
        LogIt.info("\n\nAnnotated:\n${annotatedPgm}")

        script.assignDataflowWeights()
        val weightedPgm = script.pgm.joinToString("\n")
        LogIt.info("\nWeighted:\n${weightedPgm}")
        script.sortByWeight()
        val weightedSortedPgm = script.dump()
        LogIt.info("\nSorted Weighted:\n${weightedSortedPgm}")
        script.instantiateIntoNexaVm()
        LogIt.info("\nCompiled:\n${script.print(true)}")
        refactored = script.refactor()
        LogIt.info("\nRefactored:\n${script.print(true)}")
    }
}

/** Remove all temporary state created during compilation in global objects like bindings */
fun NSL.compilationCleanup()
{
    for (b in bindingUses)
    {
        b.key.weight = Int.MAX_VALUE
    }
}

val sideEffecting = setOf<OP>(OP.VERIFY, OP.CHECKDATASIGVERIFY, OP.CHECKLOCKTIMEVERIFY, OP.CHECKSEQUENCEVERIFY, OP.CHECKMULTISIGVERIFY, OP.CHECKSIGVERIFY,
    OP.EQUALVERIFY, OP.NUMEQUALVERIFY)


fun minInt(a: Int, b: Int) = if (a<b) a else b


/** determines whether this script has any side effects (any state exported out-of-VM)
 */
fun NSL.markSideEffect(): Boolean
{
    // This assumes that all steps in the program are reachable.  We should eliminate unreachable code before running this
    for (step in pgm)
    {
        for (op in step.opcode)
        {
            // Any step that contains a side effect is not dead code -- it produces an out-of-VM result that cannot be lost
            if (sideEffecting.contains(op))
            {
                hasSideEffects = true
                return true
            }
            // Check if subs have side-effects
            for (sub in step.sub)
            {
                if (sub.hasSideEffects == true)
                {
                    hasSideEffects = true
                    return true
                }
            }
        }
    }
    hasSideEffects = false
    return false
}

fun NSL.markDeadCode()
{
    val bindingIsLive = mutableMapOf<NBinding, Boolean>()
    // Any bindings used in the output are live code
    var bindings2Process = (endVm.stack.toSet() + endVm.alt).toMutableSet()

    for (e in bindings2Process)
    {
        bindingIsLive[e] = true
    }

    var steps2Process = mutableSetOf<Step>()
    // To begin look for any steps that produce output (any VERIFY operation "produces output" -- whether the tx passes that check or not)
    for (step in pgm)
    {
        step.deadcode = true // assume its dead
        for (op in step.opcode)
        {
            // Any step that contains a side effect is not dead code -- it produces an out-of-VM result that cannot be lost
            if (sideEffecting.contains(op) || hasSideEffects)
            {
                step.deadcode = false
                break // For each step it only matters if one of the substeps is side-effecting
            }
            // Check if subs have side-effects
            if (step.deadcode == false) for (sub in step.sub)
            {
                if (sub.hasSideEffects == true)
                {
                    step.deadcode = false
                    break
                }
            }
        }
        if (step.deadcode == false) steps2Process.add(step)
    }

    while (bindings2Process.isNotEmpty() || steps2Process.isNotEmpty())
    {
        val nextBindings = mutableSetOf<NBinding>()
        var nextSteps = mutableSetOf<Step>()

        for (e in bindings2Process) // go through all the current bindings (e for edge of the binding, stmt DAG)
        {
            // We would not be processing this binding if it isn't live
            mycheck(bindingIsLive[e] == true)

            // If a step produces ANY bindings that are subsequently used, then that step is live
            val creation = bindingCreated[e]
            if (creation != null)  // instantiables are not necessarily created so this could be null
            {
                if (creation.deadcode == true)  // We haven't analyzed this step yet
                {
                    creation.deadcode = false
                    nextSteps.add(creation)
                }
            }
        }

        // For all steps that are now live, all the input bindings need to be marked as live
        for (s in steps2Process)
        {
            var inBindings = s.entry.stack + s.entry.alt
            // And since we changed this step, we now need to analyze all bindings that it used to see if we can weight them.
            for (b in inBindings)
            {
                bindingIsLive[b] = true
                nextBindings.add(b)
            }
        }
        bindings2Process = nextBindings  // ok process the next group of bindings
        steps2Process = nextSteps
    }
}


const val UNWEIGHTED = Int.MAX_VALUE
fun NSL.assignDataflowWeights()
{
    var lastStep = 999 // UNWEIGHTED - 1

    // In case this binding was used within another script, clear its weight now
    bindingUses.keys.forEach { it.weight = UNWEIGHTED}

    // Assign heaviest weight to all output data (the two initial stacks and the implicit binding of all transaction data)
    // In subroutines there might be output bindings, but in full scripts there is not.
    var bindings2Process = (endVm.stack.toSet() + endVm.alt).toMutableSet()

    for (e in bindings2Process)
    {
        e.weight = lastStep
    }

    var steps2Process = findOutputSteps(pgm)
    steps2Process.forEach { it.weight = lastStep }

    // Ok time to go through everything on the process list trying to weight its parents
    while (bindings2Process.isNotEmpty() || steps2Process.isNotEmpty())
    {
        val nextBindings = mutableSetOf<NBinding>()
        var nextSteps = mutableSetOf<Step>()

        for (e in bindings2Process) // go through all the current bindings (e for edge of the binding, stmt DAG)
        {
            var allUsesWeighted = true
            var minWeight:Int = Int.MAX_VALUE

            // first time thru bindings might already be weighted because they are exit bindings
            if (e.weight == UNWEIGHTED)
            {
                // find all steps that use that binding
                for (step in bindingUses[e] ?: setOf())
                {
                    if (step.deadcode == false && step.weight == UNWEIGHTED)
                    {
                        allUsesWeighted = false
                        break
                    }
                    minWeight = minInt(minWeight, step.weight)
                }

                if (allUsesWeighted) e.weight = minWeight - 1
            }

            if (allUsesWeighted)
            {
                    val bc = bindingCreated[e]
                    if (bc == null)
                    {
                        if (e.instantiable == null)  // If its instantiable, its ok to be not previously created
                        {
                            println("binding never created")
                            throw Err(NslException("binding ${e.name} never created!?", bindingUses[e]))
                        }
                    }
                    else
                    {
                        nextSteps.add(bc)
                    }
            }
        }

        // For all steps that create a change binding
        for (s in steps2Process)
        {
            // Look through all the bindings this step created and see if they are all weighted
            // (if they are all weighted, it means all the uses of the created binding has been found)
            // Bindings that are used but not destroyed are not considered created, because other instructions that use the binding
            // can be issued either before or after this one.
            var createdBindings = s.exit.stack.toSet() + s.exit.alt
            createdBindings = createdBindings - s.entry.stack - s.entry.alt
            var allCreatedBindingsWeighted = true

            var minWeight = Int.MAX_VALUE
            if (s.weight == UNWEIGHTED)  // Find the weight of this step (if we can)
            {
                var minWeight = Int.MAX_VALUE
                for (b in createdBindings)
                {
                    if (bindingUses[b] != null)  // some statement is using this created binding
                    {
                        if (b.weight == UNWEIGHTED)
                        {
                            allCreatedBindingsWeighted = false; break
                        }
                    }
                    minWeight = minInt(minWeight, b.weight)
                }
                if (allCreatedBindingsWeighted) s.weight = minWeight-1
            }

            // If all created bindings weighted, then we can assign this step's weight to be one less than the minimum of that.
            if (allCreatedBindingsWeighted)
            {
                var inBindings = s.entry.stack + s.entry.alt
                // And since we changed this step, we now need to analyze all bindings that it used to see if we can weight them.
                for (b in inBindings)
                    nextBindings.add(b)
            }
        }
        bindings2Process = nextBindings  // ok process the next group of bindings
        steps2Process = nextSteps
    }

}

fun NSL.findOutputSteps(pgm: List<Step>, includeDeadCode: Boolean = true):MutableSet<Step>
{
    val ret = mutableSetOf<Step>()
    // Look for any steps that produce output (any VERIFY operation "produces output" -- whether the tx passes that check or not)
    for (step in pgm)
    {
        mycheck(step.deadcode != null)  // We need to have determined step deadness before running this function

        for (op in step.opcode)
        {
            // We should start the backwards analysis with this step because it is a result
            if (sideEffecting.contains(op))
            {
                // If a binding was created, this side effect is not the end so ignore it
                val stepCreated = (step.exit.stack.toSet() + step.exit.alt) - step.entry.stack - step.entry.alt
                if (stepCreated.isEmpty())
                {
                    ret.add(step)
                }
                break // We only need to add the step once
            }
        }
        // For the purposes of weighting, we can imagine that every piece of dead code is executed at the end of the script
        // (even though they won't actually be executed)
        if (step.deadcode == true)
        {
            if (includeDeadCode) ret.add(step)
        }
    }
    return ret
}

fun NSL.assignProgramOrder()
{
    var lastStep = 999 // UNWEIGHTED - 1

    // In case this binding was used within another script, clear its weight now
    bindingUses.keys.forEach { it.weight = UNWEIGHTED}

    // Assign heaviest weight to all output data
    // In subroutines there might be output bindings, but in full scripts there is not.
    var bindings2Process = (endVm.stack.toSet() + endVm.alt).toMutableSet()

    for (e in bindings2Process)
    {
        e.weight = lastStep
    }

    var steps2Process = findOutputSteps(pgm)


    // Ok time to go through everything on the process list trying to weight its parents
    while (bindings2Process.isNotEmpty() || steps2Process.isNotEmpty())
    {
        val nextBindings = mutableSetOf<NBinding>()
        var nextSteps = mutableSetOf<Step>()


        for (e in bindings2Process) // go through all the current bindings (e for edge of the binding, stmt DAG)
        {
            var allUsesWeighted = true
            var minWeight:Int = Int.MAX_VALUE

            // first time thru bindings might already be weighted because they are exit bindings
            if (e.weight == UNWEIGHTED)
            {
                // find all steps that use that binding
                for (step in bindingUses[e] ?: setOf())
                {
                    if (step.deadcode == false && step.weight == UNWEIGHTED)
                    {
                        allUsesWeighted = false
                        break
                    }
                    minWeight = minInt(minWeight, step.weight)
                }

                if (allUsesWeighted) e.weight = minWeight - 1
            }

            if (allUsesWeighted)
            {
                val bc = bindingCreated[e]
                if (bc == null)
                {
                    if (e.instantiable == null)  // If its instantiable, its ok to be not previously created
                    {
                        println("binding never created")
                        throw Err(NslException("binding ${e.name} never created!?", bindingUses[e]))
                    }
                }
                else
                {
                    nextSteps.add(bc)
                }
            }
        }

        // For all steps that create a change binding
        for (s in steps2Process)
        {
            // Look through all the bindings this step created and see if they are all weighted
            // (if they are all weighted, it means all the uses of the created binding has been found)
            // Bindings that are used but not destroyed are not considered created, because other instructions that use the binding
            // can be issued either before or after this one.
            var createdBindings = s.exit.stack.toSet() + s.exit.alt
            createdBindings = createdBindings - s.entry.stack - s.entry.alt
            var allCreatedBindingsWeighted = true

            var minWeight = Int.MAX_VALUE
            if (s.weight == UNWEIGHTED)  // Find the weight of this step (if we can)
            {
                var minWeight = Int.MAX_VALUE
                for (b in createdBindings)
                {
                    if (bindingUses[b] != null)  // some statement is using this created binding
                    {
                        if (b.weight == UNWEIGHTED)
                        {
                            allCreatedBindingsWeighted = false; break
                        }
                    }
                    minWeight = minInt(minWeight, b.weight)
                }
                if (allCreatedBindingsWeighted) s.weight = minWeight-1
            }

            // If all created bindings weighted, then we can assign this step's weight to be one less than the minimum of that.
            if (allCreatedBindingsWeighted)
            {
                var inBindings = s.entry.stack + s.entry.alt
                // And since we changed this step, we now need to analyze all bindings that it used to see if we can weight them.
                for (b in inBindings)
                    nextBindings.add(b)
            }
        }
        bindings2Process = nextBindings  // ok process the next group of bindings
        steps2Process = nextSteps
    }

}

fun NSL.assignDataflowWeightsForward()
{
    // Create some handy look up tables
    calcBindingCreated()
    calcBindingUseDict()

    var usedButNotCreated = (bindingUses.keys - bindingCreated.keys)
    // remove all the bindings that can be created on stack on demand
    usedButNotCreated = usedButNotCreated.filter { it.instantiable == null }.toSet()

    if (usedButNotCreated.isNotEmpty())
    {
        if (unspecifiedEntryExit)
        {
            startVm.stack.addAll(usedButNotCreated)
            calcStartBindingCreated()
        }
        else
        {
            // TODO create genesis statements for all constant bindings
            val t = StringJoiner("\n   ")
            for (b in usedButNotCreated) t.add(b.toString() + "(" + b.srcLoc + ")")
            Warn("Bindings are used but never created:\n   ${t}")
        }
    }


    // abs is all the currently known bindings
    var abs = startVm.stack.toSet() + startVm.alt + ThisTransaction

    // Assign weight 0 to all input data (the two initial stacks and the implicit binding of all transaction data)
    for (e in abs)
    {
        e.weight = 0
    }

    while (abs.isNotEmpty())
    {
        val next = mutableSetOf<NBinding>()
        for (e in abs) // go through all the current bindings
        {
            // find all steps that use that binding
            for (step in bindingUses[e] ?: setOf())
            {
                if (step.weight == Int.MAX_VALUE)  // we haven't resolved this step yet
                {
                    // find the maximum weight for all the inputs of this step
                    val tmp = max(
                        step.entry.stack.fold(-1) { a, b -> max(a, b.weight) },
                        step.entry.alt.fold(-1) { a, b -> max(a, b.weight) })

                    // If an input binding has not been assigned a weight, it will be set to MAX_VALUE so
                    // a step that cannot be resolved yet will be MAX_VALUE
                    if (tmp < Int.MAX_VALUE)
                    {
                        // This step's input bindings are fully resolved, so we can now apply a weight to this step
                        step.weight = tmp
                        for (b in step.createdBindings)
                        {
                            mycheck(b.weight == Int.MAX_VALUE)  // oh, no we created it twice??!!?
                            b.weight = tmp + 1
                            next.add(b)  // Since we assigned a weight to this binding, we can now process it
                        }
                    }
                }
            }
        }
        abs = next  // ok process the next group of bindings
    }
}

fun NSL.sortByWeight()
{
    pgm.sortWith(object : Comparator<Step> {
        override fun compare(p0: Step?, p1: Step?): Int
        {
            // There really shouldn't be any nulls provided to this function
            if (p0 == null && p1 == null) return 0
            if (p0 == null) return -1
            if (p1 == null) return 1
            // order the lowest weight first
            if (p0.weight < p1.weight) return -1
            if (p0.weight > p1.weight) return 1
            // If the weights are equal, prefer the way the user wrote the program
            if (p0.programOrder < p1.programOrder) return -1
            if (p0.programOrder > p1.programOrder) return 1

            throw Err(IllegalStateException("Step count cannot ever be equal"))
            // these are helper instructions generated within the context of another instruction
            // just find any differentiation
            //if (p0.opcode.size < p1.opcode.size) return -1
            //if (p0.opcode.size > p1.opcode.size) return 1
            return 0
        }

    })

    // Assign a unique count to every program instruction
    var ord = 0
    for (s in pgm)
    {
        s.ordinal = ord
        ord++
    }
}
