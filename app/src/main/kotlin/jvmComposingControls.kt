@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.composing

import androidx.compose.foundation.background
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.foundation.*
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.draganddrop.DragAndDropEvent
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import kotlinx.coroutines.flow.MutableStateFlow
import org.wallywallet.wew.highlightBackgroundColor
import java.awt.datatransfer.DataFlavor
import java.awt.dnd.DropTargetDropEvent
import java.io.File
import javax.swing.JFileChooser
import javax.swing.filechooser.FileSystemView

import kotlin.properties.Delegates


val fileSys = FileSystemView.getFileSystemView()
val fileChooser = JFileChooser(fileSys)

fun FileChooserDialog(title: String, onResult: (result: File) -> Unit)
{
    //val fileChooser = JFileChooser(fileSys)
    //fileChooser.currentDirectory = File(System.getProperty("user.dir"))
    fileChooser.dialogTitle = title
    fileChooser.fileSelectionMode = JFileChooser.FILES_AND_DIRECTORIES
    fileChooser.isAcceptAllFileFilterUsed = true
    fileChooser.selectedFile = null
    //fileChooser.currentDirectory = null
    if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        val file = fileChooser.selectedFile
        println("choosen file or folder is: $file")
        onResult(file)
    } else {
//        onResult(java.io.File(""))
        println("No Selection ")
    }
}

class CCFileChooser(var title: String):Composing, TriggeredRefresh()
{
    val fileSys = FileSystemView.getFileSystemView()
    val fileChooser = JFileChooser(fileSys)

    val value: MutableStateFlow<File?> = MutableStateFlow(null)
    val button = CCButton(text(title)) {
    fileChooser.dialogTitle = title
    fileChooser.fileSelectionMode = JFileChooser.FILES_AND_DIRECTORIES
    fileChooser.isAcceptAllFileFilterUsed = true
    fileChooser.selectedFile = value.value
    if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        value.value = fileChooser.selectedFile
        }
    }
    @Composable()
    override fun compose()
    {
        Row(verticalAlignment = Alignment.CenterVertically) {
            button.compose()
            val f = value.collectAsState()
            Text(f.value?.absolutePath ?: "not chosen")
        }
    }
}

class CCDirChooser(var title: String, startingDir: String?=null, var onChoice: CCDirChooser.(File)->Boolean = {true}):Composing, TriggeredRefresh()
{
    val fileSys = FileSystemView.getFileSystemView()
    val fileChooser = JFileChooser(fileSys)

    val value: MutableStateFlow<File?> = MutableStateFlow(startingDir?.let { File(startingDir)})
    val button = CCButton(text(title)) {
    fileChooser.dialogTitle = title
    fileChooser.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
    fileChooser.isAcceptAllFileFilterUsed = false
    fileChooser.selectedFile = value.value
    if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        value.value = fileChooser.selectedFile
        onChoice(this, fileChooser.selectedFile)
        }
    }
    @Composable()
    override fun compose()
    {
        Row(verticalAlignment = Alignment.CenterVertically) {
            button.compose()
            val f = value.collectAsState()
            Text(f.value?.absolutePath ?: "not chosen")
        }
    }
}



class CCDropDir(var inside: Composing, width: Dp?=null, height:Dp?=null):Composing, TriggeredRefresh()
{
    var color : Color by Delegates.observable(defaultBoxColor, this::changed)
    var colStack = mutableListOf<Color>()
    var background: Color = defaultBoxBackground
    var shape : Shape = defaultBoxShape
    var borderWidth : Dp = defaultBoxBorderWidth
    var padding = defaultBoxPadding
    val size = Sizeable(this,width,height)

    @OptIn(ExperimentalFoundationApi::class)
    @Composable()
    override fun compose()
    {
        val callback = remember {
            object : DragAndDropTarget
            {
                override fun onDrop(event: DragAndDropEvent): Boolean
                {
                    print(event)
                    return true
                }
            }
        }
        var mod = Modifier.background(background, shape).border(BorderStroke(borderWidth, color), shape).then(padding).dragAndDropTarget(shouldStartDragAndDrop = { event ->
            print(event)
            true
        }, target = callback)
        size.width?.let { mod = mod.width(it) }
        size.height?.let { mod = mod.height(it) }

        onRefresh {
            Box(mod) {
                inside.compose()
            }
        }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        if (push)
        {
            colStack.add(color)
            color = highlightBackgroundColor
        }
        else
        {
            color = colStack.removeLastOrNull() ?: defaultBoxColor
        }
    }
}


class CCJavaDropDir(var inside: Composing, width: Dp?=null, height:Dp?=null):Composing, TriggeredRefresh()
{
    var color : Color by Delegates.observable(defaultBoxColor, this::changed)
    var colStack = mutableListOf<Color>()
    var background: Color = defaultBoxBackground
    var shape : Shape = defaultBoxShape
    var borderWidth : Dp = defaultBoxBorderWidth
    var padding = defaultBoxPadding
    val size = Sizeable(this,width,height)

    @Composable()
    override fun compose()
    {
        var mod = Modifier.background(background, shape).border(BorderStroke(borderWidth, color), shape).then(padding)
        size.width?.let { mod = mod.width(it) }
        size.height?.let { mod = mod.height(it) }

        var droppedDirectory by remember { mutableStateOf<File?>(null) }
        // Add a DropTarget to accept drag and drop of files and directories.
        DisposableEffect(Unit) {
            val frame = java.awt.Frame.getFrames().firstOrNull() ?: return@DisposableEffect onDispose {}

            frame.dropTarget = object : java.awt.dnd.DropTarget() {
                override fun drop(evt: DropTargetDropEvent) {
                    evt.acceptDrop(java.awt.dnd.DnDConstants.ACTION_COPY)
                    val transferable = evt.transferable
                    val dataFlavors = transferable.transferDataFlavors

                    // Check if the data contains a list of files.
                    dataFlavors.forEach { flavor ->
                        if (flavor == DataFlavor.javaFileListFlavor) {
                            val files = transferable.getTransferData(flavor) as List<*>
                            val directory = files.firstOrNull() as? File
                            // Ensure that the dropped item is a directory.
                            if (directory?.isDirectory == true) {
                                droppedDirectory = directory
                            }
                        }
                    }
                    evt.dropComplete(true)
                }
            }

            onDispose {
                frame.dropTarget = null
            }
        }

        onRefresh {
            Box(mod) {
                inside.compose()
            }
        }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        if (push)
        {
            colStack.add(color)
            color = highlightBackgroundColor
        }
        else
        {
            color = colStack.removeLastOrNull() ?: defaultBoxColor
        }
    }
}
