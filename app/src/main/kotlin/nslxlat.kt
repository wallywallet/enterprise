package Nexa.npl

import org.nexa.libnexakotlin.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ByteArraySerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
//import kotlin.native.internal.*
import java.io.*
import kotlin.concurrent.thread
import kotlin.math.min

private val LogIt = GetLog("wew.nslxlat")

fun intToByteArray (data: Int) : ByteArray =
    ByteArray (4) { i -> (data.toLong() shr (i*8)).toByte() }

private fun byteArrayToInt(buffer: ByteArray): Int
{
    return (buffer[3].toInt() shl 24) or (buffer[2].toInt() and 0xff shl 16) or (buffer[1].toInt() and 0xff shl 8) or (buffer[0].toInt() and 0xff)
}

@Serializable
/** Describe stack state.
 * use numbers starting from 0 to indicate unique stack entities.  Use the same number to indicate the same value
 * the end (highest index) of the ByteArray is the top of the stack
 */
data class StateDescriptor(
    var stack: ByteArray,
    var altstack: ByteArray)
{
    companion object
    {
        /** Load a StateDescriptor from the passed (binary) stream */
        fun load(stream: InputStream): StateDescriptor
        {
            var len = byteArrayToInt(stream.readNBytes(4))
            val stack = ByteArray(len)
            stream.read(stack)
            len = byteArrayToInt(stream.readNBytes(4))
            val altstack = ByteArray(len)
            stream.read(altstack)
            return StateDescriptor(stack, altstack)
        }
    }
    /** Allow just the main stack to be specified because most operations occur there */
    //constructor(stack:ByteArray): this(stack, byteArrayOf())

    /** Allow just the main stack to be specified without wrapping in a byte array */
    constructor(vararg stack: Byte): this(stack, byteArrayOf())

    /** Shortcut for nothing */
    constructor(): this(byteArrayOf(),byteArrayOf())

    /** Copy constructor */
    constructor(sd: StateDescriptor): this(sd.stack.clone(), sd.altstack.clone())

    /** Save a StateDescriptor to the passed (binary) stream */
    fun save(stream: OutputStream)
    {
        stream.write(intToByteArray(stack.size))
        stream.write(stack)
        stream.write(intToByteArray(altstack.size))
        stream.write(altstack)
    }


    /** Change this state descriptor as specified by the passed transition.
     * @return false if the state transition cannot be applied (this object is unmodified)
     */
    fun apply(st: StateTransition): Boolean
    {
        if (st.begin.stack.size > stack.size ) return false
        if (st.begin.altstack.size > altstack.size ) return false

        /*
        // The state transition has numbered its stack.  Make a map of those numbers to the numbers in the state descriptor
        // If the transition does not specify anything, map to itself because its unchanged
        val stackXlat:ByteArray = ByteArray(stack.size, { stack[it] })

        for ((index, value) in st.begin.stack.withIndex())
        {
            // The transition applies to the END of the stack bytearray (the TOP of the stack)
            stackXlat[value.toInt()] = stack[stack.size - st.begin.stack.size + value.toInt()]
        }
        val altXlat:ByteArray = ByteArray(altstack.size, { altstack[it] })
        for ((index, value) in st.begin.altstack.withIndex())
        {
            // The transition applies to the END of the stack bytearray (the TOP of the stack)
            altXlat[value.toInt()] = altstack[altstack.size - st.begin.altstack.size + value.toInt()]
        }

         */

        // The state transition has numbered its stack.  Make a map of those numbers to the numbers in the state descriptor
        // If the transition does not specify anything, map to itself because its unchanged
        val xlat:ByteArray = ByteArray(stack.size + altstack.size, { if (it < stack.size) stack[it] else altstack[it - stack.size] })

        for ((index, value) in st.begin.stack.withIndex())
        {
            // The transition applies to the END of the stack bytearray (the TOP of the stack)
            xlat[value.toInt()] = stack[stack.size - st.begin.stack.size + index]
        }

        for ((index, value) in st.begin.altstack.withIndex())
        {
            // The transition applies to the END of the stack bytearray (the TOP of the stack)
            xlat[value.toInt()] = altstack[altstack.size - st.begin.altstack.size + index]
        }


        // Now cut off all the consumed elements in the state transition, and then add what was produced
        val unconsumed = stack.size - st.begin.stack.size
        val newstack = ByteArray(unconsumed + st.end.stack.size,
            {

                if (it < unconsumed) stack[it] // duplicate anything that's not consumed
                else // Otherwise translate all produce items
                    xlat[st.end.stack[it - unconsumed].toInt()]
            })
        val unconsumedAlt = altstack.size - st.begin.altstack.size
        val newalt = ByteArray(unconsumedAlt + st.end.altstack.size,
            {
                if (it < unconsumedAlt) altstack[it]
                else
                    xlat[st.end.altstack[it - unconsumedAlt].toInt()]
            })

        stack = newstack
        altstack = newalt
        return true
    }

    override fun toString(): String
    {
        return "(${stack.toCoords()}, ${altstack.toCoords()})"
    }

    override fun equals(other: Any?): Boolean
    {
        if (other is StateDescriptor)
        {
            val ret = (stack contentEquals other.stack) && (altstack contentEquals other.altstack)
            return ret
        }
        return false
    }

    override fun hashCode(): Int
    {
        return ((stack.size shl 16) xor (altstack.size shl 16)) xor ((stack.contentHashCode() shl 8) xor altstack.contentHashCode())
    }
}



object StateTransitionSerializer : KSerializer<StateTransition>
{
    override val descriptor: SerialDescriptor = ByteArraySerializer().descriptor

    override fun serialize(encoder: Encoder, value: StateTransition)
    {
        encoder.encodeString(value.toEncoding())
    }

    override fun deserialize(decoder: Decoder): StateTransition {
        val s = decoder.decodeString()
        return StateTransition.fromEncoding(s)
    }
}


class StateTransition(
    val begin: StateDescriptor,
    val end: StateDescriptor
)
{
    constructor(data: InputStream):this(StateDescriptor.load(data), StateDescriptor.load(data))
    {
    }

    companion object
    {
        fun fromEncoding(s:String): StateTransition
        {
            val be = s.split(">")
            val b = be[0].split(":").map { if (it.length > 0) it.fromHex() else bao() }
            val e = be[1].split(":").map { if (it.length > 0) it.fromHex() else bao() }
            return StateTransition(SD(b[0], b[1]), SD(e[0], e[1]))
        }

        fun load(stream: InputStream): StateTransition
        {
            return StateTransition(stream)
        }
    }

    override fun toString(): String
    {
        return "$begin\u21d2$end"
    }

    fun toEncoding():String
    {
        return(begin.stack.toHex() + ":" + begin.altstack.toHex() + ">" +
            end.stack.toHex() + ":" + end.altstack.toHex())
    }

    fun save(stream: OutputStream)
    {
        begin.save(stream)
        end.save(stream)
    }


    override fun equals(other: Any?): Boolean
    {
        if (other is StateTransition)
        {
            val ret = (begin == other.begin) && (end == other.end)
            /* Debug code to compare binary equality with string equality
            if (ret == false)
            {
                val s0 = this.toString()
                val s1 = other.toString()
                if (s0 == s1)
                {
                    println("strin equality!!")
                }
            }
             */
            return ret
        }
        /* Debug code to compare binary equality with string equality
        if (true)
        {
                val s0 = this.toString()
                val s1 = other.toString()
                if (s0 == s1)
                {
                    println("string equality!!")
                }
         }
         */
        return false
    }
    override fun hashCode(): Int
    {
        val ret = (begin.hashCode() shl 16) xor end.hashCode()
        return ret
    }
}

object ClauseSerializer : KSerializer<Clause>
{
    override val descriptor: SerialDescriptor = ByteArraySerializer().descriptor

    override fun serialize(encoder: Encoder, value: Clause)
    {
        encoder.encodeString(value.toEncoding())
    }

    override fun deserialize(decoder: Decoder): Clause {
        val s = decoder.decodeString()
        return Clause.fromEncoding(s)
    }
}

fun op2ByteArray(opcodes: List<OP>):ByteArray
{
    val ret = ByteArrayOutputStream()
    for (op in opcodes)
    {
        ret.write(op.v)
    }
    return ret.toByteArray()
}

@Serializable(with = ClauseSerializer::class)
class Clause(var opcodes: ByteArray = byteArrayOf())
{
    /** For efficiency the state transition (which is the key of the map) isn't held in the code clause until the clause is used
     */
    var xSpec: StateTransition? = null
    constructor(stream: InputStream):this()
    {
        load(stream)
    }
    companion object
    {
        fun fromEncoding(s: String):Clause  // only works for 1 byte opcodes
        {
            val ba = s.fromHex()
            return Clause(ba)
        }
    }

    fun toEncoding():String
    {
        /*
        val ret = StringBuilder()
        for (o in opcodes)
        {
            ret.append(o.v.toHex())
        }
        return ret.toString()
         */
        return opcodes.toHex()
    }

    constructor(_opcodes:List<OP>, _xSpec:StateTransition?):this(op2ByteArray(_opcodes)) { xSpec = _xSpec}

    /** Convert this code clause to a Step.
     * @param entry: the top of the stacks in the VM must be as described (and will be popped)
     * @param exit: these items will be pushed onto the stacks
     * If a stack item is used but not removed, it should be placed on both the entry and exit VmStates (as if it was deleted and recreated)
     */
    fun toStep(converter: Map<Int, NBinding>): Step
    {
        mycheck(xSpec != null)

        val bs = xSpec!!.begin.stack.map { converter[it.toInt()]!! }
        val ba = xSpec!!.begin.altstack.map { converter[it.toInt()]!! }

        val es = xSpec!!.end.stack.map { converter[it.toInt()]!! }
        val ea = xSpec!!.end.altstack.map { converter[it.toInt()]!! }

        val entry = VmState(bs, ba)
        val exit = VmState(es, ea)

        return Step(-1, entry, OP.decompile(opcodes), exit)
    }

    operator fun plus(other: Clause): Clause
    {
        return Clause(opcodes + other.opcodes)
    }

        /** Save this object to the passed (binary) stream */
    fun save(stream: OutputStream)
    {
        stream.write(intToByteArray(opcodes.size))
        stream.write(opcodes)
    }

    /** Load this object from the passed (binary) stream */
    fun load(stream: InputStream)
    {
       var len = byteArrayToInt(stream.readNBytes(4))
       opcodes = ByteArray(len)
       stream.read(opcodes)
    }
}

/** A table of VM state transitions and the code required to get from one to another
 * This table uses a succinct binding notation of just a single unique character to denote each binding.
 * So it is a little different from the rest of the code.
 * */

class StateTransitions
{
    var table = HashMap<StateTransition, Clause >()

    val size:Int
        get() = table.size

    /** Only adds it if its smaller than the existing one (or if no transition exists) */
    fun add(begin: StateDescriptor, end: StateDescriptor, vararg op: OP): Boolean
    {
        val c = Clause(op2ByteArray(op.toList()))
        val st = StateTransition(begin, end)
        return add(st, c)
    }

    fun add(begin: StateDescriptor, end: StateDescriptor, code: Clause): Boolean
    {
        val st = StateTransition(begin, end)
        return add(st,code)
    }

    fun add(st: StateTransition, code: Clause): Boolean
    {
        var ret = false
        val r = table.getOrPut(st, { ret=true; code })
        // If the proposed set of opcodes are less than the current one, then update it
        if (code.opcodes.size < r.opcodes.size)
        {
            //r.opcodes = code.opcodes
            table[st] = code
            return true
        }
        return ret
    }

    fun add(st: StateTransition, code: MutableSet<Clause>): Boolean
    {
        // find the smallest of the set
        val c = code.sortedWith { a, b -> a.opcodes.size.compareTo(b.opcodes.size) }[0]
        return add(st, c)
    }

    fun add(vararg others:StateTransitions)
    {
        for (st in others)
        {
            for((k,v) in st.table) add(k,v)
        }
    }

    fun save()
    {
        FileOutputStream(STACK_SCRIPTS_FILE).buffered(5000000).use { stream ->
            stream.write(intToByteArray(table.size))
            for ((k,v) in table)
            {
                k.save(stream)
                v.save(stream)

            }
            // painfully inefficient to load back in: json.encodeToStream(StateTransitions.serializer(), stackX, stream)
        }
    }

    fun load():Boolean
    {
        
        val buf = try 
        {
            FileInputStream(STACK_SCRIPTS_FILE).buffered(5000000)
        }
        catch(e: FileNotFoundException)  // workaround for tests launching in a different subdir
        {
            FileInputStream("../" + STACK_SCRIPTS_FILE).buffered(5000000)
        }
        
        try
        {
            var sz = 0
            buf.use { stream ->
                sz = byteArrayToInt(stream.readNBytes(4))

                for (i in 0 until sz)
                {
                    val k = StateTransition(stream)
                    val v = Clause(stream)
                    table[k] = v
                }
            }
            LogIt.info(sourceLoc() + ": Loaded $sz stack state transition scripts.")
            return true
        }
        catch(e:FileNotFoundException)
        {
            println(e)
            println(File(STACK_SCRIPTS_FILE).absolutePath)
            return false
        }
    }
}

val stackX = StateTransitions()
typealias SD = StateDescriptor

var searchCount:Long = 0L

fun recurseSearch(depth: Int, start: SD, cur: SD, curOpcodes: Clause, available: StateTransitions, uselessStates: MutableSet<StateDescriptor>, output:StateTransitions, stackExpansion:Int=4)
{
    if (depth == 0) return
    searchCount++
    if ((searchCount and 0x7fffL) == 0L)
    {

        println(sourceLoc() + "search: " + searchCount)
        //System.gc()
        //println("GC collected")
        var rt = Runtime.getRuntime()
        val fm = rt.freeMemory()
        val tm = rt.totalMemory()
        val mm = rt.maxMemory()
        var ver = Runtime.version()
        println(sourceLoc() + "version: ${ver.version()} free: $fm total: $tm max: $mm stateTrans: ${output.size} mixinTrans: ${available.size} useless: ${uselessStates.size}")
    }


    // These must be new objects, because they will be used by output.add
    var newClause = Clause()
    var cur1 = StateDescriptor()

    for ((tr1,code1) in available.table)
    {
        cur1.stack = cur.stack
        cur1.altstack = cur.altstack
        if (cur1.apply(tr1))
        {
            if (cur1.stack.size < start.stack.size + stackExpansion)
            {
                if ((cur1 != start) && (cur1 != cur) && (!uselessStates.contains(cur1)))  // skip no-op or return to what we saw before
                {
                    newClause.opcodes = curOpcodes.opcodes + code1.opcodes
                    if (output.add(start, cur1, newClause))  // If I already got to this state in a shorter time, I've already searched with shorter possibilities
                    {
                        /*
                        val asSet = mutableSetOf<StateTransition>()
                        asSet.addAll(output.table.keys)
                        if (asSet.size != output.size)
                        {
                            println("repeat keys!")
                        } */

                        //val uselessStates1 = setOf<StateDescriptor>() + uselessStates + cur
                        uselessStates.add(cur)
                        recurseSearch(depth - 1, start, cur1, newClause, available, uselessStates, output)
                        uselessStates.remove(cur)

                        // make new ones because I gave these away in output.add()
                        newClause = Clause()
                        cur1 = StateDescriptor()
                    }
                }
            }
        }
    }
}

fun searchStateTransitions(depth: Int, entry: SD, st:StateTransitions): StateTransitions
{
    val newst = StateTransitions()
    recurseSearch(depth, entry, entry, Clause(), st, mutableSetOf(), newst)
    return newst
}

fun searchStateTransitions(newst:StateTransitions, depth: Int, entry: SD, st:StateTransitions)
{
    recurseSearch(depth, entry, entry, Clause(), st, mutableSetOf(), newst)
}



fun initBasicOps(st:StateTransitions)
{
    // Top of the stack is the right side (the end) of these

    // These are all the single-opcode stack operations

    // move to and from the alt stack
    st.add(SD(0), SD(byteArrayOf(), byteArrayOf(0)), OP.TOALTSTACK)
    st.add(SD(byteArrayOf(), byteArrayOf(0)), SD(0), OP.FROMALTSTACK)
    st.add(SD(byteArrayOf(0), byteArrayOf(1)), SD(0,1), OP.FROMALTSTACK)

    // Stack Removal
    st.add(SD(0), SD(), OP.DROP)
    st.add(SD(0,1), SD(), OP.DROP2)
    st.add(SD(0,1), SD(1), OP.NIP)

    // Stack copying
    st.add(SD(0), SD(0,0), OP.DUP)
    st.add(SD(0, 1), SD(0, 1, 0, 1), OP.DUP2)
    st.add(SD(0, 1, 2), SD(0, 1, 2, 0, 1, 2), OP.DUP3)

    st.add(SD(0, 1), SD(0, 1, 0), OP.OVER)
    st.add(SD(0, 1, 2, 3), SD(0,1,2,3,0,1), OP.OVER2)

    // some PICK and ROLL instructions
    for (n in 1 until 16 )
    {
        val initial = SD(*ByteArray(n, { it.toByte() }))
        val endRoll = SD(*ByteArray(n, { if (it == n-1) 0 else (it+1).toByte() }))
        val endPick = SD(*(initial.stack + 0)) // pick copies the nth element
        st.add(initial, endRoll, OP.push(n-1), OP.ROLL)
        st.add(initial, endPick, OP.push(n-1), OP.PICK)
    }

    // ROT and 2ROT
    st.add(SD(0,1,2), SD(1,2,0), OP.ROT)
    st.add(SD(0,1,2,3,4,5), SD(2,3,4,5,0,1), OP.ROT2)

    // SWAP and 2SWAP
    st.add(SD(0,1), SD(1,0), OP.SWAP)
    st.add(SD(0,1,2,3), SD(2,3,0,1), OP.SWAP2)

    st.add(SD(0,1), SD(1,0,1), OP.TUCK)

    // Multi opcode stack operations:

    st.add(SD(byteArrayOf(), byteArrayOf(0,1)), SD(0,1), OP.FROMALTSTACK, OP.FROMALTSTACK)
    st.add(SD(byteArrayOf(), byteArrayOf(0,1)), SD(1,0), OP.FROMALTSTACK, OP.FROMALTSTACK, OP.SWAP)

    // Drop
    st.add(SD(0,1,2), SD(), OP.DROP2, OP.DROP)
    st.add(SD(0,1,2,3), SD(), OP.DROP2, OP.DROP2)

    st.add(SD(0,1,2), SD(1,0,2), OP.ROT, OP.SWAP)

    st.add(SD(byteArrayOf(0), byteArrayOf(1)), SD(1,0), OP.FROMALTSTACK, OP.SWAP)

    //                                                                  0 1 2            0,1,2,2 0,1,2,2,3  1,2,2,0
    st.add(SD(byteArrayOf(0,1), byteArrayOf(2)), SD(1,2,2,0), OP.FROMALTSTACK, OP.DUP, OP.C3,     OP.ROLL)

    st.add(SD(0,1,2), SD(2,0,1,0), OP.ROT, OP.ROT, OP.OVER )
    //
    st.add(SD(0,1,2), SD(2,0,0,1), OP.ROT, OP.ROT, OP.OVER, OP.SWAP)
    //                                             1,2,0  1,2,0,0  1,0,0,2 1020
    st.add(SD(0,1,2), SD(1,0,2,0), OP.ROT, OP.DUP, OP.ROT, OP.SWAP)

    //                                           1,2,0    2,0,1
    st.add(SD(0,1,2), SD(2,0,1), OP.ROT, OP.ROT)
    st.add(SD(0,1), SD(1,0,0), OP.SWAP, OP.DUP)
    st.add(SD(0,1), SD(0,0,1), OP.OVER, OP.SWAP)
}

fun initStackX()
{
    if (stackX.size > 0) return

    try
    {
        stackX.load()
        println("Successfully loaded stack transformation database")
        return
    } catch (e: Exception)
    {
        println("Regenerate stack transformation database")
        throw e
    }
}

fun loadCalcStackX()
{
    if (!stackX.load()) calcStackX(true, true)
}

fun calcStackX(full: Boolean = true, save: Boolean = true)
{
    // main stack depth
    val mainDepth = if (full) 6 else 4
    // alt stack depth
    val altDepth = if (full) 4 else 2
    val squareOps = if (full) 5 else 3
    val repeatDepth = if (full) 4 else 2
    val deepMain = if (full) 6 else 5
    val deepOps = if (full) 5 else 3

    if (stackX.size > 0) return

    initBasicOps(stackX)
    // create 3 copies of the basic operations so the threads don't conflict
    val mixins0 = StateTransitions()
    initBasicOps(mixins0)
    val mixins1 = StateTransitions()
    initBasicOps(mixins1)
    val mixins2 = StateTransitions()
    initBasicOps(mixins2)

    fun add(depth: Int, stk: Int, alt: Int, newSts: StateTransitions, mixins: StateTransitions)
    {
        searchStateTransitions(newSts, depth, SD(ByteArray(stk, { it.toByte() }), ByteArray(alt, { (stk + it).toByte() })), mixins)
    }

    val newSts1 = StateTransitions()
    val th1 = thread {
        for (stk in 0..mainDepth)
        {
            for (alt in 0..altDepth)
            {
                println("discover programs for ($stk, $alt). Currently at ${newSts1.size} transformations")
                add(min(squareOps, stk + alt + 4), stk, alt, newSts1, mixins0)
            }
        }
        println("square search done")
    }


    val newSts2 = StateTransitions()
    val th2 = thread {
        println("mainstack repeats")
        for (first in 0..repeatDepth)
        {
            for (sec in 0..repeatDepth)
            {
                // println("$first $sec")
                searchStateTransitions(newSts2, 3, SD(byteArrayOf(first.toByte(), sec.toByte()), byteArrayOf()), mixins1)
                for (third in 0..repeatDepth)
                {
                    println("mainstack repeats: $first $sec $third. Currently at ${newSts2.size} transformations")
                    searchStateTransitions(newSts2, 3, SD(byteArrayOf(first.toByte(), sec.toByte(), third.toByte()), byteArrayOf()), mixins1)
                    if (full)
                    {
                        for (fourth in 0..repeatDepth)
                        {
                            println("mainstack repeats: $first $sec $third $fourth")
                            searchStateTransitions(newSts2, 3, SD(byteArrayOf(first.toByte(), sec.toByte(), third.toByte(), fourth.toByte()), byteArrayOf()), stackX)
                            for (fifth in 0..repeatDepth)
                            {
                                // println("$first $sec $third $fourth")
                                searchStateTransitions(newSts2, 3, SD(byteArrayOf(first.toByte(), sec.toByte(), third.toByte(), fourth.toByte(), fifth.toByte()), byteArrayOf()), stackX)
                            }
                        }
                    }
                }
            }
        }
        println("repeats done: discovered ${newSts2.size} transformations")
    }


    val newSts3 = StateTransitions()
    val th3 = thread {
        for (stk in 5..deepMain)
        {
            for (alt in 0..1)
            {
                println("discover deepstack programs for ($stk, $alt).  Currently at ${newSts3.size} transformations")
                add(min(deepOps, stk + alt + 2), stk, alt, newSts3, mixins2)
            }
        }
        println("deep main done: discovered ${newSts3.size} transformations")
    }

    val newSts4 = StateTransitions()
    val th4 = thread {
        for (stk in 1..4)
        {
            println("discover deep repeat programs for ($stk, 0) up to depth ${stk + 5}.  Currently at ${newSts4.size} transformations")
            add(stk + 5, stk, 0, newSts4, mixins2)
        }
        println("deep repeat done: discovered ${newSts4.size} transformations")
    }

    th1.join()
    th2.join()
    th3.join()
    th4.join()

    stackX.add(newSts1)
    stackX.add(newSts2)
    stackX.add(newSts3)
    stackX.add(newSts4)
    // No-op transformation
    stackX.add(SD(byteArrayOf(), byteArrayOf()), SD(byteArrayOf(), byteArrayOf()))

    println("total: ${stackX.size} stack programs")
    if (save) stackX.save()
    println("save completed")
}
