@file:OptIn(ExperimentalUnsignedTypes::class)

package org.wallywallet.composing
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.interaction.InteractionSource
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.DisableSelection
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.key.*
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.res.loadSvgPainter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.*
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.rememberWindowState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.nexa.libnexakotlin.UnimplementedException
import org.nexa.libnexakotlin.laterJob
import org.nexa.libnexakotlin.millinow
import org.nexa.libnexakotlin.toHex
import org.nexa.threads.Mutex
import org.wallywallet.wew.*
import java.awt.Cursor
import java.io.File
import kotlin.Error
import kotlin.math.roundToInt

import kotlin.properties.Delegates
import kotlin.reflect.KProperty

const val LazyRowMin = 10000

// Define a bunch of defaults that you may override, but that make things look OK if you do not.

/** Inject your own converters from a type (specified by the name of the type -- you can use either the full or short name) to a Composing object
    Translators from Any<*> to Composing will check this.  For example: cify(), rowOf(), and colOf() */
var toComposing: MutableMap<String, (Any?)->Composing> = mutableMapOf()

var defaultTextFontSize:TextUnit = 18.sp
var defaultTextStyle:(()->TextStyle) = { TextStyle(fontSize = defaultTextFontSize) }
var screenDensity = Density(80.0f)

var defaultBoxColor = Color(0.3f,0.0f,0.4f,0.75f)
var defaultBoxBackground = Color.Unspecified
var defaultBoxPadding = Modifier.padding(16.dp, 1.dp)
var defaultBoxBorderWidth = 3.dp
var defaultBoxShape = RoundedCornerShape(5)

var defaultButtonShape = RoundedCornerShape(25)

var defaultButtonSurroundBackground = Color.Unspecified
var defaultButtonSurroundPadding = PaddingValues(2.dp, 1.dp)

var defaultButtonContentPadding = PaddingValues(16.dp, 1.dp)
var defaultButtonHeight = null // null means wrapContentSize // 28.dp

var defaultDensity = Density(1f, 1f)

var defaultFloatingWindowWidth=500
var defaultFloatingWindowHeight=400

var defaultExpanderIndent = 50
var walletExpanderTitleStyle: (@Composable ()->TextStyle) = { defaultTextStyle() }
var walletExpanderSubtitleStyle:(@Composable ()->TextStyle) = { defaultTextStyle() }


fun Dp?.toPx(): Int?
{
    if (this == null) return null
    return with(defaultDensity) { this@toPx.toPx().toInt() }
}

// Call this function as soon as possible within your composable context
@Composable
fun initDefaults()
{
    defaultDensity = LocalDensity.current
    println("default density: " + defaultDensity.density)

}

/** Scale text */
fun TextUnit.scale(amt: Double):TextUnit
{
    if (this.isUnspecified) return defaultTextFontSize*amt
    else return this*amt
}

/** Include a constant text object; this is a little more efficient than using the dynamic one */
class ctext(val text: String, val textStyle: TextStyle? = null, val modifier: Modifier? = null):Composing
{
    @Composable
    override fun compose()
    {
        val m = modifier ?: Modifier.wrapContentSize()
        androidx.compose.material.Text(text, m, style = (textStyle ?: LocalTextStyle.current))
    }
}

/**
 * Handler to make any lazy column (or lazy row) infinite. Will notify the [onLoadMore] callback once needed.
 * @see https://dev.to/luismierez/infinite-lazycolumn-in-jetpack-compose-44a4
 * @param listState state of the list that needs to also be passed to the LazyColumn composable.  Get it by calling rememberLazyListState()
 * @param buffer the number of items before the end of the list to call the onLoadMore callback
 * @param onLoadMore will notify when we need to load more
 */
@Composable
fun InfiniteListHandler(listState: LazyListState, buffer: Int = 10, onLoadMore: (Int) -> Unit)
{
    val readAhead = buffer
    val loadMore: State<Pair<Int, Int>> = remember {
        derivedStateOf {
            val layoutInfo = listState.layoutInfo
            val totalItemsNumber = layoutInfo.totalItemsCount
            val lastVisibleItemIndex = (layoutInfo.visibleItemsInfo.lastOrNull()?.index ?: 0) + 1
            //LogIt.info("Load More $lastVisibleItemIndex > ($totalItemsNumber - $buffer) = ${lastVisibleItemIndex > (totalItemsNumber - buffer)}")
            Pair(lastVisibleItemIndex,(totalItemsNumber - buffer))
        }
    }

    LaunchedEffect(loadMore) {
        snapshotFlow { loadMore.value }
            .filter { it.first > it.second}
            .collect {
                onLoadMore(readAhead)
            }
    }
}


/** Modifiable text, style and modifier */
class CCText(text: String, style: TextStyle? = null, modifier: Modifier? = null):Composing, Refreshing
{
    fun<T> changed(a: KProperty<*>, b: T, c: T)
    {
        refresh()
    }

    constructor(t: MutableStateFlow<*>, style: TextStyle? = null, modifier: Modifier? = null):this(t.value.toString(), style, modifier)
    {
        CoroutineScope(Dispatchers.Main).launch {
            t.collect { newValue ->
                if (text != newValue) text = newValue.toString()
            }
        }
    }

    var text:String by Delegates.observable(text, this::changed)
    var style:TextStyle? by Delegates.observable(style, this::changed)
    var modifier: Modifier? by Delegates.observable(modifier, this::changed)
    var minLines: Int by Delegates.observable(1, this::changed)
    var maxLines: Int by Delegates.observable(3, this::changed)

    protected val trigger = MutableStateFlow(0)

    @Composable
    override fun compose()
    {
        key(trigger.collectAsState().value)
        {
            val m = modifier ?: Modifier.wrapContentSize()
            androidx.compose.material.Text(text, m, style = style ?: LocalTextStyle.current, minLines = minLines, maxLines = maxLines)
        }
    }

    override fun refresh()
    {
        trigger.value += 1
    }
}

fun text(text: String, style: TextStyle? = null, modifier: Modifier? = null):CCText
{
    return CCText(text, style, modifier)
}

/** A button that executes some function when pressed */
class CCButton(var show:Composing, var height:Dp? = defaultButtonHeight, var fn: ()->Any):Composing, Refreshing
{
    // var color : Color = Color.Unspecified
    var background: Color = defaultButtonSurroundBackground
    var shape : Shape = defaultButtonShape
    var padding: PaddingValues = defaultButtonContentPadding
    var modifier: Modifier = Modifier
    var colors: ButtonColors? = null

    @Composable
    override fun compose()
    {
        var mod = Modifier.background(background, shape).padding(defaultButtonSurroundPadding).then(modifier)
        val h = height
        if (h != null)
        {
            mod = mod.height(h)
        }
        else
            mod = mod.wrapContentSize()

        val bcols = colors ?: ButtonDefaults.buttonColors()
        DisableSelection {
            Button(modifier = mod, contentPadding = padding, colors = bcols, onClick = {
                fn()
            }) { show.compose() }
        }
    }

    override fun refresh()
    {

    }
}

/** Define a button */
fun button(stuff: Any?, fn: ()->Unit): CCButton
{
    val insides = cify(stuff)
    return CCButton(insides, fn = fn)
}

/** A button that executes some function when pressed
 * @param changed Callback can just return true/false to accept the proposed string change.  Or it can use @this.proposedChange to access/modify the full TextFieldValue
 * returning true to accept the change the callback places in @this.proposedChange
 * */
@OptIn(ExperimentalComposeUiApi::class)
class CCTextEntry(startValue: String, var label: String? = null, var changed: (CCTextEntry.(String)->Boolean)?):Composing, TriggeredRefresh()
{
    // var color : Color = Color.Unspecified
    var shape : Shape? = null
    var colors : TextFieldColors? = null
    var modifier: Modifier = Modifier

    var maxLines = 1

    var textStyle: TextStyle? = null
    var keyboardType = KeyboardType.Text
    var imeAction = ImeAction.Done
    var ia: MutableInteractionSource = MutableInteractionSource()

    var currentValue: MutableState<TextFieldValue> = mutableStateOf(TextFieldValue(startValue))
    var proposedChange: TextFieldValue? = null

    var standardControlKeys = true

    val value = MutableStateFlow<String>(startValue)

    fun get() = value.value

    // Workaround for a bug where moving focus prevents the keystroke from being consumed.
    var focusTime = 0L

    @Composable
    override fun compose()
    {
        onRefresh {
            val localFocusManager = LocalFocusManager.current
            val ts = textStyle ?: LocalTextStyle.current.copy()
            val sh = shape ?: TextFieldDefaults.OutlinedTextFieldShape
            val co = colors ?: TextFieldDefaults.outlinedTextFieldColors()
            // val coroutineScope = rememberCoroutineScope()
            val m = modifier.onKeyEvent { ke ->
                true  // Consume all key events, otherwise they show up both in this box and in the parent
            }.onPreviewKeyEvent { ke ->
                if (standardControlKeys && (focusTime < millinow()-250))
                {
                    if ((ke.key == Key.Enter)&&(maxLines==1))
                    {
                        localFocusManager.moveFocus(FocusDirection.Next)
                        true
                    }
                    else if (ke.key == Key.Tab)
                    {
                        println("TAB at $label")
                        // coroutineScope.launch { delay(1500L); localFocusManager.moveFocus(FocusDirection.Next) }
                        localFocusManager.moveFocus(FocusDirection.Next)
                        true
                    }
                    else if (ke.key == Key.Escape)
                    {
                        localFocusManager.moveFocus(FocusDirection.Exit)
                        true
                    }
                    else false
                }
                else false
            }.onFocusChanged {
                if (it.hasFocus)
                {
                    // println("FOCUS at $label")
                    focusTime = millinow()
                }
            }
            OutlinedTextField(
                value = currentValue.value,
                onValueChange = { it: TextFieldValue ->
                    val ch = changed
                    if (ch != null)
                    {
                        proposedChange = it
                        if (ch(it.text)) proposedChange?.let {
                            currentValue.value = it
                            value.value = it.text
                        }
                    }
                    else
                    {
                        currentValue.value = it
                        value.value = it.text
                    }
                                },
                label = { Text(label ?: "") },
                textStyle = ts,
                // interactionSource = ia,
                modifier = m,
                keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
                keyboardActions = KeyboardActions(
                    onDone = { localFocusManager.clearFocus() }
                ),
                shape = sh,
                colors = co,
                maxLines = maxLines
            )
        }

    }
}



class CCWindow(var c: Composing, title: String, width: Dp, height: Dp):Composing,TriggeredRefresh()
{
    var isOpen = MutableStateFlow(true)
    val size = Sizeable(this,width,height)
    var title by Delegates.observable(title, this::changed)

    @Composable
    override fun compose()
    {
        if (isOpen.collectAsState().value)
        {
            val icon = painterResource("icon128.png")
            // without DisableSelection you get java.lang.IllegalArgumentException: layouts are not part of the same hierarchy
            // when you try to select/click something in the child window.
            DisableSelection {
                val w = Window(
                    onCloseRequest = { isOpen.value = false },
                    title = title,
                    icon = icon,
                    state = rememberWindowState(width = size.width ?: 500.dp, height = size.height ?: 500.dp)
                              )
                {
                    c.compose()
                }
            }
        }
    }

}

fun window(c:Any?, title: String, width: Int=defaultFloatingWindowWidth, height:Int=defaultFloatingWindowHeight):CCWindow
{
    return CCWindow(cify(c),title,width.dp,height.dp)
}

class CCImage(width:Dp?=null, height:Dp?=null):Composing,TriggeredRefresh()
{

    constructor(_filename: String, width:Dp?=null, height:Dp?=null, _resource:Boolean?=null):this(width, height)
    {
        resource = _resource
        filename = _filename
        // no need to manually call build() because observables
        /*
        if (resource == false) painter = FilePainter(_filename, size.width.toPx(), size.height.toPx())
        if (resource == null) // If its null see if it exists, if not assume resource
        {
            val fil = File(filename)
            if (fil.exists()) painter = FilePainter(filename, size.width.toPx(), size.height.toPx())
            else if (resource == false) throw FileNotFoundException(filename)
        }*/
    }
    
    constructor(_painter: Painter, width:Dp?=null, height:Dp?=null):this(width, height)
    {
        painter = _painter
    }

    val rebuild = TriggeredRefresh({ build() })
    
    val size = Sizeable(rebuild,width,height)  // Because some painters use the size, I need to rebuild if the size changes
    var painter by Delegates.observable(null as Painter?, this::changed)
    var filename by Delegates.observable(null as String?, rebuild::changed)
    var resource by Delegates.observable(null as Boolean?, rebuild::changed)

    init { build() }

    fun build()
    {
        val f = filename
        if (f != null)
        {
            if (resource == false) painter = FilePainter(f, size.width.toPx(), size.height.toPx())
            if (resource == null) // If its null see if it exists, if not assume resource
            {
                val fil = File(f)
                if (fil.exists()) painter = FilePainter(f, size.width.toPx(), size.height.toPx())
            }
        }
        // otherwise fp IS the thing that was given to us
    }

    @Composable()
    override fun compose()
    {
        onRefresh {
            if (painter == null)  // If we have no painter, lets see if we need to load a resource (can't do that outside of a composable)
            {
                filename?.let {
                    if (checkFilename(it) != null)
                        painter = painterResource(it)
                }
            }
            painter?.let {
                var mod = size.width?.let { Modifier.width(it) } ?: Modifier.wrapContentWidth()
                mod = size.height?.let { mod.height(it) } ?: mod.wrapContentHeight()
                Image(it, "", mod)
            }
        }
    }
}

fun image(filename: String, width:Int?=null, height:Int?=null, resource:Boolean?=null):CCImage = CCImage(filename, width?.dp, height?.dp)
fun image(bmp: ImageBitmap, width:Int?=null, height:Int?=null):Composing = CCImage(BitmapPainter(bmp), width?.dp, height?.dp)
fun image(paint: Painter, width:Int?=null, height:Int?=null):Composing
{
    val im = CCImage(paint, width?.dp, height?.dp)
    return im
}

fun alignedRow(vararg items: Composing): RowCol
{
    return RowCol(listOf(items.toList())).alignStart()
}

class RowCol(): ComposingContainer, Refreshing
{
    protected val trigger = MutableStateFlow(0)
    fun<T> changed(a: KProperty<*>, b: T, c: T)
    {
        refresh()
    }

    override var items:MutableList<MutableList<Composing>> by Delegates.observable(mutableListOf(), this::changed)

    var rowValign by Delegates.observable(Alignment.CenterVertically, this::changed)
    var rowHarrange by Delegates.observable(Arrangement.Start, this::changed)
    var rowMod by Delegates.observable(Modifier.then(Modifier), this::changed)

    var colHalign by Delegates.observable(Alignment.CenterHorizontally, this::changed)
    var colVarrange by Delegates.observable(Arrangement.Center, this::changed)
    var colMod by Delegates.observable(Modifier.then(Modifier), this::changed)

    constructor(rows: List<List<Composing>>):this()
    {
        // Split into a list of rows
        var curRow = mutableListOf<Composing>()
        for (r in rows)
        {
            for (i in r)
            {
                if (i == CNL)
                {
                    if (curRow.isNotEmpty()) items.add(curRow)
                    curRow = mutableListOf<Composing>()
                }
                else curRow.add(i)
            }
            if (curRow.isNotEmpty()) items.add(curRow)
            curRow = mutableListOf<Composing>()
        }
        if (curRow.isNotEmpty()) items.add(curRow)
    }


    @Composable()
    override fun compose()
    {
        key(trigger.collectAsState().value)
        {
            // Should we drop the "Row" if there's only 1 element, or the "Column" if there's only 1 row?
            Column(colMod, verticalArrangement = colVarrange, horizontalAlignment = colHalign) {
                for (line in items)
                    Row(rowMod, horizontalArrangement = rowHarrange, verticalAlignment = rowValign) {
                        for (i in line)
                        {
                            i.compose()
                        }
                    }
            }
        }
    }

    override fun refresh()
    {
        trigger.value += 1
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        for(i in items) highlight(push, purpose)
    }

    /** Align each row in this object with the start side, returns this object */
    fun alignStart(): RowCol
    {
        rowHarrange = Arrangement.Start
        colHalign = Alignment.Start
        colMod = Modifier.fillMaxWidth()
        rowMod = Modifier.fillMaxWidth()
        return this
    }
}

/*
class CCInfiniteRow(var refreshIt:(CCLazyRow.()->MutableList<MutableList<Composing>>?)?=null): ComposingContainer, Refreshing
{
    override var items:MutableList<MutableList<Composing>> = mutableListOf(mutableListOf<Composing>())
    init
    {
        refreshIt?.invoke(this)?.let { items = it }
    }

    constructor(rows: List<List<Composing>>):this()
    {
        // Split into a list of rows
        var curRow = mutableListOf<Composing>()
        for (r in rows)
        {
            for (i in r)
            {
                if (i == CNL)
                {
                    items.add(curRow)
                    curRow = mutableListOf<Composing>()
                }
                else curRow.add(i)
            }
            items.add(curRow)
            curRow = mutableListOf<Composing>()
        }
        if (curRow.isNotEmpty()) items.add(curRow)
    }


    @Composable()
    override fun compose()
    {
        Column {
            for (line in items)
                LazyRow() {
                    for (i in line)
                    {
                        item {
                            i.compose()
                        }
                    }
                }
        }
    }

    override fun refresh()
    {
        refreshIt?.let { it()?.let { items = it } }

        for (r in items)
        {
            for (i in r)
            {
                if (i is Refreshing) i.refresh()
            }
        }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        for(i in items) highlight(push, purpose)
    }
}

 */

class CCLazyRow(var listState: LazyListState = LazyListState(), var refreshIt:(CCLazyRow.()->MutableList<MutableList<Composing>>?)?=null): ComposingContainer, Refreshing
{
    var wrapperMod:Modifier = Modifier
    var accessLock = Mutex()
    override var items:MutableList<MutableList<Composing>> = mutableListOf(mutableListOf<Composing>())
    protected val trigger = MutableStateFlow(0)
    init
    {
        refreshIt?.invoke(this)?.let { items = it }
    }

    constructor(rows: List<List<Composing>>):this()
    {
        accessLock.lock {
            // Split into a list of rows
            var curRow = mutableListOf<Composing>()
            for (r in rows)
            {
                for (i in r)
                {
                    if (i == CNL)
                    {
                        items.add(curRow)
                        curRow = mutableListOf<Composing>()
                    } else curRow.add(i)
                }
                items.add(curRow)
                curRow = mutableListOf<Composing>()
            }
            if (curRow.isNotEmpty()) items.add(curRow)
        }
    }


    @Composable()
    override fun compose()
    {
        key(trigger.collectAsState().value) {
            Column(wrapperMod) {
                var cpy = accessLock.lock {
                    items.toList()
                }
                for (line in cpy)
                    LazyRow(state = listState) {
                        for (i in line)
                        {
                            item { i.compose() }
                        }
                    }

            }
        }
    }

    override fun refresh()
    {
        refreshIt?.let { it()?.let { items = it } }

        accessLock.lock {
            for (r in items)
            {
                for (i in r)
                {
                    if (i is Refreshing) i.refresh()
                }
            }
        }
        trigger.update { it+1 }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        for(i in items) highlight(push, purpose)
    }
}

class CCLazyColumn(var listState: LazyListState = LazyListState(), var refreshIt:(CCLazyColumn.()->MutableList<MutableList<Composing>>?)?=null): ComposingContainer, Refreshing
{
    var accessLock = Mutex()
    var mod = Modifier.fillMaxSize()
    override var items:MutableList<MutableList<Composing>> = mutableListOf(mutableListOf<Composing>())
    protected val trigger = MutableStateFlow(0)
    init
    {
        refreshIt?.invoke(this)?.let { items = it }
    }

    constructor(rows: List<List<Composing>>):this()
    {
        // Split into a list of rows
        accessLock.lock {
            var curRow = mutableListOf<Composing>()
            for (r in rows)
            {
                for (i in r)
                {
                    if (i == CNL)
                    {
                        items.add(curRow)
                        curRow = mutableListOf<Composing>()
                    } else curRow.add(i)
                }
                items.add(curRow)
                curRow = mutableListOf<Composing>()
            }
            if (curRow.isNotEmpty()) items.add(curRow)
        }
    }


    @Composable()
    override fun compose()
    {
        val ls = remember { listState }
        val scrollState = rememberScrollState()
        val cpy = accessLock.lock {
                    items.toList()
                }
        key(trigger.collectAsState().value) {
            LazyColumn(modifier = mod.scrollable(scrollState, Orientation.Vertical), state = ls) {
                for (line in cpy)
                {
                    item {
                        Row {
                            for (i in line)
                            {
                                i.compose()
                            }
                        }
                    }
                }
            }
        }
    }


    override fun refresh()
    {
        refreshIt?.let { it()?.let { items = it } }

        accessLock.lock {
            for (r in items)
            {
                for (i in r)
                {
                    if (i is Refreshing) i.refresh()
                }
            }
        }
        trigger.update { it+1 }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
         accessLock.lock {
             for (i in items) highlight(push, purpose)
         }
    }
}


/*
class ComposingList:Composing
{
    val lst = MutableStateFlow<SnapshotStateList<Composing>>(mutableStateListOf())

    @Composable()
    override fun compose()
    {
        for()
    }
}
 */

/* TODO
class CCVScrollBox(var width: Dp? = null, var height: Dp? = null, inside: Composing, loadMore: ((Int) -> Boolean)? = null): CCScroll(ls, CCBox(inside, width, height) , loadMore)
{
    val box = inside as CCBox // because we created it above
    val vScrollState = ScrollState(0)

    @Composable
    override fun compose()
    {
        var tmp:Modifier = if (height != null) Modifier.verticalScroll(vScrollState) else Modifier
        box.mod = tmp
        super.compose()
    }
}

class CCScrollBox(var width: Dp? = null, var height: Dp? = null, inside: Composing, loadMore: ((Int) -> Boolean)? = null): CCScroll(ls, CCBox(inside, width, height) , loadMore)
{
    val box = inside as CCBox // because we created it above
    val vScrollState = ScrollState(0)
    val hScrollState = ScrollState(0)

    @Composable
    override fun compose()
    {
        var tmp:Modifier = if (height != null) Modifier.verticalScroll(vScrollState) else Modifier
        tmp = if (width != null) tmp.horizontalScroll(hscrollState) else tmp
        box.mod = tmp
        super.compose()
    }
}
*/

class ScrollableOutput(val title: Composing?=null, var height: Dp = Dp.Unspecified, makeComposable: () -> Unit):Composing
{
    constructor(t: String, h: Dp = Dp.Unspecified, makeComposable: () -> Unit): this(CCText(t, headingTextStyle(2)), h, makeComposable)

    val listState = LazyListState()
    val output: ComposingOutput = ComposingOutput()
    var scroll:Composing? = null
    var xp:CCExpander? = null
    init {
        laterJob {
            val tmp = getThreadOutput()
            setThreadOutput(output)
            val result = try
            {
                makeComposable()
            } finally
            {
                setThreadOutput(tmp)
            }
        }
        val col = CCLazyColumn(listState = listState) { output.items }
        val s = CCScroll(listState, col)
        if (height != Dp.Unspecified)
        {
            s.modifier = Modifier.heightIn(20.dp, height).wrapContentHeight()
            col.mod = Modifier.heightIn(20.dp, height).wrapContentHeight()
        }
        scroll = s
        if (title != null)
        {
            xp = expander(title, org.wallywallet.wew.cli.indent(30.dp, s), true)
            xp?.colMod = Modifier.heightIn(20.dp, height).wrapContentHeight()
        }

    }

    @Composable override fun compose()
    {
          xp?.compose() ?:  scroll?.compose()
    }

}

/**
 *
 * @param onLoadMore Callback to extend the list.  A -1 is passed to indicate "scrolling past the end".  The callback should return true if there are even more elements, or false if there was nothing left to add
 */
open class CCScroll(var listState: LazyListState = LazyListState(), var inside: Composing, var onLoadMore: ((Int) -> Boolean)? = null):Composing, TriggeredRefresh()
{
    var color : Color by Delegates.observable(defaultBoxColor, this::changed)
    var colStack = mutableListOf<Color>()
    var modifier: Modifier = Modifier.fillMaxSize()
    var background: Color = defaultBoxBackground
    var shape : Shape = RoundedCornerShape(1)
    var borderWidth : Dp = defaultBoxBorderWidth
    var knobColor: Color = Color.DarkGray
    var knobMinHeight = 30f
    var scrollDelay = 300L
    @Volatile
    var iAdjusted = 0

    fun knobHeightCalc(): Float
    {
        // Change the knob size depending on the ratio of visible items to total items
        val tot = if (listState.layoutInfo.totalItemsCount == 0) 1 else listState.layoutInfo.totalItemsCount
        val pctVisible = listState.layoutInfo.visibleItemsInfo.size.toFloat() / tot.toFloat()
        // println("vis: ${listState.layoutInfo.visibleItemsInfo.size}  tot: $tot  pct: $pctVisible")
        val knobTmp = listState.layoutInfo.viewportSize.height * pctVisible
        return if (knobTmp < knobMinHeight) knobMinHeight else knobTmp
    }

    fun knobOffsetCalc(knobHeight: Float, curItem: Int ): Pair<Float,Float>
    {
        val winHeight = listState.layoutInfo.viewportSize.height.toFloat() - knobHeight
        // <2 to fix /0 issues below
        val totalItems = (if (listState.layoutInfo.totalItemsCount<2) 2 else listState.layoutInfo.totalItemsCount).toFloat()
        // -1 because we want item 0 to position at 0 and the last item (whose index is totalItems-1, because 0 based)
        // to position at winHeight (since I already lopped off the knob height
        val itemPx = winHeight.toFloat()/(totalItems-1)
        return Pair(itemPx * curItem.toFloat(), itemPx)
    }

    fun itemFromKnobOffset(knobOffset: Float, knobHeight: Float): Int
    {
        // Subtract knobHeight so that the farthest down is the end of the list
        var ms2 = listState.layoutInfo.viewportSize.height.toFloat() - knobHeight
        if (ms2 <= 0) ms2 = 1f
        val scrollRatio = knobOffset / ms2
        //println("delta $delta, winsz $ms2 knobOffset $knobOffset  ratio $scrollRatio")
        val item = listState.layoutInfo.totalItemsCount * scrollRatio
        return item.toInt()
    }

    @Composable()
    override fun compose()
    {
        val coroutineScope = rememberCoroutineScope()
        val mod = modifier.background(background, shape).border(BorderStroke(borderWidth, color), shape) //.then(padding)

        var knobOffset by remember { mutableStateOf(0) }  // How far down from the top
        var knobHeight by remember { mutableStateOf(knobHeightCalc()) }  // How big is the knob (varies according to the # of elements in the list)

        // adjust the knob if something else changes the scroll position

        LaunchedEffect(listState) {
            snapshotFlow { listState.firstVisibleItemIndex }.collect { index ->

                if (iAdjusted == 0)
                {
                    knobHeight = knobHeightCalc()
                    val (offset, chunk) = knobOffsetCalc(knobHeight, index)
                    // If we've moved at least one row
                    if ((knobOffset < offset) || (knobOffset > offset + chunk))
                    {
                        knobOffset = offset.toInt()
                    }
                }
                else iAdjusted--
            }
        }

        LaunchedEffect(listState) {
            snapshotFlow { listState.layoutInfo.totalItemsCount }.collect { index ->
                val item = itemFromKnobOffset(knobOffset.toFloat(), knobHeight)
                listState.scrollToItem(item)
                // If the list gets extended so the knob size is changing, then pull both ends into the middle
                // by changing the knobOffset downwards by a bit
                val newHeight = knobHeightCalc()
                val delta = (knobHeight - newHeight).roundToInt()
                if (newHeight != knobHeight)
                {
                    val tmp = knobOffset + delta/2
                    if (tmp > 0) knobOffset = tmp
                    knobHeight = newHeight
                }
            }
        }


        onRefresh {
            Row(mod) {
                // The scroll bar -- the space the scroll grabber fits in
                Box(Modifier.width(10.dp)) {  // fillMaxHeight()) {
                    // The actual scroll grabber
                    Box(Modifier.padding(0.dp, knobOffset.dp, 0.dp, 0.dp).fillMaxWidth().height(knobHeight.pxToDp()).background(knobColor)
                        .draggable(
                            orientation = Orientation.Vertical,
                            state = rememberDraggableState { delta ->
                                var tmp = knobOffset + delta.toInt()
                                if (tmp < 0) tmp = 0
                                knobOffset = tmp
                                //knobOffset = knobOffsetCalc(listState.layoutInfo.viewportSize.height, listState.layoutInfo.totalItemsCount, listState.firstVisibleItemIndex)

                                coroutineScope.launch {
                                    var done = true
                                    do
                                    {
                                        // Change the knob size depending on the ratio of visible items to total items
                                        knobHeight = knobHeightCalc()

                                        // Bound the slider knob
                                        var ms2 = listState.layoutInfo.viewportSize.height.toFloat() - knobHeight
                                        if (ms2 <= 0) ms2 = 1f
                                        if (knobOffset > ms2)
                                        {
                                            knobOffset = ms2.toInt()
                                        }
                                        val item = itemFromKnobOffset(knobOffset.toFloat(), knobHeight)

                                        if (item!=listState.firstVisibleItemIndex)
                                        {
                                            iAdjusted++
                                            listState.scrollToItem(item.toInt())
                                        }
                                        // Infinite scroll when hit bottom
                                        var endRatio = (knobOffset) / ms2
                                        onLoadMore?.run {
                                            if (endRatio > 0.99)
                                            {
                                                // println("load more $endRatio")
                                                done = !this(-1)
                                                delay(scrollDelay)
                                                endRatio = (knobOffset + knobHeight) / ms2
                                            }
                                            else done = true
                                        }
                                    } while (!done)
                                }
                            }
                        ))
                }
                inside.compose()
            }

        }
    }
}

fun Blank(width:Dp?=null, height:Dp?=null): CCBox
{
    val ret = CCBox(nothing, width, height)
    ret.color = Color.Transparent
    ret.background = Color.Transparent
    ret.shape = RectangleShape
    ret.borderWidth = 0.dp
    ret.padding = Modifier
    return ret
}

class CCBox(var inside: Composing, width:Dp?=null, height:Dp?=null):Composing, TriggeredRefresh()
{
    var color : Color by Delegates.observable(defaultBoxColor, this::changed)
    var colStack = mutableListOf<Color>()
    var background: Color = defaultBoxBackground
    var shape : Shape = defaultBoxShape
    var borderWidth : Dp = defaultBoxBorderWidth
    var padding = defaultBoxPadding
    val size = Sizeable(this,width,height)
    var mod:Modifier = Modifier

    @Composable()
    override fun compose()
    {
        var mod2 = mod.background(background, shape).border(BorderStroke(borderWidth, color), shape).then(padding)
        size.width?.let { mod2 = mod2.width(it) }
        size.height?.let { mod2 = mod2.height(it) }

        onRefresh {
            Box(mod2) {
                inside.compose()
            }
        }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        super.highlight(push, purpose)
        if (push)
        {
            colStack.add(color)
            color = highlightBackgroundColor
        }
        else
        {
            color = colStack.removeLastOrNull() ?: defaultBoxColor
        }
    }
}

fun box(item: Composing, width:Dp?=null, height:Dp?=null): Composing
{
    return CCBox(item, width, height)
}


class CCExpander(var title: Composing, var contents: Composing, expanded: Boolean = false, var expanderFontSize: ()->TextUnit = { defaultTextFontSize.times(1.75) }):Composing
{
    val expanded = MutableStateFlow(expanded)
    var colMod: Modifier = Modifier
    var rowMod: Modifier = Modifier.fillMaxWidth()
    var valign = Alignment.CenterVertically
    @Composable()
    override fun compose()
    {
        var clickMod = Modifier.clickable { expanded.value = !expanded.value }.pointerHoverIcon(PointerIcon(Cursor(Cursor.DEFAULT_CURSOR)))
        val exp = expanded.collectAsState().value
        Column(modifier = colMod) {
            //Row(verticalAlignment = Alignment.CenterVertically)
            Row(modifier = rowMod, verticalAlignment = valign, horizontalArrangement = Arrangement.Start) {
                DisableSelection {
                    Text(if (exp) " ▽ " else " ▷ ", clickMod, fontSize = expanderFontSize())
                }
                title.compose()
            }
            if (exp) contents.compose()
        }
    }

}

fun expander(title: Composing, contents: Composing, expanded: Boolean = false):CCExpander
{
    return CCExpander(title, contents, expanded)
}


/** A vertical sash is a vertical divider that you can drag to the left and right to resize the windows on either side */
var defaultSashMinWidth = 50.dp
class CCVerticalSash(): DropTarget, TriggeredRefresh()
{
    var defaultWidth = 100.dp
    var leftMod by Delegates.observable(Modifier.fillMaxSize(), this::changed)
    var rightMod by Delegates.observable(Modifier, this::changed)
    var sashMod by Delegates.observable(Modifier.fillMaxHeight().width(10.dp).background(Color.Black), this::changed)
    var minWidth by Delegates.observable(defaultSashMinWidth, this::changed)
    val contents = mutableStateListOf<Composing>()
    val spacing = mutableListOf<MutableStateFlow<Dp>>()

    constructor(vararg items: Composing):this()
    {
        for (i in items) contents.add(i)
    }

    override fun add(obj: Composing, pos: Offset?)
    {
        contents.add(obj)
    }

    override fun remove(obj: Composing): Boolean
    {
        return contents.remove(obj)
    }

    fun setSashWidth(itemIdx: Int, width: Dp)
    {
        if (itemIdx >= contents.size) throw IllegalArgumentException("item does not exist")
        while (spacing.size <= itemIdx)
        {
            spacing.add(MutableStateFlow(defaultWidth))
        }
        spacing[itemIdx].value = width
    }

    @Composable
    override fun compose()
    {
        onRefresh {
            Row(modifier = Modifier.fillMaxSize())
            {
                val last = contents.size - 1
                for ((idx, c) in contents.withIndex())
                {
                    if (idx == spacing.size)
                    {
                        spacing.add(MutableStateFlow(defaultWidth))
                    }
                    val width = spacing[idx].collectAsState()
                    //println("width ${width}")
                    if (idx != last)
                    {
                        Box(Modifier.width(width.value)) { c.compose() }
                        // Draw the sash
                        androidx.compose.foundation.layout.Box(sashMod.pointerInput(Unit) {
                            //println("sash drag!")
                            detectDragGestures { change, dragAmount ->
                                change.consume()
                                spacing[idx].value = max(minWidth, (width.value + dragAmount.x.toDp()))
                                //println("sash drag gesture $width -> ${spacing[idx]} ${dragAmount.x}")
                            }
                        })
                        {

                        }
                    }
                    else Box() { c.compose() }
                }
            }
        }
    }
}

fun vsash(vararg items: Composing):CCVerticalSash
{
    val ret = CCVerticalSash(*items)
    return ret
}


class indentO(amt: Dp, item: Composing):Composing, TriggeredRefresh()
{
    var amt by Delegates.observable(amt, this::changed)
    var item by Delegates.observable(item, this::changed)

    @Composable()
    override fun compose()
    {
        var mod = Modifier.padding(amt,0.dp,0.dp,0.dp)
        Box(mod) {
            item.compose()
        }
    }
}

fun indent(amt: Dp, item: Composing) = indentO(amt, item)
fun indent(amt: Int, item: Composing) = indentO(amt.dp, item)

/** Helper function for cify: Take a bunch of stuff and make composables out of them, wrapping them in rows or columns as needed so they don't sit right
 * on top of each other
 */
fun composing(items: Collection<*>, style: TextStyle? = null, modifier: Modifier = Modifier,  column: Boolean = false): Composing
{
    val composingItems = items.map { cify(it, style, modifier) }.toList()
    // in column mode do listOf(listOf(item), ... listOf(item)), in row mode do listOf(listOf(item ... item))
    val listed = if (column) composingItems.map { listOf(it) } else listOf(composingItems)
    if ((items.size < LazyRowMin) || column)
    {
        val tmp = RowCol(listed)
        return tmp
    }
    else
    {
        val tmp = CCLazyRow(listed)
        return tmp
    }
}

/** Helper function for cify: Take a bunch of stuff and make composables out of them, wrapping them in rows or columns as needed so they don't sit right
 * on top of each other
 */
fun composing(items: Array<*>, style: TextStyle? = null, modifier: Modifier = Modifier, column: Boolean = false): Composing
{
    val composingItems = items.map { cify(it, style, modifier) }.toList()
    // in column mode do listOf(listOf(item), ... listOf(item)), in row mode do listOf(listOf(item ... item))
    val listed = if (column) composingItems.map { listOf(it) } else listOf(composingItems)

    if ((items.size < LazyRowMin) || column)
    {
        val tmp = RowCol(listed)
        return tmp
    }
    else
    {
        val tmp = CCLazyRow(listed)
        return tmp
    }
}

/** Create rows of lots of things.  Calls cify to resolve things into composables.  You may go to the next row using the CNL object */
fun rowOf(vararg things: Any?, style: TextStyle? = null, mod: Modifier = Modifier):ComposingContainer = cify(things, style, mod) as ComposingContainer

/** Create a column of lots of things.  Calls cify to resolve things into composables. */
fun colOf(vararg things: Any?, style: TextStyle? = null, mod: Modifier = Modifier):ComposingContainer
{
    // workaround, if you call colOf with a list, it turns that into a list of a list which looks like a single column of a row of things
    // so fix that particular issue
    //if (things.size == 1) return cify(things[0], style, mod, column = true) as ComposingContainer
    // else
        return cify(things, style, mod, column = true) as ComposingContainer
}

/** Create a column of lots of things.  Calls cify to resolve things into composables. */
fun colOfList(things: MutableList<Any?>, style: TextStyle? = null, mod: Modifier = Modifier):ComposingContainer = cify(things, style, mod, column = true) as ComposingContainer


/** Composing-ify.  Turn anything into a GUI element */
fun cify(thing: Any?, instyle: TextStyle? = null, mod: Modifier = Modifier, column: Boolean = false):Composing
{
    fun stringify(d: Any?): String = d.toString()

    val style:TextStyle = if (instyle == null) defaultTextStyle() else instyle


    var evaledThing = thing
    // First resolve any functions to an object by calling them
    while (evaledThing is Function<*>) evaledThing = (evaledThing as? ()->Any?)?.invoke()

    // Now handle null
    val fo = evaledThing
    if (fo == null) return toComposing["null"]?.invoke(fo) ?: text("(null)",style = style, modifier = mod)

    // Next see if there is a custom display installed
    var name = fo::class.qualifiedName
    var df = toComposing[name]
    if (df != null) return df.invoke(fo)
    name = fo::class.simpleName
    df = toComposing[name]
    if (df != null) return df.invoke(fo)

    // Finally use our default converters
    return when (fo)
    {
        is Dp                -> Blank(fo)  // just providing a Dp creates a horizontal space by that amount
        is Char              -> text(fo.toString(), style = style, modifier = mod)
        is Byte              -> text(stringify(fo), style = style, modifier = mod)
        is Short             -> text(stringify(fo), style = style, modifier = mod)
        is Int               -> text(stringify(fo), style = style, modifier = mod)
        is Long              -> text(stringify(fo), style = style, modifier = mod)

        is UByte             -> text(stringify(fo), style = style, modifier = mod)
        is UShort            -> text(stringify(fo), style = style, modifier = mod)
        is UInt              -> text(stringify(fo), style = style, modifier = mod)
        is ULong             -> text(stringify(fo), style = style, modifier = mod)
        is String            -> text(fo, style = style, modifier = mod)
        is ByteArray         -> text(fo.toHex() + "h", style = style, modifier = mod)
        is UByteArray        -> text(fo.toHex() + "h", style = style, modifier = mod)
        is ImageBitmap       -> image(fo )
        is Painter           -> image(fo )
        is CmdRepresentation -> fo
        is Composing         -> fo
        is Collection<*>     -> composing(fo, style = style, modifier = mod, column = column)
        is Array<*>          -> composing(fo, style = style, modifier = mod, column = column)
        is MutableStateFlow<*> -> {
            if (fo.value == null) CCText(fo, style, Modifier.padding(1.dp, 1.dp).then(mod))
            else when(fo.value)
            {
                is String -> CCText(fo, style, Modifier.padding(1.dp, 1.dp).then(mod))
                is File -> CCText(fo, style, Modifier.padding(1.dp, 1.dp).then(mod))
                else -> throw UnimplementedException("Make a generic MSB container")
            }
        }
        else                 -> text(fo.toString(), style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod))
    }
}

/** Check whether a file name exists */
fun checkFilenameAndThrow(name: String, allowNonExistent: Boolean = false, mustNotExist: Boolean = false):File
{
        var f = File(name)
        if (mustNotExist)
        {
            if (f.exists())
            {
                throw Error("File already exists: " + f.absolutePath)
            }
        }
        if (allowNonExistent) return f
        if (!f.exists())
        {
            throw Error("File does not exist: " + f.absolutePath)
        }
        return f
}

fun checkFilename(name: String, allowNonExistent: Boolean = false, mustNotExist: Boolean = false):File?
{
    var f = File(name)
    if (mustNotExist)
    {
        if (f.exists())
        {
            return null
        }
    }
    if (allowNonExistent) return f
    if (!f.exists())
    {
        return null
    }
    return f
}

/** This helper function suggests in image size, given chosen dimensions, image dimensions and canvas dimensions.
 * Based on flags, it will preserve the image aspect ratio, and/or fit the image inside the available space.
 */
fun sizer(chosenWidth: Int?, chosenHeight:Int?, imWidth:Int, imHeight: Int, cvsWidth:Int, cvsHeight:Int, preserveAR: Boolean=true, fit:Boolean=true): IntSize
{
    val imAR = imWidth.toFloat()/imHeight.toFloat()

    if (chosenHeight != null && chosenWidth != null)
    {
        if (preserveAR)
        {
            val chosenAR = chosenWidth.toFloat()/chosenHeight.toFloat()
            if (chosenAR > imAR)  // chosen space is too wide
                return IntSize((chosenHeight * imAR).toInt() , chosenHeight)
            if (chosenAR < imAR)  // chosen space is too high
                return IntSize(chosenWidth, (chosenWidth / imAR).toInt())
        }
        return IntSize(chosenWidth, chosenHeight)
    }
    else if (chosenWidth == null && chosenHeight == null)  // not chosen fit to canvas
    {
        if (fit)
        {
            if (preserveAR)
            {
                val cvsAR = cvsWidth.toFloat() / cvsHeight.toFloat()
                if (cvsAR > imAR)  // chosen space is too wide
                    return IntSize((cvsHeight * imAR).toInt(), cvsHeight)
                if (cvsAR < imAR)  // chosen space is too high
                    return IntSize(cvsWidth, (cvsWidth / imAR).toInt())
            }
            else return IntSize(cvsWidth, cvsHeight)
        }
        else
        {
            return IntSize(imWidth, imHeight)
        }
    }
    else if (chosenHeight != null)  // height drives it
    {
        if (preserveAR)
            return IntSize((chosenHeight * imAR).toInt(), chosenHeight)
        else if (fit) return IntSize(cvsWidth, chosenHeight)
        else return IntSize(imWidth, chosenHeight)
    }
    else if (chosenWidth != null)  // width drives it
    {
        if (preserveAR)
            return IntSize(chosenWidth, (chosenWidth / imAR).toInt())
        else if (fit) IntSize(chosenWidth, cvsHeight)
        else return IntSize(chosenWidth, imHeight)
    }

    return IntSize(imWidth, imHeight)
}


class FilePainter(var bmp: ImageBitmap?=null, var svg:Painter?=null, var width:Int?=null, var height:Int?=null): Painter()
{
    constructor(filename: String, width:Int?=null, height:Int?=null): this(null,null, width, height)
    {
        val f = checkFilenameAndThrow(filename).inputStream()
        if (filename.endsWith(".svg"))
            svg = loadSvgPainter(f, screenDensity)
        else
            bmp = loadImageBitmap(f)

    }

    override val intrinsicSize: Size
        get()
        {
            // in getting the intrinsic size, the "canvas" is the same as the "image" & so it is only the size overrides that might change things.
            // TODO you could also use the canvas size to force a max size
            bmp?.let {
                val destSize = sizer(width, height, it.width, it.height, it.width, it.height)
                return Size(destSize.width.toFloat(), destSize.height.toFloat())
            }
            svg?.let {
                val sz = it.intrinsicSize
                val destSize = sizer(width, height, sz.width.toInt(), sz.height.toInt(), sz.width.toInt(), sz.height.toInt())
                return Size(destSize.width.toFloat(), destSize.height.toFloat())
            }
            return Size(100f,100f)
        }

    override fun DrawScope.onDraw()
    {
        val pageWidth = this.size.width.toInt()
        val pageHeight = this.size.height.toInt()
        val im = bmp
        if (im!=null)
        {
            val me = this@FilePainter
            val paint = Paint()
            this.drawIntoCanvas { cvs ->
                //cvs.drawImage(ibmp, Offset(0f,0f), paint)
                val destSize = sizer(width, height, im.width, im.height, this.size.width.toInt(), this.size.height.toInt())
                cvs.drawImageRect(
                    im, IntOffset.Zero, IntSize(im.width, im.height),
                    IntOffset.Zero, destSize, paint
                                 )
            }
        }
        else
        {
            val sim = svg
            if (sim != null)
            {
                val me = this@FilePainter
                with(sim)
                {
                    val destSize = sizer(width, height, pageWidth, pageHeight, pageWidth, pageHeight)
                    draw(destSize.toSize())
                }
            }

        }
    }
}