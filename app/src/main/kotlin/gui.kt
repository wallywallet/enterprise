// Copyright (c) 2021 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package org.wallywallet.wew
import org.wallywallet.composing.*
import org.nexa.libnexakotlin.*
import org.nexa.threads.*
// import androidx.compose.desktop.AppManager
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.ui.window.application
import androidx.compose.ui.window.Window
//import androidx.compose.ui.window.Events
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.*
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.selection.SelectionContainer
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*

import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.key.*
import androidx.compose.ui.input.pointer.*
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.window.Tray
import androidx.compose.ui.window.rememberWindowState

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flow
import java.util.*

import kotlin.coroutines.CoroutineContext
import kotlinx.coroutines.newFixedThreadPoolContext
import org.jetbrains.skia.EncodedImageFormat
import org.jetbrains.skiko.toImage
import org.nexa.threads.Mutex
import org.wallywallet.composing.Composer
import java.time.Instant
import kotlin.properties.Delegates
import java.awt.Toolkit
import java.io.ByteArrayInputStream
import java.io.FileOutputStream
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import kotlin.math.pow


var dpFactor = 1f
var dpi = 160
fun getScreenSize(): DpSize
{
    val toolkit = Toolkit.getDefaultToolkit()

    // Broken: dpi = toolkit.screenResolution
    val screenSize = toolkit.screenSize
    dpFactor = dpi.toFloat() / 160f
    return DpSize(Dp(screenSize.width/dpFactor), Dp(screenSize.height/dpFactor))
}

fun<T> List<T>.tween(o:()->T):List<T>
{
    val ret = mutableListOf<T>()
    for ((idx, i) in this.withIndex())
    {
        ret.add(i)
        if (idx<this.size-1) ret.add(o())
    }
    return ret
}
fun<T> List<T>.tween(o: T):List<T>
{
    val ret = mutableListOf<T>()
    for ((idx, i) in this.withIndex())
    {
        ret.add(i)
        if (idx<this.size-1) ret.add(o)
    }
    return ret
}

private val LogIt = GetLog("BU.enterpriseWallet.gui")

val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-M-d h:m:s").withZone(ZoneId.from(ZoneOffset.UTC))
//val dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.from(ZoneOffset.UTC))
val cmdFontScale = 1.2
var cmdFontSize = { defaultTextFontSize.times(cmdFontScale) }
var headingFontScale = 2.0
var headingFontSize = { defaultTextFontSize.times(headingFontScale) }
var headingColor = Color(0xff751665)
var fieldColor = Color(0xff100590)
var fieldTextStyle = @Composable { defaultTextStyle() }
var consoleTextStyle = @Composable { defaultTextStyle() }

fun headingTextStyle(subHeading:Int=0): TextStyle {
    val mul = 0.90.pow(subHeading)
    return defaultTextStyle.copy(color = headingColor, fontSize = headingFontSize().times(mul))
}

var highlightBackgroundColor:Color = Color(0xFFc837b8)
var toolbarBackgroundColor: Color = Color(0xFFC0C0C0)

var defaultDropTargetBackground: Color = Color(0xFFC0C0C0)
var dragModeColor = Color(0xFFc837b8)


enum class DataDisplayStyle
{
    SIMPLE, MULTI
}

enum class DataRole
{
    DATA, DATAPATH
}
/** if a field is clicked, this is where the data is sent by default */
var defaultDataSink:((Any?, DataRole) -> Boolean)? = ::injectTextIntoCmdLine
var defaultTextStyle = TextStyle()

@Composable
fun initGlobals()
{
    defaultTextStyle = LocalTextStyle.current
    defaultTextFontSize = LocalTextStyle.current.fontSize
    if (defaultTextFontSize.isUnspecified) defaultTextFontSize = 18.sp

    walletExpanderTitleStyle = { defaultTextStyle.copy(color = headingColor, fontSize = headingFontSize()) }
    walletExpanderSubtitleStyle = { defaultTextStyle.copy(color = headingColor, fontSize = headingFontSize().times(0.85)) }

    fieldTextStyle = @Composable { LocalTextStyle.current.copy(color = fieldColor, fontSize = defaultTextFontSize.times(1.10), fontWeight = FontWeight.Bold) }

    screenDensity = LocalDensity.current

    toComposing["Wallet"] = { w -> WalletFace(w as Wallet) }
    toComposing["Bip44Wallet"] = { w -> WalletFace(w as Wallet) }
    toComposing["Blockchain"] = { w -> BlockchainFace(w as Blockchain)}
    toComposing["TransactionHistory"] = { w -> TxHistoryFace(w as TransactionHistory)}
    toComposing["iTransaction"] = { w-> TxFace(w as iTransaction)}
    toComposing["NexaTransaction"] = { w-> TxFace(w as iTransaction)}
    toComposing["BchTransaction"] = { w-> TxFace(w as iTransaction)}
    toComposing["iBlockHeader"] = { w -> BlockHeaderFace(w as iBlockHeader)}
    toComposing["NexaBlockHeader"] = { w ->
        val ret = BlockHeaderFace(w as iBlockHeader)
        ret
    }
    toComposing["BchBlockHeader"] = { w ->
        val ret = BlockHeaderFace(w as iBlockHeader)
        ret
    }
}

fun assertInGui()
{
    val tname = Thread.currentThread().name
    assert(tname == "main")
}

fun assertNotInGui()
{
    val tname = Thread.currentThread().name
    assert(tname != "main")
}


val coMiscCtxt: CoroutineContext = newFixedThreadPoolContext(6, "app")
//val coMiscScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coMiscCtxt)


/** Do whatever you pass within the user interface context, synchronously */
fun <RET> doUI(fn: suspend () -> RET): RET
{
    return runBlocking(Dispatchers.Main) {
        fn()
    }
}


/** Do whatever you pass within the user interface context, asynchronously */
fun later(fn: suspend () -> Unit): Unit
{
    GlobalScope.launch(coMiscCtxt) {
        try
        {
            fn()
        }
        catch (e: Exception)
        {
            handleThreadException(e,"Exception in later", sourceLoc())
        }

    }
}

/** Do whatever you pass within the user interface context, asynchronously */
fun laterUI(fn: suspend () -> Unit): Unit
{
    GlobalScope.launch(Dispatchers.Main) {
        try
        {
            fn()
        }
        catch (e: Exception)
        {
            handleThreadException(e,"Exception in laterUI", sourceLoc())
        }

    }
}

fun guiIni()
{
    /*
    application.setEvents(
        // onAppStart = { println("onAppStart") }, // Invoked before the first window is created
        onAppExit = {  } // Override the default handler which shuts down the entire app
    )
     */
}

fun getWindowIcon(): Painter?
{
    var image: BufferedImage? = null
    try
    {
        image = ImageIO.read(File("media/bitcoin_cash_token.png"))
    }
    catch (e: Exception)
    {
        // image file does not exist
    }

    if (image == null)
    {
        image = BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB)
    }

    if (image == null) return null
    return BitmapPainter(image.toComposeImageBitmap())
}


abstract class UiCtrlRefreshing(var iconSize:Int, startingState:Int):Refreshing
{
    val refreshState = MutableStateFlow(startingState)

    init {
        launch {
            while(true)
            {
                if (refreshState.value == 1)
                {
                    refresh()
                }
                delay(1000)
            }
        }
    }

    open fun refreshView():Composing
    {
        return object:Composing
        {
            @Composable()
            override fun compose()
            {
                var shouldRefresh = refreshState.collectAsState()
                var fp: Painter? = null
                var rp: Painter? = null
                var mod = iconSize.let { Modifier.width(it.dp)} ?: Modifier.wrapContentWidth()
                mod = iconSize.let { mod.height(it.dp)} ?: mod.wrapContentHeight()

                mod = mod.clickable {
                    refreshState.value = 1-refreshState.value
                }

                val im = if (shouldRefresh.value == 0)
                {
                    if (fp == null) fp = painterResource("freeze.svg")
                    fp
                }
                else
                {
                    if (rp == null) rp = painterResource("refresh.svg")
                    rp
                }
                Image(im, "", mod)
            }
        }
    }

}


/** An interface describing how command output is represented on the screen */
interface CmdRepresentation:Composing, Refreshing
{
    abstract fun copyAndMod(o: Any?=null):CmdRepresentation
    var lastSize: Size  // This helps accurate scrolling so we can tell how big stuff probably is even if not onscreen
    val sizeChanged:Callbacker2<Size, Size>
    fun dragMode(on: Boolean) {}
}


@Composable
fun composeAnything(out: Any?, modifier: Modifier = Modifier, stylefn: @Composable ()->TextStyle, dataStyle: DataDisplayStyle, maxLines:Int=Int.MAX_VALUE)
{
    fun stringify(d: Any?, dataStyle: DataDisplayStyle): String
    {
        when(dataStyle)
        {
            DataDisplayStyle.SIMPLE -> return d.toString()
            DataDisplayStyle.MULTI -> return when(d)
            {
                is Byte -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is Short -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is Int    -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is Long -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is UByte  -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is UShort  -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is UInt  -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                is ULong -> "$d 0x${d.toString(16)} b${d.toString(2)}"
                else -> d.toString()
            }
        }
    }

    val mod = if (dataStyle == DataDisplayStyle.SIMPLE) Modifier.padding(1.dp, 1.dp).then(modifier)
    else Modifier.padding(16.dp, 1.dp).then(modifier)

    val style = stylefn()
    if (out == null) Text("(null)",style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod))
    else
    {
        var fo = if (out is Function<*>) (out as ()->Any?)?.invoke() else out
        when (fo)
        {
            is Char              -> Text(fo.toString(), style=style, modifier = mod, maxLines = maxLines)
            is Byte              -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is Short             -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is Int               -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is Long              -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)

            is UByte             -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is UShort            -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is UInt              -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is ULong             -> Text(stringify(fo, dataStyle), style = style, modifier = mod, maxLines = maxLines)
            is String            -> Text(fo, style = style, modifier = mod, maxLines = maxLines)
            is ByteArray         -> Text(fo.toHex() + "h", style=style, modifier = mod, maxLines = maxLines)
            is UByteArray        -> Text(fo.toHex() + "h", style=style, modifier = mod, maxLines = maxLines)
            is ImageBitmap       -> androidx.compose.foundation.Image(bitmap = fo, "")
            is Painter           -> Image(fo,"")
            is CmdRepresentation -> fo.compose()
            is Composing         -> fo.compose()
            is MutableStateFlow<*> -> {
                when(fo.value)
                {
                    is String -> CCText(fo, style, Modifier.padding(1.dp, 1.dp).then(mod))
                    is File -> CCText(fo, style, Modifier.padding(1.dp, 1.dp).then(mod))
                    else -> throw UnimplementedException("Make a generic MSB container")
                }
            }
            is Map<*,*>          -> {
                /*
                val sb = StringBuilder()
                val sep = if (fo.size < maxLines && fo.size!=0) "\n" else " "
                sb.append("{$sep")
                for ((k,v) in fo)
                {
                    sb.append("  ${k.toString()} to ${v.toString()},$sep")
                }
                sb.append("}")
                Text(sb.toString(), style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod), maxLines = maxLines)
                 */
                val sep = if (fo.size < maxLines && fo.size!=0) "\n" else " "
                Text("{$sep", style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod), maxLines = maxLines)
                for ((k,v) in fo)
                {
                    rowOf(listOf(composeAnything(k, modifier, stylefn, dataStyle, maxLines), composeAnything(v, modifier, stylefn, dataStyle, maxLines)))
                }
                Text("}$sep", style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod), maxLines = maxLines)
            }
            else                 -> {
                Text(fo.toString(), style = style, modifier = Modifier.padding(1.dp, 1.dp).then(mod), maxLines = maxLines)
            }
        }
    }
}

/** Represent a command and its result */
class ObjectCmd(val cmdline: String, val output: ComposingOutput = ComposingOutput()): CmdRepresentation, TriggeredRefresh()
{
    val mutex = Mutex()
    var cmdstyle: ()->TextStyle = { TextStyle(fontSize = cmdFontSize(), color = Color.Blue) } //, background = Color.Yellow)
    var textstyle: @Composable ()->TextStyle = { TextStyle(fontSize = defaultTextFontSize) } // , background = Color.Cyan)

    var dragMode = MutableStateFlow(false)

    override var lastSize = Size(0f,0f)
    override val sizeChanged = Callbacker2<Size, Size>()

    override fun dragMode(on: Boolean)
    {
        super.dragMode(on)
        dragMode.value = on
    }

    constructor(prior: ObjectCmd, append: Any?) : this(prior.cmdline, prior.output)
    {
        mutex.lock {
            cmdstyle = prior.cmdstyle
            textstyle = prior.textstyle

            if (append != null)
            {
                // Look for customizable displays first
                var name = append::class.qualifiedName
                var df = toComposing[name]
                if (df != null)
                {
                    output.stdout(df.invoke(append))
                    //output.add(df.invoke(append))
                    return@lock
                }

                name = append::class.simpleName
                df = toComposing[name]
                if (df != null)
                {
                    output.stdout(df.invoke(append))
                    //output.add(df.invoke(append))
                    return@lock
                }

                output.stdout(append)
                /*
                // Put the next object onto the output list
                when (append)
                {

                    is String ->
                    {
                        // If the last print was a string, keep going
                        if (output.isEmpty()) output.add(append)
                        else
                        {
                            val v = output.last()
                            if (v is String) output[output.lastIndex] = v + append
                            // otherwise we need to create a new string object at the end of the list
                            else output.add(append)
                        }
                    }

                    is ImageBitmap -> output.add(append)
                    is CmdRepresentation -> output.add(append)
                    is Long, Int, ULong, UInt -> output.add(append)
                    equals(null) -> output.add("(null)")
                    else -> output.add(append)
                }
                */
            }
        }
    }


    /** Draw this command and its output */
    @Composable
    override fun compose()
    {
            onRefresh {
                Column(Modifier.onGloballyPositioned {
                    val tmp = it.size.toSize()
                    sizeChanged.invoke(lastSize, tmp)
                    if (tmp != lastSize) lastSize = tmp
                }) {
                // Modifier.border(2.dp, Color.Magenta)
                Text(cmdline.trimEnd(), style = cmdstyle(), modifier = Modifier.fillMaxWidth().padding(2.dp, 1.dp))
                val dmBox = if (dragMode.collectAsState().value) Modifier.border(1.dp, dragModeColor) else Modifier.border(
                    0.dp,
                    Color.Unspecified
                )

                Column(modifier = dmBox) {
                    output.compose()
                    /*
                    val cpy = mutex.lock {  // stop concurrent mod exceptions
                        output.toList()
                    }
                    for (out in cpy)
                    {
                        composeAnything(out, stylefn = textstyle, dataStyle = DataDisplayStyle.MULTI)
                    }
                     */
                }
            }
        }
    }

    override fun copyAndMod(o: Any?):CmdRepresentation
    {
        val r = ObjectCmd(this, o)
        return r
    }
}

/** handler for the output (text, errors) of any commands -- this class gets plugged into the bottom of various
 * print functions in the interpreter.
 *
 * This class just forwards all the output calls into the interpreter state object (which is tracking the current state of the console and interpreter)
 */
class CmdOutput(val cmdIdx: Int, val state: InterpreterPanelState): Output()
{

    override fun stdout(s:Any?)
    {
        if (s != null)
        {
            state.append(cmdIdx, s)
        }
    }
    override fun error(s:String?)
    {
        if (s != null)
        {
            state.appendText(cmdIdx, s)
        }
    }
    override fun info(s:String?)
    {
        if (s != null)
        {
            state.appendText(cmdIdx, s)
        }
    }
}


/** handler for the output (text, errors) of any commands -- this class gets plugged into the bottom of various
 * print functions in the interpreter.
 *
 * This class just forwards all the output calls into the interpreter state object (which is tracking the current state of the console and interpreter)
 */
class ListOutput(mod: Modifier = Modifier): Output(),ComposingContainer
{
    val lstState = LazyListState()
    val lst = CCLazyColumn(lstState)

    init {
        lst.mod = mod
    }

    //val lst = MutableStateFlow<List<Composing>>(listOf())
    // Horribly inefficient
    override fun stdout(s:Any?)
    {
        if (s != null)
        {
            if (s is String)
            {
                var curLine = lst.items.last()
                val sLines = s.splitKeepDelimiter("\n")
                lst.accessLock.lock {
                    for (line in sLines)
                    {
                        if (line == "\n")
                        {
                            curLine = mutableListOf()
                            lst.items.add(curLine)
                        }
                        else if (line != "") curLine.add(cify(line))
                    }
                    lst.refresh()
                }
            }
            else
            {
                lst.accessLock.lock {
                    lst.items.add(mutableListOf(cify(s)))
                    lst.refresh()
                }
            }
        }
    }
    override fun error(s:String?)
    {
        if (s != null)
        {
            lst.accessLock.lock {
                lst.items.add(mutableListOf(cify(s)))
                lst.refresh()
            }
        }
    }
    override fun info(s:String?)
    {
        if (s != null)
        {
            var curLine = lst.items.last()
            val sLines = s.split("\n")
            lst.accessLock.lock {
                if (sLines.size == 1)
                {
                    curLine.add(cify(s))
                }
                else for (line in sLines)
                {
                    curLine.add(cify(s))
                    curLine = mutableListOf()
                    lst.items.add(curLine)
                }
                lst.refresh()
            }
        }
    }

    override var items: MutableList<MutableList<Composing>>
        get() = lst.items
        set(value)
        {
            lst.accessLock.lock {
                lst.items = value
                lst.refresh()
            }
        }

    @Composable override fun compose(): Unit
    {
        lst.compose()
    }
}

/*
class ListOutputCol(mod: Modifier = Modifier): Output(),ComposingContainer
{
    val lstState = LazyListState()
    val lst = CCLazyColumn(lstState)

    init {
        lst.wrapperMod = mod
    }

    //val lst = MutableStateFlow<List<Composing>>(listOf())
    // Horribly inefficient
    override fun stdout(s:Any?)
    {
        if (s != null)
        {
            lst.accessLock.lock {
                lst.items.add(mutableListOf(cify(s)))
                lst.refresh()
            }
        }
    }
    override fun error(s:String?)
    {
        if (s != null)
        {
            lst.accessLock.lock {
                lst.items.add(mutableListOf(cify(s)))
                lst.refresh()
            }
        }
    }
    override fun info(s:String?)
    {
        if (s != null)
        {
            lst.accessLock.lock {
                lst.items.add(mutableListOf(cify(s)))
                lst.refresh()
            }
        }
    }

    override var items: MutableList<MutableList<Composing>>
        get() = lst.items
        set(value)
        {
            lst.accessLock.lock {
                lst.items = value
                lst.refresh()
            }
        }

    @Composable override fun compose(): Unit
    {
        lst.compose()
    }
}
*/

/** Create a horizontal bar which can accept any class that implements ComposeFace.
 * In other words, implement a button or action bar
 */
@cli(Display.Dev, "A row in which UI elements can be placed")
class HorizDropTarget: DropTarget,TriggeredRefresh()
{
    lateinit var lstate : LazyListState
    val contents = mutableStateListOf<Composing>()
    var background by Delegates.observable<Color>(defaultDropTargetBackground, this::changed)
    var bkgColStack = mutableListOf<Color>()

    @cli(Display.Dev, "Remove all items")
    fun clear()
    {
        contents.clear()
    }

    @cli(Display.Dev, "Add an item to the end of this row")
    override fun add(item: Composing, pos: Offset?)
    {
        contents.add(item)
    }

    @cli(Display.Dev, "Add an item before the passed item (by index) in this row")
    fun add(index: Int, item: Composing)
    {
        contents.add(index, item)
    }

    @cli(Display.Dev, "Remove an item from this row")
    override fun remove(item: Composing): Boolean
    {
        return contents.remove(item)
    }

    @cli(Display.Dev, "Remove the 0-based Nth item from this row")
    fun remove(itemIdx: Int): Composing
    {
        return contents.removeAt(itemIdx)
    }

    @cli(Display.Dev, "Remove an item based on id")
    fun remove(itemId: String)
    {
        for (i in contents)
        {
            if (i.id() == itemId) contents.remove(i)
        }
    }

    override fun highlight(push: Boolean, purpose: String?)
    {
        if (push)
        {
            bkgColStack.add(background)
            background = highlightBackgroundColor
        }
        else
        {
            background = bkgColStack.removeLastOrNull() ?: defaultDropTargetBackground
            refresh()
        }
        for (c in contents) c.highlight(push, purpose)
    }

    @Composable
    fun init()
    {
        lstate = rememberLazyListState()
    }

    @Composable
    override fun compose()
    {
        onRefresh {
            LazyRow(
                Modifier.background(background).heightIn(30.dp, 100.dp).fillMaxWidth().padding(0.dp), contentPadding = PaddingValues(0.dp), horizontalArrangement = Arrangement.spacedBy(0.dp), state = lstate
                   )
            {
                items(contents) {
                    it.compose()
                }
            }
        }
    }
}

/** The ongoing state of the entire panel that's housing the interpreter */
class InterpreterPanelState(val dw: WallyEnterpriseWallet)
{
    val CB_KEY = 1238974583
    var prompt by mutableStateOf("0>")
    //var text by mutableStateOf(TextFieldValue(text = ""))
    var bindingInfo by mutableStateOf("")
    var walletInfo by mutableStateOf("")
    var blockchainInfo by mutableStateOf("")
    var blockchainDisplay = mutableStateMapOf<ChainSelector, BlockchainFace>()
    var walletDisplay = mutableStateMapOf<String, WalletFace>()
    var helpForObject by mutableStateOf("")
    var help by mutableStateOf("")

    var completionPrefix by mutableStateOf("")  // command prefix that has nothing to do with this completion
    var completionObject by mutableStateOf("")
    var completing by mutableStateOf("")
    var completionChoices =  mutableStateOf<List<String>>(listOf()) //mutableStateListOf<String>()   // mutableStateOf<SortedMap<String, Any?>>(sortedMapOf())
    var completionUpdater by mutableStateOf<((String)->Unit)?>(null)

    var walletCallbackRegistrations = mutableMapOf<Wallet, Int>()
    var blockchainCallbackRegistrations = mutableMapOf<Blockchain, Int>()

    // Don't use mutable types in compose
    // https://developer.android.com/jetpack/compose/state#use-other-types-of-state-in-jetpack-compose
    //val console by mutableStateOf(listOf<CmdRepresentation>())
    val console = mutableStateListOf<CmdRepresentation>()
    var totalHeight by mutableStateOf(0f)

    var top = CCVerticalSash()

    /** Automatically scroll to what changed, when it changes */
    var autoScrollToChange = false

    fun sizeChanged(old: Size, new: Size)
    {
        val tmp = totalHeight + new.height - old.height
        if (tmp != totalHeight)
        {
            LogIt.info("Console height delta: ${new.height} - ${old.height}: $tmp")
            totalHeight = if (tmp < 0) 0f else tmp
        }
    }

    fun updateConsole(itemIndex: Int, item: CmdRepresentation)
    {
        val tmp = console[itemIndex]
        tmp.sizeChanged.remove(CB_KEY)
        console.set(itemIndex, item) // sets the element at 'itemIndex' to 'item'
        item.sizeChanged.add(CB_KEY, ::sizeChanged)
        val newHeight = totalHeight + item.lastSize.height - tmp.lastSize.height
        if (newHeight != totalHeight)
            totalHeight = if (newHeight<0) 0f else newHeight
    }


    lateinit var consoleState : LazyListState
    lateinit var toolbar: HorizDropTarget

    val guiPool = ThreadJobPool("guiJobs", 4)

    /** Composable things must be called in a composable function, so we need a separate init function for them */
    @Composable
    fun init()
    {
        consoleState = rememberLazyListState()
        toolbar = HorizDropTarget()
        toolbar.init()
        totalHeight = 0f
    }


    /** Create a new spot in the console where output can be appended.
     * Note that you can have a few of these going on at the same time (if you fork), potentially causing things to update all over the screen rather
     * than just at the bottom as is traditional.
     */
    fun newOutput(i: Long, cmd:String): Output
    {
        val tmp = ObjectCmd(cmd)
        console.add(tmp)
        tmp.sizeChanged.add(CB_KEY, ::sizeChanged)
        return CmdOutput(console.size-1, this)
    }

    /** Append some text to a console output item
     * @param idx The index of the console output item you want to append to
     * @param s What string to append
     */
    fun appendText(idx:Int, s:String?)
    {
        if (s == null) return
        if (console.size == 0) return
        // To trigger a redraw, you need to change the object in the list, you can't just modify the object
        val cur = console[idx]
        val newObj = cur.copyAndMod(s)
        updateConsole(idx, newObj)
        flush()
        updateUI()
        if (autoScrollToChange) laterUI { delay(100); consoleState.scrollToItem(idx) }
    }

    /** Append some object to a console output item
     * @param idx The index of the console output item you want to append to
     * @param s What to append
     */
    fun append(idx:Int, s:Any?)
    {
        if (s == null) return
        if (console.size == 0) return
        // To trigger a redraw, you need to change the object in the list, you can't just modify the object
        val cur = console[idx]
        val newObj = cur.copyAndMod(s)
        updateConsole(idx, newObj)
        flush()
        updateUI()
        if (autoScrollToChange) laterUI {
            try
            {
                delay(100); consoleState.scrollToItem(idx)
            }
            catch (_: CancellationException)
            {
            }
        }
    }


    /** Redraw the full UI */
    fun updateUI()
    {
        laterUI {
            updateBlockchainDashboard()
            updateWalletDashboard()
            updateBindingDashboard()
        }
    }

    fun updateCommandOutput()
    {
        laterUI {
            for (c in console)
            {
                (c as? Refreshing)?.refresh()
            }
        }
    }

    /** Show any pending state changes */
    fun flush()
    {
        // consoleStr = console.toString()
    }

    /** Execute whatever command is in the command entry field, and update all fields with the result */
    suspend fun execute(cmd:String?)
    {
        var s:String? = cmd
        val oldPrompt = dw.prompt()
        val output = newOutput(dw.cmdCount(), oldPrompt + s + "\n")

        do  // Execute might resolve to a different command that needs to be run
        {
            flush()
            delay(100)
            s = dw.execute(s, output)
        } while (s != null)
        // Note for continuous updates as execution goes, you would have to do this periodically in the callback of execute
        flush()
        prompt = dw.prompt()
        // must delay so the scroll has gotten updated
        doUI {
            updateBindingDashboard()
            updateBlockchainDashboard()
            updateWalletDashboard()
            scrollConsoleToBottom()
            enteredCmdLine.value = TextFieldValue("")
        }
    }

    /** after a tiny delay, scroll the console to the last item */
    suspend fun scrollConsoleToBottom()
    {
        laterUI {
            delay(200);
            consoleState.scrollToItem(console.lastIndex)
        }
    }

    /** Update the dashboard blockchain text */
    fun updateBlockchainDashboard()
    {
        val result = StringBuilder()
        for (b in blockchains.values)
        {
            result.append(b.name + " at " + b.curHeight + " cnxns " + b.net.p2pCnxns.size + "\n")
            if (!blockchainDisplay.contains(b.chainSelector))  // Add any missing
            {
                blockchainDisplay[b.chainSelector] = BlockchainFace(b, true)
            }
        }
        /* TODO kills performance because the blockchain goes thru all old block headers */
        /*
        for (b in blockchainDisplay)  // Remove any that are gone
        {
            if (!blockchains.contains(b.key)) blockchainDisplay.remove(b.key)
            else b.value.refresh()
        }
         */
        blockchainInfo = result.toString()
    }

    /** Update the wallet dashboard text */
    fun updateWalletDashboard()
    {
        return
        val result = StringBuilder()

        for (w in dw.accounts)
        {
            // TODO walletdisplay: if (!walletDisplay.contains(w.key)) walletDisplay[w.key] = WalletFace(w.value, true)

            val curHeight = w.value.syncedHeight
            var nOutUnconf = 0
            var nInUnconf = 0
            var nTxUnconf = 0
            result.append(w.key + " on " + w.value.blockchain.chainSelector + " at " + w.value.syncedHeight + " balance " + w.value.balance + ":" + w.value.balanceUnconfirmed + "\n" )
            w.value.forEachTxo {
                if (it.spentUnconfirmed)
                {
                    nOutUnconf++
                }
                // count the incoming ignoring change
                if (it.commitUnconfirmed > 0 && !it.spentUnconfirmed)
                {
                    nInUnconf++
                }
                false
            }
            w.value.forEachTx {
                if (it.confirmedHeight <= 0)
                {
                    nTxUnconf++
                }
                false
            }

            val stats = (w.value as? CommonWallet)?.statistics()
            stats?.let {
                result.append("  " + stats.numUsedAddrs + " used of " + (stats.numUsedAddrs+stats.numUnusedAddrs) + " addresses.\n")
                result.append("  " + stats.numTransactions + " total transactions.\n")
                result.append("  Last receive in block " +stats.lastReceiveHeight+ " last send in block " + stats.lastSendHeight + ".\n")
            }
            result.append("  " + nTxUnconf + " unconfirmed transactions\n")
            result.append("  " + nOutUnconf + " outgoing unconfirmed coins\n")
            result.append("  " + nInUnconf + " incoming unconfirmed coins\n")
            w.value.forEachUtxo {
                if (it.isUnspent)
                {
                    result.append("  " + it.amount + " on " + it.addr + " (" + (curHeight - it.commitHeight) + " confs)  tx: ${it.commitTxIdem.toHex()}\n")
                }
                false
            }
        }
        walletInfo = result.toString()

        if (false) for (w in walletDisplay)  // Remove any that are gone
        {
            if (!dw.accounts.contains(w.key)) walletDisplay.remove(w.key)
            else w.value.trigger.value = w.value.trigger.value + 1
        }
    }

    /** Update the bindings dashboard (not working right now so not showing bindings) */
    fun updateBindingDashboard()
    {
        val skipThis = true
        if (!skipThis)
        {
            val result = StringBuilder()

            val global = dw.symbols()
            for(item in global)  // or ScriptContext.GLOBAL_SCOPE
            {
                result.appendLine(item.key) // + ":" + item.value.toString())
            }
            bindingInfo = result.toString()
        }
    }

    fun updateCompletionDashboard(unrelatedPrefix:String, objName:String, rest:String, options: List<String>?, updater: ((String)->Unit)?=null)
    {
        completionPrefix = unrelatedPrefix
        completionObject = objName
        completing = rest
        completionUpdater = updater
        if (options != null)
        {
            completionChoices.value = options
        }

    }
}

fun onEngineReset(ds: WallyEnterpriseWallet, panelState: InterpreterPanelState, firstTime:Boolean)
{
    if (!firstTime) panelState.toolbar.clear()
    ds.injectSymbol("_panel", panelState)
    ds.invisibleEval("""val toolbar: DropTarget get() = (bindings["_panel"]!! as InterpreterPanelState).toolbar""")
    ds.prompt = { ds.inputLines.size.toString() + "> " }
}

/** Create a new interpreter panel */
fun guiNewPanel(ds: WallyEnterpriseWallet)
{
    val screenSize = getScreenSize()
    val defaultStartingWindowWidth = screenSize.width * 4 / 5
    val defaultStartingWindowHeight = screenSize.height * 3 / 4
    val panelState = InterpreterPanelState(ds)

    onEngineReset(ds, panelState, true)
    ds.engResetActions.add({ onEngineReset(ds, panelState, false) })

    panelState.top.add(Composer {
        Column(
            Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Bottom
        )
        {
            Row(Modifier.fillMaxWidth().background(Color.LightGray)) { panelState.toolbar.compose() }
            // This is the terminal window
            Row(Modifier.fillMaxWidth().weight(1f).onKeyEvent {
                //val k = (it.nativeKeyEvent as KeyEvent).keyChar
                val k = it.utf16CodePoint.toChar()
                if ((k.isLetterOrDigit() || k.isWhitespace()) && it.type == KeyEventType.KeyDown)
                {
                    val tmp = enteredCmdLine.value.text + k
                    enteredCmdLine.value = TextFieldValue(text = tmp, selection = TextRange(tmp.length))
                    if (k == '\n') launch { panelState.execute(tmp) }

                    true
                }
                else if (it.key.toString() == "Key: Alt") // || it.key.toString() == "Key: ⌃")
                {
                    if (it.isAltPressed)
                    {
                        panelDragMode.value = true
                    }
                    else
                    {
                        panelDragMode.value = false
                    }
                    false
                }
                else false
            }
               )
            {
                // Terminal window
                SelectionContainer()
                {
                    ConsolePanel(panelState)
                }
            }
            // This is the gray space on the bottom to enter new commands
            Row(
                Modifier.fillMaxWidth().background(Color.LightGray), verticalAlignment = Alignment.CenterVertically
            ) {
                val v = panelState.dw.processing.collectAsState().value
                Text(
                    if (v == null) panelState.prompt else "Processing: $v",
                    fontSize = defaultTextFontSize, fontStyle = FontStyle.Italic, fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.background(Color.LightGray),
                )
                CommandEntry(panelState)
            }
        }
    })

    panelState.top.add(Composer {
        DashboardPanel(panelState)
    })

    panelState.top.setSashWidth(0, defaultStartingWindowWidth * 75 / 100)


    Thread {
        while(true)
        {
            for (w in panelState.dw.accounts.values)
            {
                panelState.walletCallbackRegistrations.getOrPut(w, {
                    w.setOnWalletChange { wal, txes -> panelState.updateWalletDashboard() }
                })
            }
            for (b in blockchains.values)
            {
                panelState.blockchainCallbackRegistrations.getOrPut(b, {
                    b.onChange.add({ panelState.updateBlockchainDashboard() }) })
            }
            millisleep(250UL)
        }
}

    application(false)
    {
        initGlobals()
        panelState.init()

        LaunchedEffect("init") {
            delay(500)
            try
            {
                // Execute plugins script if it exists
                if (true)
                {
                    val (file, contents) = ds.resolveScript("plugins.kts")
                    if (file != null)
                    {
                        val output = panelState.newOutput(ds.cmdCount(), "executing plugin script ${file.toString()} last modified on ${Date(file.lastModified())}")
                        ds.executeScript(contents, output)
                        panelState.updateUI()
                        panelState.flush()
                    }
                }
                // Execute startup script if it exists
                val (file, contents) = ds.resolveScript("init.kts")
                if (file != null)
                {
                    val output = panelState.newOutput(ds.cmdCount(), "executing startup script ${file.toString()} last modified on ${Date(file.lastModified())}")
                    ds.executeScript(contents, output)
                    panelState.updateUI()
                    panelState.flush()
                }
            } catch (e: Exception)
            {
                handleThreadException(e)
            }
        }


        var isOpen by remember { mutableStateOf(true) }
        if (isOpen)
        {
            val icon = painterResource("icon128.png")
            //val icon = useResource("icon.svg") {
            //    loadSvgPainter(it, LocalDensity.current)
            //}
            Tray(icon = icon,
                 menu = {
                    Item("Exit", onClick = ::exitApplication)
                })
            val w = Window(
                onCloseRequest = { isOpen = false },
                title = _x_(WallyTitle),
                icon = icon,
                state = rememberWindowState(width = defaultStartingWindowWidth, height = defaultStartingWindowHeight)
            )
            {
                MaterialTheme()
                {
                    panelState.top.compose()
                }
            }
        }
    }
}


/** Generate a QR code for the provided text at the provided size
 * @param text the text the QR image encodes
 * @param sz the size of the QR image in pixels
 */
fun generateQRCodeToBufferedImage(text: String, sz:Int=500): BufferedImage?
{
    val width = sz
    val height = sz

    try
    {
        val codeWriter = MultiFormatWriter()
        val bitMatrix = codeWriter.encode(text, BarcodeFormat.QR_CODE, width, height)
        val image = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        for (x in 0 until width)
        {
            for (y in 0 until height)
            {
                image.setRGB(x, y, if (bitMatrix[x, y]) java.awt.Color.BLACK.rgb else java.awt.Color.WHITE.rgb)
            }
        }
        return image
    }
    catch (e: WriterException)
    {
        // Handle the exception.
    }
    return null
}


/** Generate a QR code for the provided text at the provided size
 * @param text the text the QR image encodes
 * @param sz the size of the QR image in pixels
 */
fun generateQRCode(text: String, sz:Int=500): ImageBitmap?
{
    val width = sz
    val height = sz

    try
    {
        val codeWriter = MultiFormatWriter()
        val bitMatrix = codeWriter.encode(text, BarcodeFormat.QR_CODE, width, height)
        val image = BufferedImage(width, height, BufferedImage.TYPE_INT_RGB)
        for (x in 0 until width)
        {
            for (y in 0 until height)
            {
                image.setRGB(x, y, if (bitMatrix[x, y]) java.awt.Color.BLACK.rgb else java.awt.Color.WHITE.rgb)
            }
        }
        val im = image.toComposeImageBitmap()

        return im
    }
    catch (e: WriterException)
    {
        // Handle the exception.
    }
    return null
}

fun ImageBitmap.savePng(f: File)
{
    val data = this.toAwtImage().toImage().encodeToData(EncodedImageFormat.PNG)
    FileOutputStream(f).use { outputStream -> outputStream.write(data!!.bytes) }
}

