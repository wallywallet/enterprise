package org.wallywallet.wew.examplePlugin  // The name of this package MUST be org.wallywallet.wew.<name of file before -version>
import org.wallywallet.wew.Output
import org.wallywallet.wew.WallyEnterpriseWallet
import org.wallywallet.wew.cli.*

class Initializer
{
    fun CliInstaller(wew: WallyEnterpriseWallet, output:Output):Unit
    {
        oprintln(output,"running initializer")
        wew.eval("import examplePlugin.*", output)
        wew.eval("""toolbar.add(examplePlugin.exampleTextEntry)""", output)
        wew.eval("""toolbar.add(button("Example", { examplePlugin.example() }))""", output)
        oprintln(output, "initializer completed")
    }
}