package examplePlugin

import org.wallywallet.wew.cli.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.nexa.libnexakotlin.*
import java.io.File
import org.nexa.nft.*
import org.wallywallet.composing.*
import org.wallywallet.wew.*
import java.io.FileNotFoundException

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import org.nexa.threads.millisleep
import org.wallywallet.wew.Objectify
import java.nio.file.Path
import kotlin.io.path.*

private val LogIt = GetLog("examplePlugin")

val exampleTextEntry = textentry("echoer")

fun example()
{
    print("\nHello ", exampleTextEntry.value, " from the example plugin!\n")
}