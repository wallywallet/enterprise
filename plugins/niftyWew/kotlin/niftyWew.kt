package niftyWew

import androidx.compose.animation.core.snap
import androidx.compose.foundation.layout.*
import org.wallywallet.wew.cli.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.nexa.libnexakotlin.*
import java.io.File
import org.nexa.nft.*
import org.wallywallet.composing.*
import org.wallywallet.wew.*
import java.io.FileNotFoundException

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshotFlow
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.vector.Group
import androidx.compose.ui.util.fastJoinToString
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import okio.buffer
import org.nexa.threads.millisleep
import org.wallywallet.wew.Objectify
import java.nio.file.Path
import kotlin.io.path.*
import okio.sink
import okio.source
import org.nexa.libnexakotlin.simpleapi.ofToken
import org.nexa.libnexakotlin.simpleapi.payTo
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

private val LogIt = GetLog("niftyWew")

// A place for us to store NFT files
var NFT_DIR = "nfts"

class NftException(s:String): Exception(s)


val NiftyArtTandC_1_0 = """NiftyArt Terms and Conditions V1.0
    
 Definitions:
    NiftyArt NFT: A unique piece of data that is described by a record in a blockchain.  This record contains a probabilistically unique identifier of a data file containing and/or describing the represented Work.
    Owner/Ownership: (of the NiftyArt NFT)  Modifying, destroying, or transferring an NiftyArt NFT record (called a transaction) is defined by rules specified by the blockchain and by each NiftyArt NFT record.  Successfully following these rules requires pieces of data that are provided as part of the transaction.  Ownership of an NiftyArt NFT is defined as possessing the ability to provide the data required to confirm a transaction on the blockchain.  Proving ownership occurs solely by one of two mechanisms.  First, by executing and confirming said transaction on the blockchain.  This confirms Ownership for the exact duration of the transaction.  Second, by producing a unique transaction upon demand with the correct required data but that is not admissible onto the blockchain for some other reason.  This confirms Ownership until the NFT blockchain record is modified or destroyed.  Every use of the word 'Owner' in these Terms and Conditions implies cryptographic verification of ownership as described.
    Creator: (of the NiftyArt NFT) The entity that originally created the NiftyArt NFT on the blockchain.
    Work: The artwork referenced by this NiftyArt NFT.  The Work may be fully embodied within files named 'public' and/or 'private'.  Additionally, the Work may include a physical and/or external components as specified by the 'info' field within the 'info.json' file.
    Owner Work:  The portion of the Work visible only by the Owner, as described below.
    Public Work:  The portion of the Work visible to anyone, as authorized by the Owner and these Terms and Conditions.
    Service: A third party entity that facilitates the use, inclusion, participation, management, storage, display or trade of NiftyArt NFTs.
   
 Inseparability of Work from the NiftyArt NFT:
    Ownership of this NiftyArt NFT confers all licenses to the Work described herein.  Transfer of this NiftyArt NFT transfers the same.  The licenses described below are forever conferred by and inseparable from Ownership of this NiftyArt NFT.
    The Owner does NOT have the right to create or enter into any contract, agreement or document, electronic, physical or otherwise that changes or overrides the NiftyArt NFT's licenses to the Work as described herein.
 
Visibility of the Work:
   Third parties may reproduce and publish the media files located within this NiftyArt NFT prefixed by 'cardf' and 'cardb' (with various suffixes and extensions) and the data within the 'info.json' file within the context of identifying this NiftyArt NFT.  The fair use of the title and author of a book shall act as precedent for when this data may be published.
   If media files or directories prefixed by 'owner' (with various suffixes and extensions) exist, this data is the Owner Work.   No Service may use, copy, display, or publish this file except to a proven current Owner.  Services that manage, store, use, or trade NiftyArt NFTs may store this file on behalf of and for sole use by the Owner.
   If media files or directories prefixed by 'public' (with various suffixes and extensions) exist, this data is the Public Work.  'cardf' and 'cardb' prefixed files are also within the Public Work.  Control of the Public Work is specific to each NFT and described in the following sections.

Creator Copyright Ownership:
  The Creator of the NiftyArt NFT asserts that they had sole ownership of exclusive copyright to the Work, had the right to transfer that ownership, and that the creation of this NiftyArt NFT inseparably binds the copyright and/or licenses described herein to Ownership of this NiftyArt NFT.
  
"""

val LicenseToPublish = "The Owner of the NiftyArt NFT that includes this notice is hereby granted an unlimited, worldwide license to use, copy, display and publish the Public Work described by the NiftyArt NFT for personal or commercial use.  This right may not be delegated.  This right is not exclusive.  Upon transfer of this NiftyArt NFT, the previous Owner loses all rights to reproduce and publish the public Work.  The public Work must be removed from all locations under the control of the previous Owner, including but not limited to web sites, software, and mobile/tablet applications."

val LicenseToAssociate = """
A Service may display or use of the Public Work as an avatar and/or icon associated with the Owner for the purpose of identifying the Owner to other participants.   
The service may display or use the Public Work on behalf of the Owner while the Owner is using the service.  When the Owner leaves the service, the use or display must cease.
For example, the Work may be used within Services by the Owner as the Owner's avatar or as an ancillary item that unlocks additional functionality or abilities.  
Once the Public Work has been displayed or used in the manner described above, the historical record of that use may continue to be displayed, even if the Owner transfers the NFT, provided that the display or use clearly indicates the time and date of the original use."""

val LicenseToDerivativeWorks = """
The Owner of the NiftyArt NFT that includes this notice is hereby granted an unlimited, worldwide license to use, copy, and display the Public Work for the purpose of creating derivative works based upon the Public Work (“Commercial Use”).
If these derivative works are NiftyArt NFTs, they must be significantly and obviously distinct from this Work.  Examples of such Commercial Use would e.g. be narratives involving the Public Work, additional likenesses of the Public Work, combining 2 or more NiftyArt NFTs to create new NiftyArt NFTs with some traits from the originals, or merchandise products displaying copies of or in some way embodying the Public Work.
""".trimIndent()

fun CommonWallet.forEachAsset(doit: (org.nexa.libnexakotlin.Spendable) -> kotlin.Boolean): Unit
{
    forEachUtxo {
        val gi = it.groupInfo
        if (gi != null)
        {
            doit(it)
        }
        else false
    }
}

fun CommonWallet.numAssets(): Long
{
    var count = 0L
    forEachAsset {
        count++
        false
    }
    return count
}

fun CommonWallet.find(gid: GroupId):List<Spendable>
{
    val ret = filterInputs(Long.MAX_VALUE, 0) {
        if (it.groupInfo?.groupId == gid) it.groupInfo?.tokenAmt ?: 1
        else 0
    }
    return ret
}

fun CommonWallet.ownsSome(gid: GroupId):Boolean
{
    val ret = filterInputs(1, 0) {
        if (it.groupInfo?.groupId == gid) 1
        else 0
    }
    return ret.size > 0
}


fun String.filenameify(rep: Char = '_'):String
{
    val invalidChars = "[]()\\/:*?\"\'<>|"
    val ca = this.toCharArray()
    for (idx in 0 until ca.size)
    {
        if (ca[idx] in invalidChars) ca[idx] = rep
        if (ca[idx].isWhitespace()) ca[idx] = rep
        if ((ca[idx].category == CharCategory.CONTROL) || (ca[idx].category == CharCategory.FORMAT)) ca[idx] = rep
    }
    return ca.toString()
}

@OptIn(ExperimentalPathApi::class)
fun createNfts(dir: Path, w: Wallet, url:String)
{
    val items = dir.walk(PathWalkOption.BREADTH_FIRST)
    items.sorted().forEach {
        if ((it.extension == "nft")||(it.extension == "zip"))
        {
            org.wallywallet.wew.cli.println("Creating NFT for file $it")
            runBlocking {
                CreateNiftyArtNft(it.toFile(), w, 1, url)
            }
            org.wallywallet.wew.cli.println("\n")
            millisleep(2000UL)
        }
    }
}

@Serializable
data class CreateZipResponse(val id: String?=null, val tx: String?=null, val error: String? = null)

suspend fun CreateNiftyArtNft(nftFile: File, wallet: Wallet, quantity: Long = 1, niftyArtUrl: String="http://niftyart.cash", pout: Output?=null)
{
    val NFT_CREATE_MAX_PRICE = 1000000  // nexa satoshis
    val client = HttpClient(CIO) {
        engine {
            requestTimeout = 300_000  // 5 min for debugging
        }
    }
    val dest = niftyArtUrl + "/_api/0/createFromZip"
    //oprintln(pout, dest)

    val response: HttpResponse = client.submitFormWithBinaryData(
        url = dest,
        formData = formData {
            append("quantity", quantity)
            append("file", nftFile.readBytes(), Headers.build {
                append(HttpHeaders.ContentType, ContentType.Application.OctetStream)
                append(HttpHeaders.ContentDisposition, """filename="${nftFile.name}"""")
            })
        })

    LogIt.info("Niftyart creation request submitted")
    val resptext = response.bodyAsText()
    // oprint(pout, "NiftyArt proposes: $resptext")
    val js = Json
    val resp = js.decodeFromString(CreateZipResponse.serializer(), resptext)
    if (resp.error != null)
    {
        LogIt.info("Niftyart response error ${resp.error}")
        throw Exception(resp.error)
    }
    client.close()
    if (resp.tx == null) throw Exception("No error or half-transaction provided")
    val tx = txFromHex(wallet.chainSelector, resp.tx)
    // org.wallywallet.wew.cli.println(tx)
    for (out in tx.outputs)
    {
        val gi = out.groupInfo()
        if (gi !=null)
        {
            val grpId = gi.groupId
            LogIt.info("GroupId:  ${grpId}  Hex: ${grpId.toHex()}")
            oprint(pout, "GroupId: ", grpId.toString(), " ", grpId.toHex())
        }
    }
    if (tx.outputTotal - tx.inputTotal > NFT_CREATE_MAX_PRICE)
    {
        LogIt.info("NFT is too expensive at: ${(tx.outputTotal - tx.inputTotal)/100} nexa!")
        throw Exception("NFT is too expensive at: ${(tx.outputTotal - tx.inputTotal)/100} nexa!")
    }
    // TODO make sure it pays an NFT to me
    LogIt.info("txcompleter")
    wallet.txCompleter(tx, 0, TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.SIGN or TxCompletionFlags.BIND_OUTPUT_PARAMETERS)
    LogIt.info("send")
    wallet.send(tx)
    LogIt.info("NFT created!  Transaction ${tx.idem.toHex()}\nHex: ${tx.toHex()}")
    oprintln(pout,"NFT created!  Transaction ${tx.idem.toHex()}\nHex: ${tx.toHex()}")
    oprintln(pout, tx)
}


/** Make NFT data files from images in a source directory and outputs them to a destination directory */
fun makeNftDataFiles(fromDir: File, toDir: File, titleSpec:String="", author:String="", collection:String="", cat:String?=null, kw:List<String> = listOf(), info:String="",
                     license: String="", bindata: ByteArray? = null, appuri: String = "", quantity:Long? = 1, out: Output? = null, stopper: Objectify<Boolean>? = null)
{
    if (!fromDir.exists()) throw Exception("$fromDir does not exist")
    if (!toDir.exists()) throw Exception("$toDir does not exist")
    if (!toDir.isDirectory()) throw Exception("$toDir is not a directory")
    if (toDir.listFiles()!!.size>0) throw Exception("$toDir must be empty")


    val j = Json

    var idx = 0
    while(stopper?.obj != true)
    {
        idx++

        var differentiated = true
        val title = try
            {
                titleSpec.format(idx)
            }
            catch(e: Exception)
            {
                differentiated = false
                titleSpec
            }

        // Back and front files can be custom to the index or not
        val backf = fromDir.resolveNftMedia("back/back$idx") ?: fromDir.resolveNftMedia("back$idx") ?: fromDir.resolveNftMedia("back")
        if (backf == null)
        {
            oprintln(out, "back file for $idx does not exist.")
            break
        }
        val frontf = fromDir.resolveNftMedia("front/front$idx") ?: fromDir.resolveNftMedia("front$idx") ?: fromDir.resolveNftMedia("front")
        if (frontf == null)
        {
            oprintln(out, "Note: front file for $idx does not exist (the public file will be used).  Tried 'front/front$idx.*', 'front$idx.*', or just 'front.*'")
        }
        // Public file MUST be customized for the index
        //val publicf = fromDir.resolveNftMedia("public/public$idx") ?: fromDir.resolveNftMedia("public$idx")
        val publicf = fromDir.resolveNftMedia("public", Regex(".*\\D$idx\$")) ?: fromDir.resolveNftMedia("public$idx")
        if (publicf == null)
        {
            oprintln(out, "Error: public file for $idx does not exist.  Tried 'public/*$idx.*', 'public$idx.*'")
            break
        }

        val ownerf = fromDir.resolveNftMedia("owner/owner$idx") ?: fromDir.resolveNftMedia("owner$idx") ?: fromDir.resolveNftMedia("owner")

        // ExtraJson is optional
        val extrajsf = fromDir.resolveExists("extraJson/extraJson$idx.json") ?: fromDir.resolveExists("extraJson$idx.json") ?: fromDir.resolveExists("extraJson.json")

        // ExtraData file is optional
        // val extraDataFile = fromDir.resolveExists("data/data$idx") ?: fromDir.resolveExists("data$idx") ?: fromDir.resolveExists("data")

        oprintln(out,"processing $idx using  front: ${frontf?.name}  public: ${publicf.name}  back: ${backf.name}  json: ${extrajsf?.name}\n")

        // Remove windows style line endings for consistency
        val jdata = extrajsf?.readText()?.replace("\r","")

        if (jdata != null)
        {
            j.parseToJsonElement(jdata)
        }

        val cd = NFTCreationData(
            dataFile = publicf.absolutePath, mediaType = publicf.canonicalExtension,
            ownerFile = ownerf?.absolutePath, ownerMediaType = ownerf?.canonicalExtension,
            cardFrontFile = frontf?.absolutePath, cardfMediaType = frontf?.canonicalExtension,
            cardBackFile = backf.absolutePath, cardbMediaType = backf.canonicalExtension,
            license = license, bindata = bindata,
            title = title, series = collection, author = author,
            keywords = kw,  info = info, category = cat,
            appuri = appuri,
            data = jdata ?: "",
            quantity = quantity ?: idx.toLong()  // If quantity is passed as null, the quantity is set to the index so the higher you go the more exist
        )

        //val outputFile = if (differentiated) toDir.resolve("${title.filenameify()}.nft") else toDir.resolve("${title.filenameify()}$idx.nft")

        try
        {
            val finalFile = makeNftyZip(toDir.toPath(), cd)
            oprintln(out,"Created" + finalFile.toAbsolutePath())
        }
        catch(e: FileNotFoundException)
        {
            oprint(out, "Missing file or directory: ${e.stackTraceToString()}")
        }
    }
}

fun makeNifties(wal: Wallet?=null, titleIn:String="", authorIn:String="", collectionIn:String="", catIn:String="", kwIn:String="", infoIn:String="", licenseIn: String = "", inDir:String?=null, outDir:String?=null)
{
    val exampleTitles = text("")
    val change: (()->Boolean) = { true }

    // Just grab a wallet
    val wallet:Wallet = wal ?: WallyEnterpriseWallet.accounts.toList().first().second

    fun calcExampleTitles(fmt: String):String
    {
        val sb = StringBuilder()
        for (i in listOf(1,2,3))
        {
            try
            {
                sb.append(fmt.format(i))
            }
            catch(e: Exception)
            {
                sb.append(fmt)
            }
            sb.append(", ")
        }
        sb.append("...")

        return sb.toString()
    }

    val title = textentry("Title", titleIn, {
        change()
        exampleTitles.text.value = calcExampleTitles(it)
        true
    })
    exampleTitles.text.value = calcExampleTitles(titleIn)
    val author = textentry("Author", authorIn)
    val collection = textentry("Collection", collectionIn)


    val subheading = 3
    print(CCText("NFT Title: ", headingTextStyle(subheading)))
    println(title)
    println("Use %01d, %02d, %03d, etc in the title field to specify the NFT series number. '03' means a series number with preceding zeros that is 3 digits.")
    print(CCText("NFT Author: ",headingTextStyle(subheading)), author, 20.dp)
    print(CCText("NFT Collection (or series): ",headingTextStyle(subheading)), collection, 20.dp)

    val category = textentry("Category", catIn, {change()})
    val keywords = textentry("Keywords", kwIn, {change()})
    val info = textentry("Information", infoIn, {change()})
    info.modifier = info.modifier.fillMaxWidth()

    category.modifier.wrapContentSize(Alignment.CenterStart)
    keywords.modifier.wrapContentSize(Alignment.CenterStart)

    info.maxLines = 1000

    print(CCText("Category: ",headingTextStyle(3)), category)
    println("The category is a set of dot separated words (for example: NFT.PFP).  Wallets may organize large NFT collections based on category.\nTop level categories are:" +
            """ "coin", "NFT", "ticket", "identity", "coupon", "security", "item".  You may make up subcategories. """)
    println(CCText("Enter comma separated keywords:", headingTextStyle(3)))
    println(keywords)
    println(CCText("Text Information:", headingTextStyle(3)))
    println(info)

    println(CCText("\nSummary:",headingTextStyle(subheading)))
    println(CCText("Titles are: ",headingTextStyle(5)), exampleTitles, CCText("  Author is: ",headingTextStyle(5)), author.value,
          CCText("  Collection is: ",headingTextStyle(5)), collection.value,
          CCText("  Category is: ",headingTextStyle(5)), category.value)
    println(CCText("Keywords are: ",headingTextStyle(5)), keywords.value)
    println(CCText("Information is: ",headingTextStyle(5)), info.value, CNL)
    val errors = MutableStateFlow<String>("")

    //val nftDir = CCDropDir(text("Drag and Drop a directory here that contains all of your NFT data files"), height = 80.dp)
    val nftDir = CCDirChooser("Choose", inDir) {
        if (!it.exists())
        {
            errors.value = "NFT source data directory does not exist"
            false
        }
        else
        {
            val err = StringBuilder()
            if (!it.resolve("back").exists() && (null == it.resolveNftMedia("back")))  err.append("'back' subdirectory does not exist\n")
            if (!it.resolve("front").exists()) err.append("'front' subdirectory does not exist\n")
            if (!it.resolve("public").exists()) err.append("'public' subdirectory does not exist\n")
            errors.value = err.toString()
            true
        }
    }
    val destDir = CCDirChooser("Choose", outDir) {
        if (!it.exists())
        {
            errors.value = "Output directory does not exist"
            false
        }
        else true
    }

    println(CCText("Directory containing all your NFT data subdirectories and data files:",headingTextStyle(3)))
    println("This directory should contain subdirectories named 'back', 'front', 'public', 'owner', and 'extraJson'.  Inside those directories should be files named <dir name><number>.  For example: public001.png.  " +
            "If you want the same data for every NFT, for front, back, owner and extraJson, you can eliminate the directory and use a single file, for example 'front.jpg'.")
    println(nftDir)
    println(CCText("NFT data file output Directory:",headingTextStyle(3)))
    println(destDir)

    /*
    fun makeCmd()
    {
        command.text.value = """makeNfties("${title.value.value}")"""
    }
    change = { makeCmd(); true }
    makeCmd()
     */
    println(CCText("Command line (your selections above can be auto-inserted with the following call)", headingTextStyle(3)))
    //print(command)
    val qcq = "\",\""
    val qc = "\","
    println("makeNifties(wallet, \"", title.value, qcq, author.value, qcq, collection.value, qcq, category.value, qcq, keywords.value, qcq, info.value, qc, "inDir=\"", nftDir.value, qc, "outDir=\"", destDir.value, "\")" )
    val genOutput = ListOutput(Modifier.heightIn(50.dp, 600.dp))

    println()
    println(CCText("Errors:",headingTextStyle(3)))
    println(errors)

    val stop = Objectify<Boolean>(false)
    println(button("Make NFT data files (this does not write to the blockchain)") {
        laterJob {
            stop.obj = false
            val err = StringBuilder()
            val nftDirFrozen = nftDir.value.value
            if (nftDirFrozen == null)
            {
                err.append("No data file directory chosen.\n")
            }
            val destDirFrozen = destDir.value.value
            if (destDirFrozen == null)
            {
                err.append("No data file directory chosen.\n")
            }
            if (title.get() == "") err.append("No title.\n")
            if (author.get() == "") err.append("No author.\n")
            errors.value = err.toString()
            if (err.toString().isBlank())  // no errors found so try to continue
            {
                val kw = keywords.get().split(',').map { it.trim() }
                try
                {
                    makeNftDataFiles(
                        nftDirFrozen!!, destDirFrozen!!, title.get(), author.get(), collection.get(), category.get(), kw, info.get(),
                        license = licenseIn,
                        out = genOutput,
                        stopper = stop
                    )
                }
                catch (e: Exception)
                {
                    errors.value = e.message ?: e.stackTraceToString()
                }
            }
        }
    }, button("Abort") {
        stop.obj = true
        Unit
    })
    println(CCText("Datafile Execution:",headingTextStyle(3)))
    println(CCScroll(genOutput.lstState, genOutput))

    val availNftFiles = text("")
    var nfiles = 0
    launch {
        destDir.value.collect {
            nfiles = 0
            if (it != null && it.exists())
            {
                it.toPath().forEach { if (it.isRegularFile()) nfiles++ }
            }
            availNftFiles.text.value = "$nfiles are available to send to NiftyArt."
        }
    }

    val stopm = Objectify<Boolean>(false)
    val blockchain = wallet.blockchain.name
    val niftyArtUrl = NIFTY_ART_WEB[wallet.chainSelector]
    val nftOutput = ListOutput(Modifier.heightIn(50.dp, 600.dp))
    println(button("Make NiftyArt NFTs (in $blockchain) using $niftyArtUrl") {
        stopm.obj = false
        val nd = destDir.value.value
        if (nd != null)
        {
            laterJob("MakeNiftyArtNfts") {
                val files = nd.listFiles().sorted()
                for (p in files)
                {
                    if (stopm.obj == true) break
                    if (p.isFile())
                    {
                        oprintln(nftOutput, "Creating ${p}")
                        try
                        {
                            runBlocking {
                                try
                                {
                                    CreateNiftyArtNft(p, wallet, 1, niftyArtUrl!!, pout=nftOutput)
                                }
                                catch (e: Exception)
                                {
                                    println("  Error in coroutine ${e.message}")
                                }
                            }
                            millisleep(2000U)
                        }
                        catch (e: Exception)  // probably already exists
                        {
                            oprintln(nftOutput, "  Error ${e.message}")
                            if (e.message == "already exists")
                            {
                                p.delete()
                                org.wallywallet.wew.cli.millisleep(1000U)
                            }
                            else org.wallywallet.wew.cli.millisleep(500U)
                        }
                    }
                }
            }
        }
    },
        button("Abort") {
            stopm.obj = true
            Unit
        }, "  into account: ", wallet.name)
    println(CCText("NFT creation Execution:",headingTextStyle(3)))
    println(CCScroll(nftOutput.lstState, nftOutput))
}

fun checkDupAssets(w: CommonWallet)
{
    val aMap = mutableMapOf<GroupId, Spendable>()
    w.forEachAsset {
        if (aMap.contains(it.groupInfo!!.groupId))
        {
            org.wallywallet.wew.cli.print("DUP ${it.groupInfo!!.groupId.toString()} ${aMap[it.groupInfo!!.groupId]}")
            aMap[it.groupInfo!!.groupId] = it
        }
        false
    }
}

fun CommonWallet.syncNfts(nftDir: File? = null)
{
    val outDir = nftDir ?: File(NFT_DIR)
    if (!outDir.exists()) outDir.mkdirs()
    val assets = mutableListOf<Spendable>()
    forEachAsset { sp -> assets.add(sp); false }
    for (sp in assets)
    {
        val gi = sp.groupInfo()
        if (gi != null)
        {
            val gid = gi.groupId
            if (!outDir.resolve(gid.toStringNoPrefix() + ".zip").exists())
            {
                var td:TokenDesc = getTokenDesc(blockchain, gid, { this.blockchain.net.getElectrum() })
                val n = getNftFile(td, gid)
                if (n!=null)
                {
                    val (url, f) = n
                    println("Getting: $url")
                    val file = outDir.resolve(gid.toStringNoPrefix() + ".zip")
                    file.sink().buffer().use { it.writeAll(f.openAt(0))}
                }
            }
            else
            {
                println("Exists: $gid")
            }
        }
    }
}

class Nft(var groupId: GroupId,var nftDir: File? = null)
{
    var ef: EfficientFile? = null
    var data: NexaNFTv2? = null

    init {
        if (nftDir == null) nftDir = File(NFT_DIR)
        val e = getNft(groupId, nftDir)
        ef = e
        data = nftData(e)
    }

    val front:ImageFace
        get()
    {
        return ImageFace(ByteArrayPainter(nftCardFront(ef!!).second!!))
    }
    val back:ImageFace
        get()
    {
        return ImageFace(ByteArrayPainter(nftCardBack(ef!!).second!!))
    }
    val public:ImageFace
        get()
    {
        return ImageFace(ByteArrayPainter(nftPublicMedia(ef!!).second!!))
    }
    val owner:ImageFace
        get()
    {
        return ImageFace(ByteArrayPainter(nftOwnerMedia(ef!!).second!!))
    }

    constructor(sp: Spendable, nftDir: File? = null): this(sp.groupInfo?.groupId ?: throw NftException("Not a NFT"),nftDir)
    constructor(s: String,nftDir: File? = null):this(GroupId(s),nftDir)
    //constructor(f:File,nftDir: File? = null):this(stringToGroupId(s),nftDir)
}

fun getNft(gid: GroupId, nftDir: File? = null, blockchain:Blockchain? = null):EfficientFile
{
    val outDir = nftDir ?: File(NFT_DIR)
    val destFile = outDir.resolve(gid.toStringNoPrefix() + ".zip")
    if (!destFile.exists())
    {
        val bc = blockchain ?: blockchains[gid.blockchain]
        if (bc == null) throw NetException("Not connected to ${gid.blockchain}")
        var td:TokenDesc = getTokenDesc(bc, gid, { bc.net.getElectrum() })
        val n = getNftFile(td, gid)
        if (n!=null)
        {
            val (url, f) = n
            //println("Getting: $url")
            destFile.sink().buffer().use { it.writeAll(f.openAt(0))}
            return(f)
        }
        throw NetException("Cannot load")
    }
    else
    {
        return EfficientFile(destFile.source().buffer())
    }
}

fun image(nftFile: Pair<String?, ByteArray?>):ImageFace?
{
    nftFile.second?.let { return ImageFace(it) }
    return null
}

fun showNft(f: File):EfficientFile
{
    val src = f.source().buffer()
    val ef = EfficientFile(src)
    showNft(ef)
    return ef
}

fun showNft(ef: EfficientFile):EfficientFile
{
    val data = nftData(ef)
    if (data != null)
    {
        println("Title: ${data.title}  Author: ${data.author}  Collection: ${data.series}  Category: ${data.category}  Keywords: ${data.keywords}")
        val cf = image(nftCardFront(ef))
        val pub = image(nftPublicMedia(ef))
        val priv = image(nftOwnerMedia(ef))
        val back = image(nftCardBack(ef))
        println(cf, " ", pub, " ", priv, " ", back)
    }
    else println("no information found")
    return ef
}

fun showNft(gid: GroupId, nftDir: File? = null):EfficientFile
{
    val d = nftDir ?: File(NFT_DIR)
    val nftFile = d.resolve(gid.toStringNoPrefix() + ".zip")
    if (nftFile.exists())
    {
        return showNft(nftFile)
    }
    else
    {
        return showNft(getNft(gid, nftDir))
    }
}

fun showNft(s: String, nftDir: File? = null):EfficientFile
{
    val f = File(s)
    if (f.exists()) return showNft(f)
    try
    {
        val gid = GroupId(s)  // also tries hex
        return showNft(gid)
    }
    catch(_:Exception)
    {

    }
    try
    {
        val ba = s.fromHex()
        for (b in blockchains)
        {
            val gid = GroupId(b.value.chainSelector, ba)
            try
            {
                val nft = getNft(gid, nftDir)
                return showNft(nft)
            }
            catch (_:Exception)
            {}
        }
    }
    catch(_:Exception)
    {}
    throw NftException("$s is not understood as an NFT")
}

/** Checks all NFTs in a particular directory to see if their title matches.
 * Places all NFTs with no match in the provided directory */
fun crossCheckNfts(dir: File, soloDir: File)
{
    soloDir.mkdirs()
    val titleMap = mutableMapOf<String, MutableSet<File>>()
    for (f in dir.listFiles()!!)
    {
        if (f.isFile)
        {
            println("analyzing ${f.name}")
            //val src = f.source().buffer()
            //val data = nftData(EfficientFile(src))
            val ba = f.readBytes()
            println("  size: ${ba.size}")
            val data = nftData(EfficientFile(ba))
            if (data != null)
            {
                titleMap.getOrPut(data.title, { mutableSetOf<File>() }).add(f)
            }
            //src.close()
        }
    }
    for ((k,files) in titleMap)
    {
        if (files.size == 1)
        {
            println("$k:  One NFT: ${files.first()}")
            val f = files.first()
            Files.move(f.toPath(), soloDir.resolve(f.name).toPath(), StandardCopyOption.ATOMIC_MOVE, StandardCopyOption.REPLACE_EXISTING)
        }
        else
        {
            val s = files.map { it.name }.fastJoinToString(", ")
            println("$k:  DUPS: ${s}")
        }
    }
}



/** Sends all NFTs located in a directory @dir and in the @from wallet to the destination wallet */
fun sendNftsInDir(dir: File, from: CommonWallet, to: CommonWallet, chunks:Long = 20)
{
    val toDest = to.getCurrentDestination()
    val outputs = mutableListOf<iTxOutput>()
    for (f in dir.listFiles()!!)
    {
        if (f.isFile)
        {
            println("sending ${f.name}")
            val gid = GroupId("nexa:" + f.nameWithoutExtension)
            if (from.ownsSome(gid))
            {
                val out = 1.ofToken(gid).payTo(toDest)
                outputs.add(out)
                if (outputs.size >= chunks)
                {
                    val tx = from.send(*outputs.toTypedArray(), sync = true)
                    println(tx)
                    outputs.clear()
                }
            }
        }
    }
    if (outputs.size > 0)
    {
        val tx = from.send(*outputs.toTypedArray(), sync = true)
        println(tx)
    }
}