package org.wallywallet.wew.niftyWew  // The name of this package MUST be org.wallywallet.wew.<name of file before -version>
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow
import niftyWew.makeNifties
import org.wallywallet.composing.*
import org.wallywallet.wew.*

val makeNiftiesUI: ComposingOutput = ComposingOutput()
var xp:CCExpander? = null

fun nftForgeButton()
{
    if (xp == null)
    {
        val tmp = getThreadOutput()
        setThreadOutput(makeNiftiesUI)
        try
        {
            makeNifties()
        }
        finally
        {
            setThreadOutput(tmp)
        }
        xp = expander(CCText("Make Nifties", headingTextStyle(2)), indent(30.dp, makeNiftiesUI), true)

        org.wallywallet.wew.cli.print(xp)
    }
    else
    {
        xp?.expanded?.value = !(xp?.expanded?.value ?: false)
    }
}

class Initializer
{
    fun CliInstaller(wew: WallyEnterpriseWallet, output:Output):Unit
    {
        println("Installing NFT tools...")
        wew.eval("import niftyWew.*", output)
        wew.eval("""toolbar.add(button("NFT forge", { org.wallywallet.wew.niftyWew.nftForgeButton() }))""", output)
        println("NFT tools installed")
    }
}