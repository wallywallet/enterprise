pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        //maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
        maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpThreads
        maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") }  // Nexa RPC into nexad
        maven { url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven") }  // Nexa Script Machine
        maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
        maven { url = uri("https://jitpack.io") }
        mavenLocal()
    }
}

rootProject.name = "wew"
include("app")
