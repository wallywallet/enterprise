import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

// for reading the local properties file
import java.util.Properties
import java.io.File
import java.io.FileInputStream

// see: https://youtrack.jetbrains.com/issue/KT-63158/Kotlin-version-1.9.20-breaks-Kotlin-Interactive-Shell before updating
val kotlinVersion: String by extra("2.1.20-Beta2")

plugins {
    kotlin("jvm") version "2.1.20-Beta2"
    id("org.jetbrains.kotlin.plugin.compose") version("2.1.10")  // https://github.com/JetBrains/kotlin/releases/tag/v2.1.10
    // id("org.jetbrains.compose")  // https://github.com/JetBrains/compose-multiplatform/releases
    kotlin("plugin.serialization") version "2.0.0"
    // id("io.ktor.plugin").version("2.3.12").apply(false)
    application
    distribution
}

dependencies {
    implementation(project(":app"))
}

application {
   mainClass.set("org.wallywallet.wew.WallyEnterpriseWallet")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
     compilerOptions {
        jvmTarget.set(JvmTarget.JVM_17)
    }
}
java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    withSourcesJar()
}

val BASEDIR = rootProject.name
val BINDIR = BASEDIR + "/bin"
val JARDIR = BASEDIR + "/lib"

tasks.getByName<Zip>("distZip") {
    with(copySpec {
        from("./app/build/plugins/niftyWew-1.0.jar").into(BINDIR)
    })
    dependsOn("app:niftyWew")
}

/*  This needs to be defined in the sub-project build.gradle.kts (for example in app/app)
tasks.withType<Test> {
    println("there is a test task called $this")
    maxHeapSize = "4096m"
    minHeapSize = "2048m"
    //jvmArgs = listOf("-Xmx4096m")
    jvmArgs = listOf("-XX:MaxMetaspaceSize=16g")
    println("Max heap size: ${this.maxHeapSize}")
    doFirst {
        println("JVM Arguments: ${javaLauncher.toString()} ${javaLauncher.orNull?.executablePath}")
        //println("JVM Arguments: ${Jvm.current().toString()}")
    }
}
*/