# Wally Enterprise Wallet
*Scriptable Desktop Nexa and Bitcoin Cash Wallet*


## Use the WEW library

Look here https://gitlab.com/nexa/nexascriptmachinekotlin/-/packages for the latest package version.  Let's say its "9.9.9".
In the application level build.gradle.kts include the repository:
```
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/15615113/packages/maven") }
}
```

Then include the library as a dependency:

```
dependencies {
implementation("WallyWallet","wew","9.9.9")
}
```

or if you are using this library in your test code:

```
testImplementation("WallyWallet","wew","9.9.9")
```


## Setup

* Copy libnexa.so, from the Nexa full node, into this project's base directory

* By default in this prereleased state, this project uses a local version of libbitcoincashkotlin so:
  * Copy libbitcoincashkotlinjvm.jar into this project's "jars" directory
  

## Build

Load the project within IDEA (https://www.jetbrains.com/idea/) and build.

### Build for distribution

```
./gradlew distZip
```

Distribution is located at build/distributions/wew.zip

### build Plugins

```
./gradlew examplePlugin
./gradlew niftyWew
```

Plugins are located at app/build/plugins

### Build Library

#### Setup

* In build.gradle.kts

* Get a project deploy token from gitlab web -> settings -> Repository -> Deploy tokens.

Name it "Deploy-Token".  Select write_registry in your scope.  **DO NOT CREATE AN ACCESS TOKEN (Settings->Access Tokens).  IT WONT WORK.**

* Place it in your local.properties file
> DeployTokenValue=xxxxxxxx


* Bump the version of the project in build.gradle.kts 

> version = "1.0.0"

* From the terminal run

> ./gradlew publish


* Verify by browsing to https://gitlab.com/api/v4/projects/15615113/packages

#### Setup for local JAR file creation

* File->Project Structure->Artifacts->+->JAR->From modules with dependencies
  (Create a manifest file: I'm unsure what to do here)
* Build->Build Artifacts


unzip .../build/distributions/enterprise.zip


## Run

Within IDEA, you should see a "Main" target.  This will run the GUI.


## Test

Functional tests exist in Test.kt.  These are junit tests so execute the "Test" class to run all tests.

The tests presume that a full node is running the Nexa Regtest blockchain on "localhost" using
**nonstandard** ports 7327/7328, with RPC user "regtest" and password "regtest".  

You may change these defaults at the top of the Test.kt file.

## Examples

### Function declarations & long scripts with dependencies:

Best practice is to create a separate .kt file in a local home directory. Say you enter a function
directly into the CLI. Now you want to change one line of that function; the entire function now needs
to be reentered into the CLI and any dependent code which follows the function declaration must be
reentered, as the trailing code has already been compiled based on the code preceding it. Each command
is executed according to the code's sequence of entry, with no refactoring.

Once you have your script, run the command ```!x <filename>``` from the CLI. Any var or val from your
local file can now be accessed directly from the command line!

### Setting up a web client, requesting oracle data:

Utilize standard Kotlin or Java imports to implement your client in Kotlin code:

```
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlinx.coroutines.runBlocking



suspend fun getOracleData(urlString: String): String
{
    val client = HttpClient(CIO)
    return try {
        val response: HttpResponse = client.get(urlString)
        response.bodyAsText()
    } 
    catch (e: Exception) 
    {
        println("Error: ${e.message}")
        ""
    }
    finally
    {
        client.close()
    }
}

var result = ""
corun{result = getOracleData("http://wallywallet.org/_api/v0/now/hourlyavg/usdt/nexa")}
```

```result``` as well as ```getOracleData()``` can now be accessed directly from the command line! Corun is a function
built in to the CLI. Since its return type is Unit, if your suspend function called using ```corun()``` returns
something, declare a var prior to the call, and your suspend function can write to that var inside ```corun()```, as
seen above.

### Parsing oracle prices from wallywallet.org:

** Refer to OracleTest class in Test.kt **

The following called classes and functions are built in to the CLI. These lines can be entered as individual commands
to the command line, or can be added to the local script file for execution with ```!x```. This
code will parse and display all the data which is being signed by wallywallet.org. The data is then converted to
byteArray and hashed, into the message which will be provided to verifySigForHash for Schnorr signature verification.

[//]: # (NOTE: NEED TO REVIEW RUN/CORUN FUNCTION FOR POTENTIAL USE IN THIS CASE)

```

val oracleObject = parseOracleObject(result)

// String.fromHex() from libexakotlin.libnexa
val byteArray = oracleObject.msg.data.fromHex()
val parsedData = parseOracleData(byteArray)

println(parsedData.tickerA)
println(parsedData.tickerB)
println(parsedData.epochSeconds.toString())
println((parsedData.price / 1e16).toString())       // corresponding price from DB * 1e16
```
### Setting up signature verification:

The following places the data into the same class as is used by the oracle. After converting to a byteArray, the data is
hashed. The hashed data, the oracle's pubkey, and the hex signature converted to byteArray are given as args to
```verifySignedDataSchnorr()```.

```
// store in data type used by wallywallet.org DB
val priceDataPoint = PriceDataPoint()
priceDataPoint.setVals(parsedData.tickerA, parsedData.tickerB, parsedData.epochSeconds, parsedData.price)

val checkData = priceDataPoint.toByteArray()

// this is the message arg for signHashSchnorr()
val hashedData = libnexa.sha256(checkData)
val pubkey = 
val sig = oracleObject.msg.signature.fromHex()

val sigValid = libnexa.verifySignedDataSchnorr(hashedData, pubkey, sig)
```